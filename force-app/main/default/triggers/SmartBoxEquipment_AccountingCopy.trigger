trigger SmartBoxEquipment_AccountingCopy on Smartbox__c (After Insert, After Update) {
    for (Smartbox__c SB: Trigger.New)    {
        if((SB.Smartbox_Leased__c == true && SB.Opportunity__c != '0066000000PRyC1') || (SB.Smartbox_Leased__c == false && SB.Opportunity__c != '0066000000PRyC1'))
            {Smartbox_Accounting__c SBA = new Smartbox_Accounting__c(
                Chassis_Serial__c = SB.Chassis_Serial__c,  
                CAID__c = SB.CAID__c,
                CSG_Sub_Account__c = SB.CSG_Sub_Account__c,
                MEID__c = SB.MEID__c,
                Opportunity__c = SB.Opportunity__c,
                CSG_Master__c = SB.CSG_Account_Number__c,
                Opp_Launch_Date__c = SB.Opp_Launch_Date__c,
                Part_Number__c = SB.Part_Number__c,    
                Serial_Number__c = SB.Serial_Number__c,
                SmartCard__c = SB.SmartCard__c,
                Opportunity_Status__c = SB.SF_Status__c,
                SF_Status__c = SB.Status__c,
                CSG_Status__c = SB.CSG_Status__c,
                Smartbox_Leased__c=SB.Smartbox_Leased__c,
                Equipment_Added_to_SF_Date__c = SB.CreatedDate,
                Equipment_Last_Modified_Date__c = SB.LastModifiedDate,
                CaseRequestedCompletionDate__c = SB.CaseRequestedCompletionDate__c,
                CaseRequestType__c = SB.CaseRequestType__c,
                Case_Number__c = SB.Case_Number__c,
                Most_Recent_Case_Create_Date__c = SB.Most_Recent_Case_Create_Date__c,
                Most_Recent_Case_ID__c = SB.Most_Recent_Case_ID__c,
                Most_Recent_Case_Number__c = SB.Most_Recent_Case_Number__c,
                Most_Recent_Case_Pre_Activation_Date__c = SB.Most_Recent_Case_Pre_Activation_Date__c,
                Most_Recent_Case_RA__c = SB.Most_Recent_Case_RA__c,
                Most_Recent_Case_Record_Type__c = SB.Most_Recent_Case_Record_Type__c,
                Most_Recent_Case_Request_Completion_Date__c = SB.Most_Recent_Case_Request_Completion_Date__c,
                Most_Recent_Case_Type__c = SB.Most_Recent_Case_Type__c,
                Most_Recent_Property_Bulk_Launch_Date__c = SB.Most_Recent_Property_Bulk_Launch_Date__c,
                Most_Recent_Property_Status__c = SB.Most_Recent_Property_Status__c,
                Type_of_Equipment__c = SB.Type_of_Equipment__c);
 
                insert SBA; 
            }
        }
}