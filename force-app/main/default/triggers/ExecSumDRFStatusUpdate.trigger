Trigger ExecSumDRFStatusUpdate on Executive_Summary__c (After Insert, After Update) {
Set<Id> FORIds = new Set<Id>();


executive_summary__c exs= trigger.new[0];
   if (exs.Status__c == 'In Review (OPS Director)')       {       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'In Review (OPS Director)' )         FORIds.add(c.Front_Office_Request__c);                        }         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from          Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'In Review (OPS Director)'){         fo.Front_Office_Decision__c = 'In Review (OPS Director)';        }         Update fo;
}
}else   if (exs.Status__c == 'In Review (VP Sales)')       {
       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'In Review (VP Sales)' )         FORIds.add(c.Front_Office_Request__c);            }

         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from          Front_Office_Request__c where Id in :FORIds]) {
         if(fo.Front_Office_Decision__c != 'In Review (VP Sales)'){         fo.Front_Office_Decision__c = 'In Review (VP Sales)';         }         Update fo;}}
else
   if (exs.Status__c == 'In Review (F.O. Manager)')       {       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'In Review (F.O. Manager)' )         FORIds.add(c.Front_Office_Request__c);              }
         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from          Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'In Review (F.O. Manager)'){         fo.Front_Office_Decision__c = 'In Review (F.O. Manager)';         }         Update fo;
}}
else
   if (exs.Status__c == 'Rejected (OPS Director)')       {       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'Rejected (OPS Director)' )         FORIds.add(c.Front_Office_Request__c);                }
         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from          Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'Rejected (OPS Director)'){         fo.Front_Office_Decision__c = 'Rejected (OPS Director)';         } Update fo;
}
}
else   if (exs.Status__c == 'Rejected (VP Sales)')       {       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'Rejected (VP Sales)' )         FORIds.add(c.Front_Office_Request__c);                }   for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from          Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'Rejected (VP Sales)'){         fo.Front_Office_Decision__c = 'Rejected (VP Sales)';         }         Update fo;}
}
else
   if (exs.Status__c == 'Escalated by Sales Manager')       {       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'Escalated by Sales Manager' )         FORIds.add(c.Front_Office_Request__c);                }         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from          Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'Escalated by Sales Manager'){         fo.Front_Office_Decision__c = 'Escalated by Sales Manager';         }         Update fo;}
}
else   if (exs.Status__c == 'In Review (SVP/EVP)')       {       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'In Review (SVP/EVP)' )         FORIds.add(c.Front_Office_Request__c);                }

         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from 
         Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'In Review (SVP/EVP)'){         fo.Front_Office_Decision__c = 'In Review (SVP/EVP)';         }         Update fo;}
}
else
   if (exs.Status__c == 'Rejected (SVP/EVP)')       {       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'Rejected (SVP/EVP)' )         FORIds.add(c.Front_Office_Request__c);
    
         }         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from 
         Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'Rejected (SVP/EVP)'){         fo.Front_Office_Decision__c = 'Rejected (SVP/EVP)';         }         Update fo;}}
else   if (exs.Status__c == 'Recommendations Pending - FO')
       {

       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'Recommendations Pending - FO' )         FORIds.add(c.Front_Office_Request__c);
       
         }

         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from          Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'Recommendations Pending - FO'){         fo.Front_Office_Decision__c = 'Recommendations Pending - FO';         }         Update fo;}}
else
   if (exs.Status__c == 'Recalled')       {       for (Executive_Summary__c c: Trigger.New){         if (c.Status__c == 'Recalled' )         FORIds.add(c.Front_Office_Request__c);
                }         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from          Front_Office_Request__c where Id in :FORIds]) {         if(fo.Front_Office_Decision__c != 'Recommendations Pending - FO'){         fo.Front_Office_Decision__c = 'Recommendations Pending - FO';         }         Update fo;}
}else
   if (exs.Status__c == 'Approved')
       {

       for (Executive_Summary__c c: Trigger.New){
         if (c.Status__c == 'Approved' )
         FORIds.add(c.Front_Office_Request__c);            }
         for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from 
         Front_Office_Request__c where Id in :FORIds]) {
         if(fo.Front_Office_Decision__c != 'Approved'){         fo.Front_Office_Decision__c = 'Approved';         }         Update fo;}}}