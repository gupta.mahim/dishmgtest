Trigger SmartBoxDeleteDupes on Smartbox__c (AFTER INSERT, AFTER UPDATE) {
   
LIST<Smartbox__c> deleterows  = new  LIST<Smartbox__c>();
FOR(Smartbox__c sbd :[SELECT Id, CAID__c, Type_of_Equipment__c FROM Smartbox__c WHERE Id IN : Trigger.NEW AND (CAID__C = 'DUPE' OR CSG_Status__c = 'Drop Completed') AND (Type_of_Equipment__c != 'Cell Modem (Fusion Wireless)') ]) {     deleterows.add(sbd);
}
IF(deleterows.SIZE() != 0){ {     Database.DeleteResult[] deleterows_del = Database.DELETE(deleterows);     FOR(Database.DeleteResult del: deleterows_del)        {             IF(del.isSuccess())
                {
                        SYSTEM.DEBUG(' Deleted record(Smartbox__c)Id  :'+del.getId());
                }
                ELSE
                {
                    SYSTEM.DEBUG('ERROR : Error occurs unable to delete (Smartbox__c)record :'+del.getErrors());
                }
        }
                            

}}}