trigger LeadDataUpdate on Data__c (before insert) {
 for(Data__c data:Trigger.new){
if (data.RecordtypeId=='0126000000017cm'){


date rd = data.Reporting_Date__c; 
date rd2 = data.Reporting_Date__c+1; 
date d = system.today().addDays(-180);

 Date dToday = data.Reporting_Date__c;
 Datetime dt = datetime.newInstanceGMT(dToday.year(), dToday.month(),dToday.day(),7,0,0);
 Datetime dt2 = datetime.newInstanceGMT(rd2.year(), rd2.month(),rd2.day(),7,0,0);

        
        Integer i1 = [select count() from Lead where createdDate > :dt AND createdDate < :dt2 AND Category__c = 'Public' AND LeadSource = 'Direct Call']; 
        Integer i2 = [select count() from Lead where createdDate > :dt AND createdDate < :dt2 AND Category__c = 'Private' AND LeadSource = 'Direct Call']; 
        Integer i3 = [select count() from Lead where createdDate > :dt AND createdDate < :dt2 AND Category__c = 'I/R' AND LeadSource = 'Direct Call'];  
        Integer i4 = [select count() from Lead where createdDate > :dt AND createdDate < :dt2 AND Category__c = 'MDU' AND LeadSource = 'Direct Call'];  
        Integer i5 = [select count() from Lead where createdDate > :dt AND createdDate < :dt2 AND Category__c = 'MDU Residential' AND LeadSource = 'Direct Call'];  
        Integer i6 = [select count() from Lead where createdDate > :dt AND createdDate < :dt2 AND Category__c = 'FTG' AND LeadSource = 'Direct Call'];  
        Integer i7 = [select count() from Lead where createdDate > :d AND Lead_Inbox_Available_Time__c > :rd AND Lead_Inbox_Available_Time__c < :rd2 AND (Category__c = 'Public' OR Category__c = 'Private' OR Category__c = 'I/R' ) ];
        Integer i8 = [select count() from Lead where createdDate > :d AND Lead_Inbox_Available_Time__c > :rd AND Lead_Inbox_Available_Time__c < :rd2 AND (Category__c = 'MDU' OR Category__c = 'FTG') ];
                 
data.Public_Leads_Created__c = i1;
data.Private_Leads_Created__c = i2;
data.I_R_Leads_Created__c = i3;
data.MDU_Leads__c = i4;
data.MDU_Residential_Leads_Created__c = i5;
data.FTG_Leads_Created__c = i6;
data.Business_Solutions_Leads_Distributed__c = i7;
data.Property_Solutions_Distributed__c = i8;

}
}


}