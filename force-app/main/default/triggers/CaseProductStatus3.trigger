trigger CaseProductStatus3 on Case (After Update) {

Case ca = trigger.new[0];
    if (ca.Opportunity__c != null && (ca.Status == 'Request Completed' || ca.Status == 'Request Recalled' || ca.Status == 'Rejected') && (ca.RequestedAction__c == 'Pre-Activation + Activation' || ca.RequestedAction__c == 'Activation' || ca.RequestedAction__c == 'Change' ||  ca.RequestedAction__c == 'Installation/Activation' ||  ca.RequestedAction__c == 'Pre-Activation' ||   ca.RequestedAction__c == 'Restart' ||   ca.RequestedAction__c == 'Two (2) Hour Activation' ||   ca.RequestedAction__c == 'Pre-Build' || ca.RequestedAction__c == 'Disconnect' || ca.RequestedAction__c == 'Disconnection'))
        {
            Set<Id> caseIds = new Set<Id>();
            Set<Id> opIds = new Set<Id>();
                for (Case c: Trigger.New){
                    opIds.add(c.Opportunity__c);
                    caseIds.add(c.Id);
         }
            list<opportunity> olic2 = [select id, Hidden_Products__c from opportunity where Id in :opIds ];
            list<opportunityLineItem> oli = [select id, OpportunityId, Status__c, CaseID__c, Submitted__c from opportunityLineItem where OpportunityId in :opIds AND CaseID__c in :caseIDs AND Submitted__c = True ];
            list<case> c2 = [select id, Status, CaseNumber, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Status = 'Request Completed' AND (RequestedAction__c = 'Pre-Activation + Activation' OR RequestedAction__c = 'Activation' OR RequestedAction__c = 'Change' OR  RequestedAction__c = 'Installation/Activation' OR  RequestedAction__c = 'Pre-Activation' OR  RequestedAction__c = 'Restart' OR RequestedAction__c = 'Two (2) Hour Activation' OR RequestedAction__c = 'Pre-Build' OR RequestedAction__c = 'Disconnect' OR RequestedAction__c = 'Disconnection')];
            list<case> c22 = [select id, Status, CaseNumber, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND (Status = 'Rejected' or Status = 'Request Recalled') AND (RequestedAction__c = 'Pre-Activation + Activation' OR RequestedAction__c = 'Activation' OR RequestedAction__c = 'Change' OR  RequestedAction__c = 'Installation/Activation' OR  RequestedAction__c = 'Pre-Activation' OR  RequestedAction__c = 'Restart' OR RequestedAction__c = 'Two (2) Hour Activation' OR RequestedAction__c = 'Pre-Build' OR RequestedAction__c = 'Disconnect' OR RequestedAction__c = 'Disconnection')];            


                for (opportunityLineItem oli2: oli) {
                    if(oli.size() > 0 && c2.size() > 0 && c22.size() < 1 ){
                    
                        if(oli2.Status__c == 'Add Requested' && oli2.Submitted__c == True )
                            {
                                oli2.Status__c = 'Add Completed';
                                oli2.CaseID__c = NULL;
                                oli2.Change_Schedule__c = NULL;
                                oli2.Submitted__c = False;
                                for (Opportunity olic322: olic2) 
                                    {
                                        olic322.Hidden_Products__c = null;
                                     }
                             }
                       if(oli2.Status__c == 'Remove Requested' && oli2.Submitted__c == True )                             {                               oli2.Status__c = 'Remove Completed';                               oli2.CaseID__c = NULL;                               oli2.Change_Schedule__c = NULL;                               oli2.Submitted__c = False;                               for (Opportunity olic322: olic2)                                    {                                        olic322.Hidden_Products__c = null;
                                     }
                              }}
                      if(oli.size() > 0 && c22.size() > 0 ){                              if(oli2.Status__c == 'Add Requested' && oli2.Submitted__c == True )                             {                               oli2.CaseID__c = NULL;                               oli2.Change_Schedule__c = NULL;                               oli2.Submitted__c = False;                               oli2.Flags__c = 'Delete';                               for (Opportunity olic322: olic2)                                     {                                        olic322.Hidden_Products__c = null;
                                     }
                              }}
                        if(oli2.Status__c == 'Remove Requested' && oli2.Submitted__c == True ){                             {                               oli2.Status__c = 'Active';                               oli2.CaseID__c = NULL;                               oli2.Change_Schedule__c = NULL;                                oli2.Submitted__c = False;                               for (Opportunity olic322: olic2)                                     {                                        olic322.Hidden_Products__c = null;
                                     }
                              }}
                    }
        Database.update(oli);
        Database.update(olic2);
    }
}