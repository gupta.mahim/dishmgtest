trigger OppFrontOffice on Opportunity (After Update) { 
List<Front_Office_Request__c> sr = new List<Front_Office_Request__c>();


       for (Opportunity o: Trigger.New){
  
      
       Front_Office_Request__c[] fo = [SELECT Id, Front_Office_Decision__c  FROM Front_Office_Request__c WHERE Opportunity__c = :o.Id]; 

               
         if (fo.size() == 0 && o.Send_to_FO_for_Verification__c==true){
                          
            sr.add (new Front_Office_Request__c(
                     RecordTypeId = '0126000000017k7',   
                     OwnerId = '00G60000001Evwv',
                     Account__c = o.AccountId,
                     Opportunity__c = o.Id,
                     Reported_Drop_Count__c = o.Number_of_Units__c,
                     Property_Address__c = o.Address__c,
                     Property_City__c = o.City__c,
                     Property_State__c = o.State__c,
                     Property_Zip_Code__c = o.Zip__c,
                     Front_Office_Decision__c = 'In Queue',
                     Request_Type__c = 'Amenity Incentive Verification'));   

   
 //  Messaging.reserveSingleEmailCapacity(2);         
// Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
// String[] toAddresses = new String[] {'rick.hodgskiss@dish.com'};
// mail.setToAddresses(toAddresses);
// mail.setReplyTo('salesforceadmin@dish.com');
// mail.setSenderDisplayName('Salesforce Admin');
// mail.setSubject('A new Drop Count Verification Request has been created');
// mail.setBccSender(false);
// mail.setUseSignature(false);
// mail.setPlainTextBody('A new Amenity Incentive Verification Request has been created and assigned to the Front Office Queue.');
// Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

         }
   insert sr;
 }}