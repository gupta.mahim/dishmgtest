trigger Lead_Type_FTG on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c > 49)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Prisons' && L.Location_of_Service__c == 'Cells' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c > 49)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
// For Missing info due to Kieth's web form update
if(L.Type_of_Business__c == 'Prisons' && L.Location_of_Service__c == NULL && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c == NULL )
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary'; L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Casinos' && L.Location_of_Service__c == 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c > 49)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
// For World Cinema
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c > 49 && L.State != 'ME' && L.State != 'NH' && L.State != 'NY' && L.State != 'MA' && L.State != 'RI' && L.State != 'CT' && L.State != 'PA' && L.State != 'NJ' && L.State != 'DE' && L.State != 'VT' )
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
// For Warehouse 5
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.Number_of_Units__c > 99 && L.State == 'CA' && L.State != 'NV' )
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
// For Warehouse 5
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.Number_of_Units__c < 100 && L.State == 'CA' && L.State != 'NV' )
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondry';}
else
// For Pure HD
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c > 149 && (L.State == 'ME' && L.State == 'NH' || L.State == 'NY' || L.State == 'MA' || L.State == 'RI' && L.State == 'CT' || L.State == 'PA' || L.State == 'NJ' || L.State == 'DE' || L.State == 'VT') )
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c < 149 && (L.State == 'ME' && L.State == 'NH' || L.State == 'NY' || L.State == 'MA' || L.State == 'RI' && L.State == 'CT' || L.State == 'PA' || L.State == 'NJ' || L.State == 'DE' || L.State == 'VT') )
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
// For Missing info due to Kieth's web form update
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == NULL && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c == NULL  )
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary'; L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == NULL && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c > 49)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c != 'Common Area'  && L.Location_of_Service__c != 'Food Service' && L.Location_of_Service__c != 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c > 49)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c < 50 && L.Number_of_Units__c > 0)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Prisons' && L.Location_of_Service__c == 'Cells' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c < 50 && L.Number_of_Units__c > 0)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Casinos' && L.Location_of_Service__c == 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c < 50 && L.Number_of_Units__c > 0)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c < 50 && L.Number_of_Units__c > 0)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == NULL && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c < 50 && L.Number_of_Units__c > 0)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c != 'Common Area'  && L.Location_of_Service__c != 'Food Service' && L.Location_of_Service__c != 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'FTG' && L.Number_of_Units__c < 50 && L.Number_of_Units__c > 0)
{ L.sundog_deprm2__Lead_Type__c = 'FTG'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
}