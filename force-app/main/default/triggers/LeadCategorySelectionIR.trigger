trigger LeadCategorySelectionIR on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'Firehouse/Oil Rig/EMT' && L.Category__c != 'I/R')
{L.Category__c = 'I/R';} // else nothing
}