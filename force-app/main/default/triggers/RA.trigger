trigger RA on Smartbox__c (After Update) {

    for( Smartbox__c SB : Trigger.new) {

          if(SB.Smartbox_Leased__c == true 
          && SB.CaseID__c != null 
          && SB.RA_Number__c != null ) {
              RA__c SBA = new RA__c(
                  cid__c = SB.CaseID__c,
                  RANumber__c =  SB.RA_Number__c,
                  Shipping_Tracking__c = SB.Shipping_Tracking__c,
                  Serial_Number__c = SB.Serial_Number__c,
                  SmartCard__c = SB.SmartCard__c,
                  CAID__c = SB.CAID__c,
                  Opportunity__c = SB.Opportunity__c
                  );

                  insert SBA;
        }
    }
}