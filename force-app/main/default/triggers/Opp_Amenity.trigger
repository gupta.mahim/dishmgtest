trigger Opp_Amenity on Opportunity (Before Update, Before Insert) { 
Opportunity O = trigger.new[0];
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '30-49 (PCO 2013 - AI 36)' )
{O.Amenity__c = '24'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 4.25; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '50-99 (PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 4.25; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '100-149 (PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 1000.00; O.Amenity_Minimum_Programming__c = 4.25; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '150-199 (PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 2000.00; O.Amenity_Minimum_Programming__c = 4.25; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '200-399 (PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 3000.00; O.Amenity_Minimum_Programming__c = 4.25; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '400+ (PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 7000.00; O.Amenity_Minimum_Programming__c = 4.25; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '50-99 (PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 650.00; O.Amenity_Minimum_Programming__c = 7.65; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '100-149 (PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c =2800.00; O.Amenity_Minimum_Programming__c = 7.65; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '150-199 (PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 5000.00; O.Amenity_Minimum_Programming__c = 7.65; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '200-399 (PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 7.65; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '400+ (PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 15000.00; O.Amenity_Minimum_Programming__c = 7.65; }
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '50-99 (PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 650.00; O.Amenity_Minimum_Programming__c = 7.65; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '100-149 (PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 2800.00; O.Amenity_Minimum_Programming__c = 7.65; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '150-199 (PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 5000.00; O.Amenity_Minimum_Programming__c = 7.65; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '200-399 (PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 7.65;  } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '400+ (PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 15000.00; O.Amenity_Minimum_Programming__c = 7.65;  }
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '30-49 (RTL 2013 - AI 36)')
{O.Amenity__c = '24'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '50-99 (RTL 2013 - AI 36)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '100-149 (RTL 2013 - AI 36)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 1000.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '150-199 (RTL 2013 - AI 36)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 2000.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '200-399 (RTL 2013 - AI 36)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 3000.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '400+ (RTL 2013 - AI 36)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 7000.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '50-99 (RTL 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 650.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '100-149 (RTL 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 2800.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '150-199 (RTL 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 5000.00;  O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '200-399 (RTL 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '400+ (RTL 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 15000.00; O.Amenity_Minimum_Programming__c = 9.00; }
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '50-99 (RTL 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 650.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '100-149 (RTL 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 2800.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '150-199 (RTL 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 5000.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '200-399 (RTL 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '400+ (RTL 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 15000.00; O.Amenity_Minimum_Programming__c = 9.00; }
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '30-49 (RTL/PCO 2013 - AI 36)' )
{O.Amenity__c = '24'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '50-99 (RTL/PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 5.000; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '100-149 (RTL/PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 1000.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '150-199 (RTL/PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 2000.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '200-399 (RTL/PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 3000.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if(O.Equipment_Model__c == '311' && O.Amenity_tier__c == '400+ (RTL/PCO 2013 - AI 36)' )
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 7000.00; O.Amenity_Minimum_Programming__c = 5.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '50-99 (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 650.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '100-149 (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 2800.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '150-199 (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 5000.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '200-399 (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '211' || O.Equipment_Model__c == '211(k)') && O.Amenity_tier__c == '400+ (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '18'; O.Approved_Incentive_Amount__c = 15000.00; O.Amenity_Minimum_Programming__c = 9.00; }
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '50-99 (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 650.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '100-149 (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 2800.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '150-199 (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 5000.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '200-399 (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 9.00; } 
else
if((O.Equipment_Model__c == '222' || O.Equipment_Model__c == '222(k)') && O.Amenity_tier__c == '400+ (RTL/PCO 2013 - AI 60)' )
{O.Amenity__c = '12'; O.Approved_Incentive_Amount__c = 15000.00; O.Amenity_Minimum_Programming__c = 9.00; }
else
if(O.Amenity_Program_Type__c == 'PCO-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '70-99 (PCO-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '100-149 (PCO-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 2250.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '150-199 (PCO-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 6500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '200-299 (PCO-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 8500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '300-399 (PCO-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '400+ (PCO-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 13750.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '70-99 (PCO-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 1250.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '100-149 (PCO-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 4250.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '150-199 (PCO-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 6500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '200-299 (PCO-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 8500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '300-399 (PCO-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'PCO-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '400+ (PCO-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 13750.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_Program_Type__c == 'RTL-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '70-99 (RTL-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_Program_Type__c == 'RTL-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '100-149 (RTL-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 2250.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_Program_Type__c == 'RTL-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '150-199 (RTL-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 6500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_Program_Type__c == 'RTL-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '200-299 (RTL-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 8500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_Program_Type__c == 'RTL-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '300-399 (RTL-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_Program_Type__c == 'RTL-MDU 1-2-2014 (AI 60)' && O.Amenity_tier__c == '400+ (RTL-MDU 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 13750.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_Program_Type__c == 'RTL-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '70-99 (RTL-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 1250.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_Program_Type__c == 'RTL-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '100-149 (RTL-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 4250.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_Program_Type__c == 'RTL-FTG 1-2-2014 (AI 60)' && O.Amenity_tier__c == '150-199 (RTL-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 6500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '200-299 (RTL-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 8500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '300-399 (RTL-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '400+ (RTL-FTG 1/2/2014 - AI 60)')
{O.Amenity__c = '48'; O.Approved_Incentive_Amount__c = 13750.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '70-99 (RTL-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 1250.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '100-149 (RTL-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 4250.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '150-199 (RTL-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 6500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '200-299 (RTL-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 8500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '300-399 (RTL-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '400+ (RTL-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 13750.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '70-99 (PCO-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 1250.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '100-149 (PCO-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 4250.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '150-199 (PCO-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 6500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '200-299 (PCO-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 8500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '300-399 (PCO-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '400+ (PCO-FTG Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 13750.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
// Start requested / provided by Stephen to allow Amenity with Smartbox

if(O.Amenity_tier__c == '70-99 (RTL-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '100-149 (RTL-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 2250.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '150-199 (RTL-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 6500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '200-299 (RTL-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 8500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '300-399 (RTL-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '400+ (RTL-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 13750.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if(O.Amenity_tier__c == '70-99 (PCO-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '100-149 (PCO-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 2250.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '150-199 (PCO-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 6500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '200-299 (PCO-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 8500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '300-399 (PCO-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 10500.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if(O.Amenity_tier__c == '400+ (PCO-MDU Smartbox 1/2/2014 - AI 60)')
{O.Amenity__c = '2'; O.Approved_Incentive_Amount__c = 13750.00; O.Amenity_Minimum_Programming__c = 8.08; }
else

if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '50-69 (PCO - Smartbox Upgrade - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 1000.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '70-99 (PCO - Smartbox Upgrade - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 1250; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '100-149 (PCO - Smartbox Upgrade - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 3250; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '150-199 (PCO - Smartbox Upgrade - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 6500; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '200-299 (PCO - Smartbox Upgrade - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 8500; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '300-399 (PCO - Smartbox Upgrade - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 14000; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '400+ (PCO - Smartbox Upgrade - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 20000; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '50-69 (RTL - Smartbox Upgrade - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 1000.00; O.Amenity_Minimum_Programming__c = 9.50; }
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '70-99 (RTL - Smartbox Upgrade - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 1250; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '100-149 (RTL - Smartbox Upgrade - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 3250; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '150-199 (RTL - Smartbox Upgrade - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 6500; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '200-299 (RTL - Smartbox Upgrade - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 8500; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '300-399 (RTL - Smartbox Upgrade - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 14000; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '400+ (RTL - Smartbox Upgrade - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 20000; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && (O.Amenity_Tier__c == '50-69 (RTL - New MDU - 3-12-2015)' || O.Amenity_Tier__c == '50-69 (RTL - New FTG - 3-12-2015)' ) ))
{O.Approved_Incentive_Amount__c = 1000; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '70-99 (RTL - New FTG - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 3800; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '100-149 (RTL - New FTG - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 6800; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '70-99 (RTL - New MDU - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 2600; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '100-149 (RTL - New MDU - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 4800; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && (O.Amenity_Tier__c == '150-199 (RTL - New FTG - 3-12-2015)' || O.Amenity_Tier__c == '150-199 (RTL - New MDU - 3-12-2015)' ) ))
{O.Approved_Incentive_Amount__c = 9000; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && (O.Amenity_Tier__c == '200-299 (RTL - New FTG - 3-12-2015)' || O.Amenity_Tier__c == '200-299 (RTL - New MDU - 3-12-2015)' ) ))
{O.Approved_Incentive_Amount__c = 11000; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && (O.Amenity_Tier__c == '300-399 (RTL - New FTG - 3-12-2015)' || O.Amenity_Tier__c == '300-399 (RTL - New MDU - 3-12-2015)' ) ))
{O.Approved_Incentive_Amount__c = 16500; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && (O.Amenity_Tier__c == '400+ (RTL - New FTG - 3-12-2015)' || O.Amenity_Tier__c == '400+ (RTL - New MDU - 3-12-2015)' ) ))
{O.Approved_Incentive_Amount__c = 22500; O.Amenity_Minimum_Programming__c = 9.50;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '50-69 (PCO - New MDU - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 1000.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '70-99 (PCO - New MDU - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 2600; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '100-149 (PCO - New MDU - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 4800; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '150-199 (PCO - New MDU - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 9000; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '200-299 (PCO - New MDU - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 11000; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '300-399 (PCO - New MDU - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 16500; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '400+ (PCO - New MDU - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 22500; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '50-69 (PCO - New FTG - 3-12-2015)' ))
{O.Approved_Incentive_Amount__c = 1000.00; O.Amenity_Minimum_Programming__c = 8.08; }
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '70-99 (PCO - New FTG - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 3800; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '100-149 (PCO - New FTG - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 6800; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '150-199 (PCO - New FTG - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 9000; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '200-299 (PCO - New FTG - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 11000; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '300-399 (PCO - New FTG - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 16500; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c == 'Amenity Incentive 60' && O.Amenity_Tier__c == '400+ (PCO - New FTG - 3-12-2015)'  ))
{O.Approved_Incentive_Amount__c = 22500; O.Amenity_Minimum_Programming__c = 8.08;}
else
if((O.Promotion__c != 'Amenity Incentive 60' || O.Promotion__c != 'Amenity Incentive 36' ))
{O.Amenity__c = 'None'; O.Approved_Incentive_Amount__c = 0.00; O.Amenity_Minimum_Programming__c = 0.00; }
}