trigger createShares on Distributor_Retailer_Association__c (before delete, after insert, after update) {
//this trigger manages shares on the retailer account and its leads to the associated distributor account

//need to create share on retailer account for disti portal role	
	List<Id> distiAccountIds = new List<Id>();
	List<Id> retailerAccountIds = new List<Id>();
	if(trigger.isInsert){
		for (Distributor_Retailer_Association__c distiRetailerAssociation : trigger.new) {
			distiAccountIds.add(distiRetailerAssociation.Distributor__c);
			retailerAccountIds.add(distiRetailerAssociation.Retailer__c);
		}
		Map<Id, Id> accountIdUserRoleIdMap = GroupSharingHelper.getRoles(distiAccountIds);
		Map<Id,Id> gMap = GroupSharingHelper.getGroups(accountIdUserRoleIdMap.values());
		GroupSharing.createShares(retailerAccountIds, accountIdUserRoleIdMap, gMap);
	
	} else if(trigger.isDelete){
		for (Distributor_Retailer_Association__c distiRetailerAssociation : trigger.old) {
			if (distiRetailerAssociation.Distributor__c != null && distiRetailerAssociation.Retailer__c != null) {
				distiAccountIds.add(distiRetailerAssociation.Distributor__c);
				retailerAccountIds.add(distiRetailerAssociation.Retailer__c);
			}
		}
		Map<Id, Id> accountIdUserRoleIdMap = GroupSharingHelper.getRoles(distiAccountIds);
		Map<Id,Id> gMap = GroupSharingHelper.getGroups(accountIdUserRoleIdMap.values());
		GroupSharing.removeShares(retailerAccountIds, distiAccountIds, accountIdUserRoleIdMap, gMap);
	
	}else{ //update
		List<Id> oldDistiAccountIds = new List<Id>();
		List<Id> oldRetailerAccountIds = new List<Id>();
		
		for (Distributor_Retailer_Association__c distiRetailerAssociation : trigger.new) {
			if (distiRetailerAssociation.Distributor__c != trigger.oldMap.get(distiRetailerAssociation.Id).Distributor__c ||
				distiRetailerAssociation.Retailer__c != trigger.oldMap.get(distiRetailerAssociation.Id).Retailer__c)
			{
				oldDistiAccountIds.add(trigger.oldMap.get(distiRetailerAssociation.Id).Distributor__c);
				oldRetailerAccountIds.add(trigger.oldMap.get(distiRetailerAssociation.Id).Retailer__c);

				distiAccountIds.add(distiRetailerAssociation.Distributor__c);
				retailerAccountIds.add(distiRetailerAssociation.Retailer__c);
			}
		}
		//remove for old
		Map<Id, Id> accountIdUserRoleIdMap = GroupSharingHelper.getRoles(oldDistiAccountIds);
		Map<Id,Id> gMap = GroupSharingHelper.getGroups(accountIdUserRoleIdMap.values());
		GroupSharing.removeShares(oldRetailerAccountIds, oldDistiAccountIds, accountIdUserRoleIdMap, gMap);

		//create for new
		accountIdUserRoleIdMap = GroupSharingHelper.getRoles(distiAccountIds);
		gMap = GroupSharingHelper.getGroups(accountIdUserRoleIdMap.values());
		GroupSharing.createShares(retailerAccountIds, accountIdUserRoleIdMap, gMap);

	}
}