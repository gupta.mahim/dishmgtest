trigger Lead_Type_MDU on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'Apartment/Condo/Townhome' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';} 
else
if(L.Type_of_Business__c == 'Planned Unit Development' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Connection' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'University' && L.Location_of_Service__c == 'Resident Room' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'School' && L.Location_of_Service__c == 'Resident Room' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Dock Hookup' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Assisted Living' && L.Location_of_Service__c == 'Resident Room' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Assisted Living' && L.Location_of_Service__c != 'Common Area' && L.Location_of_Service__c != 'Other' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Retirement Community' && L.Location_of_Service__c == 'Resident Room' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';} 
else
if(L.Type_of_Business__c == 'Retirement Community' && L.Location_of_Service__c != 'Common Area' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU';L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';} 
else
if(L.Type_of_Business__c == 'Apartment/Condo/Townhome' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';} 
else
if(L.Type_of_Business__c == 'Planned Unit Development' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Connection' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'University' && L.Location_of_Service__c == 'Resident Room' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'School' && L.Location_of_Service__c == 'Resident Room' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Dock Hookup' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Assisted Living' && L.Location_of_Service__c == 'Resident Room' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Assisted Living' && L.Location_of_Service__c != 'Common Area' && L.Location_of_Service__c != 'Other' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';}
else
if(L.Type_of_Business__c == 'Retirement Community' && L.Location_of_Service__c == 'Resident Room' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';} 
else
if(L.Type_of_Business__c == 'Retirement Community' && L.Location_of_Service__c != 'Common Area' &&  L.Number_of_Units__c < 100 &&  L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU')
{L.sundog_deprm2__Lead_Type__c = 'MDU';L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';} 
else
if(L.Type_of_Business__c == 'Apartments/Condos' && L.Number_of_Units__c < 100 && L.sundog_deprm2__Lead_Type__c != 'MDU'){L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary'; L.Type_of_Business__c = 'Apartment/Condo/Townhome';}
else
if(L.Type_of_Business__c == 'Apartments/Condos' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU'){L.sundog_deprm2__Lead_Type__c = 'MDU'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary'; L.Type_of_Business__c = 'Apartment/Condo/Townhome';}
else
if(L.Type_of_Business__c == 'Senior Living' && L.Number_of_Units__c < 100 && L.Number_of_Units__c > 0 && L.sundog_deprm2__Lead_Type__c != 'MDU'){L.sundog_deprm2__Lead_Type__c = 'MDU';L.sundog_deprm2__Lead_Sub_Type__c = 'Secondary';} 
else
if(L.Type_of_Business__c == 'Senior Living' && L.Number_of_Units__c > 99 && L.sundog_deprm2__Lead_Type__c != 'MDU'){L.sundog_deprm2__Lead_Type__c = 'MDU';L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';} 
// else nothing
}