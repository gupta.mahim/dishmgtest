trigger SBoxDuplicateLocator on Smartbox__c (before insert, before update) {

    Map<String, Smartbox__c> SBoxMap =new Map <String, Smartbox__c>();
    for (Smartbox__c s : System.Trigger.new) {
        
        // Make sure we don't treat a Serial_Number__c that isn't changing during an update as a duplicate.
    
        if ((Smartbox__c.Serial_Number__c !=null ) && (s.NoTrigger__c == False )&& (System.Trigger.isInsert || (s.Unique_Opp_SN__c != System.Trigger.oldMap.get(s.Id).Unique_Opp_SN__c))) {
    

//                (s.Serial_Number__c != 
//                    System.Trigger.oldMap.get(s.Id).Serial_Number__c))) {
        
            // Make sure another new Smartbox__c isn't also a duplicate 
    
            if (SBoxMap.containsKey(s.Unique_Opp_SN__c)) {                   s.CAID__c = 'DUPE';
//                s.Serial_Number__c.addError('Another new Smartbox__c has the '
//                                    + 'same Serial_Number__c address.');
            }else {
                SBoxMap.put(s.Unique_Opp_SN__c, s);
            }
       }
    }
    
    // Using a single database query, find all the Smartbox__c's in
    // the database that have the same Serial_Number__c address as any
    // of the Smartbox__cs being inserted or updated.
    
    for (Smartbox__c s : [SELECT Unique_Opp_SN__c FROM Smartbox__c
                      WHERE Unique_Opp_SN__c IN :SboxMap.KeySet()]) {         Smartbox__c newSbox = SBoxMap.get(s.Unique_Opp_SN__c);          newSbox.CAID__c = 'DUPE';
//        newSbox.Serial_Number__c.addError('A Smartbox__c with this Serial_Number__c '
//                               + 'address already exists.');
    }
}