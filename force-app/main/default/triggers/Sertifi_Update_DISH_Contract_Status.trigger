trigger Sertifi_Update_DISH_Contract_Status on Sertifi2_0__TestContract__c (After Update, After Insert) {

 // Collect the ID's from the Sertifi Record and the DISH Contract listed on it.
        Sertifi2_0__TestContract__c SR = trigger.new[0];
            Set<Id> SRIds = new Set<Id>();
            Set<Id> DCIds = new Set<Id>();
            for (Sertifi2_0__TestContract__c SR2: Trigger.New){
                DCIds.add(SR2.DISH_Contract__c);
                SRIds.add(SR2.Id);
                                                               }
         Map<Id,Sertifi2_0__TestContract__c> SRMapp=new  Map<Id,Sertifi2_0__TestContract__c> ([select Id,Status__c,DISH_Contract__c from Sertifi2_0__TestContract__c where Id in :SRIds]);

 //Create a bunch of lists, each list is unique for certian processes.
        list<Sertifi2_0__TestContract__c> SRL = [select Id,Status__c,DISH_Contract__c from Sertifi2_0__TestContract__c where Id in :SRIds];
        list<DISH_Contract__c> DCL = [select Id,Status__c from DISH_Contract__c where Id in :DCIds];

 // #1.0 Make the DISH Contract Status the same as the current Sertifi Status.
     if(SRL.size() > 0 && DCL.size() > 0)
     {
         for (DISH_Contract__c dc1: DCL) 
             { DCL[0].Status__c = SRL[0].Status__c; }
Update DCL[0];
     }
}