trigger LeadCategorySelectionFTG on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Prisons' && L.Location_of_Service__c == 'Cells' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Casinos' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == NULL && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c != 'Common Area'  && L.Location_of_Service__c != 'Food Service' && L.Location_of_Service__c != 'Rooms' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Prisons' && L.Location_of_Service__c == 'Cells' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Casinos' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == NULL && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c != 'Common Area'  && L.Location_of_Service__c != 'Food Service' && L.Location_of_Service__c != 'Rooms' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotels' && L.Category__c != 'FTG'){L.Category__c = 'FTG'; L.Type_of_Business__c='Hotel/Motel';}
else
if(L.Type_of_Business__c == 'RV Parks Marinas' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Connection' && L.Category__c != 'FTG' && L.Category__c != 'FTG' ){L.Category__c = 'FTG'; }
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Dock Hookup' && L.Category__c != 'FTG' && L.Category__c != 'FTG' ){L.Category__c = 'FTG'; }
else
if(L.Type_of_Business__c == 'other' && L.Category__c != 'FTG' ){L.Category__c = 'FTG'; }
else
if(L.Type_of_Business__c == 'bulk' && L.Category__c != 'FTG' ){L.Category__c = 'FTG'; }

}