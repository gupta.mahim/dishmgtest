trigger LeadownerCopy on Lead (before Insert, before Update) {
for(Lead x : Trigger.New){

        if( x.OwnerId == null )                {x.Lead_Owner_Copy__c = '00560000000lra6';}
        else // check that owner is a user (not a queue)
        if( ((String)x.OwnerId).substring(0,3) == '005' )
                {x.Lead_Owner_Copy__c = x.OwnerId;}
        else // In case of queue, use admin Id 
        if( ((String)x.OwnerId).substring(0,3) != '005' )
                {x.Lead_Owner_Copy__c = '00560000000lra6';}
    }
}

            // in case of Queue we clear out our copy field
            // x.Lead_Owner_Copy__c = null;