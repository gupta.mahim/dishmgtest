trigger PropertyStatus_SalesStage_Update on Opportunity (Before Update, Before Insert) {
Opportunity O = trigger.new[0];

if(O.RecordTypeID=='01260000000LuQM' && O.Launch_Date__c != NULL && O.Property_Status__c != 'Pre-Activation' && O.Property_Status__c != 'Pending Activation' && O.Property_Status__c != 'Active' && O.Property_Status__c != 'Pre-Built' && O.Disconnect_Date__c == NULL)  {        O.Property_Status__c = 'Inactive';        O.StageName = 'Closed Won';    }
else
if(O.RecordTypeID=='01260000000LuQM' && O.Launch_Date__c == NULL && O.StageName == 'Closed Lost' && O.Disconnect_Date__c == NULL)     {        O.Property_Status__c = 'Inactive';    }
else
if(O.RecordTypeID=='01260000000LuQM' && O.Launch_Date__c != NULL && O.StageName == 'Closed Won' && O.Disconnect_Date__c != NULL)    {        O.Property_Status__c = 'Disconnected';    }
else
if(O.RecordTypeID=='01260000000LuQM' && O.Launch_Date__c == NULL && O.StageName != 'Closed Lost' && O.StageName != 'Closed Won' && O.Property_Status__c != 'Pending Activation' && O.Property_Status__c != 'Active' && O.Property_Status__c != 'Pre-Built' && O.Disconnect_Date__c == NULL)    {        O.Property_Status__c = 'Inactive';}
}