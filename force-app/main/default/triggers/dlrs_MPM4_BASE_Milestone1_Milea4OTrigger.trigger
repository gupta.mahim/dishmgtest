/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_MPM4_BASE_Milestone1_Milea4OTrigger on MPM4_BASE__Milestone1_Milestone__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler(MPM4_BASE__Milestone1_Milestone__c.SObjectType);
}