trigger Equipment_HiddenFieldOpportunity on Equipment__c (After Insert, After Update) {

    Set<Id> oppId=new Set<Id>();
    Set<Id> eqId=new Set<Id>();
    for(equipment__c CA1:Trigger.New)    {
        oppId.add(CA1.Opportunity__c);
        eqId.add(CA1.Id);
}//End of for loop



        if(oppId.size()>0){         

List<Opportunity> oppupdatelist = new List<Opportunity>();
System.debug('=== contents of List Opp: ' +oppupdatelist );


List<Opportunity> oppswitheqs = [select Id, (select id, Name, statis__c, Programming__c, Opportunity__c, Channel__c, lastModifiedDate, LastModifiedBy.Name from equipment__r where (Action__c = 'ADD' OR Action__c = 'REMOVE' ) AND CaseID__c = null) from Opportunity where Id in :oppId ];
System.debug('=== contents of List Opp with EQ: ' +oppswitheqs );

if(oppswitheqs.size()>0){                
for(Opportunity Opp :oppswitheqs){
           
                              List<String> NewList= new List<String>();

                   for (equipment__c eq2: Opp.equipment__r )    {
                       NewList.add(eq2.Statis__c);
                       NewList.add(' - ');
                       NewList.add(eq2.Name);
                       NewList.add(' - Packages =');
                       NewList.add(eq2.Programming__c);
                       NewList.add(' - Channels =');
                       NewList.add(eq2.Channel__c);
                       NewList.add(' - ');
                       NewList.add(eq2.LastModifiedBy.Name);
                       NewList.add(' on ');
                       String str1 = '' + eq2.lastModifiedDate;
                       NewList.add(str1);
                       NewList.add(' GMT 0');
                       NewList.add(' <br />');
                       System.debug('=== contents of NewList: ' +NewList);
                   }
               
                  String s = '';
                   for(String c : NewList)    {
                       s = s + c;
                       system.debug('This data should go into the Opp' +  c);
                     }
                    
                    Opp.Hidden_Bulk_Equipment__c = s;


                                oppupdatelist.add(Opp);


                }//End of for(Opportunity Opp :oppswitheqs)
          

            if(oppupdatelist.size()>0){
                               Database.update(oppswitheqs);

                           }

}
}//End of if(oppId.size()>0)

}