trigger Lead_Type_Private on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'Health Care' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';} 
else
if(L.Type_of_Business__c == 'Sports Facilities' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Office' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c != 'Rooms' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Government' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Health/Fitness' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Automotive' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Banks' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hair salon' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Beauty Services' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Grocery/Health Food Store' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Church' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Professional Services' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Nail Salon' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Stadiums' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Other' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Military Bases' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Private Club' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Office' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'School' && L.Location_of_Service__c == 'Classroom' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'School' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'School' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'PrivateClubs' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Service Industry' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Retail Store' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Retail' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Medical / Dental' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
// else nothing
}