trigger OLIChangeLogBD on OpportunityLineItem (After Delete) {

   for (OpportunityLineItem OD: Trigger.Old)    {
           {OLIChangeLog__c OAD = new OLIChangeLog__c(
               Old_ID__c = OD.ID18__c,
               Opportunity__c = OD.OpportunityId,
               Product_Name__c = OD.Product_Name__c,
               Quantity__c = OD.Quantity,
//             Product__c = OD.Product2Id,
               Action__c = OD.Action__c,
               Trigger_Action__c = 'Delete');
           insert OAD;
       }
    }
}