trigger OLIChangeLogAI on OpportunityLineItem (After Insert) {

   for (OpportunityLineItem OI: Trigger.New)    {
           {OLIChangeLog__c OAI = new OLIChangeLog__c(
               Old_ID__c = OI.ID18__c,
               Opportunity__c = OI.OpportunityId,
               Product_Name__c = OI.Product_Name__c,
               Quantity__c = OI.Quantity,
//             Product__c = OI.Product2Id,
               Action__c = OI.Action__c,
               Trigger_Action__c = 'Insert');
           insert OAI;
       }
    }
}