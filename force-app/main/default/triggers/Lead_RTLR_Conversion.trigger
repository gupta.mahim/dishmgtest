// Converts PRM Owned Leads into an opportunity - No Contact - Attaches to PRM owners Partner Account (MDU and FTG)

trigger Lead_RTLR_Conversion on Lead (After Update) {
Lead L = trigger.new[0];
if(L.Convert_Lead__c == 'RTL - FTG/MDU' && L.isConverted == false ){  Database.LeadConvert lc = new Database.LeadConvert();      lc.setLeadId(L.Id);     lc.setAccountId(UserUtil.CurrentUser.Account__c);     lc.setDoNotCreateOpportunity(false);          lc.ConvertedStatus='Converted';     Database.LeadConvertResult 
  lcr = Database.convertLead(lc);         
  //  System.assert(lcr.isSuccess());
   }}