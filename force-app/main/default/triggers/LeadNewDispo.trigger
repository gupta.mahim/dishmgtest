trigger LeadNewDispo on Lead (Before Update, Before Insert)
    { 
        Lead L = trigger.new[0];
            if(L.Lead_Sold__c == FALSE && L.Lead_Lost__c  == FALSE && L.Sales_Stage__c != 'Customer Contacted' && L.Sales_Stage__c != 'Lead Accepted' && L.Sales_Stage__c != 'Returned to Dish - Invalid Lead' && L.Sales_Stage__c != 'Returned to Dish - Duplicate' )
                {
                    L.Status = 'Open';
                    L.Sales_Status__c = 'Open';
                    L.Lead_Sold_Date__c = Null;
                    L.Lost_Reason__c = '';
                    L.Other_Reason__c = '';
                    L.CSG_Account_Number__c = '';
                }
            else
            if(L.Lead_Sold__c == TRUE && L.Lead_Lost__c  == FALSE )
                {
                    L.Status = 'Closed';
                    L.Sales_Status__c = 'Closed Won';
                    L.Lost_Reason__c = '';
                    L.Other_Reason__c = '';
                    L.Sales_Stage__c = 'Closed Won';
                 }
            else
                 if(L.Lead_Sold__c == FALSE && L.Lead_Lost__c  == TRUE && L.Sales_Stage__c != 'Returned to Dish - Duplicate' && L.Sales_Stage__c != 'Returned to Dish - Invalid Lead' )
                {
                    L.Status = 'Closed';
                    L.Sales_Status__c = 'Closed Lost';
                    L.Sales_Stage__c = 'Closed Lost';
                 }
            else
            if(L.Lead_Sold__c == FALSE && L.Lead_Lost__c  == FALSE && L.Sales_Stage__c == 'Customer Contacted' )
                {
                    L.Status = 'Open';
                    L.Sales_Status__c = 'Open';
                    L.Lost_Reason__c = '';
                    L.Other_Reason__c = '';
                    L.Lead_Sold_Date__c = Null;
                    L.Customer_Contacted_Date__c=System.today();
                 }
            else
            if(L.Lead_Sold__c == FALSE && L.Lead_Lost__c  == FALSE && L.Sales_Stage__c == 'Lead Accepted' )
                {
                    L.Status = 'Open';
                    L.Sales_Status__c = 'Open';
                    L.Lost_Reason__c = '';
                    L.Other_Reason__c = '';
                    L.Lead_Sold_Date__c = Null;
                    L.Retailer_Owner_Accepted_Date_Time__c=System.today();
                 }
            else
            if(L.Lead_Sold__c == FALSE && L.Sales_Stage__c == 'Returned to Dish - Invalid Lead' )
               {
                    L.Status = 'Closed';
                    L.Sales_Status__c = 'Closed Lost';
                    L.Lost_Reason__c = 'Invalid Lead';
                    L.Other_Reason__c = '';
                    L.Lead_Sold_Date__c = Null;
                    L.Lead_Lost__c = TRUE;
                 }
             else
            if(L.Lead_Sold__c == FALSE && L.Sales_Stage__c == 'Returned to Dish - Duplicate' )
                {
                    L.Status = 'Closed';
                    L.Sales_Status__c = 'Closed Lost';
                    L.Lost_Reason__c = 'Duplicate';
                    L.Other_Reason__c = '';
                    L.Lead_Sold_Date__c = Null;
                    L.Lead_Lost__c = TRUE;
                 }

    }