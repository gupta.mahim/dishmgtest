// This should copy attachments from the DISH Contract record to the DISH Contract record where the SF ID is the same as the ID in the contract name.
trigger DISH_Contracts_CopyAttachments_Singed on DISH_Contract__c (after update) {
    
    //System.debug('KA:: Dish Contract Trg Called');
    // Map<Id,DISH_Contract__c> dcMapp=new  Map<Id,DISH_Contract__c> ([select Id, Status__c, Related_DISH_Contract__c from DISH_Contract__c where Id = :Trigger.new[0].Id]);
    list<DISH_Contract__c> dc = [select id, Status__c, Do_not_allow_Sertifi_Updates__c, Related_DISH_Contract__c from DISH_Contract__c where Id = :Trigger.new[0].Id];
    String recordID = Trigger.new[0].Id+'';
    //System.debug('KA: recordID length' +recordID.length());
    System.debug('KA:: '+ dc[0].Status__c + ' '+ dc[0].Do_not_allow_Sertifi_Updates__c);
    if(dc[0].Status__c == 'Fully Signed' && dc[0].Do_not_allow_Sertifi_Updates__c == false )      
    {
        ID acctID ;
        Attachments_Classic_Lightning__c obj = Attachments_Classic_Lightning__c.getOrgDefaults();
        System.debug('KA:: Is File Upload: '+  obj.Is_File_Upload__c);
        if(obj.Is_File_Upload__c == true)
        {
            //DISH_Contract__c accontdata = [select account__c from DISH_Contract__c where id =:Trigger.new[0].Id];
            //Attachment[] attList = [select id, name, Description, body  from Attachment where ParentId = :Trigger.new[0].Id];
            ContentDocumentLink[] contDocLinkList = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink  where LinkedEntityId=:Trigger.new[0].Id ];List<ContentDocumentLink> insertLinks = new List<ContentDocumentLink>();List<ContentDocumentLink> deleteLinks = new List<ContentDocumentLink>();if(contDocLinkList.size() > 0) { for(ContentDocumentLink condcLink : contDocLinkList ){ContentDocumentLink newclnk = condcLink.clone();ContentDocument contDocList = [Select id, title from ContentDocument where id=:condcLink.ContentDocumentId];String s1 = contDocList.title;String s2 = s1.substringBeforeLast('.'); String s3 = s2.substringBeforeLast('.'); String s4 = s3.Right(18);
                                            System.debug('String Very Top 4: ' + s4);
                    acctID = s4;
                    System.debug('KA Account ID: ' + acctID);
                    newclnk.LinkedEntityId = acctID;newclnk.ShareType = 'V';insertLinks.add(newclnk); deleteLinks.add(condcLink);

                        System.debug('String Top 1: ' + s1);
                        System.debug('String Top 2: ' + s2);
                        System.debug('String Top 3: ' + s3);
                        System.debug('String Top 4: ' + s4);
                }
                if(insertLinks.size() > 0) { try{ insert insertLinks;  delete deleteLinks; }catch(DmlException ex) { 
                        System.debug('KA:: DmlException ' + ex.getMessage());
                    }
                }   
            }   
        }
        else
        {
            Attachment[] attList = [select id, name, Description, body  from Attachment where ParentId = :Trigger.new[0].Id];
            
            Attachment[] insertAttList = new Attachment[]{};
                
                if(AttList.size() > 0 ) {
                    
                    for(Attachment a: attList)
                    { 
                        String s1 = a.Name;
                        System.debug('KA:: '+s1);
                        String s2 = s1.substringBeforeLast('.');
                        String s3 = s2.substringBeforeLast('.');
                        String s4 = s3.Right(15);

                            System.debug('String Top 1: ' + s1);    
                            System.debug('String Top 2: ' + s2);
                            System.debug('String Top 3: ' + s3);
                            System.debug('String Top 4: ' + s4);

                        Attachment  att = new Attachment(name = a.name, body = a.body, parentid = s4);
                        
                        System.debug('KA: '+ att.parentid);
                        insertAttList.add(att); 
                    }
                    if(insertAttList.size() > 0) 
                    {
                        insert insertAttList; delete attList;
                    }
                }
        }
    }
}