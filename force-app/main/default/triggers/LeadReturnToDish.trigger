trigger LeadReturnToDish on Lead (before update, before insert)
    {
        // Start MDU and FTG
        Lead L = trigger.new[0];
            if (L.Sales_Stage__c == 'Returned to Dish - Invalid Lead' && L.Category__c == 'FTG')
                {   
                    L.OwnerId = ('00G60000001ORIJEA4');
                    Database.DMLOptions dlo = new Database.DMLOptions();
                    dlo.EmailHeader.triggerAutoResponseEmail = false;
                    dlo.EmailHeader.triggerOtherEmail = false;
                    dlo.EmailHeader.triggerUserEmail = false;
                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
            Else
               if (L.Sales_Stage__c == 'Returned to Dish - Invalid Lead' && L.Category__c == 'MDU')                {                       L.OwnerId = ('00G60000001ORIJEA4') ;                    Database.DMLOptions dlo = new Database.DMLOptions();                    dlo.EmailHeader.triggerAutoResponseEmail = false;                    dlo.EmailHeader.triggerOtherEmail = false;                    dlo.EmailHeader.triggerUserEmail = false;                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
            Else
               if (L.Sales_Stage__c == 'Returned to Dish - Duplicate' && L.Category__c == 'FTG')
                {   
                    L.OwnerId = ('00G60000001ORIJEA4') ;
                    Database.DMLOptions dlo = new Database.DMLOptions();
                    dlo.EmailHeader.triggerAutoResponseEmail = false;
                    dlo.EmailHeader.triggerOtherEmail = false;
                    dlo.EmailHeader.triggerUserEmail = false;
                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
            Else
               if (L.Sales_Stage__c == 'Returned to Dish - Duplicate' && L.Category__c == 'MDU')                {                       L.OwnerId = ('00G60000001ORIJEA4') ;                    Database.DMLOptions dlo = new Database.DMLOptions();                    dlo.EmailHeader.triggerAutoResponseEmail = false;                    dlo.EmailHeader.triggerOtherEmail = false;                    dlo.EmailHeader.triggerUserEmail = false;                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
           // Start P/P
            Else
                if (L.Sales_Stage__c == 'Returned to Dish - Invalid Lead' && L.Category__c == 'I/R')                    {                           L.OwnerId = ('00G600000016h6A') ;                        Database.DMLOptions dlo = new Database.DMLOptions();                        dlo.EmailHeader.triggerAutoResponseEmail = false;                        dlo.EmailHeader.triggerOtherEmail = false;                        dlo.EmailHeader.triggerUserEmail = false;                        dlo.assignmentRuleHeader.useDefaultRule= false;
                    }
            Else
                if (L.Sales_Stage__c == 'Returned to Dish - Duplicate' && L.Category__c == 'I/R')                    {                           L.OwnerId = ('00G600000016h6A') ;                        Database.DMLOptions dlo = new Database.DMLOptions();                        dlo.EmailHeader.triggerAutoResponseEmail = false;                        dlo.EmailHeader.triggerOtherEmail = false;                        dlo.EmailHeader.triggerUserEmail = false;                        dlo.assignmentRuleHeader.useDefaultRule= false;
                    }
           Else
               if (L.Sales_Stage__c == 'Returned to Dish - Invalid Lead' && L.Category__c == 'MDU Residential')                {                       L.OwnerId = ('00G600000016h6A') ;                    Database.DMLOptions dlo = new Database.DMLOptions();                    dlo.EmailHeader.triggerAutoResponseEmail = false;                    dlo.EmailHeader.triggerOtherEmail = false;                    dlo.EmailHeader.triggerUserEmail = false;                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
           Else
               if (L.Sales_Stage__c == 'Returned to Dish - Duplicate' && L.Category__c == 'MDU Residential')                {                       L.OwnerId = ('00G600000016h6A') ;                    Database.DMLOptions dlo = new Database.DMLOptions();                    dlo.EmailHeader.triggerAutoResponseEmail = false;                    dlo.EmailHeader.triggerOtherEmail = false;                    dlo.EmailHeader.triggerUserEmail = false;                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
            Else
               if (L.Sales_Stage__c == 'Returned to Dish - Invalid Lead' && L.Category__c == 'National Account')
                {   
                    L.OwnerId = ('00G600000016h6A') ;
                    Database.DMLOptions dlo = new Database.DMLOptions();
                    dlo.EmailHeader.triggerAutoResponseEmail = false;
                    dlo.EmailHeader.triggerOtherEmail = false;
                    dlo.EmailHeader.triggerUserEmail = false;
                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
            Else
               if (L.Sales_Stage__c == 'Returned to Dish - Duplicate' && L.Category__c == 'National Account')
                {   
                    L.OwnerId = ('00G600000016h6A') ;
                    Database.DMLOptions dlo = new Database.DMLOptions();
                    dlo.EmailHeader.triggerAutoResponseEmail = false;
                    dlo.EmailHeader.triggerOtherEmail = false;
                    dlo.EmailHeader.triggerUserEmail = false;
                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
            Else
               if (L.Sales_Stage__c == 'Returned to Dish - Invalid Lead' && L.Category__c == 'Private')                {                       L.OwnerId = ('00G600000016h6A') ;                    Database.DMLOptions dlo = new Database.DMLOptions();                    dlo.EmailHeader.triggerAutoResponseEmail = false;                    dlo.EmailHeader.triggerOtherEmail = false;                    dlo.EmailHeader.triggerUserEmail = false;                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
            Else
               if (L.Sales_Stage__c == 'Returned to Dish - Duplicate' && L.Category__c == 'Private')                {                       L.OwnerId = ('00G600000016h6A') ;                    Database.DMLOptions dlo = new Database.DMLOptions();                    dlo.EmailHeader.triggerAutoResponseEmail = false;                    dlo.EmailHeader.triggerOtherEmail = false;                    dlo.EmailHeader.triggerUserEmail = false;                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
            Else
               if (L.Sales_Stage__c == 'Returned to Dish - Invalid Lead' && L.Category__c == 'Public')                {                       L.OwnerId = ('00G600000016h6A') ;                    Database.DMLOptions dlo = new Database.DMLOptions();                    dlo.EmailHeader.triggerAutoResponseEmail = false;                    dlo.EmailHeader.triggerOtherEmail = false;                    dlo.EmailHeader.triggerUserEmail = false;                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
             Else
               if (L.Sales_Stage__c == 'Returned to Dish - Duplicate' && L.Category__c == 'Public')                {                       L.OwnerId = ('00G600000016h6A') ;                    Database.DMLOptions dlo = new Database.DMLOptions();                    dlo.EmailHeader.triggerAutoResponseEmail = false;                    dlo.EmailHeader.triggerOtherEmail = false;                    dlo.EmailHeader.triggerUserEmail = false;                    dlo.assignmentRuleHeader.useDefaultRule= false;
                }
   
        }