/*
* Author: M Fazal Ur Rehman
* Description: Validates the address information through Address Scrub API
*/
trigger taValidateAdressTrg on Tenant_Account__c (before insert, before update)
{
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
    {
        Tenant_Account__c newRec=Trigger.new[0];// The address validation is a manual process and should happen on individual record.
        Tenant_Account__c oldRec=Trigger.isUpdate?Trigger.old[0]:null;
        
         Boolean callAPI=newRec.Valid_Address__c==false && (newRec.Invoke_NetQual__c || (oldRec!=null && oldRec.Address__c!=newRec.Address__c ) || (oldRec!=null && oldRec.city__c!=newRec.city__c) || (oldRec!=null && oldRec.State__c!=newRec.State__c) || (oldRec!=null && oldRec.Zip__c!=newRec.Zip__c));
        
        newRec.Invoke_NetQual__c=callAPI;
         
         //Boolean validateAddress=(oldRec==null && newRec.Override_NetQual__c==false) || (oldRec!=null && oldRec.Override_NetQual__c==true && newRec.Override_NetQual__c==false)?true:false;
        
        
        //if(validateAddress==true && newRec.Valid_Address__c==false )ValidateAdressCtrl.validateTenantAccount(newRec.id);
    }
}