trigger CaseHiddenProductCopyToCase on Case (Before Insert)
   {
       Set<Id> caseIds=new Set<Id>();
        for(Case c:Trigger.New)
         {
            if(c.RecordTypeID == '012f20000005Pu4' || c.RecordTypeID == '0126000000017LQ' || c.RecordTypeID == '0126000000018w6'  || c.RecordTypeID == '012600000001Ccy'){
             caseIds.add(c.Opportunity__c);

         Map<Id,Opportunity> opptyMap=new  Map<Id,Opportunity> ([select Id,Hidden_Products__c,  Hidden_Bulk_Equipment__c, Hidden_Smartbox_Equipment__c from Opportunity where Id in :caseIds ]);
         {
         if(OpptyMap.size() > 0){
              c.Product_Change_Request_s__c = opptyMap.get(c.Opportunity__c).Hidden_Products__c;
              c.Bulk_Equipment_Change_Request__c = opptyMap.get(c.Opportunity__c).Hidden_Bulk_Equipment__c;
              c.Smartbox_Equipment_Change_Request__c = opptyMap.get(c.Opportunity__c).Hidden_Smartbox_Equipment__c;
         }}
    }}}