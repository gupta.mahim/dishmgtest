trigger CalloutTrigger on Contact (before update) {

// Processes for SF->AmDocs to call @Future API callout triigers.
Contact S = trigger.new[0];

// CreateCustomer - Used to create the customer account and financial account.
if(S.CM_CreateCustomer__c == True){ S.CM_CreateCustomer__c = false; S.API_Status__c = ''; S.Transaction_Code__c = ''; S.Transaction_Description__c = ''; CMaster_CalloutClass_CreateCustomerCTC.CMasterCalloutCreateCustomerCTC(S.Id); System.debug('IF 1' +S);}
else
if(S.UpdateFromCustomerMaster__c == True){ S.UpdateFromCustomerMaster__c = false; S.API_Status__c = ''; S.Transaction_Code__c = ''; S.Transaction_Description__c = ''; CMaster_CalloutClassAcct_Search.CMasterCalloutSearchCustomer(S.Id); System.debug('IF 2' +S);}
else
if(S.CreateLogin__c == True){ S.CreateLogin__c = false; S.API_Status__c = ''; S.Transaction_Code__c = ''; S.Transaction_Description__c = ''; CMaster_CalloutClass_CreateCustomerLogin.CMasterCalloutCreateCustomerLogin(S.Id); System.debug('IF 3' +S);}
else
if(S.AddSecurityPin__c == True){ S.AddSecurityPin__c = false; S.API_Status__c = ''; S.Transaction_Code__c = ''; S.Transaction_Description__c = ''; CMaster_CalloutClass_CreateCustomerPIN.CMasterCalloutCreateCustomerPIN(S.Id); System.debug('IF 4' +S);}
else
if(S.CM_Forgot_Username__c == True){ S.CM_Forgot_Username__c = false; S.API_Status__c = ''; S.Transaction_Code__c = ''; S.Transaction_Description__c = ''; CMaster_CalloutClassAcctFrgtUsername.CMasterCalloutFrgtUsername(S.Id); System.debug('IF 5' +S);}
else
if(S.Update_Password__c == True){ S.Update_Password__c = false; S.API_Status__c = ''; S.Transaction_Code__c = ''; S.Transaction_Description__c = ''; CMaster_CalloutClass_CreateCustomerPWU.CMasterCalloutCreateCustomerPWU(S.Id); System.debug('IF 6' +S);}
else{}
}