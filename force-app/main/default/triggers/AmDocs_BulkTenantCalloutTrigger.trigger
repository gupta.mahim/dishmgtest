trigger AmDocs_BulkTenantCalloutTrigger on Tenant_Equipment__c (before update) {

// Processes for SF->AmDocs to call @Future API callout triigers.
Tenant_Equipment__c S = trigger.new[0];

if(S.Action__c == 'Mass Trip / HIT' && S.Mass_Trip_Requested__c == TRUE)    {        S.Action__c = 'Mass Trip / HIT - Pending';        S.AmDocs_Transaction_Code__c = '';        S.AmDocs_Transaction_Description__c = '';        S.AmDocs_FullString_Return__c = '';        S.API_Status__c = '';        AmDocs_CalloutClass_DBulkResendAll.AmDocs_CalloutClass_DBulkResendAll(S.Id); System.debug('IF 1' +S);}
else{}
}