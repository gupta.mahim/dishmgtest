trigger TenantProductLineItemInsert on Tenant_Product_Line_Item__c (After Insert) {
   for (Tenant_Product_Line_Item__c OD: Trigger.New) {
           {Tenant_Product_Line_Item_Log__c OAD = new Tenant_Product_Line_Item_Log__c(
               AmDocs_Incremental_Caption__c = OD.AmDocs_Incremental_Caption__c,
               AmDocs_Incremental_ProductID__c = OD.AmDocs_Incremental_ProductID__c,
               AmDocs_Individual_Caption__c = OD.AmDocs_Individual_Caption__c,
               AmDocs_Individual_ProductID__c = OD.AmDocs_ProductID__c,
               Name = OD.Name,
               Price__c = OD.Price__c,
               Quantity__c = OD.Quantity__c,
               Tenant_Account__c = OD.Tenant_Account__c,
               Action__c = OD.Action__c,
               Trigger_Action__c = 'Insert');
           insert OAD;
       }
    }
}