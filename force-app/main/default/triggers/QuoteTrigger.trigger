trigger QuoteTrigger on SBQQ__Quote__c (before update) {
    if(Trigger.isBefore){
        if(Trigger.IsUpdate){
            QuoteTriggerHandler.onUpdate(Trigger.new);
        }
    }
}