trigger FrontOfficeOppUpdate on Front_Office_Request__c (After Insert, After Update) {
Set<Id> oppIds = new Set<Id>();

Front_Office_Request__c fod= trigger.new[0];

if (fod.Front_Office_Decision__c == 'In Queue' && fod.RecordTypeID == '0126000000017rc' && fod.OwnerId == '00G60000001Evwv' && fod.Opportunity__c != null){       for (Front_Office_Request__c c: Trigger.New){         if (c.Front_Office_Decision__c == 'In Queue' && c.RecordTypeID == '0126000000017rc' && c.OwnerId == '00G60000001Evwv' && c.Opportunity__c != null)         oppIds.add(c.Opportunity__c);         }         for (opportunity opp: [select id, StageName from          opportunity where Id in :oppIds]) {         if(opp.StageName != 'FO review'){         opp.StageName = 'FO review';                       }         Update opp;}}
else
if (fod.Front_Office_Decision__c == 'Declined' && fod.RecordTypeID == '0126000000017rc'){       for (Front_Office_Request__c c: Trigger.New){         if (c.Front_Office_Decision__c == 'Declined' && c.RecordTypeID == '0126000000017rc')         oppIds.add(c.Opportunity__c);         }         for (opportunity opp: [select id, StageName  from          opportunity where Id in :oppIds]) {         if(opp.StageName != 'Site survey' ){         opp.StageName = 'Site survey';                      }         Update opp;
}}else
if (fod.Front_Office_Decision__c == 'In Progress' && fod.RecordTypeID == '0126000000017rc'){
       for (Front_Office_Request__c c: Trigger.New){
         if (c.Front_Office_Decision__c == 'In Progress' && c.RecordTypeID == '0126000000017rc')
         oppIds.add(c.Opportunity__c);
         }

         for (opportunity opp: [select id, StageName  from 
         opportunity where Id in :oppIds]) {
         if(opp.StageName != 'FO review' ){
         opp.StageName = 'FO review';         }
         Update opp;}}

else
if (fod.Front_Office_Decision__c == 'Approved' && fod.RecordTypeID == '0126000000017rc'){
       for (Front_Office_Request__c c: Trigger.New){
         if (c.Front_Office_Decision__c == 'Approved' && c.RecordTypeID == '0126000000017rc')
         oppIds.add(c.Opportunity__c);
         }

         for (opportunity opp: [select StageName, Drop_Count_Approved__c  from 
         opportunity where Id in :oppIds]) {
         if(opp.Drop_Count_Approved__c == false || opp.StageName != 'FO review'){
         opp.StageName = 'FO review';
         opp.Drop_Count_Approved__c = true;    
         
         }
         Update opp;
}}
else
 if (fod.Opportunity__c != null && fod.Front_Office_Decision__c == 'Approved' && fod.RecordTypeID == '0126000000017k7' ){       for (Front_Office_Request__c c: Trigger.New){         if (c.Opportunity__c != null && c.Front_Office_Decision__c == 'Approved' && c.RecordTypeID == '0126000000017k7' )         oppIds.add(c.Opportunity__c);
         }
         for (Opportunity opp: [select id, Drop_Count_Approved__c from          Opportunity where Id in :oppIds]) {         if(opp.Drop_Count_Approved__c == false){         opp.Drop_Count_Approved__c = true;         }         Update opp;
        }    
}}