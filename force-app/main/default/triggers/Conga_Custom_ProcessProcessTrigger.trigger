trigger Conga_Custom_ProcessProcessTrigger on Conga_Custom_Process__c (after insert, before insert, before update, after update, before delete){
    FSTR.COTriggerHandler.handleProcessObjectTrigger();
}