trigger AmDocs_BulkEQCalloutTrigger on Equipment__c (before update) {

// Processes for SF->AmDocs to call @Future API callout triigers.
Equipment__c S = trigger.new[0];

if(S.Action__c == 'Send Trip / HIT')
{ S.AmDocs_FullString_Return__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_Transaction_Code__c = ''; S.AmDocs_Order_ID__c = ''; S.API_Status__c = ''; AmDocs_CalloutClass_EQReSend.AmDocsMakeCalloutEQResend(S.Id); System.debug('IF 2' +S);}
else
if(S.Action__c == 'SWAP SMARTCARD')
{ S.Action__c = 'SWAP PENDING'; S.AmDocs_FullString_Return__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_Transaction_Code__c = ''; S.AmDocs_Order_ID__c = ''; S.API_Status__c = ''; AmDocs_CalloutClass_BulkHESCSwap.AmDocsMakeCalloutBulkHESCSwap(S.Id); System.debug('IF 2' +S);}
// else
}