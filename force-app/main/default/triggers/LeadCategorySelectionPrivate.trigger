trigger LeadCategorySelectionPrivate on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'Health Care' && L.Category__c != 'Private'){L.Category__c = 'Private'; } 
else
if(L.Type_of_Business__c == 'Sports Facilities' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Office' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c != 'Rooms' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Government' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Health/Fitness' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Automotive' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Banks' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Hair salon' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Beauty Services' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Grocery/Health Food Store' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Church' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Professional Services' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Nail Salon' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Stadiums' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Other' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Military Bases' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Private Club' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Office' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'School' && L.Location_of_Service__c == 'Classroom' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'School' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'School' && L.Category__c != 'Private'){L.Category__c = 'Private';}
else
if(L.Type_of_Business__c == 'PrivateClubs' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Service Industry' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Retail Store' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Retail' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Medical / Dental' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'Lobby/Office/Retail' && L.Category__c != 'Private'){L.Category__c = 'Private'; }
else
if(L.Type_of_Business__c == 'retailoffices' && L.Category__c != 'Private'){L.Category__c = 'Private'; L.Type_of_Business__c = 'Lobby/Office/Retail';}
    

// else nothing
}