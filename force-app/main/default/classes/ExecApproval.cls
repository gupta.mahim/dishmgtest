public with sharing class ExecApproval {
private Executive_Summary__c es;
public ExecApproval(ApexPages.StandardController controller) {
this.es=(Executive_Summary__c)controller.getRecord();
}
public PageReference onSave() {
if((ApexPages.currentPage().getParameters().get('uid')=='00560000001S9tG' )
&& ApexPages.currentPage().getParameters().get('dec')=='Approved' ){
es.Ops_Director_Approval_Date__c = system.today();
es.Operations_Director_Approval__c='Approved';
}
if((ApexPages.currentPage().getParameters().get('uid')=='00560000001S9tG' ) 
&& ApexPages.currentPage().getParameters().get('dec')=='Declined' ){
es.Ops_Director_Approval_Date__c = system.today();
es.Operations_Director_Approval__c='Declined';
}
if((ApexPages.currentPage().getParameters().get('uid')=='00560000001ClYw' )
&& ApexPages.currentPage().getParameters().get('dec')=='Approved' ){
es.Operations_SVP_Approval_Date__c = system.today();
es.Operations_SVP_Approval__c='Approved';
}
if((ApexPages.currentPage().getParameters().get('uid')=='00560000001ClYw' ) 
&& ApexPages.currentPage().getParameters().get('dec')=='Declined' ){
es.Operations_SVP_Approval_Date__c = system.today();
es.Operations_SVP_Approval__c='Declined';
}
if((ApexPages.currentPage().getParameters().get('uid')=='00560000001TXaU' 
|| ApexPages.currentPage().getParameters().get('uid')=='00560000001V73j') && ApexPages.currentPage().getParameters().get('dec')=='Approved' ){
es.Sales_VP_Approval_Date__c= system.today();
es.Sales_VP_Approval__c='Approved';
}
if((ApexPages.currentPage().getParameters().get('uid')=='00560000001TXaU'  
|| ApexPages.currentPage().getParameters().get('uid')=='00560000001V73j') && ApexPages.currentPage().getParameters().get('dec')=='Declined' ){
es.Sales_VP_Approval_Date__c= system.today();
es.Sales_VP_Approval__c='Declined';
}
if((ApexPages.currentPage().getParameters().get('uid')=='00560000001ClZ1' )
&& ApexPages.currentPage().getParameters().get('dec')=='Approved' ){
es.Sales_SVP_Approval_Date__c= system.today();
es.Sales_SVP_Approval__c='Approved';
}
if((ApexPages.currentPage().getParameters().get('uid')=='00560000001ClZ1') 
&& ApexPages.currentPage().getParameters().get('dec')=='Declined' ){
es.Sales_SVP_Approval_Date__c= system.today();
es.Sales_SVP_Approval__c='Declined';
}
update es;
return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
} 
public PageReference onclose() {
return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
} 
}