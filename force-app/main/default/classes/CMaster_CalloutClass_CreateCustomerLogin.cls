public class CMaster_CalloutClass_CreateCustomerLogin {

public class person {
    public String loginId;
}

    @future(callout=true)

    public static void CMasterCalloutCreateCustomerLogin(String Id) {

 list<Contact> C = [select Id, FirstName, LastName, Role__c, Email, Phone, ExtRefNum__c, Pin__c, dishCustomerId__c, partyId__c, Username__c, Password__c from Contact where Id =:id Limit 1];    
        if( C.size() < 1 || ((C[0].Username__c == '' || C[0].Username__c == Null) || (C[0].Password__c == '' || C[0].Password__c == Null )) || (C[0].PartyId__c == '' || C[0].PartyId__c == Null) || ( C[0].dishCustomerId__c == '' || C[0].dishCustomerId__c == Null)) {
            Contact sbc = new contact();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Transaction_Code__c='';
//                sbc.Transaction_Description__c='Create -Customer Login- requires the Username, Password, Dish Customer ID, and PartyId. Your data: Username = ' +C[0].Username__c;

        if( (C[0].Username__c == '' || C[0].Username__c == Null )) {
            sbc.Transaction_Description__c='Create Business Care Login requires the Username, Password, and the Customer must be Registered. Your data is missing the Username.';
        }
        if( (C[0].Password__c == '' || C[0].Password__c == Null )) {
            sbc.Transaction_Description__c='Create Business Care Login requires the Username, Password, and the Customer must be Registered. Your data is missing the Password.';
        }
        if( (C[0].PartyId__c == '' || C[0].PartyId__c == Null )) {
            sbc.Transaction_Description__c='Create Business Care Login requires the Username, Password, and the Customer must be Registered. Your data is missing a Registered Customer.';
        }
        if( (C[0].dishCustomerId__c == '' || C[0].dishCustomerId__c == Null )) {
            sbc.Transaction_Description__c='Create Business Care Login requires the Username, Password, and the Customer must be Registered. Your data is missing a Registered Customer.';
        }
        update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Contact sbc = new contact();
                        sbc.id=id;
                        sbc.API_Status__c='';
                        sbc.Transaction_Code__c='';
                        sbc.Transaction_Description__c='ERROR: CMaster_CalloutClass_CreateCustomerCTC is missing a valid ENDPOINT';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);
                
                JSONGenerator jsonObj = JSON.createGenerator(true);
                    jsonObj.writeStartObject();
                            jsonObj.writeStringField('domain', 'DISH');
                            if(C[0].PartyId__c != '' && C[0].PartyId__C != Null) {
                                jsonObj.writeStringField('partyId', C[0].PartyId__c);
                            }
                            if(C[0].Username__c != '' && C[0].Username__c != Null) {
                                jsonObj.writeStringField('userName', C[0].Username__c);
                            }
                            if(C[0].Password__c != Null && C[0].Password__c != '') {
                                jsonObj.writeStringField('password', C[0].Password__c);
                            }
                            jsonObj.writeBooleanField('passwordChangeRequired', false);
                        jsonObj.writeEndObject();
    
                String finalJSON = jsonObj.getAsString();
                    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
      
                HttpRequest request = new HttpRequest();
                    String endpoint = CME[0].Endpoint__c;
                    request.setEndPoint(endpoint+'/cm-registration/customer/'+C[0].DishCustomerId__c+'/login');
                    request.setBody(jsonObj.getAsString());
                    request.setHeader('Content-Type', 'application/json');
                    request.setHeader('Customer-Facing-Tool', 'Salesforce');
                    request.setHeader('User-ID', UserInfo.getUsername());
                    request.setMethod('POST');
                    request.setTimeout(101000);
                        System.debug('DEBUG Get UserInfo: ' +UserInfo.getUsername());   
     
                    HttpResponse response = new HTTP().send(request);
                        System.debug(response.toString());
                        System.debug('STATUS:'+response.getStatus());
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug(response.getBody());

                        if (response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());   

                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());


JSONParser parser = JSON.createParser(strjson); person obj = (person)parser.readValueAs( person.class);

                            if(obj.loginId != Null) {
                                Contact sbc = new contact();{
                                    sbc.id=id;
                                    sbc.API_Status__c='SUCCESS';
                                    sbc.Transaction_Description__c='The DISH Busienss Care Login has been created.';
                                    sbc.loginId__c=obj.loginId;
                                    update sbc;
                                }
                            }
                            if(obj.loginId == Null) {
                                Contact sbc = new contact();{
                                    sbc.id=id;
                                    sbc.API_Status__c='SUCCESS';
                                    sbc.Transaction_Description__c='The DISH Busienss Care Login createtion has FAILED. The system says: '+response.getBody();
                                    update sbc;
                                }
                            }
                                API_Log__c apil = new API_Log__c();{
                                    apil.Record__c=id;
                                    apil.Object__c='Contact';
                                    apil.Status__c='SUCCESS';
                                    apil.Results__c=response.getBody();
                                    apil.API__c='Create Customer Login';
                                    apil.User__c=UserInfo.getUsername();
                                insert apil;
                                }
                        }

                        if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());
                                                                Contact sbc = new contact();{
                                Contact sbc2 = new contact();{
                                    sbc2.id=id;
                                    sbc2.API_Status__c='ERROR';
                                    sbc2.Transaction_Description__c=response.getBody();
                                    update sbc2;
                                }
                                API_Log__c apil2 = new API_Log__c();{
                                    apil2.Record__c=id;
                                    apil2.Object__c='Contact';
                                    apil2.Status__c='ERROR';
                                    apil2.Results__c=response.getBody();
                                    apil2.API__c='Create Customer Login';
                                    apil2.User__c=UserInfo.getUsername();
                                insert apil2;
                                }
                }
            }
        }
    }
}
}