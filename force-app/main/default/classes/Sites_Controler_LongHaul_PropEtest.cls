@isTest (seeAllData=true)
Public class Sites_Controler_LongHaul_PropEtest{

    public static testMethod void testSites_Controler_LongHaul_PropE(){
            Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity(
             
               LeadSource='BTVNA Portal', 
               Property_Type__c='Private', 
               RecordTypeId='0126000000053vu', 
               BTVNA_Request_Type__c='Activation',
                misc_code_siu__c='N123', 
                Restricted_Programming__c='True', 
                CloseDate=system.Today(), 
                StageName='Closed Won');
        
        Case c = new Case();
        c.Opportunity__c=o.Id;
        c.Account_Name__c=a.Id;
        c.Status='Form Sumitted';
        c.Origin='BTVNA Portal';
        c.Property_Type__c='Private';
        String equip=a.Equipment_Options__c;
        
    
    PageReference pageRef = Page.Sites_LongHaul_PropE;
    Test.setCurrentPage(pageRef);
    Sites_Controler_LongHaul_PropE controller = new Sites_Controler_LongHaul_PropE();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Private');
    System.currentPagereference().getParameters().put('prog','True');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Activation');
        
ApexPages.StandardController sc = new ApexPages.standardController(o);       
Sites_Controler_LongHaul_PropE RUSC = new Sites_Controler_LongHaul_PropE (sc);

   
  RUSC.getprods();  
  RUSC.createOppty();

  }
  
 
  }