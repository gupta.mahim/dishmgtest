public class GetReadAccessOpp {

    
    public class FlowOutput{
        
        @Invocablevariable
        Public string Opportunityid;
        @Invocablevariable
        Public integer count=0;
        
        
        
        
    }
    @InvocableMethod (label='GetAccessopportunity')
    Public static List<Flowoutput> getopportunityId()
    {
        List<FlowOutput>result= new LIST <FlowOutput>();
        flowoutput output = new FlowOutput();
        list<opportunity> OppList= new list<opportunity>([Select id from opportunity where Status__c = 'Active' and AmDocs_tenantType__c !='Digital Bulk' limit 15]);
        list<id> idlst = new list<Id>();
        for(opportunity opp: OppList){idlst.add(opp.id);}
        List<UserRecordAccess> RecList= new list<UserRecordAccess>([SELECT RecordId FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND HasReadAccess = true AND RecordId IN : idlst]);
  for(UserRecordAccess Ur:RecList){if(RecList.size()>0){if(output.count>1){break;}output.opportunityid=Ur.Recordid;output.count+=1;}}result.add(output);return result;}
    
}