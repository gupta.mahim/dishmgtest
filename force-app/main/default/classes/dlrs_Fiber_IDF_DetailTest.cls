/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Fiber_IDF_DetailTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Fiber_IDF_DetailTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Fiber_IDF_Detail__c());
    }
}