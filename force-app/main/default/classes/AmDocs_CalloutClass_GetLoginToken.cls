public class AmDocs_CalloutClass_GetLoginToken {
 
    @future(callout=true)

    public static void makeCallout(String Id) {
    
    list<AmDocs_Login__c> A = [select id, UserName__c, UserPassword__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
        System.debug('RESULTS of the LIST lookup to the Amdocs Login object' +A);  

    JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartObject();
            jsonObj.writeFieldName('Credentials');
            jsonObj.writeStartObject();
                for(Integer i = 0; i < A.size(); i++){ jsonObj.writeStringField('user',A[i].UserName__c); jsonObj.writeStringField('password',A[i].UserPassword__c); }
            jsonObj.writeEndObject();
        jsonObj.writeEndObject();
    String finalJSON = jsonObj.getAsString();

    if (!Test.isRunningTest()) { HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/Login'; request.setEndPoint(endpoint); request.setMethod('POST');  
      request.setHeader('Content-Type', 'application/json'); request.setHeader('Accept', 'application/json'); request.setHeader('User-Agent', 'SFDC-Callout/45.0'); request.setBody(jsonObj.getAsString()); request.setTimeout(101000);   HttpResponse response = new HTTP().send(request); if (response.getStatusCode() == 200) { } AmDocs_Login__c sbc = new AmDocs_Login__c(); sbc.name=Id; sbc.UXF_Token__c=String.valueOf(response.getHeader('Uxfauthorization')); insert sbc;         System.debug(response.toString());
           System.debug('DEBUG REQUEST: ' + request); 
           System.debug('DEBUG FINAL JSON: ' + finalJSON);
           System.debug('DEBUG FINAL BODY: ' + jsonObj.getAsString());
           System.debug('Full JSON response payload: ' +response.getBody());
           System.debug('Full Status response payload: ' +response.getStatus());
           System.debug('Full Status Code response payload: ' +response.getStatusCode());
           System.debug('DEBUG RESPONSE GET HEADER KEYS: ' +response.getHeaderKeys());
           System.debug('DEBUG RESPONSE GET HEADER KEY AUTHORIZATION: ' +response.getHeader('Uxfauthorization'));
           System.debug('STATUS:'+response.getStatus());
           System.debug('STATUS_CODE:'+response.getStatusCode());
           System.debug(response.getBody());
           System.debug('sbc'+sbc);
        }        
}
//}
}