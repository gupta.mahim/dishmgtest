/*
Name        : CM_CalloutClass_CreateCstmerPINTA_Test Apex class 
Author      : Khateeb Ibrahim
Date        : 08/29/2018
Version     : 1.0
Description : Test Class for CMaster_CalloutClass_CreateCstmerPINTA Class
*/
@isTest
private class CM_CalloutClass_CreateCstmerPINTA_Test {

    @testSetup static void testSetupdata(){
   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Incremental';
            opp1.AmDocs_ServiceID__c='012';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;
        Tenant_Account__c ta = new Tenant_Account__c();
            ta.Address__c ='1011 Coliie Path';
            ta.City__c = 'Round Rock';
            ta.State__c = 'TX';
            ta.Zip__c = '78664';
            ta.Unit__c = '1A';
            ta.Phone_Number__c = '5123835201';
            ta.Email_Address__c = 'jerry.clifft1111111111111@dish.com';
            ta.First_Name__c = 'Jerry';
            ta.Last_Name__c = 'Clifft';
            ta.Type__c = 'Incremental';
            ta.Opportunity__c = opp1.id;
            ta.PinSec__c = '1234';
            ta.Pin__c = '1234';
            ta.PinSecHint__c = '1234';
            ta.dishCustomerId__c = 'DISH196408168661';
            ta.partyId__c = '5b858c134f94675bddd42929';
        insert ta;       
        
        Tenant_Account__c ta2 = new Tenant_Account__c();
            ta2.Address__c ='1011 Coliie Path';
            ta2.City__c = 'Round Rock';
            ta2.State__c = 'TX';
            ta2.Zip__c = '78664';
            ta2.Unit__c = '1A';
            ta2.Phone_Number__c = '5123835201';
            ta2.Email_Address__c = 'jerry.clifft1111111111111@dish.com';
            ta2.First_Name__c = 'Brad';
            ta2.Last_Name__c = 'Clifft';
            ta2.Type__c = 'Incremental';
            ta2.Opportunity__c = opp1.id;
        insert ta2;
        
        CustomerMaster__c cm =new CustomerMaster__c();
            cm.EndPoint__c = 'https://test-api-gateway.dish.com/amdocs-api-gateway';
        insert cm;
        
    }
    
    static testMethod void testCreateCustomerLogin(){
        Tenant_Account__c ta1 = [select id from Tenant_Account__c where First_Name__c='Jerry' limit 1];
        string id = ta1.id;
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"Name": "sForceTest1"}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            CMaster_CalloutClass_CreateCstmerPINTA.CMasterCalloutCreateCstmrPINTA(Id); 
            CMaster_CalloutClass_CreateCstmerPINTA.CMasterCalloutCreateCstmrPINTA(Id); 
        Test.stopTest(); 
    }
    
    static testMethod void testCreateCustomerLoginCode300(){
        Tenant_Account__c ta1 = [select id from Tenant_Account__c where First_Name__c='Jerry' limit 1];
        string id = ta1.id;
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(300,
                                                 'Complete',
                                                 '{"Name": "sForceTest1"}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            CMaster_CalloutClass_CreateCstmerPINTA.CMasterCalloutCreateCstmrPINTA(Id); 
        Test.stopTest(); 
    }
    
    static testMethod void testCreateCustomerLoginNullEndPoint(){
        Tenant_Account__c ta1 = [select id from Tenant_Account__c where First_Name__c='Jerry' limit 1];
        string id = ta1.id;
        CustomerMaster__c cm =[select id,EndPoint__c from CustomerMaster__c limit 1];
        delete cm;
        
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(300,
                                                 'Complete',
                                                 '{"Name": "sForceTest1"}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            CMaster_CalloutClass_CreateCstmerPINTA.CMasterCalloutCreateCstmrPINTA(Id); 
        Test.stopTest(); 
    }   
    
    static testMethod void testCreateCustomerLoginNullValues(){
        Tenant_Account__c ta1 = [select id from Tenant_Account__c where First_Name__c='Brad' limit 1];
        string id = ta1.id;
        //CustomerMaster__c cm =[select id,EndPoint__c from CustomerMaster__c limit 1];
        //delete cm;
        
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(300,
                                                 'Complete',
                                                 '{"Name": "sForceTest1"}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            CMaster_CalloutClass_CreateCstmerPINTA.CMasterCalloutCreateCstmrPINTA(Id); 
        Test.stopTest(); 
    }  
    
}