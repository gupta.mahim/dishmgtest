public class CMaster_CalloutClassTA_Search {

public class person {
    public String dishCustomerId;
    Public cls_results [] results;
}
class cls_results {
    public String dishCustomerId;
    cls_customerOwner customerOwner;
    cls_addresses [] addresses;
}
class cls_customerOwner {
    public String Id;
    cls_emailContacts [] emailContacts;
}
class cls_addresses {
    public String city;
    public String postalCode;
    public String state;
    public String line1;
    public String line2;
}  
class cls_emailContacts{
    public String emailAddress;
}

    
    @future(callout=true)
    
    public static void CMasterCalloutSearchTACustomer(String Id) {

 list<Tenant_Account__c> C = [select Id,  First_Name__c, Last_Name__c, Address__c, City__c, State__c, Zip__c, Unit__c, Email_Address__c, API_ID__c, Phone_Number__c, AmDocs_ServiceID__c, PIN__c, dishCustomerId__c, partyId__c from Tenant_Account__c where Id =:id Limit 1];    
//        if( C.size() < 1 || ((C[0].First_Name__c == '' || C[0].First_Name__c == Null) || (C[0].Last_Name__c == '' || C[0].Last_Name__c == Null )) || (C[0].Email_Address__c == '' || C[0].Email_Address__c == Null) || (C[0].PIN__c == '' || C[0].PIN__c == Null ) || ( C[0].API_ID__c == '' || C[0].API_ID__c == Null)) {
        if( C.size() < 1 || (( C[0].API_ID__c == '' || C[0].API_ID__c == Null)) ) {
            Tenant_Account__c sbc = new Tenant_Account__c();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Amdocs_Transaction_Code__c='';

//                if( (C[0].First_Name__c == '' || C[0].First_Name__c == Null)) {sbc.Amdocs_Transaction_Description__c='Subscriber SEARCH failed, the search requires the APID, or First & Last Name, Email and Subscriber PIN. Your are missing: First Name';}
//                if( (C[0].Last_Name__c == '' || C[0].Last_Name__c == Null )) {sbc.Amdocs_Transaction_Description__c='Subscriber SEARCH failed, the search requires the APID, or First & Last Name, Email and Subscriber PIN. Your are missing: Last Name';}
//                if( (C[0].PIN__c == '' || C[0].PIN__c == Null )) {sbc.Amdocs_Transaction_Description__c='Subscriber SEARCH failed, the search requires the APID, or First & Last Name, Email and Subscriber PIN. Your are missing: Subscriber Pin';}
                if( (C[0].API_ID__c == '' || C[0].API_ID__c == Null )) {sbc.Amdocs_Transaction_Description__c='Subscriber SEARCH failed, the search requires the APID. Your are missing: APID';}

//                sbc.Amdocs_Transaction_Description__c='Subscriber SEARCH failed, the search requires the APID, or First & Last Name, Email and Subscriber PIN. Your are missing:' 
//                    if( (C[0].First_Name__c == '' || C[0].First_Name__c == Null)) {+' First Name'}
//                    if( (C[0].Last_Name__c == '' || C[0].Last_Name__c == Null )) {+' Last Name'}
//                    if( (C[0].PIN__c == '' || C[0].PIN == Null )) {+' Subscriber Pin'}
//                    if( (C[0].AP_ID__c == '' || C[0].AP_ID == Null )) {+' APID'}
            update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Tenant_Account__c sbc = new Tenant_Account__c();
                        sbc.id=id;
                        sbc.API_Status__c='';
                        sbc.Amdocs_Transaction_Code__c='';
                        sbc.Amdocs_Transaction_Description__c='ERROR: CMaster is missing a valid ENDPOINT - Contact System Administrator';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);            

        HttpRequest request = new HttpRequest();
            String endpoint = CME[0].Endpoint__c;
            if(C[0].AmDocs_ServiceID__c!= '' && C[0].API_ID__c!= Null){
                request.setEndPoint(endpoint +'/cm-search/customers?extRefNum='+C[0].API_ID__c);
            }
//            else if(C[0].API_ID__c == '' || C[0].API_ID__c != Null){
//                request.setEndPoint(endpoint +'/cm-search/customers?firstName='+C[0].First_Name__c+'&lastName='+C[0].Last_Name__c+'&Phone='+C[0].Phone_Number__c+'&Email='+C[0].Email_Address__c+'&maxResultsCount=40');
//            }                                                                                                                                                                    
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Customer-Facing-Tool', 'Salesforce');
            request.setHeader('User-ID', UserInfo.getUsername());
            request.setMethod('GET');
            request.setTimeout(101000);
     
            HttpResponse response = new HTTP().send(request);
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
                System.debug('STATUS_CODE:'+response.getStatusCode());
                String strjson = response.getbody();
                System.debug('DEBUG Get body:  ' +response.getBody());
                
           if(response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
               System.debug('DEBUG 0 ======== strjson: ' + strjson);

        
           JSONParser parser = JSON.createParser(strjson);
           parser.nextToken();
           person obj = (person)parser.readValueAs( person.class); 
 
            if(obj.results.size() > 0){
                Tenant_Account__c sbc = new Tenant_Account__c(); {
                   sbc.id=id;
                   sbc.API_Status__c='SUCCESS';
                   sbc.Amdocs_Transaction_Description__c='Subscriber record found! - Next, please create a DISH Anywhere Login the Subscriber.';
                   sbc.dishCustomerId__c=obj.results[0].dishCustomerId;
                   sbc.partyId__c=obj.results[0].customerOwner.Id;
                 if(obj.results[0].addresses[0].city != Null){
                       if(C[0].City__c == '' || C[0].City__c == Null){sbc.City__c = obj.results[0].addresses[0].city;
                       }
                    }
                    if(obj.results[0].addresses[0].postalCode != Null){
                       if(C[0].Zip__c == '' || C[0].Zip__c == Null){ sbc.Zip__c = obj.results[0].addresses[0].postalCode;
                        }
                    }
                    if(obj.results[0].addresses[0].state != Null){
                       if(C[0].State__c == '' || C[0].State__c == Null){ sbc.State__c = obj.results[0].addresses[0].state;
                       }
                   }
                   if(obj.results[0].addresses[0].line1 != Null){
                       if(C[0].Address__c == '' || C[0].Address__c == Null){ sbc.Address__c = obj.results[0].addresses[0].line1;
                        }
                    }
                    if(obj.results[0].addresses[0].line2 != Null){
                       if(C[0].Unit__c == '' || C[0].Unit__c == Null){ sbc.Unit__c = obj.results[0].addresses[0].line2;
                       }
                   }
                   if( obj.results[0].customerOwner.emailContacts != Null) {
                       if(C[0].Email_Address__c == '' || C[0].Email_Address__c == Null) { sbc.Email_Address__c= obj.results[0].customerOwner.emailContacts[0].emailAddress;
                       }
                  }          
               update sbc;
               }
               API_Log__c apil = new API_Log__c();{
                   apil.Record__c=id;
                   apil.Object__c='Tenant Account';
                   apil.Status__c='SUCCESS';
                   apil.Results__c=response.getBody();
                   apil.API__c='Search Subscriber';
                   apil.User__c=UserInfo.getUsername();
               insert apil;
               }
            }
            datetime myDateTime = datetime.now();
            
            if(obj.results.size() < 1){
                Tenant_Account__c sbc = new Tenant_Account__c(); {
                   sbc.id=id;                   sbc.API_Status__c='SUCCESS';                  sbc.Amdocs_Transaction_Description__c='No Subscriber record found! - Next, please register the Subscriber.';
               update sbc;
               }
               API_Log__c apil = new API_Log__c();{
                   apil.Record__c=id;
                   apil.Object__c='Tenant Account';
                   apil.Status__c='SUCCESS';                   apil.Results__c=response.getBody();                   apil.API__c='Search Subscriber';                   apil.User__c=UserInfo.getUsername();
              insert apil;
               }
           }
           }
           if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600){
               System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
               System.debug('DEBUG 0 ======== strjson: ' + strjson);
               System.debug('DEBUG Get body:  ' +response.getBody());
               Tenant_Account__c sbc2 = new Tenant_Account__c(); {
                   sbc2.id=id;
                   sbc2.API_Status__c='ERROR';
                   sbc2.Amdocs_Transaction_Description__c='Subscriber Search: ' +response.getBody();
               update sbc2;
               }
               API_Log__c apil2 = new API_Log__c();{
                   apil2.Record__c=id;
                   apil2.Object__c='Tenant Account';
                   apil2.Status__c='ERROR';
                   apil2.Results__c=response.getBody();
                   apil2.API__c='Search Customer';
                   apil2.User__c=UserInfo.getUsername();
               insert apil2;
               }
            }
        }
    }
}}