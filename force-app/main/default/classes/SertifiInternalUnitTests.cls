@isTest
private class SertifiInternalUnitTests {
    static TestMethod void triggerTest() {
        Account act = new Account();
            act.Name = 'Test';
            insert act;
            
        Opportunity opp = new Opportunity();
            opp.Name = 'Test';
            opp.StageName = 'Test';
            opp.CloseDate = date.newinstance(2012,12,12);
            opp.Account = act;
            opp.AccountId = act.Id;
            insert opp;
            
        Task tempTask = new Task();
            insert tempTask;

        Contact newContact = new Contact();
            newContact.LastName = 'test';
            insert newContact;
            
//This next thing must be the custom object being linked to, make sure to check this
    DISH_Contract__c newContract = new DISH_Contract__c ();
        newContract.Account__c = act.Id;
        insert newContract;

    Sertifi2_0__TestContract__c sertifiContract = new Sertifi2_0__TestContract__c();
        insert sertifiContract;

    sertifiContract.Sertifi2_0__CustomLinkID__c = newContract.id;
        update sertifiContract;
        
//System.assertEquals(newContract.id, sertifiContract.Contract__c);
    }
}