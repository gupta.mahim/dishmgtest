@isTest
private class CM_CalloutClass_CreateCustomerPIN_Test {

    @testSetup static void testSetupdata(){
   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;
        
        Account acct2 = new Account();
            acct2.Name = 'Test Account 1';
            acct2.ToP__c = 'Integrator';
            acct2.Pyscical_Address__c = '1011 Collie Path';
            acct2.City__c = 'Round Rock';
            acct2.State__c = 'TX';
            acct2.Zip__c = '78664';
            acct2.Amdocs_CreateCustomer__c = false;
            acct2.Distributor__c = 'PACE';
            acct2.OE_AR__c = '12345';
          acct2.AmDocs_CustomerID__c = 'Chewbacca101';
        insert acct2;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = 'jerry.clifft@dish.com';
            ctc1.Role__c = 'Billing Contact';
            ctc1.AccountId = acct1.Id;
            ctc1.PinSec__c = '1234';
            ctc1.Pin_Hint__c = '5678';
            ctc1.Password__c = 'Sling11!@';
            ctc1.PartyId__c = 'A1B2';
            ctc1.dishCustomerId__c = 'A2B2';
        insert ctc1;
        
        // Create a U.S. based contact
        Contact ctc2 = New Contact();
            ctc2.FirstName = 'Brad';
            ctc2.LastName = 'Clawson';
            ctc2.Phone = '512-383-5201';
            ctc2.Email = 'jerry.test@dish.com';
            ctc2.Role__c = 'Billing Contact';
            ctc2.AccountId = acct2.Id;
            ctc2.Password__c = 'Sling11!@';
            ctc2.dishCustomerId__c = 'DISH775768547075';
            ctc2.Username__c = 'jerry.test@dish.com';
            ctc2.partyId__c = '5b8436484f94675bdd9c6fbf';
            ctc1.PinSec__c = '1234';
            ctc1.Pin_Hint__c = '5678';
        insert ctc2;
        
        CustomerMaster__c cm =new CustomerMaster__c();
          cm.EndPoint__c = 'https://test-api-gateway.dish.com/amdocs-api-gateway';
        insert cm;
        
        CustomerMaster__c cm2 =new CustomerMaster__c();
            cm2.EndPoint__c = '';
        insert cm2;
        
    }
    
    static testMethod void testCreateCustomerLogin(){
        Contact con1 = [select id from Contact where firstName ='Jerry' limit 1];
        Contact con2 = [select id from Contact where firstName ='Brad' limit 1];
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"Name": "sForceTest1"}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
          CMaster_CalloutClass_CreateCustomerPIN.CMasterCalloutCreateCustomerPIN(con1.Id);
          CMaster_CalloutClass_CreateCustomerPIN.CMasterCalloutCreateCustomerPIN(con2.Id);
        Test.stopTest(); 
    }
    
    static testMethod void testCreateCustomerLoginCode300(){
        Contact con1 = [select id from Contact where firstName ='Jerry' limit 1];
        Contact con2 = [select id from Contact where firstName ='Brad' limit 1];
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(301,
                                                 'Complete',
                                                 '{"Name": "sForceTest1"}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
          CMaster_CalloutClass_CreateCustomerPIN.CMasterCalloutCreateCustomerPIN(con1.Id);
          CMaster_CalloutClass_CreateCustomerPIN.CMasterCalloutCreateCustomerPIN(con2.Id);
        Test.stopTest(); 
    }
    
    static testMethod void testCreateCustomerLoginNullEndPoint(){
        Contact con2 = [select id from Contact where firstName ='Brad' limit 1];
        CustomerMaster__c cm =[select id,EndPoint__c from CustomerMaster__c limit 1];
        delete cm;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(300,
                                                 'Complete',
                                                 '{"Name": "sForceTest1"}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
          CMaster_CalloutClass_CreateCustomerPIN.CMasterCalloutCreateCustomerPIN(con2.Id);
        Test.stopTest(); 
    }
    static testMethod void testCreateCustomerLoginNullEndPoint2(){
        Contact con2 = [select id from Contact where firstName ='Brad' limit 1];
        CustomerMaster__c cm =[select id,EndPoint__c from CustomerMaster__c where EndPoint__c = '' limit 1];
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(300,
                                                 'Complete',
                                                 '{"Name": "sForceTest1"}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
          CMaster_CalloutClass_CreateCustomerPIN.CMasterCalloutCreateCustomerPIN(con2.Id);
        Test.stopTest(); 
    }
    
}