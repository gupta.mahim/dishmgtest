@IsTest
private class ResProductLineItems_NoDupe_TEST {
    static testMethod void ResProductLineItems_NoDupe1() {
    
         Account a = new Account();
            a.Name = 'Test Account';
        insert a;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test';
            o.AccountId = a.Id;
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
        insert o;    
        
        Res_Account__c r = new Res_Account__c();
            r.Opportunity__c=o.Id;
            r.connect_date__c=system.Today();
            r.Name='9999999';
            r.Address__c='1011 Collie Path';
            r.City__c='Round Rock';
            r.Zip_Code__c='TX';
            r.First_Name__c='PresidentRockMan';
        insert r;
        
        Res_Product_Line_Item__c p = new Res_Product_Line_Item__c();
        p.SDS_Account__c=r.Id;
        p.Quantity__c=1;
        p.Res_ProductEntry__c='a2S60000000w6Gw';
        insert p;
        
        
    }
}