@IsTest
private class Lead_Type_Private_TEST {
    static testMethod void validateLead_Type_SelectionPrivate() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Health Care',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

       insert L;
        
       L = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L.Id];
       System.debug('Category after trigger fired: ' + L.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', L.sundog_deprm2__Lead_Type__c);
    }
     static testMethod void validateLead_Type_SelectionPrivate2() {
        Lead LL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Sports Facilities',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Category after trigger fired: ' + LL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate3() {
        Lead LLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hospitals',
        Location_of_Service__c ='Not Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLL;
        
       LLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLL.Id];
       System.debug('Category after trigger fired: ' + LLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate4() {
        Lead LLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hospitals',
        Location_of_Service__c='Office',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Category after trigger fired: ' + LLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate5() {
        Lead LLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Government',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLL;
        
       LLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate6() {
        Lead LLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Health/Fitness',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLL;
        
       LLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate7() {
        Lead LLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Automotive',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLL;
        
       LLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate8() {
        Lead LLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Banks',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLL;
        
       LLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate9() {
        Lead LLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hair salon',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLL;
        
       LLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate10() {
        Lead LLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Beauty Services',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLL;
        
       LLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate11() {
        Lead LLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Grocery/Health Food Store',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLL;
        
       LLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate12() {
        Lead LLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Church',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLL;
        
       LLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate13() {
        Lead LLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Professional Services',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLL;
        
       LLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate14() {
        Lead LLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Nail Salon',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate15() {
        Lead LLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Stadiums',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate16() {
        Lead LLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Other',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate17() {
        Lead LLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Military Bases',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate18() {
        Lead LLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Private Club',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate19() {
        Lead LLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Office',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate20() {
        Lead LLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='School',
        Location_of_Service__c='Classroom',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate21() {
        Lead LLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='School',
        Location_of_Service__c='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate25() {
        Lead LLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='PrivateClubs',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate26() {
        Lead LLLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Service Industry',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate27() {
        Lead LLLLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retail Store',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate28() {
        Lead LLLLLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retail',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_SelectionPrivate29() {
        Lead LLLLLLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Medical / Dental',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c);
}
}