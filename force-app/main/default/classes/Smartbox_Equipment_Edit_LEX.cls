public class Smartbox_Equipment_Edit_LEX {
	public Smartbox__c[] equip;
    @AuraEnabled
    public static Smartbox__c[] getSmartboxItems(id oppId) {
        
        System.debug('KA:: '+oppId);
        Smartbox__c[] equip = [SELECT ID,Action__c, Name, Remove_Equipment__c, CAID__c, Chassis_Serial__c, Part_Number__c,  Serial_Number__c, SmartCard__c, Type_of_Equipment__c, Status__c 
                               FROM Smartbox__c WHERE Opportunity__c =:oppId];
        return equip;
        
    }
    @AuraEnabled
    public static String saveRecords(String strEquipData) {
        System.debug('KA:: Save records Method called');
 		List<Smartbox__c> recordsToUpdate=new List<Smartbox__c>(); 
        List<Smartbox__c> equipData=(List<Smartbox__c>)JSON.deserialize(strEquipData, List<Smartbox__c>.class);
        System.debug('equipData::'+equipData);
        for(Smartbox__c anEquip:equipData) {  recordsToUpdate.add(anEquip);
        }
        System.debug(recordsToUpdate.size());
        if(recordsToUpdate.size()>0) { update recordsToUpdate;    }
        
        return 'SUCCESS';
    }
    
    //Milestone# MS-001252 - 3Jun20 - Start
    @AuraEnabled
    public static userAccessCheck.userAccessCheckResponse getUserAccess(){
        userAccessCheck.userAccessCheckResponse resp = new userAccessCheck.userAccessCheckResponse();
        list<Custom_Button_Access_Detail__mdt> objectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c='Edit Smartbox' Limit 1];
        
        if(objectFieldDetail.size()>0)
        	resp=userAccessCheck.getCurrentUserAccess(objectFieldDetail[0].Object_Name__c, objectFieldDetail[0].Field_Name__c);
        else{
            resp.currentUserHaveAccess = false;
            resp.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }
        
        return resp;
    }
    //Milestone# MS-001252 - End
}