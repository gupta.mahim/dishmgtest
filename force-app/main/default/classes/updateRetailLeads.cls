public class updateRetailLeads {
	@future
	
	public static void updateLeads(List<string> leadIdList){
		
		list<Lead> leadList = new list<Lead>();
		
		for (Lead l : [select Id from Lead where Id IN :leadIdList and sundog_deprm2__Use_Automatic_Lead_Assignment__c = true]){
			leadList.add(l);
		}
		
		update leadList;
	}

}