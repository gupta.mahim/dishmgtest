@isTest
public class DISH_Contracts_AttachmentsUnsigned_Test {  
     static Attachments_Classic_Lightning__c obj = new Attachments_Classic_Lightning__c();
    @isTest
    public static void copyAttachmentsLightning()
    {

        obj.Name ='Test';
        obj.Is_File_Upload__c = true;
        insert obj;
         Account a = new Account();
            a.Name = 'Test Account';
  //          a.Programming__c = 'Starter';
  //          a.phone = '(303) 555-5555';
        insert a;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test';
            o.AccountId = a.Id;
    //        o.BTVNA_Request_Type__c = 'Site Survey';
    //        o.LeadSource='BTVNA Portal';
    //        o.Property_Type__c='Private';
    //        o.misc_code_siu__c='N999';
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
    //        o.Restricted_Programming__c = 'True';
        insert o;
       
      Case c = new Case();
           c.Opportunity__c = o.id;
           c.AccountId = a.id;
           c.Status = 'Request Completed';
           c.Origin = 'PRM';
//           c.RequestedAction__c = 'Activation';
           c.RequestedAction__c = 'General Inquiry';
           c.Description = 'Test';
//           c.RecordTypeId = '012600000001EeC';
           c.CSG_Account_Number__c = '82551234567890';
           c.Requested_Actvation_Date_Time__c = system.Today();
      insert c;
        Contact ctc = new Contact();
//            ctc.Account = a.id;
            ctc.FirstName='Test First';
            ctc.LastName='Last Name';
            ctc.email='test@dish.com';
            ctc.Phone='512-383-5201';
        insert ctc;
      update c;
        DISH_Contract__c dc1 = new DISH_Contract__c();
            dc1.Account__c = a.id;
            dc1.Opportunity__c=o.id;
            dc1.Case__c=c.id;
            dc1.Contact__c=ctc.id;
            dc1.Contract_Start_Date__c=system.Today();
            dc1.Contract_Term_Months__c=120;
            dc1.Contract_Type__c='Direct Bulk';
            dc1.Status__c='Inactive';
        
        try{
            insert dc1;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }

        DISH_Contract__c dc2 = new DISH_Contract__c();
            dc2.Account__c = a.id;
            dc2.Opportunity__c=o.id;
            dc2.Case__c=c.id;
            dc2.Contact__c=ctc.id;
            dc2.Contract_Start_Date__c=system.Today();
            dc2.Contract_Term_Months__c=120;
            dc2.Contract_Type__c='Direct Digital';
            dc2.Status__c='Send attachment to parent contract record for E-Signature';
            dc2.Related_DISH_Contract__c=dc1.id;
        insert dc2;
        
        // Ceation of content document
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test'+a.Id+'.jpg',
            PathOnClient = 'Test'+a.Id+'.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        
        try{
            insert contentVersionInsert;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        //ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
        //List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = new ContentDocumentLink();
        ContentVersion contentDocId = [select ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
        cdl.ContentDocumentId =  contentDocId.ContentDocumentId;
        cdl.LinkedEntityId = dc2.id; //accontdata.Account__c;
        cdl.ShareType = 'V';
        try{
            insert cdl;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }

        ///// now updaing DISH_Contract__c status to Fully Signed.
        DISH_Contract__c dishconractRead = [select id, status__c from DISH_Contract__c where id=:dc2.id LIMIT 1];
        dishconractRead .Status__c = 'Send attachment to parent contract record for E-Signature';
        Test.startTest();
        try{
            update dishconractRead;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        Test.stopTest();
    }
    @isTest
    public static void copyAttachmentsClassic()
    { 
        
        obj.Is_File_Upload__c = false;
        //update obj;
        Account a = new Account();
        a.Name = 'Test Account Classic';
        //          a.Programming__c = 'Starter';
        //          a.phone = '(303) 555-5555';
        insert a;
        DISH_Contract__c dishconract = new DISH_Contract__c();
        dishconract.Account__c = a.id;
        dishconract.Contract_Start_Date__c =system.today();
        dishconract.Status__c ='Send attachment to parent contract record for E-Signature';
        try{
            insert dishconract;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        Attachment attachment = new Attachment();
            attachment.body = Blob.valueOf( 'this is an attachment test' );
            attachment.name = 'attachment'+a.id+'.jpg';
            attachment.parentId = dishconract.id;
        try{
            System.debug('KA:: '+ attachment);
             insert attachment;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
       
         ///// now updaing DISH_Contract__c status to Fully Signed.
        DISH_Contract__c dishconractReadClassic = [select id, status__c from DISH_Contract__c dishconract where id=:dishconract.id LIMIT 1];
        dishconractReadClassic .Status__c = 'Fully Signed';
        Test.startTest();
        
        try{
            update dishconractReadClassic;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        Test.stopTest(); 
    } 
}