@isTest
private class AmDocs_CalloutClass_GetTntByBulk_TA_Test { 
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Incremental';
            opp1.AmDocs_ServiceID__c='123456';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;
        
        Tenant_Account__c ta = new Tenant_Account__c();
            ta.Address__c ='1011 Coliie Path';
            ta.City__c = 'Round Rock';
            ta.State__c = 'TX';
            ta.Zip__c = '78664';
            ta.Unit__c = '1A';
            ta.Phone_Number__c = '5123835201';
            ta.Email_Address__c = 'jerry.clifft1111111111111@dish.com';
            ta.First_Name__c = 'Jerry';
            ta.Last_Name__c = 'Clifft';
            ta.Type__c = 'Incremental';
            ta.Opportunity__c = opp1.id;
            ta.PinSec__c = '1234';
            ta.Pin__c = '1234';
            ta.PinSecHint__c = '1234';
            ta.AmDocs_ServiceID__c = '123456';
            ta.dishCustomerId__c = 'DISH196408168661';
            ta.partyId__c = '5b858c134f94675bddd42929';
        insert ta;       
        
        Tenant_Account__c ta2 = new Tenant_Account__c();
            ta2.Address__c ='1011 Coliie Path';
            ta2.City__c = 'Round Rock';
            ta2.State__c = 'TX';
            ta2.Zip__c = '78664';
            ta2.Unit__c = '1A';
            ta2.Phone_Number__c = '5123835202';
            ta2.Email_Address__c = 'jerry.clifft1111111111111@dish.com';
            ta2.First_Name__c = 'Brad';
            ta2.Last_Name__c = 'Clifft';
            ta2.Type__c = 'Incremental';
            ta2.Opportunity__c = opp1.id;
        insert ta2;
}  
  
static testMethod void testGetTenantByBulkTA1(){
        Tenant_Account__c con1 = [select id from Tenant_Account__c where Phone_Number__c = '5123835201' limit 1];
        Tenant_Account__c con2 = [select id from Tenant_Account__c where Phone_Number__c = '5123835202' limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplGetTenantByBulkRestOutput":{"tenantList":[{"serviceId": "123456"}]}}',
//                                                 '{"ImplGetTenantByBulkRestOutput":{"tenantList":[{"serviceId":"1370434","productStatus":"Active","apId":"400307425517"}], "bulkStatus":"Active"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_GetTenantByBulk_TA.AmDocsMakeCalloutGetTenantByBulkTA(con1.Id); 
            AmDocs_CalloutClass_GetTenantByBulk_TA.AmDocsMakeCalloutGetTenantByBulkTA(con2.Id); 
        Test.stopTest(); 
    }
    static testMethod void testGetTenantByBulkTA2(){
        Tenant_Account__c con1 = [select id from Tenant_Account__c where Phone_Number__c = '5123835201' limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(301,
                                                 'Complete',
                                                 '{"ImplGetTenantByBulkRestOutput":{"tenantList":[{"serviceId": "123456"}]}}',
//                                                 '{"ImplGetTenantByBulkRestOutput":{"tenantList":[{"serviceId":"1370434","productStatus":"Active","apId":"400307425517"}], "bulkStatus":"Active"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_GetTenantByBulk_TA.AmDocsMakeCalloutGetTenantByBulkTA(con1.Id); 
        Test.stopTest(); 
    }

}