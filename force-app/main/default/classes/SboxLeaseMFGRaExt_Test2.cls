@isTest
private class SboxLeaseMFGRaExt_Test2{
  @testSetup
  static void setupTestData(){
    test.startTest();
    Smartbox__c smartbox_Obj = new Smartbox__c(Chassis_Serial__c = 'Chass476', Serial_Number__c = 'Seria553', Submitted__c = false, Opportunity__c = '0066000000PRyC1', Remove_Equipment__c = false, CaseID__c = '5006000000omuEx', Archive__c = false, Verizon_Do_It_Login__c = false, Verizon_Do_It_Get_M2M_Token__c = false, Verizon_Do_It_Action__c = false, Smartbox_Leased2__c = false);
    smartbox_Obj.CaseID__c = '5006000000omuEx';
    Insert smartbox_Obj; 
    Case case_Obj = new Case(IsEscalated = false, Hidden_Case_History__c = false, Accept_Terms__c = false, Channel_Takedown_Accept_Terms_Conditions__c = false, Authorize_All_Receivers__c = false, Start_ETF_Timer__c = false, RA_Sent_to_Manufacturing__c = false, ETF_Acknowledgement__c = false, Pre_Build_Complete__c = false, Send_Email_Notifcation__c = false);
    Insert case_Obj; 
    test.stopTest();
  }
  static testMethod void test_getSmartbox_UseCase1(){
    List<Smartbox__c> smartbox_Obj  =  [SELECT Chassis_Serial__c,Serial_Number__c,Submitted__c,Opportunity__c,Remove_Equipment__c,CaseID__c,Archive__c,Verizon_Do_It_Login__c,Verizon_Do_It_Get_M2M_Token__c,Verizon_Do_It_Action__c,Smartbox_Leased2__c from Smartbox__c WHERE CaseID__c = '5006000000omuEx'];
    System.assertEquals(true,smartbox_Obj.size()>0);
    List<Case> case_Obj  =  [SELECT IsEscalated,Hidden_Case_History__c,Accept_Terms__c,Channel_Takedown_Accept_Terms_Conditions__c,Authorize_All_Receivers__c,Start_ETF_Timer__c,RA_Sent_to_Manufacturing__c,ETF_Acknowledgement__c,Pre_Build_Complete__c,Send_Email_Notifcation__c from Case];
    System.assertEquals(true,case_Obj.size()>0);
    PageReference pageRef = Page.SmartboxLeaseMFG_RAEntry;
    pageRef.getParameters().put('cid','test');
    Test.setCurrentPage(pageRef);
    SboxLeaseMFGRaExt obj01 = new SboxLeaseMFGRaExt(new ApexPages.StandardController(smartbox_Obj[0]));
//    obj01.StandardSetController = new ApexPages();
      ApexPages.StandardController setCon = new ApexPages.standardController(smartbox_Obj[0]);
    obj01.getSmartbox();
  }
  static testMethod void test_onsave_UseCase1(){
    List<Smartbox__c> smartbox_Obj  =  [SELECT Chassis_Serial__c,Serial_Number__c,Submitted__c,Opportunity__c,Remove_Equipment__c,CaseID__c,Archive__c,Verizon_Do_It_Login__c,Verizon_Do_It_Get_M2M_Token__c,Verizon_Do_It_Action__c,Smartbox_Leased2__c from Smartbox__c WHERE CaseID__c = '5006000000omuEx'];
    System.assertEquals(true,smartbox_Obj.size()>0);
    List<Case> case_Obj  =  [SELECT IsEscalated,Hidden_Case_History__c,Accept_Terms__c,Channel_Takedown_Accept_Terms_Conditions__c,Authorize_All_Receivers__c,Start_ETF_Timer__c,RA_Sent_to_Manufacturing__c,ETF_Acknowledgement__c,Pre_Build_Complete__c,Send_Email_Notifcation__c from Case];
    System.assertEquals(true,case_Obj.size()>0);
    PageReference pageRef = Page.SmartboxLeaseMFG_RAEntry;
    pageRef.getParameters().put('cid','test');
    Test.setCurrentPage(pageRef);
    SboxLeaseMFGRaExt obj01 = new SboxLeaseMFGRaExt(new ApexPages.StandardController(smartbox_Obj[0]));
//    obj01.StandardSetController = new ApexPages();
      ApexPages.StandardController setCon = new ApexPages.standardController(smartbox_Obj[0]);
    obj01.onsave();
  }
}