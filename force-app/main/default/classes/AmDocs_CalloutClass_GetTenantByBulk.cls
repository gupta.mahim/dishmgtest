public class AmDocs_CalloutClass_GetTenantByBulk
{
    Public class BulkResponseWrapper
    {
        public ImplGetTenantByBulkRestOutput ImplGetTenantByBulkRestOutput;
    }
    Public class ImplGetTenantByBulkRestOutput {
        
        public String bulkStatus;
        public list<TenantList> tenantList;
        public String status;
    }    
  Public class TenantList
    {
        public List<ReceiverList> receiverList;
        public String apId;
        public String productStatus;
        public String serviceId;
    }
    
     Public class ReceiverList
    {
        public String receiverId;
        public String smartCardId;
    }
    
    
    // @future(callout=true)
    
    public static  BulkResponseWrapper AmDocsMakeCalloutGetTenantByBulk(String Id) {
      BulkResponseWrapper TenantbulkResponse=null;
        list<Opportunity> O = [select id, Name, AmDocs_ServiceID__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, Mute_Disconnect__c, AmDocs_Site_Id__c from Opportunity where Id = :id Limit 1];
        System.debug('RESULTS of the LIST lookup to the Opp object-test-' +O);
        
        list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
        System.debug('RESULTS of the LIST lookup to the Amdocs Login object' +A);  
        if(O.Size()>0)
        {
            if(O[0].AmDocs_ServiceID__c  != '' && O[0].AmDocs_ServiceID__c  != Null ){
                //      if(O[0].AmDocs_ServiceID__c  != 'Chewbacca' ){  
                Datetime dt1 = System.Now();
                list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateGetTenantByBulk__c from API_LOG__c where ServiceID__c = :O[0].Amdocs_ServiceID__c AND NextAvailableActivateGetTenantByBulk__c >= :dt1 Order By NextAvailableActivateGetTenantByBulk__c  DESC Limit 1];
                
                if( (timer.size() > 0 )) {
                    system.debug('DEBUG API LOG Timer size : ' +timer.size());
                    system.debug('DEBUG API LOG Info' +timer);
                    system.debug('DEBUG timer 1 : ' +dt1);
                    
                    Opportunity sbc = new Opportunity(); { sbc.id=id; sbc.API_Status__c='Error'; sbc.Amdocs_Transaction_Code__c='Error'; sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions';  update sbc;
                    }
                    
                    API_Log__c apil2 = new API_Log__c(); {apil2.Record__c=id;apil2.Object__c='Opportunity';apil2.Status__c='ERROR';apil2.Results__c='You must wait atleast (2) two minutes between transactions';apil2.API__c='GetTenantByBulk';apil2.ServiceID__c=O[0].AmDocs_ServiceID__c; apil2.User__c=UserInfo.getUsername();insert apil2; 
                    }
                    
                }
                
                else if(timer.size() < 1) {
                     system.debug('DEBUG API LOG Timer size : ' +timer.size());
                    
                    
                    HttpRequest request = new HttpRequest();
                    String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/getTenantByBulk?sc=SS&lo=EN&ca=SF'; 
                    request.setEndPoint(endpoint);
                    request.setHeader('User-Agent', 'SFDC-Callout/45.0'); 
                    request.setHeader('Accept', 'application/json'); 
                    request.setHeader('Content-Type', 'application/json');
                    request.setTimeout(101000);
                    request.setMethod('GET'); 
                    for(Integer i = 0; i < A.size(); i++) { 
                        String authorizationHeader = A[i].UXF_Token__c ;
                        request.setHeader('Authorization', authorizationHeader); 
                    } 
                    
                    HttpResponse response = new HTTP().send(request);
                    
                    
                    if (response.getStatusCode() == 200) { 
                        String strjson = response.getbody();
                        String ShortSTRJSON = strjson.abbreviate(32000);
                        System.debug('JSON:::'+strjson );
                        TenantbulkResponse=(BulkResponseWrapper)JSON.deserialize(strjson,BulkResponseWrapper.class);
                        System.debug('Parsed:::'+TenantbulkResponse);                       
                       
                        API_Log__c apil2 = new API_Log__c();{
                            apil2.Record__c=id;
                            apil2.Object__c='Opportunity';
                            apil2.Status__c='SUCCESS';
                            apil2.Results__c=ShortSTRJSON; //Need to limit this to 32k char.
                            apil2.API__c='Get Tenant By Bulk';
                            apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
                            apil2.User__c=UserInfo.getUsername();
                            Insert apil2;
                        }
                        Opportunity sbc = new Opportunity(); { 
                            sbc.id=id; sbc.API_Status__c='SUCCESS'; 
                            sbc.Amdocs_Transaction_Code__c='SUCCESS'; 
//                            sbc.Amdocs_Transaction_Description__c=ShortSTRJSON;
                            update sbc;
                            }
                        
                    }
                    else
                        if (response.getStatusCode() != 200) { String strjson = response.getbody(); String ShortSTRJSON = strjson.abbreviate(32000);                        
                            System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                            
                            API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id; apil2.Object__c='Opportunity';                                apil2.Status__c='ERROR';  apil2.Results__c=ShortSTRJSON; //Need to limit this to 32k char.
                                apil2.API__c='Get Tenant By Bulk'; apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;                                apil2.User__c=UserInfo.getUsername(); Insert apil2;
                            }
                            Opportunity sbc = new Opportunity(); { sbc.id=id; sbc.API_Status__c='Error';  sbc.Amdocs_Transaction_Code__c='Error';  sbc.Amdocs_Transaction_Description__c=strjson; update sbc;
                            }
                        }
                }
                //}
            }}
        
        return TenantbulkResponse;
        
    }
    
}