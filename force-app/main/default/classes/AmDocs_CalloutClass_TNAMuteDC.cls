public class AmDocs_CalloutClass_TNAMuteDC {

public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public String status;
    public cls_informationMessages[] informationMessages;
    public cls_serviceStatusOutput serviceStatusOutput;    
    
}
class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}
class cls_serviceStatusOutput {
    public String status;
    public String errorCode;
    public String errorDescription;
}


    @future(callout=true)

    public static void AmDocsMakeCalloutTNAMuteDC(String Id) {
    
    list<Tenant_Account__c> T = [select id, Name, Mute_Disconnect__c, Reset__c, Action__c, AmDocs_ServiceID__c from Tenant_Account__c where Id = :id ];
        System.debug('RESULTS of the LIST lookup to the Tenant Account object' +T);
        
    if(T[0].AmDocs_ServiceID__c == '' || T[0].AmDocs_ServiceID__c == Null ){
  
      Tenant_Account__c sbc = new Tenant_Account__c(); {
          sbc.id=id;
          sbc.API_Status__c='Error';
          sbc.Amdocs_Transaction_Code__c='Error';
          if( (T[0].AmDocs_ServiceID__c == '' || T[0].  AmDocs_ServiceID__c  == Null )) {
              sbc.Amdocs_Transaction_Description__c = T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          }
      update sbc;
      }
      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Tenant Account';
          apil2.Status__c='ERROR';
          apil2.Results__c=T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          apil2.API__c=T[0].Action__c;
          apil2.ServiceID__c=T[0].AmDocs_ServiceID__c;
          apil2.User__c=UserInfo.getUsername();
          apil2.User_Error__c=true;
      insert apil2;
      }
  }
  
 if(T[0].AmDocs_ServiceID__c != '' && T[0].AmDocs_ServiceID__c != Null ){ 
     Datetime dt1 = System.Now();
     list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :T[0].AmDocs_ServiceID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1]; 
     
     if( (timer.size() > 0 )  || T[0].Reset__c == 'ChewbaccaIsTesting' ) {
           system.debug('DEBUG API LOG Timer size : ' +timer.size());
           system.debug('DEBUG API LOG Info' +timer);
           system.debug('DEBUG timer 1 : ' +dt1);

       Tenant_Account__c sbc = new Tenant_Account__c(); {
           sbc.id=id; sbc.API_Status__c='Error'; 
           sbc.Amdocs_Transaction_Code__c='Error'; 
           sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; 
           update sbc;
       }
       API_Log__c apil2 = new API_Log__c();{ 
           apil2.Record__c=id; 
           apil2.Object__c='Tenant Account'; 
           apil2.Status__c='ERROR'; 
           apil2.Results__c='You must wait atleast (2) two minutes between transactions'; 
           apil2.API__c=T[0].Action__c; 
           apil2.ServiceID__c=T[0].AmDocs_ServiceID__c; 
           apil2.User__c=UserInfo.getUsername(); 
           apil2.User_Error__c=true;
           insert apil2; 
     }
   }   

   else if(timer.size() < 1) {
     
//    list<Tenant_Equipment__c> TE = [select id, Amdocs_Tenant_ID__c, Unit__c, Action__c, Opportunity__c from Tenant_Equipment__c where Tenant_Account__c = :id  AND ( Action__c = 'Active' OR Action__c = 'Muted' OR Action__c = 'Disconnected') AND Location__c = 'D'];
    list<Tenant_Equipment__c> TE = [select id, Amdocs_Tenant_ID__c, Unit__c, Action__c, Opportunity__c from Tenant_Equipment__c where Tenant_Account__c = :id  AND Location__c = 'D'];
        System.debug('RESULTS of the LIST lookup to the Tenant Equipment object' +TE);

//    list<Tenant_Product_Line_Item__c> TOli = [select id, Action__c from Tenant_Product_Line_Item__c where Tenant_Account__c = :id AND ( Action__c = 'Active' OR Action__c = 'Muted' OR Action__c = 'Disconnected') ];
    list<Tenant_Product_Line_Item__c> TOli = [select id, Action__c from Tenant_Product_Line_Item__c where Tenant_Account__c = :id ];
        System.debug('RESULTS of the LIST lookup to the Core OpportunityLineItem records for TOli ' +TOli);

    list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
        System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
    jsonObj.writeFieldName('ImplUpdateProductStatusInput');
        jsonObj.writeStartObject();
            if( T[0].Action__c == 'Pending Disconnect') { jsonObj.writeStringField('type', 'CE');}
            else
            if( T[0].Action__c == 'Pending Disconnect Resume') { jsonObj.writeStringField('type', 'ES'); }
            else
            if( T[0].Action__c == 'Pending Mute') { jsonObj.writeStringField('type', 'SU'); }
            else
            if( T[0].Action__c == 'Pending UnMute') {jsonObj.writeStringField('type', 'RS'); }
            jsonObj.writeStringField('reasonCode', 'CREQ');
            jsonObj.writeEndObject();
    jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
   
    HttpRequest request = new HttpRequest();
        String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+T[0].AmDocs_ServiceID__c+'/updateProductStatus?sc=SS&lo=EN&ca=SF'; 
        request.setEndPoint(endpoint); 
        request.setBody(jsonObj.getAsString());
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('POST');
        String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader);
        
    HttpResponse response = new HTTP().send(request);
        if (response.getStatusCode() == 200) { 
            String strjson = response.getbody();
            String TruncatedStrJson = strjson.abbreviate(1000); 
            JSONParser parser = JSON.createParser(strjson); 
                parser.nextToken(); 
                parser.nextToken(); 
                parser.nextToken(); 
            person obj = (person)parser.readValueAs( person.class);
                System.debug('DEBUG Full Response strjson : ' +strjson);
                System.debug('STATUS_CODE:'+response.getStatusCode());
                System.debug('DEBUG Response Body' +response.getBody());

          if(TE.size() > 0 && obj.orderID != null) { 
              for(Integer i = 0; i < TE.size(); i++){ 
                  Tenant_Equipment__c teu = new Tenant_Equipment__c(); 
                      teu.id=TE[i].id; 
                      if(T[0].Action__c == 'Pending Mute'){ teu.Action__c='Muted';  }                      else if(T[0].Action__c == 'Pending Disconnect'){ teu.Action__c='Disconnected'; }                      else if(T[0].Action__c == 'Pending Disconnect Resume'){ teu.Action__c='Active'; }                      else if(T[0].Action__c == 'Pending UnMute'){teu.Action__c='Active'; }
                  update teu; 
              }
          }

          if(TOli.size() > 0 && obj.orderID != null) { 
              for(Integer i = 0; i < TOli.size(); i++) { 
                  Tenant_Product_Line_Item__c tpu = new Tenant_Product_Line_Item__c();{
                      tpu.id=TOli[i].id; 
                      if(T[0].Action__c == 'Pending Mute'){ tpu.Action__c='Muted'; }                      else if(T[0].Action__c == 'Pending Disconnect'){ tpu.Action__c='Disconnected';}                      else if(T[0].Action__c == 'Pending Disconnect Resume'){ tpu.Action__c='Active';}                      else if(T[0].Action__c == 'Pending UnMute'){tpu.Action__c='Active'; }
                  update tpu;
                  }
              }
          }

          Tenant_Account__c sbc = new Tenant_Account__c(); {
              sbc.id=id; 
              sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
              sbc.API_Status__c=String.valueOf(response.getStatusCode());
              if(obj.orderID != null) { 
                  sbc.AmDocs_Order_ID__c=obj.orderID; 
                  if(T[0].Action__c == 'Pending Mute'){ sbc.Action__c='Muted'; }                  else if(T[0].Action__c == 'Pending Disconnect'){sbc.Action__c='Disconnected'; }                  else if(T[0].Action__c == 'Pending Disconnect Resume'){ sbc.Action__c='Active'; }                  else if(T[0].Action__c == 'Pending UnMute'){sbc.Action__c='Active'; }
                  sbc.AmDocs_Transaction_Description__c=T[0].Action__c + ' SUCCESSFUL'; 
                  sbc.AmDocs_Transaction_Code__c='SUCCESS';
              }
              if(obj.orderID == null && obj.informationMessages != null) {                   sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription;                   sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
              }
              if(obj.orderID == null && obj.informationMessages == null) {                   sbc.AmDocs_Transaction_Description__c=obj.serviceStatusOutput.errorDescription;                    sbc.AmDocs_Transaction_Code__c=obj.serviceStatusOutput.errorCode; 
              }
              
              
          update sbc;
          }
           API_Log__c apil2 = new API_Log__c();{
               apil2.Record__c=id;
               apil2.Object__c='Tenant Account';
               apil2.Status__c='SUCCESS';
               apil2.Results__c=TruncatedStrJson;
               apil2.API__c=T[0].Action__c;
               apil2.ServiceID__c=T[0].Amdocs_ServiceID__c;
               apil2.User__c=UserInfo.getUsername();
           insert apil2;
          }
      }

      else if(response.getStatusCode() != 200) {         String strjson = response.getbody();        String TruncatedStrJson = strjson.abbreviate(1000);
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('DEBUG Response Body' +response.getBody());

          Tenant_Account__c sbc = new Tenant_Account__c(); {               sbc.id=id;               sbc.API_Status__c=String.valueOf(response.getStatusCode());               sbc.AmDocs_Transaction_Description__c=T[0].Action__c + ' ERROR : ' +TruncatedStrJson;           update sbc; 
         }

         API_Log__c apil2 = new API_Log__c();{                apil2.Record__c=id;                apil2.Object__c='Tenant Account';                apil2.Status__c='ERROR';                apil2.Results__c=TruncatedStrJson;                apil2.API__c=T[0].Action__c;                apil2.ServiceID__c=T[0].Amdocs_ServiceID__c;               apil2.User__c=UserInfo.getUsername();            insert apil2;
          }
     }
  }
}
}}