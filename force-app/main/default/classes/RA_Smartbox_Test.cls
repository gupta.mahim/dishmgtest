@isTEST
private class RA_Smartbox_Test{

    private static testmethod void testTriggerForCase(){
        Account a = new Account();
            a.Name = 'Test Account';

        insert a;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test';
            o.AccountId = a.Id;
            o.LeadSource='Test Place';
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
            o.Smartbox_Leased__c = true;

    
        insert o;

        Smartbox__c s = new Smartbox__c();
            s.Chassis_Serial__c='A1234';
            s.Part_Number__c='DN004343';
            s.Serial_Number__c='L23PR6789ABC';
            s.CAID__c='R1234567';
            s.Status__c='Drop Requested';
            s.SmartCard__c='S1234567';
            s.Opportunity__c=o.id;
            s.Smartbox_Leased2__c=True;
            s.RA_Number__c = 'RA123';

            insert s;

      Case c = new Case();
           c.Opportunity__c = o.Id;
           c.AccountId = a.Id;
           c.Status = 'Request Submitted';
           c.Origin = 'TEST';
           c.RequestedAction__c = 'Change';
           c.RecordTypeId = '0126000000017LQ';
           c.Number_of_Accounts__c = 2;
           c.Requested_Actvation_Date_Time__c = system.Today();

       insert c;

        s.CaseID__c=c.id;
        update s;
        
        c.CSG_Account_Number__c = '82551234567890';
        update c;
            
        RA__c r = new RA__c ();
            r.cid__c = c.Id;
            r.RANumber__c = 'RA123';
            r.Shipping_Tracking__c = 'RAABC';
            r.Serial_Number__c = s.Serial_Number__c;
            r.SmartCard__c = s.SmartCard__c;
            r.CAID__c = s.CAID__c;
            r.Opportunity__c = o.id;
            
        insert r;
    }
}