@isTest
private class TestConga_Custom_ProcessProcessTrigger { 
    private static testMethod void Test_Conga_Custom_ProcessProcessTrigger () { 
        try{
            Conga_Custom_Process__c fw = new Conga_Custom_Process__c(); 
            insert fw; 
            System.assertNotEquals(null, fw.id); 
            update fw; 
            delete fw;
        } catch(Exception e){
            FSTR.COTestFactory.FillAllFields=true;
            Conga_Custom_Process__c fw = (Conga_Custom_Process__c)FSTR.COTestFactory.createSObject('Conga_Custom_Process__c',true); 
            insert fw; 
            System.assertNotEquals(null, fw.id); 
            update fw; 
            delete fw;
        }
    } 
}