public with sharing class CaseIAcceptTerms {
    
    String CaseId = '';
   
    public CaseIAcceptTerms(ApexPages.StandardController controller){
        CaseId = ApexPages.currentPage().getParameters().get('id');
    }
    
    
    public Pagereference IAcceptIt() {
        Case ThisCase = [Select ClosedDate, OwnerID, Status from Case where ID = :CaseID];
                if( ThisCase.ClosedDate == Null){
                        ThisCase.status = 'Request Completed';
                        ThisCase.OwnerID = UserInfo.getUserId();
                        update ThisCase;
                        return  new Pagereference('/'+CaseID);}


                        return  new Pagereference('/'+CaseID);
        
    }
    

}