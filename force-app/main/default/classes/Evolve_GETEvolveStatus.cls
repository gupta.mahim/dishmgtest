public class Evolve_GETEvolveStatus {

public class person {
    public Boolean IsOnline;
}

    @future(callout=true)

    public static void makeCallout(String Id) {
    
    list<Evolve__c> E = [select id, MAC_Address__c, Location__c, Smartbox__r.Opportunity__r.AmDocs_ServiceID__c, Smartbox__r.UUID_Opp__c from Evolve__c where Id =:Id AND Location__c = Null];
        System.debug('RESULTS of the LIST lookup to the Evolve object' +E);        



    HttpRequest request = new HttpRequest();
        String endpoint = 'https://wytp2gunxk.execute-api.us-east-1.amazonaws.com/dev/evolve/'+E[0].Smartbox__r.UUID_Opp__c+'/'+E[0].MAC_Address__c+'/status';
        request.setEndPoint(endpoint);
        request.setMethod('GET');
        request.setHeader('Content-Type', 'application/json'); 
        request.setTimeout(101000); 
        request.setHeader('Accept', 'application/json'); 
        request.setHeader('User-Agent', 'SFDC-Callout/45.0'); 
        
        HttpResponse response = new HTTP().send(request); 
            if (response.getStatusCode() == 200) { 
                String strjson = response.getbody();
                    System.debug('DEBUG 1 === RESPONSE json string: '   + strjson);
                 JSONParser parser = JSON.createParser(strjson);
                     parser.nextToken(); 
                     person obj = (person)parser.readValueAs( person.class);
 
           if( obj.IsOnline != Null ) { Evolve__c sbc = new Evolve__c(); sbc.Id=Id;sbc.IsOnline__c = obj.IsOnline; sbc.Transaction_Code__c='SUCCESS'; sbc.Transaction_Description__c=strjson; update sbc; }
           
                API_Log__c apil2 = new API_Log__c();{
              apil2.Record__c=id;       
              apil2.Object__c='Evolve';
              apil2.Status__c='SUCCESS';       
              apil2.Results__c=Strjson;
              apil2.API__c='Get Evolve Status';       
              apil2.ServiceID__c=E[0]. Smartbox__r.Opportunity__r.AmDocs_ServiceID__c ;
              apil2.User__c=UserInfo.getUsername();
          insert apil2; 
          }

              System.debug(response.toString());
              System.debug('DEBUG REQUEST: ' + request); 
              System.debug('Full JSON response payload: ' +response.getBody());
              System.debug('Full Status response payload: ' +response.getStatus());
              System.debug('Full Status Code response payload: ' +response.getStatusCode());
              System.debug('DEBUG RESPONSE GET HEADER KEYS: ' +response.getHeaderKeys());
              System.debug('STATUS:'+response.getStatus());
              System.debug('STATUS_CODE:'+response.getStatusCode());
              System.debug(response.getBody());
    }
    
     else if (response.getStatusCode() != 200) { 
                String strjson = response.getbody();
 
               Evolve__c sbc = new Evolve__c(); {
                   sbc.Id=Id;
                   sbc.Transaction_Code__c='ERROR';
                   sbc.Transaction_Description__c=strjson;
               update sbc; 
            }           

          API_Log__c apil2 = new API_Log__c();{
              apil2.Record__c=id;       
              apil2.Object__c='Evolve';
              apil2.Status__c='ERROR';
              apil2.Results__c='ERROR :' +Strjson;
              apil2.API__c='Get Evolve Status';       
              apil2.ServiceID__c=E[0]. Smartbox__r.Opportunity__r.AmDocs_ServiceID__c;
              apil2.User__c=UserInfo.getUsername();
          insert apil2; 
          }
        }
    }
}