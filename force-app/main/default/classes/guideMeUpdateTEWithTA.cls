/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 2 July 2020
Created for: Milestone# MS-001664
Description: To update TE's with TA
-------------------------------------------------------------*/

public class guideMeUpdateTEWithTA {
    
    public class FlowInput{
        @Invocablevariable
        public list<string> iVTEIds;
        
        @Invocablevariable
        public string iVTAId;
    }
    
    @InvocableMethod (label='UpdateTEWithTA')
    public static void UpdateTEWithTA(List<FlowInput> inputValue){   
        list<string> lstTEIds = inputValue[0].iVTEIds;
        string TAId = inputValue[0].iVTAId;
        list<Tenant_Equipment__c> lstTEUpdate = new list<Tenant_Equipment__c>();
        
        list<Tenant_Account__c> TARec = [SELECT Id,First_Name__c,Last_Name__c,Phone_Number__c,Email_Address__c,Unit__c,Address__c,
                                         City__c,State__c,Zip__c,Valid_Address__c FROM Tenant_Account__c
                                         WHERE Id = : TAId LIMIT 1];
        
        list<Tenant_Equipment__c> lstTERecs = [SELECT Id,Customer_First_Name__c,Customer_Last_Name__c,Email_Address__c,Phone_Number__c,
                                               Unit__c,Address__c,City__c,State__c,Zip_Code__c,Valid_Address__c,Tenant_Account__c
                                               FROM Tenant_Equipment__c WHERE Id IN : lstTEIds];
        
        if(TARec.size()>0 && lstTERecs.size()>0){
            for(Tenant_Equipment__c TE: lstTERecs){
                if(TE.Valid_Address__c == false){
                    TE.Tenant_Account__c = TARec[0].Id;
                    TE.Customer_First_Name__c = TARec[0].First_Name__c;
                    TE.Customer_Last_Name__c = TARec[0].Last_Name__c;
                    TE.Email_Address__c = TARec[0].Email_Address__c;
                    TE.Phone_Number__c = TARec[0].Phone_Number__c;
                    TE.Unit__c = TARec[0].Unit__c;
                    TE.Address__c = TARec[0].Address__c;
                    TE.City__c = TARec[0].City__c;
                    TE.State__c = TARec[0].State__c;
                    TE.Zip_Code__c = TARec[0].Zip__c;
                    TE.Valid_Address__c = true;
                }
                else{
                    TE.Tenant_Account__c = TARec[0].Id;
                    TE.Customer_First_Name__c = TARec[0].First_Name__c;
                    TE.Customer_Last_Name__c = TARec[0].Last_Name__c;
                    TE.Email_Address__c = TARec[0].Email_Address__c;
                    TE.Phone_Number__c = TARec[0].Phone_Number__c;
                }
                lstTEUpdate.add(TE);
            }
        }
        if(lstTEUpdate.size()>0)
            update lstTEUpdate;
    }
}