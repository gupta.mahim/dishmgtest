@isTEST

public class SmartBoxDeleteDupesTests{
    static testMethod void testSmartBoxDeleteDupes() {
        
// Seed the database with some records, and make sure they can
// be bulk inserted successfully.
    Account a = new Account();
        a.Name = 'Test Account';
        a.phone = '(303) 555-5555';
            insert a;

    Opportunity o = new Opportunity();
        o.Name = 'Test';
        o.AccountId = a.Id;
        o.LeadSource='Test Method';
        o.misc_code_siu__c='T999';
        o.CloseDate=system.Today();
        o.StageName='Closed Won';
            insert o;
            
    Smartbox__c s01 = new Smartbox__c();
        s01.Chassis_Serial__c='LALPFY00096K';
        s01.Serial_Number__c='LALPRZ04961A';
        s01.Opportunity__c=o.id;

    Smartbox__c s02 = new Smartbox__c();
        s02.Chassis_Serial__c='LALPFY00096K';
        s02.Serial_Number__c='LALPRZ04962A';
        s02.Opportunity__c=o.id;
        
    Smartbox__c s03 = new Smartbox__c();
        s03.Chassis_Serial__c='LALPFY00096K';
        s03.Serial_Number__c='LALPRZ04963A';
        s03.Opportunity__c=o.id;
        
    Smartbox__c[] SBoxes = new Smartbox__c[] {s01, s02, s03};
        insert SBoxes;                     

// Now make sure that some of these leads can be changed and
// then bulk updated successfully. Note that lead1 is not
// being changed, but is still being passed to the update
// call. This should be OK.
      s02.CAID__c='BLAH';
      s03.CAID__c='DUPE';
      update SBoxes;


                    
}
}