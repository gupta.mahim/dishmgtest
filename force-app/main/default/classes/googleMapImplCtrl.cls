public with sharing class googleMapImplCtrl {
    @AuraEnabled
    public static list<LocationWrapper> getLocation(String currentID){
        list<LocationWrapper> lstALW = new list<LocationWrapper>();
        Opportunity opp = [SELECT Name,description, Address__c, City__c, Zip__c, State__c FROM Opportunity 
                           Where Id = '0062D000004uycIQAQ' And Valid_Address__c = true]; //0062D000004uycIQAQ
        
        //create "locationDetailWrapper" instance and set appropriate values
        locationDetailWrapper oLocationWrap = new locationDetailWrapper();
        oLocationWrap.Street = opp.Address__c;
        oLocationWrap.PostalCode = opp.Zip__c;
        oLocationWrap.City = opp.City__c;
        oLocationWrap.State = opp.State__c;
        oLocationWrap.Country = 'USA';
        
        // create "LocationWrapper" instance, set values and add to final list. 
        LocationWrapper oWrapper = new LocationWrapper();
        oWrapper.icon = 'utility:location'; // https://www.lightningdesignsystem.com/icons/#utility
        oWrapper.title = opp.Name;
        oWrapper.description = opp.description;
        oWrapper.location = oLocationWrap;
        
        lstALW.add(oWrapper);
        
        // retrun the "LocationWrapper" list
        return lstALW;
    }
    /* wrapper class to store required properties for "lightning:map" component' */ 
    public class LocationWrapper{
        @AuraEnabled public string icon{get;set;} 
        @AuraEnabled public string title{get;set;} 
        @AuraEnabled public string description{get;set;} 
        @AuraEnabled public locationDetailWrapper location{get;set;} 
    }
    /* sub wrapper class to store location details for "LocationWrapper" location property.*/ 
    public class locationDetailWrapper{
        @AuraEnabled public string Street{get;set;}
        @AuraEnabled public string PostalCode{get;set;}
        @AuraEnabled public string City{get;set;}
        @AuraEnabled public string State{get;set;}
        @AuraEnabled public string Country{get;set;}
    }
}