public class SIMP_RecordController {
    private static final Integer MONTHS_IN_A_YEAR = 12;
    private static final Integer MONTHLY_CAPITAL_CONTRIBUTION_MONTHS_BEFORE_START = 4;
    private static final Integer LUMPSUM_CAPITAL_CONTRIBUTION_MONTHS_BEFORE_START = 2;
    private static final Integer MONTHLY_DOOR_FEES_MONTHS_BEFORE_START = 4;
    private static final Integer LUMPSUM_DOOR_FEES_MONTHS_BEFORE_START = 2;

    
    @AuraEnabled
    public static List<sObject> getExistingRelatedRecordList(ID parentRecId, String parentApiName, String objApiName, List<String> recordfieldList){
        String query = 'SELECT Id, ' + String.join(recordfieldList, ', ') + ' FROM ' + objApiName + ' WHERE ' + parentApiName + '=:parentRecId';
        List<sObject> recordList = new List<sObject>();
        recordList = Database.query(query); 
        return recordList;
    }
    
    @AuraEnabled
    public static sObject searchRecord(string recordId, String objectName){
        System.debug('recordId: ' + recordId);
        Map<String,Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        List<String> fieldList = new List<String>();
        for(Schema.SObjectField field : objectFields.values()){
            fieldList.add(String.valueOf(field));
        }
        List<sObject> recordList = new List<sObject>();
        System.debug('objectFields.values(): ' + objectFields.values());
        System.debug('fieldList: ' + fieldList);
        String query = 'SELECT ' + String.join(fieldList, ', ') + ' FROM ' + objectName + ' WHERE Id =: recordId';
        System.debug('query: ' + query);
        System.debug('recordId: ' + recordId);
        if(recordId != null){
            recordList.addAll(Database.query(query));
            sObject record = recordList[0];
            return record;
        }
        else{
            sObject record = Schema.getGlobalDescribe().get(objectName).newSObject();
            for(String field : fieldList){
                if(objectFields.get(field).getDescribe().isUpdateable()){
                    record.put(field, null);
                }
            }
            System.debug('record: ' + record);
            return record;
        }
        
    }
    
    @AuraEnabled
    public static sObject getQuoteWithLine(String quoteId){
        
        Map<String,Schema.SObjectField> quoteFields = Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap();
        Map<String,Schema.SObjectField> quoteLineFields = Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap();
        List<String> quotefieldList = new List<String>();
        List<String> quoteLinefieldList = new List<String>();
        for(Schema.SObjectField field : quoteFields.values()){
            quotefieldList.add(String.valueOf(field));
        }
        
        for(Schema.SObjectField field : quoteLineFields.values()){
            quoteLinefieldList.add(String.valueOf(field));
        }
        List<sObject> recordList = new List<sObject>();
        String query = 'SELECT ' + String.join(quotefieldList, ', ') + ', (SELECT ' + String.join(quoteLinefieldList, ', ')  + ' FROM SBQQ__LineItems__r WHERE SBQQ__OptionLevel__c = null) FROM SBQQ__Quote__c WHERE Id =: quoteId';
        return Database.query(query);
    }
    
    @AuraEnabled
    public static Boolean updateObjectRec(sObject record, sObject childRecord){
        
        try{
            update record;
            System.debug('record: ' + record);
            System.debug('childRecord: ' + childRecord);
            if(childRecord != null){
                update childRecord;
            }
            return true;
        }catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            return false;
        }
    }
    
    @AuraEnabled
    public static sObject handleBundleQuoteLine(SBQQ__Quote__c quoteRecord, sObject quoteLineRec){
        SBQQ__Quote__c quote = (SBQQ__Quote__c)getQuoteWithLine(quoteRecord.Id); 
        
        quote.Contract_Term__c = quoteRecord.Contract_Term__c; // Contract Terms
        quote.Monthly_Door_Fees__c = quoteRecord.Monthly_Door_Fees__c; //$ Monthly Door Money
        quote.Door_Fees_of_Months__c = quoteRecord.Door_Fees_of_Months__c; //# Months Door Money
        quote.Upfront_Door_Fees__c = quoteRecord.Upfront_Door_Fees__c;//$ Lump Sum Door Money
        quote.Annual_ARPU_Increase__c = quoteRecord.Annual_ARPU_Increase__c;//Annual % Increase
        quote.Month_1__c = quoteRecord.Month_1__c;//Ramp Month 1
        quote.Month_2__c = quoteRecord.Month_2__c;//Ramp Month 2
        quote.Month_3__c = quoteRecord.Month_3__c;//Ramp Month 3
        quote.Month_4__c = quoteRecord.Month_4__c;//Ramp Month 4
        quote.Month_5__c = quoteRecord.Month_5__c;//Ramp Month 5
        quote.Month_6__c = quoteRecord.Month_6__c;//Ramp Month 6
        quote.Month_7__c = quoteRecord.Month_7__c;//Ramp Month 7
        quote.Month_8__c = quoteRecord.Month_8__c;//Ramp Month 8
        quote.Month_9__c = quoteRecord.Month_9__c;//Ramp Month 9
        quote.Month_10__c = quoteRecord.Month_10__c;//Ramp Month 10
        quote.Month_11__c = quoteRecord.Month_11__c;//Ramp Month 11
        quote.Month_12__c = quoteRecord.Month_12__c;//Ramp Month 12
        quote.SBQQ__LineItems__r[0].ARPU__c = (Decimal)quoteLineRec.get('ARPU__c'); //ARPU
        SBQQ__QuoteLine__c quoteLineItem = quote.SBQQ__LineItems__r[0]; // Bundle Quote Line
        Integer contractTerms = (Integer)quote.Contract_Term__c; // Contract Terms
        Integer numberOfUnits = (Integer)quote.No_of_Units__c ; // Number of Units
        Integer contractStartMonth = 0; // Contract Start Month - Can be modified to be month dependent
        Date contractStartDate = Date.newInstance(Date.today().year(), 1, 1); // Contract Start Date - Can be Modified to be month dependent
        System.debug('contractTerms: ' + contractTerms);
        System.debug('$ Monthly Door Money: ' + quote.Monthly_Door_Fees__c);
        System.debug('# Months Door Money: ' + quote.Door_Fees_of_Months__c);
        System.debug('$ Lump Sum Door Money: ' + quote.Upfront_Door_Fees__c);
        System.debug('$ Annual % Increase: ' + quote.Annual_ARPU_Increase__c);
        System.debug('numberOfUnits: ' + numberOfUnits);
        System.debug('quoteLineItem.ARPU__c: ' + quoteLineItem.ARPU__c);
        if(contractTerms == 0 || numberOfUnits == 0){
            System.debug('Contract Term or Number of Units is 0');
            return null;
        }
        
        Decimal totalAnnualRecurringProducts = 0; // Get year 1 - 10
        Map<Integer, Decimal> annualRecurringProductMap = getAnnualRecurringProductMap(10, (sObject)quote); // Maximum Years 10
        for(Decimal yearVal : annualRecurringProductMap.values()){
            totalAnnualRecurringProducts += yearVal;
        }
        Decimal totalMonthlyRecurringProducts = quoteRecord.Monthly_Cash_Flow__c; // GET Monthly Recurring Products value field and multiply by Contract Term
        Decimal initialOutlay = quoteLineItem.SBQQ__PackageTotal__c - totalAnnualRecurringProducts -  totalMonthlyRecurringProducts + 
            quote.Correct_Roof_Requirements__c + quote.Correct_MDF_Requirements__c + quote.Correct_IDF_Requirements__c + 
            quote.Correct_In_Unit_Requirements__c + quote.Miscellaneous_Adjustments__c; // Initial Outlay
        
        //Get Monthly CashInflow
        Decimal arpu =  quoteLineItem.ARPU__c; // ARPU
        Decimal annualArpuIncrease = quote.Annual_ARPU_Increase__c; // Annual ARPU Increase
        Map<Integer,Integer> rampUnitsPerMonth = getRampUnitsPerMonth(MONTHS_IN_A_YEAR, (sObject)quote);// Create Monthly Map Value for Ramp Units Month 1 - 12 
        Integer currentNumberOfUnits = 0; // Placeholder for current number of units for Ramp
        Integer currentMonth = 1; // Placeholder for current month
        Integer yearMonthLimit = 0;
        //CAPITAL CONTRIBUTION
        Decimal capitalContributionLumpSumVal = quote.Lump_Sum_Upfront_Payments__c; // Lump Sum Capital Contribution
        Decimal capitalContributionMonthlyVal = quote.Monthly_Upfront_Payments__c; // Monthly Capital Contribution
        Integer capitalContributionStartMonth = capitalContributionMonthlyVal != 0 ? MONTHLY_CAPITAL_CONTRIBUTION_MONTHS_BEFORE_START : 0; //Monthly Capital Contribution Start Month
        Integer capitalContributionEndMonth = capitalContributionStartMonth != 0 ? capitalContributionStartMonth + (Integer)quote.No_of_Months_Upfront_Payments__c : 0; // Monthly Capital Contribution End Month
        Integer capitalContributionLumpSumStartMonth = capitalContributionLumpSumVal != 0 ? LUMPSUM_CAPITAL_CONTRIBUTION_MONTHS_BEFORE_START : 0; // Lump Sum Capital Contribution Start Month
        //DOOR MONEY
        Decimal doorMoneyLumpSumVal = quote.Upfront_Door_Fees__c; // Lump Sum Door Fees
        Decimal doorMoneyMonthlyVal = quote.Monthly_Door_Fees__c; // Monthly Door Fees
        Integer doorMoneyStartMonth = doorMoneyMonthlyVal != 0 ? MONTHLY_DOOR_FEES_MONTHS_BEFORE_START : 0; //Monthly Door Fees Start Month
        Integer doorMoneyEndMonth = doorMoneyStartMonth != 0 ? doorMoneyStartMonth + (Integer)quote.Door_Fees_of_Months__c : 0; // Monthly Door Fees End Month
        Integer doorMoneyLumpSumStartMonth = doorMoneyLumpSumVal != 0 ? LUMPSUM_DOOR_FEES_MONTHS_BEFORE_START : 0; // Lump Sum Door Fees Start Month
        
        Map<Integer, Integer> monthsPerYear = getMonthsInYear(contractTerms, contractStartMonth); // get Months per year
        Map<Integer, Decimal> monthlyNetCashflowMap = new Map<Integer, Decimal>(); 
        
        for(Integer year : monthsPerYear.keySet()){
            System.debug('year: ' + year);
            yearMonthLimit += monthsPerYear.get(year);
            for(Integer month = currentMonth ; month <= yearMonthLimit; month++){
                System.debug('month: ' + month);
                Decimal baseCashInflow = 0;
                //For Ramp Unit
                if(year == 1 && quote.Ramp_Pricing__c){
                    currentNumberOfUnits = rampUnitsPerMonth.get(month); // Check if there is a new value for Ramp Pricing. If 0 then use currentNumber
                    System.debug('currentNumberOfUnits: ' + currentNumberOfUnits);
                    baseCashInflow = arpu * currentNumberOfUnits; // Todo : Get monthly value for ramp units
                }
                else{
                    baseCashInflow = arpu * numberOfUnits; // 
                }
                System.debug('baseCashInflow: ' + baseCashInflow);
                Decimal monthlyCashInflow = baseCashInflow;//Add other factors monthly
                if(month == (doorMoneyLumpSumStartMonth + 1)){
                    monthlyCashInflow -= doorMoneyLumpSumVal; //If Door fees Month = start of lump sum payment add to month cash inflow
                }
                if(month > doorMoneyStartMonth && month <= doorMoneyEndMonth){
                    monthlyCashInflow -= doorMoneyMonthlyVal; // If Door Fees monthly start month period add to month cash inflow
                }
                if(month == (capitalContributionLumpSumStartMonth + 1)){
                    monthlyCashInflow += capitalContributionLumpSumVal; //If Capital Contributions Month = start of lump sum payment add to month cash inflow
                }
                if(month > capitalContributionStartMonth && month <= capitalContributionEndMonth){
                    monthlyCashInflow += capitalContributionMonthlyVal; // If capital contribution monthly start month period add to month cash inflow
                }
                //Get Annual Value if the month is the first month of the year
                if(math.mod(month, 12) == 1){
                    monthlyCashInflow -= annualRecurringProductMap.get((month/12)+1);
                }
                // Add Monthly recurring product Val
                if(quote.Monthly_Cash_Flow__c != null){
                    monthlyCashInflow -= quote.Monthly_Cash_Flow__c;
                }
                System.debug('monthlyCashInflow: ' + monthlyCashInflow);
                monthlyNetCashflowMap.put(month, monthlyCashInflow);
                currentMonth += 1;
            }
            arpu = arpu * (1+(annualArpuIncrease/100)); //Updates ARPU Value based on Annual Increase
        }
        //set (1 + 0.8333) fixed discount price
        Decimal yearlyDiscount = 0.10;
        Decimal monthlyDiscount = yearlyDiscount/12;
        Decimal discountRate = 1 + monthlyDiscount;
        System.debug('discountRate: ' + discountRate);
        Decimal sumOfPVs = getSumOfPVs(discountRate, monthlyNetCashflowMap);
        Decimal NPV;
        
        NPV = sumOfPVs - initialOutlay;
        System.debug('NPV: ' + NPV);
        quoteLineItem.NPV2__c = NPV;
        
        List<Decimal> cashflowList = new List<Decimal>();
        cashflowList.add(initialOutlay * -1);
        cashflowList.addAll(monthlyNetCashflowMap.values());
        quoteLineItem.IRR2__c  = calculateIRR(cashflowList) * 12;
        System.debug('IRR: ' + quoteLineItem.IRR2__c);
        
        quoteLineItem.Payback_Period__c = calculatePaybackPeriod(initialOutlay, monthlyNetCashflowMap);
        //tin added 09/05/2019 
        quoteLineItem.PaybackPercent__c = ((quoteLineItem.Payback_Period__c - (quoteLineItem.Contract_Term__c / 2)) / quoteLineItem.Contract_Term__c / 2) * 100;
        System.debug('(' + quoteLineItem.Payback_Period__c + '-  (' + quoteLineItem.Contract_Term__c + '/2))/'+ quoteLineItem.Contract_Term__c+ '/ + 2 ) * 100');
        System.debug('Payback_Period__c: ' + quoteLineItem.Payback_Period__c);
        
        List<String> retStr = new List<String>();
        //system.debug('RETURN' + QuoteLineTriggerHandler.checkApprover(quoteLineItem.IRR2__c, quoteLineItem.PaybackPercent__c, quoteLineItem.Payback_Period__c));
        retStr = QuoteLineTriggerHandler.checkApprover(quoteLineItem.IRR2__c, quoteLineItem.PaybackPercent__c, quoteLineItem.Payback_Period__c,contractTerms);
        
        if(retStr.size() > 0){
          quoteLineItem.ApproverLevel__c = retStr[0];
          quoteLineItem.ApprovalText__c = retStr[1];
        }
        else{
          quoteLineItem.ApproverLevel__c = '';
          quoteLineItem.ApprovalText__c = '';
        }
        
        return quote;
    }
    
    public static Map<Integer,Decimal> getAnnualRecurringProductMap(Integer years, Sobject record){
        Map<Integer, Decimal> annualRecurringProductMap = new Map<Integer, Decimal>();
        for(Integer year = 1 ; year <= years ; year++){
            String fieldAPIName = 'Year_' + year + '__c';
            Decimal amountPerYear = (Decimal)record.get(fieldAPIName);
            annualRecurringProductMap.put(year,amountPerYear);
        }
        return annualRecurringProductMap;
    }

    public static Map<Integer,Integer> getRampUnitsPerMonth(Integer months, Sobject record){
        Map<Integer, Integer> rampUnitsPerMonth = new Map<Integer, Integer>();
        for(Integer month = 1 ; month <= months ; month++){
            String fieldAPIName = 'Month_' + month + '__c';
            Integer unitPerMonth = Integer.valueOf(record.get(fieldAPIName));
            rampUnitsPerMonth.put(month,unitPerMonth);
        }
        return rampUnitsPerMonth;
    }
    
    public static Decimal getSumOfPVs(Decimal discountRate, Map<Integer,Decimal> cashFlowMap){
        Decimal sumOfPVs = 0; 
        for (Integer month : cashFlowMap.keySet()) {
            Decimal monthlyCF = cashFlowMap.get(month);
            System.debug('monthlyCF: ' + monthlyCF);
            Decimal divisor = discountRate.pow(month);
            System.debug('divisor: ' + divisor);
            Decimal PV = monthlyCF / divisor;
            System.debug('Month: ' + month + ' PV: ' + PV);
            sumOfPVs+=PV;  
        }
        System.debug('sumOfPVs: ' + sumOfPVs);
        return sumOfPVs;
    }
    /*
    @AuraEnabled
    public static sObject handleBundleQuoteLine2(SBQQ__Quote__c quoteRecord, sObject quoteLineRec){
        
        SBQQ__Quote__c quote = (SBQQ__Quote__c)getQuoteWithLine(quoteRecord.Id);
        
        quote.Contract_Term__c = quoteRecord.Contract_Term__c; // Contract Term
        quote.Monthly_Door_Fees__c = quoteRecord.Monthly_Door_Fees__c; //$ Monthly Door Money
        quote.Door_Fees_of_Months__c = quoteRecord.Door_Fees_of_Months__c; //# Months Door Money
        quote.Upfront_Door_Fees__c = quoteRecord.Upfront_Door_Fees__c;//$ Lump Sum Door Money
        quote.Annual_ARPU_Increase__c = quoteRecord.Annual_ARPU_Increase__c;//Annual % Increase
        System.debug('quote: ' + quote);
        System.debug('quote Term: ' + quote.Contract_Term__c);
        System.debug('quoteRecord: ' + quoteRecord);
        System.debug('updated quoteLineItem: ' + quoteLineRec);
        quote.SBQQ__LineItems__r[0].ARPU__c = (Decimal)quoteLineRec.get('ARPU__c'); //ARPU
        System.debug('quoteLineItem: ' + quote.SBQQ__LineItems__r);
        SBQQ__QuoteLine__c quoteLineItem = quote.SBQQ__LineItems__r[0]; // Bundle Quote Line
        System.debug('ARPU: ' + quoteLineItem.ARPU__c);
        System.debug('quoteLineItem: ' + quoteLineItem);
        Integer contractTerms = (Integer)quote.Contract_Term__c; // Contract Terms
        Integer numberOfUnits = (Integer)quote.No_of_Units__c ; // Number of Units
        System.debug('contractTerms: ' + contractTerms);
        System.debug('numberOfUnits: ' + numberOfUnits);
        System.debug('quoteLineItem.ARPU__c: ' + quoteLineItem.ARPU__c);
        if(contractTerms == 0 || numberOfUnits == 0){
            System.debug('Contract Term or Number of Units is 0');
            return null;
        }
        //Initial Outlay - Package Cost + Upfront Door Fees + ... + .. + ...
        //Update 09/04/2019 - TODO: Subtract total Recurring Cost Amount
        //Updated Year 1 is removed and added to Initial Outlay
        Decimal totalRecurringCostAmount = quote.Year_2__c + quote.Year_3__c + quote.Year_4__c + quote.Year_5__c + quote.Year_6__c + quote.Year_7__c + quote.Year_8__c + quote.Year_9__c + quote.Year_10__c;
        System.debug('totalRecurringCostAmount: ' + totalRecurringCostAmount);
        Decimal initialOutlay = quoteLineItem.SBQQ__PackageTotal__c - totalRecurringCostAmount + quote.Year_1__c + 
            quote.Correct_Roof_Requirements__c + quote.Correct_MDF_Requirements__c + quote.Correct_IDF_Requirements__c + 
            quote.Correct_In_Unit_Requirements__c + quote.Miscellaneous_Adjustments__c /* - quote.Lump_Sum_Upfront_Payments__c;
        Integer contractStartMonth = 0;
        System.debug('quoteLineItem.SBQQ__PackageTotal__c: ' + quoteLineItem.SBQQ__PackageTotal__c);
        System.debug('quote.Contract_Start_Date__c: ' + quote.Contract_Start_Date__c );
        Date contractStartDate = Date.newInstance(Date.today().year(), 1, 1);
        //Door Fees Values
        Decimal upfrontDoorFees = quote.Upfront_Door_Fees__c;
        Integer upfrontDoorFeeStartYear = upfrontDoorFees != 0 ? (contractStartDate.addDays(60).year() - contractStartDate.year()) + 1 : 0;
        Integer doorFeesMonths = (Integer)quote.Door_Fees_of_Months__c;
        Integer doorFeesStartMonth = contractStartDate.addDays(120).month() - 1;
        Decimal doorFeesValue = quote.Monthly_Door_Fees__c;
        //Ramp Pricing Values
        Integer rampPriceMonths = 12;
        Integer rampPriceStartMonth = 0;
        //Upfront Payment Values
        Integer upfrontMonths = (Integer)quote.No_of_Months_Upfront_Payments__c;
        Decimal upfrontValue = quote.Monthly_Upfront_Payments__c;
        Integer upfrontCapitalStartMonth = contractStartDate.addDays(120).month() - 1;
        Decimal upfrontCapitalFees = quote.Lump_Sum_Upfront_Payments__c;
        Integer upfrontCapitalStartYear = upfrontCapitalFees != 0 ? (contractStartDate.addDays(60).year() - contractStartDate.year()) + 1 : 0;
        //Getting the months per year where the monthly payments are effective
        Map<Integer, Integer> monthsPerYear = getMonthsInYear(contractTerms, contractStartMonth);
        Map<Integer, Integer> discountMonthsPerYear = getMonthsInYear(doorFeesMonths, doorFeesStartMonth);
        Map<Integer, Integer> upfrontMonthsPerYear = getMonthsInYear(upfrontMonths, upfrontCapitalStartMonth);
        Map<Integer, Integer> rampPricingMonthsPerYear = getMonthsInYear(rampPriceMonths, contractStartMonth);
        //System.debug('monthsPerYear: ' + monthsPerYear);
        // Base Cash Inflow
        Decimal baseCashInflow = quoteLineItem.ARPU__c * numberOfUnits * 12;
        Decimal annualArpuIncrease = quote.Annual_ARPU_Increase__c;
        Integer currentNumberOfUnits = 0;
        Decimal arpu =  quoteLineItem.ARPU__c;
        Decimal rampPricingVal = 0;
        System.debug('annualArpuIncrease: ' + annualArpuIncrease);
        Map<Integer, Decimal> cashOutFlowMap = New Map<Integer, Decimal>();
        Map<Integer, Decimal> yearNetCashflowMap = new Map<Integer, Decimal>();
        
        System.debug('baseCashInflow: ' + baseCashInflow);
        
        for(Integer year : monthsPerYear.keySet()){
            Decimal month = monthsPerYear.get(year);
            Decimal monthValue = month.divide(12, 3);
            Decimal upfrontVal = (upfrontMonthsPerYear.get(year) == null ? 0 : upfrontMonthsPerYear.get(year)*upfrontValue);
            if(upfrontCapitalStartYear == year){
                upfrontVal += upfrontCapitalFees;
            }
            Decimal discountVal = (discountMonthsPerYear.get(year) == null ? 0 : discountMonthsPerYear.get(year)*doorFeesValue);
            if(upfrontDoorFeeStartYear == year ){
                discountVal += upfrontDoorFees;
            }
            Integer rampPricingMonths = rampPricingMonthsPerYear.get(year) != null ? rampPricingMonthsPerYear.get(year) + rampPriceStartMonth : 0;
            System.debug('rampPricingMonths: ' + rampPricingMonths);
            
            sObject quoteRec = (sObject)quote;
            if(rampPriceStartMonth != 12){
                for(Integer i = rampPriceStartMonth ; i < rampPricingMonths ; i++){
                    String fieldAPIName = 'Month_' + (i+1) + '__c';
                    Decimal unitPerMonth = (Decimal)quoteRec.get(fieldAPIName) != 0 ? (Decimal)quoteRec.get(fieldAPIName) + currentNumberOfUnits : 0 + currentNumberOfUnits;
                    System.debug('unitPerMonth: ' + unitPerMonth);
                    rampPricingVal += (arpu * unitPerMonth);
                    rampPriceStartMonth += 1;
                    currentNumberOfUnits = (Integer)unitPerMonth;
                    System.debug('rampPricingVal: ' + rampPricingVal);
                    System.debug('currentNumberOfUnits: ' + currentNumberOfUnits);
                    //NOTE: SHOULD the RAMPPRICING MONTH PROCEED THE FIRST YEAR, WOULD THE NEW ARPU VALUE (+Annual Increase Value)
                    // AFFECT RAMP PRICING for that year.
                } 
            }
            
            System.debug('rampPriceStartMonth: ' + rampPriceStartMonth);
            Decimal baseCashInflowVal = 0.0;
            if(year != 1){
                System.debug('Year: ' + year);
                arpu = arpu + arpu*(annualArpuIncrease/100);
                System.debug('arpu: ' + arpu);
                baseCashInflowVal = (arpu * numberOfUnits * 12) * monthValue;
                System.debug('baseCashInflowVal: ' + baseCashInflowVal);
            }else{
                baseCashInflowVal = baseCashInflow*monthValue;
            }
            rampPricingVal = year == 1 && quote.Ramp_Pricing__c ?  rampPricingVal : baseCashInflowVal;
            System.debug('rampPricingVal: ' + rampPricingVal);
            System.debug('Ramp_Pricing__c: ' + quote.Ramp_Pricing__c);
            System.debug('Value to Deduct to Base Cash Inflow: ' + (baseCashInflowVal - rampPricingVal));
            String recurringYearAPIName = 'Year_' + year + '__c';
            Decimal recurringCashInflow = year == 1 ? 0 : (Decimal)quoteRec.get(recurringYearAPIName);
            System.debug('recurringCashInflow ' + year + ': ' + recurringCashInflow);
            Decimal netCashInflow = baseCashInflowVal + upfrontVal - discountVal - (baseCashInflowVal - rampPricingVal) - recurringCashInflow;//discountYear.get(year);
            yearNetCashflowMap.put(year, netCashInflow);
        }
        System.debug('yearNetCashflowMap: ' + yearNetCashflowMap);
        
        Decimal sumOfPVs = 0; 
        Decimal NPV;
        //set (1 + 0.10) fixed discount price
        Decimal discountRate = 1.10;    
        for (Integer year : yearNetCashflowMap.keySet()) {
            Decimal yearlyCF = yearNetCashflowMap.get(year);
            System.debug('yearlyCF: ' + yearlyCF);
            Decimal divisor = discountRate.pow(year);
            System.debug('divisor: ' + divisor);
            Decimal PV = yearlyCF / divisor;
            System.debug('Year: ' + year + ' PV: ' + PV);
            sumOfPVs+=PV;  
        }
        System.debug('sumOfPVs: ' + sumOfPVs);
        System.debug('initialOutlay: ' + initialOutlay);
        
        NPV = sumOfPVs - initialOutlay;
        System.debug('NPV: ' + NPV);
        quoteLineItem.NPV2__c = NPV;
        
        List<Decimal> cashflowList = new List<Decimal>();
        cashflowList.add(initialOutlay * -1);
        cashflowList.addAll(yearNetCashflowMap.values());
        quoteLineItem.IRR2__c  = calculateIRR(cashflowList);
        
        quoteLineItem.Payback_Period__c = calculatePaybackPeriod(initialOutlay, yearNetCashflowMap) * 12;
        //tin added 09/05/2019 
        quoteLineItem.PaybackPercent__c = ((quoteLineItem.Payback_Period__c - (quoteLineItem.Contract_Term__c / 2)) / quoteLineItem.Contract_Term__c / 2) * 100;
        System.debug('Payback_Period__c: ' + quoteLineItem.Payback_Period__c);
        
        return quote;
    }
    */
    public static Map<Integer, Integer> getMonthsInYear(Integer terms, Integer startMonth){
        Map<Integer, Integer> monthsPerYear = new Map<Integer, Integer>();
        Integer MONTHS_IN_A_YEAR = 12;
        Integer months = terms;
        Integer year = 1;
        while(months != 0){
            Integer monthsToAdd = 0;
            if(months - MONTHS_IN_A_YEAR >= 0){
                monthsToAdd = MONTHS_IN_A_YEAR;
            }
            else{
                monthsToAdd = months;
            }
            
            if(year == 1){
                monthsPerYear.put(year, 12 - startMonth);
                months -= 12 - startMonth ;
            }
            else{
                monthsPerYear.put(year, monthsToAdd);
                months -= monthsToAdd;
            }
            year++;
            
        }
        System.debug('monthsPerYear: ' + monthsPerYear);
        return monthsPerYear;
    }
    
    public static Decimal calculatePaybackPeriod(Decimal initialOutlay, Map<Integer, Decimal> newCashFlowMap){
        
        Decimal currentCashflow = 0;
        for(Integer year : newCashFlowMap.keySet()){
            if(initialOutlay - (currentCashflow + newCashFlowMap.get(year)) <= 0){
                return (initialOutlay - currentCashflow).divide(newCashFlowMap.get(year), 3) + (year - 1) ;
            }
            else{
                currentCashflow += newCashFlowMap.get(year);
            }
        }
        
        return 0;
    }
    
    public static Decimal calculateIRR(List<Decimal> cashFlows){
        System.debug(cashFlows);
        Decimal LOW_RATE = -2.5;
        Decimal HIGH_RATE = 2.5;
        Decimal MAX_ITERATION = 1000;
        Decimal PRECISION_REQ = 0.00000001;
        
        Double m = 0.0;
        Decimal oldVal = 0.00;
        Decimal newVal = 0.00;
        Double oldguessRate = LOW_RATE;
        Double newguessRate = LOW_RATE;
        Decimal guessRate = LOW_RATE;
        Double lowGuessRate = LOW_RATE;
        Double highGuessRate = HIGH_RATE;
        
        for (Integer i=0; i<MAX_ITERATION; i++)
        {
            Decimal npv = 0.00;
            for (Integer j=0; j<cashFlows.size(); j++)
            {
                decimal irr = (1 + guessRate);
                decimal denom = irr.pow(j);
                npv = npv + (cashFlows[j]/denom);
            }
            /* Stop checking once the required precision is achieved */
            if ((npv > 0) && (npv < PRECISION_REQ))
                break;
            if (oldVal == 0)
                oldVal = npv;
            else
                oldVal = newVal;
            newVal = npv;
            if (i > 0)
            {
                if (oldVal < newVal)
                {
                    if (oldVal < 0 && newVal < 0)
                        highGuessRate = newguessRate;
                    else
                        lowGuessRate = newguessRate;
                }
                else
                {
                    if (oldVal > 0 && newVal > 0)
                        lowGuessRate = newguessRate;
                    else
                        highGuessRate = newguessRate;
                }
            }
            oldguessRate = guessRate;
            guessRate = (lowGuessRate + highGuessRate) / 2;
            if(((Decimal)oldguessRate).setScale(PRECISION_REQ.scale()) == guessRate.setScale(PRECISION_REQ.scale())){
                System.debug('guessRate: ' + guessRate);
                break;
            }
            newguessRate = guessRate;
            System.debug('guessRate: ' + guessRate);
            if(guessRate <= LOW_RATE || guessRate >= HIGH_RATE){
                break;
            }
        }
        System.debug(' Final guessRate: ' + (guessRate * 100));
        return guessRate * 100;
    }
    
    @AuraEnabled
    public static void createClones(ID recordIdToClone, Integer numberOfCopies){
        String objectName = recordIdToClone.getSObjectType().getDescribe().getName();
        List<String> objectFields = new List<String>();
        for(String apiFields : Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().keySet()){
            objectFields.add(apiFields);
        }
        String query = 'SELECT ' + String.join(objectFields, ',') + ' FROM ' + objectName + ' WHERE ID=:recordIdToClone';
        sObject record = Database.query(query);
        system.debug('record: ' + record);
        List<sObject> clonedRecords = new List<sObject>();
        for(integer i = 0 ; i < numberOfCopies ; i++){
            sObject cloneRecord = record.clone(false,true,false,false);
            clonedRecords.add(cloneRecord);
        }
        
        if(!clonedRecords.isEmpty()){
            System.debug(clonedRecords);
            insert clonedRecords;
        }
    }
    
    @AuraEnabled
    public static void deleteAllRelatedRecords(Id parentId, String parentApiName, String objectApiName){
        //ID parentId1 = parentId;
        String query = 'SELECT ID FROM ' + objectApiName + ' WHERE ' + parentApiName + '=:parentId';
        system.debug(query);
        
        List<sObject> recordList = Database.query(query);
        
        
        system.debug(recordList);
        if(!recordList.isEmpty()){
            delete recordList;
        }
    }
    
    //tin - added 07092019
    @AuraEnabled
    public static String fetchQuoteName(Id parentId, String parentApiName, String purpose){
        
        String quoteRet;
        String query = 'SELECT ID, Name, ApprovalStatus__c, ApprovalLevelFromql__c FROM ' + parentApiName + ' WHERE ID' + '=:parentId';
        SBQQ__Quote__c quotevar = Database.query(query);
        
        if(quotevar != null){
            if(purpose == 'Name'){
                quoteRet = quotevar.Name;
            }
            else if(purpose == 'Reason'){
                quoteRet = quotevar.ApprovalLevelFromql__c;
            }
            else{
                quoteRet = quotevar.ApprovalStatus__c;   
            }
        }
        
        return quoteRet;
        
    }
    
    //tin - added 07122019
    @AuraEnabled
    public static ID cloneWithRelatedAll(Id parentRecId, String parentApiName){
        ID newID;
        List<Fiber_Building_Detail__c> fbcList = new List<Fiber_Building_Detail__c>();
        List<Fiber_IDF_Detail__c> ficList = new List<Fiber_IDF_Detail__c>();
        List<SBQQ__QuoteLine__c> qlList = new List<SBQQ__QuoteLine__c>();
        List<String> objectFields = new List<String>();
        
        for(String apiFields : Schema.getGlobalDescribe().get(parentApiName).getDescribe().fields.getMap().keySet()){
            objectFields.add(apiFields);
        }
        
        String query = 'SELECT ' + String.join(objectFields, ',') + ' FROM ' + parentApiName + ' WHERE ID=:parentRecId';
        system.debug('QUERY ' + query);
        SBQQ__Quote__c quotevar = Database.query(query);
        system.debug('QUERY1 ' + quotevar.ApprovalTextFromql__c);
        SBQQ__Quote__c sbqCopy = quotevar.clone(false,true,false,false);
        system.debug('QUERY2 ' + sbqCopy.ApprovalTextFromql__c);
        //String apprReason, apprApp;
        if(sbqCopy != null){
            //apprReason = sbqCopy.ApprovalTextFromql__c;
            //sbqCopy.isFromClone__c = true;
            //sbqCopy.CloneCount__c = 0;
            insert sbqCopy;
            newID = sbqCopy.id;
        }
        
        SBQQ__Quote__c newQuote = [SELECT ID, Name FROM SBQQ__Quote__c WHERE ID =: newID];
        ID origID = quotevar.Id;
        List<String> objectFieldsBLdg = new List<String>();
        List<Fiber_Building_Detail__c> bldgListQ = new List<Fiber_Building_Detail__c>();
        for(String apiFields : Schema.getGlobalDescribe().get('Fiber_Building_Detail__c').getDescribe().fields.getMap().keySet()){
            objectFieldsBLdg.add(apiFields);
        }
        
        query = 'SELECT ' + String.join(objectFieldsBLdg, ',') + ' FROM Fiber_Building_Detail__c WHERE Quote__c=:origID';
        bldgListQ = Database.query(query);
        
        String tempname;
        for(Fiber_Building_Detail__c fbc : bldgListQ){
            Fiber_Building_Detail__c fbcCopy = fbc.clone(false,true,false,false);
            fbcCopy.Quote__c = newID;
            tempname = fbc.Name;
            fbcCopy.Name = newQuote.Name + 'FROMCLONE-' + tempname.substring(tempName.indexOf('BLDG#') + 5, tempname.length());
            fbcList.add(fbcCopy);
        }
        if(fbcList.size() > 0){
            insert fbcList;
        }
        
        List<String> objectFieldsIDF = new List<String>();
        List<Fiber_IDF_Detail__c> IDFListQ = new List<Fiber_IDF_Detail__c>();
        for(String apiFields : Schema.getGlobalDescribe().get('Fiber_IDF_Detail__c').getDescribe().fields.getMap().keySet()){
            objectFieldsIDF.add(apiFields);
        }
        query = 'SELECT ' + String.join(objectFieldsIDF, ',') + ' FROM Fiber_IDF_Detail__c WHERE Quote__c=:origID';
        IDFListQ = Database.query(query);
        
        for(Fiber_IDF_Detail__c fic : IDFListQ){
            Fiber_IDF_Detail__c ficCopy = fic.clone(false,true,false,false);
            ficCopy.Quote__c = newID;
            tempname = fic.Name;
            ficCopy.Name = newQuote.Name + 'FROMCLONE-' + tempname.substring(tempName.indexOf('IDF#') + 4, tempname.length());
            ficList.add(ficCopy);
        }
        if(ficList.size() > 0){
            insert ficList;
        }
        
        List<String> objectFieldsQL = new List<String>();
        List<SBQQ__QuoteLine__c> QLListQ = new List<SBQQ__QuoteLine__c>();
        SBQQ__QuoteLine__c prodBundle = new SBQQ__QuoteLine__c();
        for(String apiFields : Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet()){
            objectFieldsQL.add(apiFields);
        }
        query = 'SELECT ' + String.join(objectFieldsQL, ',') + ' FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c=:origID';
        QLListQ = Database.query(query);
        
        SBQQ__QuoteLine__c qlCopy = new SBQQ__QuoteLine__c();
        for(SBQQ__QuoteLine__c ql : QLListQ){
            
            if(ql.SBQQ__OptionLevel__c == null){
                prodBundle = ql.clone();
                prodBundle.SBQQ__Quote__c = newID;
            }
            else{
                qlCopy = ql.clone();
                qlCopy.SBQQ__Quote__c = newID;
                qlList.add(qlCopy);
            }
        }
        String approver, reason;
        if(qlList.size() > 0){
            approver = prodBundle.ApprovalText__c;
            reason = prodBundle.ApproverLevel__c;
            insert prodBundle;
            insert qlList;
        }
        
        List<SBQQ__QuoteLine__c> updQLListQ = new List<SBQQ__QuoteLine__c>();
        ID dummy;
        
        for(SBQQ__QuoteLine__c sqcUpd : [SELECT ID,SBQQ__RequiredBy__c,SBQQ__OptionLevel__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c=: newID]){
            if(sqcUpd.SBQQ__OptionLevel__c == null){
                sqcUpd.SBQQ__RequiredBy__c = dummy;
            }
            else{
                sqcUpd.SBQQ__RequiredBy__c = prodBundle.ID;
            }
            updQLListQ.add(sqcUpd);
        }
        
        if(updQLListQ.size() > 0){
            update updQLListQ;        
        }
        
        List<String> objectFieldAppl = new List<String>();
        for(String apiFields : Schema.getGlobalDescribe().get('SBAA__Approval__c').getDescribe().fields.getMap().keySet()){
            objectFieldAppl.add(apiFields);
        }
        
        String queryApp;
        
        List<SBAA__Approval__c> appList = new List<SBAA__Approval__c>();
        List<SBAA__Approval__c> appListnew = new List<SBAA__Approval__c>();
        SBAA__Approval__c appro = new SBAA__Approval__c();
        queryApp = 'SELECT ' + String.join(objectFieldAppl, ',') + ' FROM SBAA__Approval__c WHERE Quote__c=:origID';
        appList = Database.query(queryApp);
        /**
List<SBAA__Approval__c> appList = new List<SBAA__Approval__c>();
**/
        for(SBAA__Approval__c appr : appList){
            appro = appr.clone();
            appro.Quote__c = newID;
            appListnew.add(appro);
        }
        
        if(appListnew.size() > 0){
            insert appListnew;
        }
        
        if(sbqCopy != null){
            sbqCopy.ApprovalLevelFromql__c = reason;
            sbqCopy.ApprovalTextFromql__c = approver;
            update sbqCopy;
        }
        
        return newID;
        
    }
    @AuraEnabled
    public static Boolean submitApprovalQte(ID appID, String reason){
        if(reason == 'submit'){
          SBAA.ApprovalAPI.submit(appID, SBAA__Approval__c.Quote__c);
        }
        else{
          SBAA.ApprovalAPI.recall(appID, SBAA__Approval__c.Quote__c);  
        }
        return true;
    }
    
    @AuraEnabled
    public static Boolean getProfileCustomSet(){
      Profile p = [select id,Name from Profile where id=:Userinfo.getProfileid()];
      Boolean retbool = false;
        
      system.debug('PROFILENAME ' + p.Name);
        
      Map<String,SIMP_Financial_Terms_Settings__c> settings = 	SIMP_Financial_Terms_Settings__c.getall();

      for(SIMP_Financial_Terms_Settings__c ftc : settings.values()){
          system.debug('PROFILENAME2' + ftc);
          if(p.Name == ftc.Profile__c){  
             retbool = true;
             break;
          }
      }  
      
      return retbool;  
    }
    
}