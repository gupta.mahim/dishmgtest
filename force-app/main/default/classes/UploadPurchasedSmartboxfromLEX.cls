public class UploadPurchasedSmartboxfromLEX {
    
    public static String[] filelines = new String[]{};
        public static List<Smartbox__c> accstoupload;
    
    
    @AuraEnabled
    public static List<Smartbox__c> ReadFile(String strNameFile, String opportunityId)
    {
        // nameFile=contentFile.toString();
        system.debug('strNameFile - '+ strNameFile);
        filelines = strNameFile.split('\n');
        List<Smartbox__c> lexAccstoupload = new List<Smartbox__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
            
            Smartbox__c a = new Smartbox__c();
            a.Chassis_Serial__c = inputvalues[0];
            a.Serial_Number__c = inputvalues[1];
            a.CAID__c = inputvalues[2];
            a.SmartCard__c = inputvalues[3];
            a.Opportunity__c = opportunityId;
            
            lexAccstoupload.add(a);         
        }
        System.debug('accstoupload='+lexAccstoupload);     
        
        return lexAccstoupload ;    }
    
    @AuraEnabled
    public static void save(String lexAccstoupload)
    {
        System.debug('Save:: lexAccstoupload '+lexAccstoupload);
        List<Smartbox__c> equipData=(List<Smartbox__c>)JSON.deserialize(lexAccstoupload, List<Smartbox__c>.class);
        try{insert equipData;}
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
            
        }
    }
    
    //Milestone# MS-001252 - 3Jun20 - Start
    @AuraEnabled
    public static userAccessCheck.userAccessCheckResponse getUserAccess(){
        userAccessCheck.userAccessCheckResponse resp = new userAccessCheck.userAccessCheckResponse();
        
        list<Custom_Button_Access_Detail__mdt> objectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c='Upload PURCHASED Smartbox' Limit 1];
        
        if(objectFieldDetail.size()>0)
        	resp=userAccessCheck.getCurrentUserAccess(objectFieldDetail[0].Object_Name__c, objectFieldDetail[0].Field_Name__c);
        else{
            resp.currentUserHaveAccess = false;
            resp.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }
               
        return resp;
    }
    //Milestone# MS-001252 - End
    
}