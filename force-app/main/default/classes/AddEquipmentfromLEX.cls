public class AddEquipmentfromLEX {
    
  //public static List<Equipment__c> equip {get; set;}
    
    @AuraEnabled
    public static String save(String lexAddEquip, String oppId)
    { 
      Id rectId= '01260000000Luri';
      String status= 'Activation Requested';// System.debug('Save:: lexAddEquip '+lexAddEquip);
      List<Equipment__c> equipData=(List<Equipment__c>)JSON.deserialize(lexAddEquip, List<Equipment__c>.class);// System.debug('equipData::'+equipData);
      for(Equipment__c a:equipData){
      a.Opportunity__c=oppId;
      a.Statis__c= status;
      a.RecordTypeId = rectId;
    }
      insert equipData;
      return null;
    }
    
  /*  @AuraEnabled
    public static void addrow(String oppId)
    {
         equip.add(new Equipment__c(Opportunity__c=oppId, Statis__c='Activation Requested', RecordTypeId = '01260000000Luri'));
    }
 */
}