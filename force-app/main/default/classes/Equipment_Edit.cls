public class Equipment_Edit{
    
    public Equipment__c[] equip = new Equipment__c[0];
    
    public Equipment_Edit(ApexPages.StandardController controller) {

    }
    
    public PageReference onsave() {
     update equip;
     return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    
public PageReference onclose() {
     return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    } 
    
    public PageReference onadd() {
     return new PageReference('/apex/equipment_add_smatv?Id=' + ApexPages.currentPage().getParameters().get('Id') + '&st=' + + ApexPages.currentPage().getParameters().get('st'));
    } 


  public Equipment__c[] getEquip() {
               
       equip = [SELECT ID, Name, Remove_Equipment__c, Receiver_S__c, Receiver_Model__c, Channel__c, Programming__c,  Statis__c FROM Equipment__c WHERE Opportunity__c = :ApexPages.currentPage().getParameters().get('Id') ORDER BY Equipment__c.Name ];

        return equip;
        
    }

    
}