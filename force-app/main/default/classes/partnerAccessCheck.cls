/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 28th May 2020
Created for: Milestone# MS-001252
Description: To check logged in partner User Access
-------------------------------------------------------------*/

public class partnerAccessCheck {
    
    public class FlowOutput{
        @Invocablevariable
        public List<Opportunity> ListOfOpps = new list <Opportunity>();
    }
    
    @InvocableMethod (label='loggedInUserAccessCheck' description='Method description')
    public static List<Flowoutput> partnerUserAccessCheck()
    {
        List<FlowOutput> result= new list<FlowOutput>();
        FlowOutput output = new FlowOutput();
        result.add(output);
        
        list<Opportunity> opps = [SELECT Id, Name FROM Opportunity 
                                      WHERE (Property_Status__c IN ('Active', 'Pre-Activation') OR Digital_Status__c = 'Active') 
                                      AND AmDocs_tenantType__c IN ('Individual','Incremental')];
        output.ListofOpps.addAll(opps);
        return result;
    }
    
    
}