public class Smartbox_LeasedEq_CSV_FileUploader_lex 
{
     Id oppId=null;
     ApexPages.StandardController stdCtrl; public Smartbox_LeasedEq_CSV_FileUploader_lex(ApexPages.StandardController std) {stdCtrl=std;oppId=std.getRecord().Id;} public PageReference quicksave() {update accstoupload ;return null;} public PageReference fileAccess() {return null;}

public Smartbox_LeasedEq_CSV_FileUploader_lex(){

}

    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Smartbox__c> accstoupload;
     public Pagereference save()
    {
        if(accstoupload==null || accstoupload.size()==0)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'There are no records to save.');
            ApexPages.addMessage(errormsg);
        }
        else{
            try{insert accstoupload;
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.info,'Records saved successfully.');
            ApexPages.addMessage(errormsg);}
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template');
            ApexPages.addMessage(errormsg);
        }
        }
        
        return null;
    }
    public Pagereference ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        accstoupload = new List<Smartbox__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            Smartbox__c a = new Smartbox__c();
            a.Chassis_Serial__c = inputvalues[0];
            a.Serial_Number__c = inputvalues[1];
            a.CAID__c = inputvalues[2];
            a.SmartCard__c = inputvalues[3];
            a.Smartbox_Leased2__c = True;
            a.Opportunity__c = oppId;
            
            accstoupload.add(a);         
        }
             
                      
        return null ;}  public List<Smartbox__c> getuploadedEquipment() { if (accstoupload!= NULL) if (accstoupload.size() > 0) return accstoupload; else return null; else return null;}}