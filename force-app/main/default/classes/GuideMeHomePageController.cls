/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 6 July 2020
Created for: Milestone# MS-001252
Description: Controller for Guide Me Flow Home screen
-------------------------------------------------------------*/

public class GuideMeHomePageController {
    
    @AuraEnabled
    public static UserAccessListWrapper getUserAccess(){
        UserAccessListWrapper userHaveAccess = new UserAccessListWrapper();
        list<Custom_Button_Access_Detail__mdt> PropobjectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c='Create New Property' Limit 1];
        list<Custom_Button_Access_Detail__mdt> TAobjectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c='Create New Tenant Account' Limit 1];
        
        if(PropobjectFieldDetail.size()>0)
        	userHaveAccess.PropertyAccess=userAccessCheck.getCurrentUserAccess(PropobjectFieldDetail[0].Object_Name__c, PropobjectFieldDetail[0].Field_Name__c);
        else{
            userHaveAccess.PropertyAccess.currentUserHaveAccess = false;userHaveAccess.PropertyAccess.noAccessMsg =  System.Label.CustomButtonNoAccessMsg;
        }
        
        if(TAobjectFieldDetail.size()>0)
        	userHaveAccess.TenantAccountAccess=userAccessCheck.getCurrentUserAccess(TAobjectFieldDetail[0].Object_Name__c, TAobjectFieldDetail[0].Field_Name__c);
        else{
            userHaveAccess.TenantAccountAccess.currentUserHaveAccess = false;userHaveAccess.TenantAccountAccess.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }
        
        return userHaveAccess;
    }
    
    public class UserAccessListWrapper{
        @AuraEnabled
        public userAccessCheck.userAccessCheckResponse PropertyAccess{get;set;}
        @AuraEnabled
        public userAccessCheck.userAccessCheckResponse TenantAccountAccess{get;set;}
        
    }   
}