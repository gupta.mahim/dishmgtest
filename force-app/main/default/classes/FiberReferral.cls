public class FiberReferral{

private final Lead L;
 public string error {get; set;}

   public FiberReferral(ApexPages.StandardController stdcontroller) {
   L = (Lead)stdController.getRecord();
    }

  public PageReference onsave() {
    if (L.Company=='') { error='Missing Property Name. Please Complete All Fields'; return null; }
    else IF ( !Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z]{2,4}', L.Billing_Email__c)) { error ='Business Email Address is not formatted correctly.'; return null; }
    else IF ( !Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z]{2,4}', L.Contact_Email__c)) { error ='Contact Email Address is not formatted correctly.'; return null;}      
    else IF ( !Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z]{2,4}', L.Referrer_Contact_Email__c)) { error ='Referral Email Address is not formatted correctly.'; return null;}     
    else IF ( L.Number_of_Units__c == Null ) { error='Missing Units passed. Please Complete All Fields.'; return null; }
    else IF ( L.Distributor__c == Null ) { error='Missing Distributor. Please Complete All Fields.'; return null; }
    else IF ( L.Billing_Name__c == '')        { error='Missing Business Name. Please Complete All Fields.'; return null; }
    else IF ( L.Contact_Name__c == '')      {        error='Missing Contact Name. Please Complete All Fields.'; return null; }
    else IF ( L.Billing_Address__c == '')        {          error='Missing Business Address. Please Complete All Fields.';          return null;        }
    else IF ( L.Contact_Address__c == '')        {          error='Missing Contact Address. Please Complete All Fields.';          return null;        }
    else IF ( L.Billing_City__c == '')        {          error='Missing Business City. Please Complete All Fields.';          return null;        }
    else IF ( L.Contact_City__c == '')        {          error='Missing Contact City. Please Complete All Fields.';          return null;        }
    else IF ( L.Contact_Name__c == '')        {  error='Missing Contact Name. Please Complete All Fields.';          return null;        }
    else IF ( L.Billing_Zip__c == '') { error='Missing Business Zip Code. Please Complete All Fields.';          return null;        }
    else IF ( L.Contact_Zip_Code__c == '')        {  error='Missing Contact Zip Code. Please Complete All Fields.';          return null;        }
    else IF ( L.Billing_Phone__c == '')        {  error='Missing Business Phone. Please Complete All Fields.';          return null;        }
      else IF ( L.Contact_Phone__c == '')        {  error='Missing Contact Phone. Please Complete All Fields.';          return null;        }
      else IF ( L.Billing_Email__c == '') { error='Missing Business Email. Please Complete All Fields.';          return null;        }
      else IF ( L.Contact_Email__c == '') { error='Missing Contact Email. Please Complete All Fields.';          return null;        }
      else IF ( L.Street == '')        {  error='Missing Property Address. Please Complete All Fields';          return null;        }
      else IF ( L.Connectivity_between_buildings__c == '')        {  error='Missing Intra-Bldg Wiring. Please Complete All Fields.';          return null;        }
      else IF ( L.City == '')        {  error='Missing Property City. Please Complete All Fields.';          return null;        }
      else IF ( L.State2__c == '')        {  error='Missing State. Please Complete All Fields.';          return null;        }
      else IF ( L.Postalcode == '') { error='Missing Property Zip Code. Please Complete All Fields.';          return null;        }
      else IF ( L.Phone == '') { error='Missing Property Phone. Please Complete All Fields.';          return null;        }
      else IF ( L.Number_of_Units__c == Null)        {  error='Missing Number of Units. Please Complete All Fields.';          return null;        }
      else IF ( L.of_Buildings__c == Null)        {  error='Missing Number of Buildings. Please Complete All Fields.';          return null;        }
      else IF ( L.Referrer_OE__c == '')        {  error='Missing Referrer OE Number. Please Complete All Fields.';          return null;        }
      else IF ( L.Referrer_Contact_Name__c == '') { error='Missing Referrer Name. Please Complete All Fields.';          return null;        }
      else IF ( L.Referrer_Contact_Email__c == '') { error='Missing Referrer Email Address. Please Complete All Fields.';          return null;        }
      else IF ( L.Referrer_Contact_Phone__c == '')        {  error='Missing Referrer Phone Number. Please Complete All Fields.';          return null;        }
      else IF ( !L.Billing_Zip__c.isNumeric() ) { error='Business Zip Code must be numeric.';      return null;    }
      else IF ( !L.Contact_Zip_Code__c.isNumeric() ) { error='Contact Zip Code must be numeric.';      return null;    }
      else IF ( !L.Billing_Phone__c.isNumeric() ) { error='Business Phone must be numeric.';      return null;    }
      else IF ( !L.Contact_Phone__c.isNumeric() ) { error='Contact Phone must be numeric.';    return null;    }
      else IF ( !L.Postalcode.isNumeric() ) { error='Property Zip Code must be numeric.';      return null;    }
      else IF ( !L.Phone.isNumeric() ) { error='Property Phone must be numeric.';      return null;    }
      else IF ( !L.Referrer_OE__c.isNumeric() )    {  error='Referrer OE Number must be numeric.';    return null;    }
      else IF ( !L.Referrer_Contact_Phone__c.isNumeric() )    {  error='Referrer Phone Number must be numeric';      return null;    }
      else IF ( L.Billing_State__c == Null ) { error='Missing Business State';      return null;    }
      else IF ( L.Contact_State__c == Null ) { error='Missing Contact State';      return null;    }
      else IF ( L.Single_or_Multi_Site__c == Null ) { error='Missing Single / Multi Site'; return null; } 
      else IF ( L.Construction_Type__c == Null ) { error='Missing Construction Type'; return null; } 
      else IF ( L.Type_of_vertical_wiring__c== Null ) { error='Missing Vertical Wiring'; return null; }
      else IF ( L.Type_of_Horizontal_wiring__c== Null ) { error='Missing Horizontal Wiring'; return null; }
      else IF ( L.Contact_Role__c== Null ) { error='Missing Contact Role'; return null; }
      else IF ( L.Existing_Service_Model__c != Null && Lead.Existing_Provider2__c == Null ) { error='Missing Existing Provider'; return null; }
      else IF ( L.Connectivity_between_buildings__c == Null && L.of_Buildings__c > 0) { error='Missing Wiring Between Buildings'; return null; } 
      else IF ( L.Construction_Type__c == 'Existing' && L.Existing_Service_Model__c == Null ) { error='Missing Existing Service Model'; return null; }
       
      else{   L.OwnerId='00G60000002PXna'; 
           L.RecordTypeId='012f2000000QLcv';
           L.LeadSource='Referral';
           L.Lead_Source_External__c='FRRP';
           insert L;
           return new PageReference('http://dish.force.com/FiberReferral/FiberReferral_Thank_You');
   }
 }
}