@isTest
private class AmDocs_CalloutClass_CaseActivation_Test { 
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

Pricebook2 pb2 = new Pricebook2();
            pb2.Name='Test PB2';
            pb2.IsActive=false;
        insert pb2;
        
        pb2.IsActive=true;
        update pb2;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.LeadSource='Test Place';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Pricebook2Id=pb2.id;
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
        insert opp1;
        
        date myDate = date.newInstance(1973, 10, 25);
        
        Case cas = new Case();
            cas.Requested_Actvation_Date_Time__c = myDate;
            cas.RequestedAction__c = 'Activation';
            cas.Opportunity__c = Opp1.Id;
            cas.subject='1';
        insert cas;

        Opportunity opp2 = New Opportunity();
            opp2.Name = 'Test Opp 123456789 Testing opp2';
            opp2.First_Name_of_Property_Representative__c = 'Jerry';
            opp2.Name_of_Property_Representative__c = 'Clifft';
            opp2.Business_Address__c = '1011 Collie Path';
            opp2.Business_City__c = 'Round Rock';
            opp2.Business_State__c = 'TX';
            opp2.Business_Zip_Code__c = '78664';
            opp2.Billing_Contact_Phone__c = '512-383-5201';
            opp2.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp2.LeadSource='Test Place';
            opp2.CloseDate=system.Today();
            opp2.StageName='Closed Won';
            opp2.Smartbox_Leased__c = false;
//            opp2.AmDocs_ServiceID__c = '1111111111';
//            opp2.AmDocs_SiteID__c = '22222';
            opp2.AccountId = acct1.Id;
            opp2.AmDocs_tenantType__c = 'Digital Bulk';
            opp2.Amdocs_CustomerID__c = '123';
            opp2.Amdocs_BarID__c = '456';
            opp2.System_Type__c='QAM';
            opp2.Category__c='FTG';
            opp2.Address__c='address';
            opp2.City__c='Austin';
            opp2.State__c='TX';
            opp2.Zip__c='78664';
            opp2.Phone__c='5122965581';
            opp2.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp2.Pricebook2Id=pb2.id;
            opp2.Number_of_Units__c=100;
            opp2.Smartbox_Leased__c=false;
        insert opp2;
        
        Case cas2 = new Case();
            cas.Requested_Actvation_Date_Time__c = myDate;
            cas2.RequestedAction__c = 'Activation';
            cas2.Opportunity__c = Opp2.Id;
            cas2.subject='2';
        insert cas2;
            
  }
  
  
 @isTest
  static void Acct1(){
    Case ca1 = [Select Id FROM Case WHERE Subject = '1' Limit 1];
    Case ca2 = [Select Id FROM Case WHERE Subject = '2' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(ca1.Id);
        lstOfOppIds.add(ca2.Id);
        System.Debug('DEBUG 0 sendThisID : ' +ca1); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID1 = ca1.Id;
        String sendThisID2 = ca2.Id;
        Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());       // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
            AmDocs_CalloutClass_CaseActivation.AmDocsMakeCalloutCaseActivation(sendThisID1);
            AmDocs_CalloutClass_CaseActivation.AmDocsMakeCalloutCaseActivation(sendThisID2);
        Test.stopTest();    
  }
}