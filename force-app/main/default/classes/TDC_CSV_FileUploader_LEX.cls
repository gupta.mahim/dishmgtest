public class TDC_CSV_FileUploader_LEX
{
    ApexPages.StandardController stdCtrl;Id accId=null;public TDC_CSV_FileUploader_LEX(ApexPages.StandardController std) {stdCtrl=std;accId=std.getRecord().Id;} public PageReference quicksave() {insert accstoupload ;accstoupload.clear();ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.info,'Records saved successfully.');ApexPages.addMessage(errormsg);return null;} public PageReference fileAccess() {return null;}

public TDC_CSV_FileUploader_LEX(){

}

    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Tenant_Disconnects__c> accstoupload;
    
    public Pagereference ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        accstoupload = new List<Tenant_Disconnects__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            Tenant_Disconnects__c a = new Tenant_Disconnects__c();
            a.Account_Number__c = inputvalues[0];
            a.Account__c = accId;
            
            accstoupload.add(a);         
        }
                      
        return null ;} public List<Tenant_Disconnects__c> getuploadedMutes() { if (accstoupload!= NULL) if (accstoupload.size() > 0) return accstoupload; else return null; else return null;}}