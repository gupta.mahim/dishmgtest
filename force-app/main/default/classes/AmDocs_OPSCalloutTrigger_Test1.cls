@isTest
private class AmDocs_OPSCalloutTrigger_Test1 {
  //Implement mock callout tests here
  
   @testSetup static void testSetupdata(){

    // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;
        
    // Create CustomerMaster URL
        CustomerMaster__c CM = new CustomerMaster__c();
            cm.EndPoint__c = 'https://business-api-gateway.dish.com/amdocs-api-gateway';
        insert CM;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'PR';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = 'jerry.clifft@dish.com';
            ctc1.Role__c = 'Billing Contact (PR)';
            ctc1.AccountId = acct1.Id;
        insert ctc1;
        
        Opportunity o1 = new Opportunity();
            o1.Name = 'Test Opp 1';
            o1.AccountId = acct1.Id;
            o1.LeadSource='Test Place';
            o1.CloseDate=system.Today();
            o1.StageName='Closed Won';
            o1.AmDocs_tenantType__c = 'Incremental';
            o1.Zip__c='78664';
            o1.Smartbox_Leased__c = false;
            o1.Mute_Disconnect__c = '';
            o1.AmDocs_ServiceID__c = '1111111111';
        insert o1;
        
        Opportunity o2 = new Opportunity();
            o2.Name = 'Test Opp 2';
            o2.AccountId = acct1.Id;
            o2.LeadSource='Test Place';
            o2.CloseDate=system.Today();
            o2.StageName='Closed Won';
            o2.AmDocs_tenantType__c = 'Incremental';
            o2.Zip__c='78664';
            o2.Smartbox_Leased__c = false;
            o2.Mute_Disconnect__c = '';
            o2.AmDocs_ServiceID__c = '1111111111';
        insert o2;
        
        Opportunity o3 = new Opportunity();
            o3.Name = 'Test Opp 3';
            o3.AccountId = acct1.Id;
            o3.LeadSource='Test Place';
            o3.CloseDate=system.Today();
            o3.StageName='Closed Won';
            o3.AmDocs_tenantType__c = 'Incremental';
            o3.Zip__c='78664';
            o3.Smartbox_Leased__c = false;
            o3.Mute_Disconnect__c = '';
            o3.AmDocs_ServiceID__c = '1111111111';
        insert o3;
        
        Opportunity o4 = new Opportunity();
            o4.Name = 'Test Opp 4';
            o4.AccountId = acct1.Id;
            o4.LeadSource='Test Place';
            o4.CloseDate=system.Today();
            o4.StageName='Closed Won';
            o4.AmDocs_tenantType__c = 'Incremental';
            o4.Zip__c='78664';
            o4.Smartbox_Leased__c = false;
            o4.Mute_Disconnect__c = '';
            o4.AmDocs_ServiceID__c = '1111111111';
        insert o4;
        
        Opportunity o5 = new Opportunity();
            o5.Name = 'Test Opp 5';
            o5.AccountId = acct1.Id;
            o5.LeadSource='Test Place';
            o5.CloseDate=system.Today();
            o5.StageName='Closed Won';
            o5.AmDocs_tenantType__c = 'Incremental';
            o5.Zip__c='78664';
            o5.Smartbox_Leased__c = false;
            o5.Mute_Disconnect__c = '';
            o5.AmDocs_ServiceID__c = '1111111111';
        insert o5;
        
        Opportunity o6 = new Opportunity();
            o6.Name = 'Test Opp 6';
            o6.AccountId = acct1.Id;
            o6.LeadSource='Test Place';
            o6.CloseDate=system.Today();
            o6.StageName='Closed Won';
            o6.AmDocs_tenantType__c = 'Incremental';
            o6.Zip__c='78664';
            o6.Smartbox_Leased__c = false;
            o6.Mute_Disconnect__c = '';
            o6.AmDocs_ServiceID__c = '1111111111';
        insert o6;

        Opportunity o7 = new Opportunity();
            o7.Name = 'Test Opp 7';
            o7.AccountId = acct1.Id;
            o7.LeadSource='Test Place';
            o7.CloseDate=system.Today();
            o7.StageName='Closed Won';
            o7.AmDocs_tenantType__c = 'Incremental';
            o7.Zip__c='78664';
            o7.Smartbox_Leased__c = false;
            o7.AmDocs_ServiceID__c = '';
            o7.Mute_Disconnect__c = '';
        insert o7;

        Opportunity o8 = new Opportunity();
            o8.Name = 'Test Opp 8';
            o8.AccountId = acct1.Id;
            o8.LeadSource='Test Place';
            o8.CloseDate=system.Today();
            o8.StageName='Closed Won';
            o8.AmDocs_tenantType__c = 'Incremental';
            o8.Zip__c='78664';
            o8.Smartbox_Leased__c = false;
            o8.AmDocs_ServiceID__c = '';
            o8.Mute_Disconnect__c = '';
        insert o8;
        
        Opportunity o9 = new Opportunity();
            o9.Name = 'Test Opp 9';
            o9.AccountId = acct1.Id;
            o9.LeadSource='Test Place';
            o9.CloseDate=system.Today();
            o9.StageName='Closed Won';
            o9.AmDocs_tenantType__c = 'Incremental';
            o9.Zip__c='78664';
            o9.Smartbox_Leased__c = false;
            o9.AmDocs_ServiceID__c = '';
            o9.Mute_Disconnect__c = '';
        insert o9;    

        Opportunity o10 = new Opportunity();
            o10.Name = 'Test Opp 10';
            o10.AccountId = acct1.Id;
            o10.LeadSource='Test Place';
            o10.CloseDate=system.Today();
            o10.StageName='Closed Won';
            o10.AmDocs_tenantType__c = 'Incremental';
            o10.Zip__c='78664';
            o10.Smartbox_Leased__c = false;
            o10.AmDocs_ServiceID__c = '123456';
            o10.Mute_Disconnect__c = '';
        insert o10;
           
    }
  
  
static testMethod void AmDocs_CalloutClass_BulkMuteDC_Test1(){
    Opportunity con1 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 1' Limit 1];
            con1.Mute_Disconnect__c = 'Non-Pay Disconnect';
            update con1;
    Opportunity con2 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 2' Limit 1];
            con2.Mute_Disconnect__c = 'Re-Connect';
            update con2;
    Opportunity con3 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 3' Limit 1];
            con3.Mute_Disconnect__c = 'Disconnect';
            update con3;
    Opportunity con4 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 4' Limit 1];
            con4.Mute_Disconnect__c = 'Non-Pay Resume';
            update con4;
    Opportunity con5 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 5' Limit 1];
            con5.Mute_Disconnect__c = 'UnMute';
            update con5;
    Opportunity con6 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 6' Limit 1];
            con6.Mute_Disconnect__c = 'Mute';
            update con6;
    Opportunity con7 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 7' Limit 1];
            con7.Mute_Disconnect__c = 'Non-Pay Disconnect';
            update con7;
    Opportunity con8 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 8' Limit 1];
            con8.Partner_BULK_Mute_UnMute__c = 'Mute';
            update con8;
    Opportunity con9 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 9' Limit 1];
            con9.Partner_BULK_Mute_UnMute__c = 'UnMute';
            update con9;
   Opportunity con10 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 10' Limit 1];
            con10.AmDocs_ReSendAll__c = TRUE;
            update con10;
 
    Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con1.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con2.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con3.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con4.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con5.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con6.Id); 
            Test.stopTest(); 
        }
}