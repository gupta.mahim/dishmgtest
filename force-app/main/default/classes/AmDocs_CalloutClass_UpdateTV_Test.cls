@isTest
private class AmDocs_CalloutClass_UpdateTV_Test{
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;
        
        API_Log__c alog = new API_Log__c();
        alog.Object__c = 'Tenant Account';
        alog.API__c = 'Update Unit';
        alog.Status__c = 'SUCCESS';
        alog.Results__c = '{Status:Success}';
        alog.ServiceID__c = '012';
        alog.User__c = 'Jerry Clifft';
        insert alog;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Incremental';
            opp1.AmDocs_ServiceID__c='012';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;

Opportunity opp2 = New Opportunity();
            opp2.Name = 'Test Opp2 123456789 Testing opp1';
            opp2.First_Name_of_Property_Representative__c = 'Jerry';
            opp2.Name_of_Property_Representative__c = 'Clifft';
            opp2.Business_Address__c = '1011 Collie Path';
            opp2.Business_City__c = 'Round Rock';
            opp2.Business_State__c = 'TX';
            opp2.Business_Zip_Code__c = '78664';
            opp2.Billing_Contact_Phone__c = '512-383-5201';
            opp2.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp2.CloseDate=system.Today();
            opp2.StageName='Closed Won';
            opp2.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp2.AmDocs_SiteID__c = '22222';
            opp2.AccountId = acct1.Id;
            opp2.AmDocs_tenantType__c = 'Individual';
            opp2.AmDocs_ServiceID__c='012';
            opp2.Amdocs_CustomerID__c = '123';
            opp2.Amdocs_BarID__c = '456';
            opp2.Amdocs_FAID__c = '789';
            opp2.System_Type__c='QAM';
            opp2.Category__c='FTG';
            opp2.Address__c='address';
            opp2.City__c='Austin';
            opp2.State__c='TX';
            opp2.Zip__c='78664';
            opp2.Phone__c='5122965581';
            opp2.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp2.Number_of_Units__c=100;
            opp2.Smartbox_Leased__c=false;
            opp2.RecordTypeId='012600000005ChE';
        insert opp2;

        Tenant_Account__c ta = new Tenant_Account__c();
            ta.Address__c ='1011 Coliie Path';
            ta.City__c = 'Round Rock';
            ta.State__c = 'TX';
            ta.Zip__c = '78664';
            ta.Unit__c = '1A';
            ta.Phone_Number__c = '5123835201';
            ta.Email_Address__c = 'jerry.clifft1111111111111@dish.com';
            ta.First_Name__c = 'Jerry';
            ta.Last_Name__c = 'Clifft';
            ta.Type__c = 'Incremental';
            ta.Opportunity__c = opp1.id;
        insert ta;
        
        Tenant_Account__c ta2 = new Tenant_Account__c();
            ta2.Address__c ='1011 Coliie Path';
            ta2.City__c = 'Round Rock';
            ta2.State__c = 'TX';
            ta2.Zip__c = '78664';
            ta2.Unit__c = '1A';
            ta2.Phone_Number__c = '5123835201';
            ta2.Email_Address__c = 'jerry.clifft2222222222222@dish.com';
            ta2.First_Name__c = 'Jerry';
            ta2.Last_Name__c = 'Clifft';
            ta2.Type__c = 'Incremental';
            ta2.Opportunity__c = opp2.id;
        insert ta2;
        
       Tenant_Equipment__c te = new Tenant_Equipment__c();
            te.Opportunity__c = opp1.Id;
            te.Name = 'R234567819';
            te.Smart_Card__c = 'S1234567891';
            te.Location__c = 'D';
            te.Action__c = 'ADD';
            te.Address__c ='1011 Coliie Path';
            te.City__c = 'Round Rock';
            te.State__c = 'TX';
            te.Zip_Code__c = '78664';
            te.Unit__c = '1A';
            te.Phone_Number__c = '5123835201';
            te.Email_Address__c = 'jerry.clifft@dish.com';
            te.Customer_First_Name__c = 'Jerry';
            te.Customer_Last_Name__c = 'Clifft';
            te.Tenant_Account__c = ta.id;
        insert te;
        
        Tenant_Equipment__c te2 = new Tenant_Equipment__c();
            te2.Opportunity__c = opp2.Id;
            te2.Name = 'R234567819';
            te2.Smart_Card__c = 'S1234567891';
            te2.Location__c = 'D';
            te2.Action__c = 'ADD';
            te2.Address__c ='1011 Coliie Path';
            te2.City__c = 'Round Rock';
            te2.State__c = 'TX';
            te2.Zip_Code__c = '78664';
            te2.Unit__c = '1A';
            te2.Phone_Number__c = '5123835201';
            te2.Email_Address__c = 'jerry.clifft@dish.com';
            te2.Customer_First_Name__c = 'Jerry';
            te2.Customer_Last_Name__c = 'Clifft';
            te2.Tenant_Account__c = ta2.id;
        insert te2;

        Tenant_Pricebook__c tb = new Tenant_Pricebook__c();
            tb.Name = 'Prcebook Name';
            tb.Pricebook_Name__c = 'Prcebook Name';
        insert tb;

        Tenant_Product__c tp = new Tenant_Product__c();
          tp.Active__c = True;
          tp.AmDocs_Incremental_Caption__c = '123';
          tp.AmDocs_Incremental_ProductID__c = '123';
          tp.AmDocs_Individual_Caption__c = '123';
          tp.AmDocs_Individual_ProductID__c = '123';
          tp.Family__c = 'Core';
          tp.Product_Name__c = 'Test Product 1';
          tp.Name = 'Test Product 1';
        insert tp;

        Tenant_ProductEntry__c tpe = new Tenant_ProductEntry__c();
            tpe.Name = 'Test Product 1';
            tpe.Active__c = True;
            tpe.Tenant_Pricebook__c = tb.id;
            tpe.Tenant_Product__c = tp.id;
        insert tpe;
        
        Tenant_Product_Line_Item__c TOli = new Tenant_Product_Line_Item__c ();
            TOli.Tenant_Account__c = ta.id;
            TOli.Action__c = 'ADD';
            TOli.Tenant_ProductEntry__c = tpe.id;
        insert TOli;
  
        Tenant_Product_Line_Item__c TOli2 = new Tenant_Product_Line_Item__c ();
            TOli2.Tenant_Account__c = ta2.id;
            TOli2.Action__c = 'ADD';
            TOli2.Tenant_ProductEntry__c = tpe.id;
        insert TOli2;
  }
  
 @isTest
  static void Acct1(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft1111111111111@dish.com' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());       // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
            AmDocs_CalloutClass_UpdateUnit.AmDocsMakeCalloutUpdateUnit(sendThisID);
        Test.stopTest();    
      
        opp = [select Email_Address__c from Tenant_Account__c where id =: opp.Id];       // Verify that the response received contains fake values        
             System.assertEquals('jerry.clifft1111111111111@dish.com',opp.Email_Address__c); 
  }
  static void Acct2(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft2222222222222@dish.com' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());       // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
            AmDocs_CalloutClass_UpdateUnit.AmDocsMakeCalloutUpdateUnit(sendThisID);
        Test.stopTest();    
      
        opp = [select Email_Address__c from Tenant_Account__c where id =: opp.Id];       // Verify that the response received contains fake values        
             System.assertEquals('jerry.clifft2222222222222@dish.com@dish.com',opp.Email_Address__c); 
//             System.assertEquals(null,opp.AmDocs_CustomerID__c); 
             
//        opr = [select Id, PriceBookEntryId from OpportunityLineItem where OpportunityId =: opp.Id];       // Verify that the response received contains fake values        
//             System.assertEquals('01t600000039UwJ',oli.PriceBookEntryId); 
     
  }
}