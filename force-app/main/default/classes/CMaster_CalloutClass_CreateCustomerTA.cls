public class CMaster_CalloutClass_CreateCustomerTA {

public class person {
    public String orderID;
    public String dishCustomerId;
    public String partyId;
}

    @future(callout=true)

    public static void CMasterCalloutCreateCustomerTA(String Id) {

 list<Tenant_Account__c> C = [select Id, First_Name__c, Last_Name__c, Amdocs_CustomerID__c, Email_Address__c, API_ID__c, Pin__c, AmDocs_ServiceID__c from Tenant_Account__c where Id =:id Limit 1];    
//        if( C.size() < 1 || ((C[0].First_Name__c == '' || C[0].First_Name__c == Null) || (C[0].Last_Name__c == '' || C[0].Last_Name__c == Null )) || (C[0].Email_Address__c == '' || C[0].Email_Address__c == Null ) || (C[0].PIN__c == '' || C[0].PIN__c == Null ) || ( C[0].API_ID__c == '' || C[0].API_ID__c == Null) || (C[0].Amdocs_CustomerID__c == '' || C[0].Amdocs_CustomerID__c == Null)) {
          if( C.size() < 1 || ((C[0].First_Name__c == '' || C[0].First_Name__c == Null) || (C[0].Last_Name__c == '' || C[0].Last_Name__c == Null )) || (C[0].API_ID__c == '' || C[0].API_ID__c == Null) || (C[0].Amdocs_CustomerID__c == '' || C[0].Amdocs_CustomerID__c == Null)) {
            Tenant_Account__c sbc = new Tenant_Account__c();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Amdocs_Transaction_Code__c='';
//                sbc.Amdocs_Transaction_Description__c='Create CMaster -Subscriber-, CM needs requires the Customer AP-ID (APID), Customer ID (Customer ID), First & Last Name along with thier Email and PIN/Last4 Number. Your data: First Name = ' + C[0].First_Name__c + ', LastName = ' +C[0].Last_Name__c + ', Email = ' + C[0].Email_Address__c + ', Pin # = ' + C[0].PIN__c + ', Customer AP-ID = ' +C[0].API_ID__c + ' Customer ID = ' +C[0].Amdocs_CustomerID__c;
                if(C[0].First_Name__c == '' || C[0].First_Name__c == Null) {
                    sbc.Amdocs_Transaction_Description__c='Register Subscriber requires the Customer AP-ID (APID), First Name, Last Name, Email and Subscriber PIN. Your data is missing: First Name';
                }
                if(C[0].Last_Name__c == '' || C[0].Last_Name__c == Null ) {
                    sbc.Amdocs_Transaction_Description__c='Register Subscriber requires the Customer AP-ID (APID), First Name, Last Name, Email and Subscriber PIN. Your data is missing: Last Name';
                }
//                if(C[0].Email_Address__c == '' || C[0].Email_Address__c == Null) {
//                    sbc.Amdocs_Transaction_Description__c='Register Subscriber requires the Customer AP-ID (APID), First Name, Last Name, Email and Subscriber PIN. Your data is missing: Email Address';
//                }
//                if(C[0].PIN__c == '' || C[0].PIN__c == Null ){
//                    sbc.Amdocs_Transaction_Description__c='Register Subscriber requires the Customer AP-ID (APID), First Name, Last Name, Email and Subscriber PIN. Your data is missing: Subscriber PIN';
//                }
                if(C[0].API_ID__c == '' || C[0].API_ID__c == Null) {
                    sbc.Amdocs_Transaction_Description__c='Register Subscriber requires the Customer AP-ID (APID), First Name, Last Name, Email and Subscriber PIN. Your data is missing: AP-ID';
                }
            update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Tenant_Account__c sbc = new Tenant_Account__c();
                        sbc.id=id;
                        sbc.API_Status__c='';
                        sbc.Amdocs_Transaction_Code__c='';
                        sbc.Amdocs_Transaction_Description__c='ERROR: CMaster_CalloutClass_CreateCustomerCTC is missing a valid ENDPOINT';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);
                
                JSONGenerator jsonObj = JSON.createGenerator(true);
                    jsonObj.writeStartObject();
                        jsonObj.writeFieldName('party');
                            jsonObj.writeStartObject();
                                if(C[0].Email_Address__c != ''&& C[0].Email_Address__c != Null) {
                                    jsonObj.writeStringField('emailAddress', C[0].Email_Address__c);
                                }
//                                if(C[0].Billing_Contact_Phone__c != ''&& C[0].Billing_Contact_Phone__c != Null) {
//                                    jsonObj.writeStringField('phone', C[0].Billing_Contact_Phone__c);
//                                }
                                if(C[0].First_Name__c != '' && C[0].First_Name__c != Null) {
                                    jsonObj.writeStringField('firstName', C[0].First_Name__c);
                                }
                                if(C[0].Last_Name__c != '' && C[0].Last_Name__c != Null) {
                                    jsonObj.writeStringField('lastName', C[0].Last_Name__c);
                                }
                                jsonObj.writeStringField('type', 'PERSON');
                                if(C[0].PIN__c != Null && C[0].PIN__c != '') {
                                    jsonObj.writeStringField('ssnLast4', C[0].PIN__c);
                                }
                              jsonObj.writeEndObject();
                        jsonObj.writeFieldName('relations');
                            jsonObj.writeStartArray();
                                jsonObj.writeStartObject();
                                    jsonObj.writeStringField('extRefNum', C[0].API_ID__c);
                                    jsonObj.writeStringField('altRefNum', C[0].Amdocs_CustomerID__c);
                                    jsonObj.writeStringField('providerId', 'AMDOCS-SUB');
                                jsonObj.writeEndObject();
                            jsonObj.writeEndArray();
                       jsonObj.writeEndObject();
    
                    String finalJSON = jsonObj.getAsString();
                        System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
      
                    HttpRequest request = new HttpRequest();
                        String endpoint = CME[0].Endpoint__c +'/cm-customer/customer';
                            //    String endpoint = 'https://test-api-gateway.dish.com/amdocs-api-gateway/cm-registration/customer/DISH377175778844/login';
                            //    String endpoint = 'https://test-api-gateway.dish.com/amdocs-api-gateway/cm-customer/swagger-ui.html#';
                            //    String endpoint = 'https://test-api-gateway.dish.com/amdocs-api-gateway/cm-search/customerParties?extRefNum=45kkkhh6kj';
                            //    String endpoint = 'https://test-api-gateway.dish.com/amdocs-api-gateway/cm-search/customers?firstName=Bilbo&lastName=Baggins&maxResultsCount=40';
                            //     request.setEndPoint(endpoint +'/customer');
                         request.setEndPoint(endpoint);
                         request.setBody(jsonObj.getAsString());
                         request.setHeader('Content-Type', 'application/json');
                         request.setHeader('Customer-Facing-Tool', 'Salesforce');
                         request.setHeader('User-ID', UserInfo.getUsername());
                         request.setMethod('POST');
                         request.setTimeout(101000);
                             System.debug('DEBUG Get UserInfo: ' +UserInfo.getUsername());   
     
                    HttpResponse response = new HTTP().send(request);
                        System.debug(response.toString());
                        System.debug('STATUS:'+response.getStatus());
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug(response.getBody());

                        if (response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());   

                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());


JSONParser parser = JSON.createParser(strjson); person obj = (person)parser.readValueAs( person.class);

                       
                                Tenant_Account__c sbc = new Tenant_Account__c(); {
                                    sbc.id=id;
                                    sbc.API_Status__c='SUCCESS';
                                    sbc.AmDocs_Transaction_Description__c='Subscriber Registered! Next, please create a Login for the Subscriber.';
                                    sbc.dishCustomerId__c=obj.dishCustomerId;
                                    sbc.partyId__c=obj.partyId;
                                    update sbc;
                                }

                               API_Log__c apil = new API_Log__c();{
                                   apil.Record__c=id;
                                   apil.Object__c='Tenant Account';
                                   apil.Status__c='SUCCESS';
                                   apil.Results__c=response.getBody();
                                   apil.API__c='Create Subscriber';
                                   apil.User__c=UserInfo.getUsername();
                               insert apil;
                               }
                           }

                        if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600 || response.getStatusCode() == Null){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());
                                
                                Tenant_Account__c sbc2 = new Tenant_Account__c(); {
                                    sbc2.id=id;
                                    sbc2.API_Status__c='ERROR';
                                    sbc2.AmDocs_Transaction_Description__c='Subscriber Registration Error: ' +response.getBody();
                                    update sbc2;
                                }

                               API_Log__c apil2 = new API_Log__c();{
                                   apil2.Record__c=id;
                                   apil2.Object__c='Tenant_Account__c';
                                   apil2.Status__c='ERROR';
                                   apil2.Results__c=response.getBody();
                                   apil2.API__c='Create Subscriber';
                                   apil2.User__c=UserInfo.getUsername();
                               insert apil2;
                               }
                }
            }
        }
    }
}