@isTest
private class AmDocs_CalloutClass_ResendALL2_Test{
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;
        
        Opportunity opp2 = New Opportunity();
            opp2.Name = 'Testing opp2';
            opp2.First_Name_of_Property_Representative__c = 'Jerry';
            opp2.Name_of_Property_Representative__c = 'Clifft';
            opp2.Business_Address__c = '1011 Collie Path';
            opp2.Business_City__c = 'Round Rock';
            opp2.Business_State__c = 'TX';
            opp2.Business_Zip_Code__c = '78664';
            opp2.Billing_Contact_Phone__c = '512-383-5201';
            opp2.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp2.CloseDate=system.Today();
            opp2.StageName='Closed Won';
            opp2.Smartbox_Leased__c = false;
            opp2.AmDocs_SiteID__c = '22222';
            opp2.AccountId = acct1.Id;
            opp2.AmDocs_tenantType__c = 'Digital Bulk';
            opp2.Amdocs_CustomerID__c = '123';
            opp2.Amdocs_BarID__c = '456';
            opp2.Amdocs_FAID__c = '789';
            opp2.System_Type__c='QAM';
            opp2.Category__c='FTG';
            opp2.Address__c='address';
            opp2.City__c='Austin';
            opp2.State__c='TX';
            opp2.Zip__c='78664';
            opp2.Phone__c='5122965581';
            opp2.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp2.Number_of_Units__c=100;
            opp2.Smartbox_Leased__c=false;
            opp2.RecordTypeId='012600000005ChE';
        insert opp2;
        
        Opportunity opp3 = New Opportunity();
            opp3.Name = 'Testing opp3';
            opp3.First_Name_of_Property_Representative__c = 'Jerry';
            opp3.Name_of_Property_Representative__c = 'Clifft';
            opp3.Business_Address__c = '1011 Collie Path';
            opp3.Business_City__c = 'Round Rock';
            opp3.Business_State__c = 'TX';
            opp3.Business_Zip_Code__c = '78664';
            opp3.Billing_Contact_Phone__c = '512-383-5201';
            opp3.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp3.CloseDate=system.Today();
            opp3.StageName='Closed Won';
            opp3.Smartbox_Leased__c = false;
            opp3.AmDocs_ServiceID__c = '1111111111';
            opp3.AmDocs_SiteID__c = '22222';
            opp3.AccountId = acct1.Id;
            opp3.AmDocs_tenantType__c = 'Digital Bulk';
            opp3.Amdocs_CustomerID__c = '123';
            opp3.Amdocs_BarID__c = '456';
            opp3.Amdocs_FAID__c = '789';
            opp3.System_Type__c='QAM';
            opp3.Category__c='FTG';
            opp3.Address__c='address';
            opp3.City__c='Austin';
            opp3.State__c='TX';
            opp3.Zip__c='78664';
            opp3.Phone__c='5122965581';
            opp3.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp3.Number_of_Units__c=100;
            opp3.Smartbox_Leased__c=false;
            opp3.RecordTypeId='012600000005ChE';
        insert opp3;
} 
  
    static testMethod void AmDocs_CalloutClass_TNResend_Test1(){
        Opportunity con1 = [select id from Opportunity where Name = 'Testing opp1' limit 1];
        Opportunity con2 = [select id from Opportunity where Name = 'Testing opp2' limit 1];
        Opportunity con3 = [select id from Opportunity where Name = 'Testing opp3' limit 1];
        
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_ReSendAll.AmDocsMakeCalloutReSendAll(con1.Id); 
            AmDocs_CalloutClass_ReSendAll.AmDocsMakeCalloutReSendAll(con2.Id); 
            AmDocs_CalloutClass_ReSendAll.AmDocsMakeCalloutReSendAll(con3.Id);                         
        Test.stopTest(); 
    }

    static testMethod void AmDocs_CalloutClass_TNResend_Test2(){
        Opportunity con1 = [select id from Opportunity where Name = 'Testing opp1' limit 1];
        Opportunity con2 = [select id from Opportunity where Name = 'Testing opp2' limit 1];
        Opportunity con3 = [select id from Opportunity where Name = 'Testing opp3' limit 1];                
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(400,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_ReSendAll.AmDocsMakeCalloutReSendAll(con1.Id); 
            AmDocs_CalloutClass_ReSendAll.AmDocsMakeCalloutReSendAll(con2.Id); 
            AmDocs_CalloutClass_ReSendAll.AmDocsMakeCalloutReSendAll(con3.Id); 
        Test.stopTest(); 
    }
}