public class AmDocs_CalloutClass_TNResend {

public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_informationMessages[] informationMessages;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String smartCardID;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}

    @future(callout=true)

    public static void AmDocsMakeCalloutTNResend(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

list<Tenant_Equipment__c> T = [select id, Amdocs_Tenant_ID__c, Smart_Card__c, Name, Reset__c, Address__c, City__c, State__c, Zip_Code__c, Unit__c, Phone_Number__c, Email_Address__c, Customer_First_Name__c, Customer_Last_Name__c, Location__c, Action__c, Opportunity__c from Tenant_Equipment__c where Id = :id AND Location__c = 'D' AND (Action__c = 'Send Trip / HIT' OR Action__c = 'Trip / HIT - Pending' OR Action__c = 'Send Trip / HIT - TEST')];
    System.debug('RESULTS of the LIST lookup to the Tenant Equipment object' +T);
    
        if(T[0].Amdocs_Tenant_ID__c == '' || T[0].Amdocs_Tenant_ID__c == Null ){
  
      Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
          sbc.id=id;
          sbc.API_Status__c='Error';
          sbc.Amdocs_Transaction_Code__c='Error';
          if( (T[0].Amdocs_Tenant_ID__c == '' || T[0].  Amdocs_Tenant_ID__c == Null )) {
              sbc.Amdocs_Transaction_Description__c = T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          }
      update sbc;
      }
      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Tenant Equipment';
          apil2.Status__c='ERROR';
          apil2.Results__c=T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          apil2.API__c='Trip/Resend';
          apil2.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
  }
 
  
  }
  if(T[0].Amdocs_Tenant_ID__c != '' && T[0].Amdocs_Tenant_ID__c != Null ){        
      Datetime dt1 = System.Now();
      list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :T[0].Amdocs_Tenant_ID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1];
            
       if( (timer.size() > 0 )  || T[0].Reset__c == 'ChewbaccaIsTesting' ) {
           system.debug('DEBUG API LOG Timer size : ' +timer.size());
           system.debug('DEBUG API LOG Info' +timer);
           system.debug('DEBUG timer 1 : ' +dt1);

       Tenant_Equipment__c sbc = new Tenant_Equipment__c(); { sbc.id=id; sbc.API_Status__c='Error';  sbc.Amdocs_Transaction_Code__c='Error'; sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; update sbc; }
       API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id; apil2.Object__c='Tenant Equipment'; apil2.Status__c='ERROR'; apil2.Results__c='You must wait atleast (2) two minutes between transactions'; apil2.API__c='TRIP/Resend'; apil2.ServiceID__c=T[0].AmDocs_Tenant_ID__c; apil2.User__c=UserInfo.getUsername(); insert apil2; 
     }
   }   

   else if(timer.size() < 1) {
    
    

    String SO = [select Opportunity__c from Tenant_Equipment__c where Id = :id LIMIT 1 ].Opportunity__c ;
        System.debug('RESULTS of the LIST SO String to the Opp object ' +SO);

list<Opportunity> O = [select id, AmDocs_ServiceID__c, Name from Opportunity where Id = :SO LIMIT 1 ];
     System.debug('RESULTS of the LIST lookup to the Opp object S From SOs ID ' +O);
     
list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the AmDocs Login object' +A);    
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplResendEquipmentRestInput');
        jsonObj.writeStartObject();
            jsonObj.writeStringField('orderActionType', 'RD');
            jsonObj.writeFieldName('equipmentList');
            jsonObj.writeStartArray();   
                for(Integer i = 0; i < T.size(); i++){
                    jsonObj.writeStartObject();
                        jsonObj.writeStringField('action', 'RESEND');
                        jsonObj.writeStringField('receiverID', T[i].Name);
                        jsonObj.writeStringField('smartCardID', T[i].Smart_Card__c);
                    jsonObj.writeEndObject();
                }
            jsonObj.writeEndArray();
        jsonObj.writeEndObject();
    jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG 0 ======== finalJSON: ' + finalJSON);
    

    HttpRequest request = new HttpRequest(); 
        String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+T[0].Amdocs_Tenant_ID__c+'/resendEquipment?sc=SS&lo=EN&ca=SF'; 
        request.setEndPoint(endpoint); 
        request.setBody(jsonObj.getAsString()); 
        request.setHeader('Content-Type', 'application/json'); 
        request.setMethod('POST'); 
        String authorizationHeader = A[0].UXF_Token__c ; 
        request.setHeader('Authorization', authorizationHeader); 
        
        HttpResponse response = new HTTP().send(request); 
        if (response.getStatusCode() == 200) { 
            String strjson = response.getbody(); 
            JSONParser parser = JSON.createParser(strjson); 
            parser.nextToken(); parser.nextToken(); parser.nextToken(); 
            person obj = (person)parser.readValueAs( person.class);
                System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
                System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
                System.debug('DEBUG 3 ======== obj.equipmentList: ' + obj.equipmentList);
                System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
                System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
        
    Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
        sbc.id=id; 
        sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
        sbc.AmDocs_Order_ID__c=obj.orderID; 
        sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
        if(obj.OrderID != Null && obj.equipmentList[0].action == 'RESEND') { 
            sbc.action__c='Active';  
            sbc.AmDocs_Transaction_Description__c='TRIP/HIT COMPLETED'; 
            sbc.AmDocs_Transaction_Code__c='SUCCESS';
        }
        if(obj.informationMessages != Null) { 
            sbc.action__c='Active'; 
            sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription; 
            sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
        }
        update sbc;
            System.debug('sbc'+sbc);
   }
   API_Log__c apil2 = new API_Log__c();{
       apil2.Record__c=id;
       apil2.Object__c='Tenant Equipment';
       apil2.Status__c='SUCCESS';
       apil2.Results__c=strjson;
       apil2.API__c='TRIP/Resend';
       apil2.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
       apil2.User__c=UserInfo.getUsername();
  insert apil2;
  }
   }
   else if (response.getStatusCode() != 200) { 
       String strjson = response.getbody(); 
       JSONParser parser = JSON.createParser(strjson); 
       parser.nextToken(); parser.nextToken(); parser.nextToken(); 
       person obj = (person)parser.readValueAs( person.class);
    
       Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
           sbc.id=id; 
           sbc.action__c='TRIP - FAILED'; 
           sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
           sbc.AmDocs_Transaction_Description__c=String.valueOf(response.getStatus()); 
           update sbc;
       }
       API_Log__c apil2 = new API_Log__c();{
           apil2.Record__c=id;
           apil2.Object__c='Tenant Equipment';
           apil2.Status__c='ERROR';
           apil2.Results__c=strjson;
           apil2.API__c='TRIP/Resend';
           apil2.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
       apil2.User__c=UserInfo.getUsername();
  insert apil2;
  }
       }
   }
}}}