public class QuoteTriggerHandler {

    public static void onUpdate(List<SBQQ__Quote__c> sqcList){     
        
        Set<ID> sqcIDSet = new Set<ID>();
        for (SBQQ__Quote__c sqcval : sqcList){
            sqcIDSet.add(sqcval.ID);
        }
        
        List<SBQQ__QuoteLine__c> sbqqQLL = [SELECT ID, Name,SBQQ__ProductCode__c,ARPU__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: sqcIDSet];
        system.debug('DUMAAN DITO!!');
        
        Boolean CheckARPU = false;
        for (SBQQ__QuoteLine__c sqcl : sbqqQLL){
            if(sqcl.SBQQ__ProductCode__c == 'Fiber_Bundle' && sqcl.ARPU__c != null){
                CheckARPU = true;
            } 	
        }
       
        
        for (SBQQ__Quote__c sqc : sqcList){
            //if(!sqc.isFromClone__c){
              if(sbqqQLL.size() == 0 || !CheckARPU){
               sqc.ApprovalLevelFromql__c = '';
               sqc.ApprovalTextFromql__c = '';
              }
             //}
        }
        
    }
    
}