/*-------------------------------------------------------------
Author: Jerry Clifft
Created on: 5th March 2020
Created for: Milestone# MS-000022
Description: To REMOVE Tenant Equipment (Will work for Digital Bulk, Incremental or Individual (Tenant Accounts))
(and Leave the Tenant Service ID (Account) active)
-------------------------------------------------------------*/

public class AmDocs_Callout_TEQGMeIsActivElsWhrREMOVE {
    /* Used by Guide ME to REMOVE Digital Equipment */
    /* Pre-REQ at least (1) one receiver / piece of Tenant Equipment on a Service ID must remain ACTIVE */
    
    public class person {
        public String serviceID;
        public String ownerServiceId;
        public String orderID;
        public String responseStatus;
        public cls_equipmentList[] equipmentList;
        public cls_componentList[] componentList;
        public cls_spsList[] spsList;
        public cls_informationMessages[] informationMessages;
        public cls_ImplUpdateBulkRestOutput [] ImplUpdateBulkRestOutput;
    }
    
    class cls_ImplUpdateBulkRestOutput{
        public String serviceID;
        public String orderID;
        public String responseStatus;
    }
    
    class cls_equipmentList {
        public String action;
        public String receiverID;
        public String location;
        public String smartCardID;
        public String leaseInd;
        public String chassisSerialNumber;
        public String serialNumber;
    }
    
    class cls_componentList {
        public String action;
        public String caption;
    }
    
    class cls_spsList {
        public String action;
        public String caption;
    }
    
    class cls_informationMessages {
        public String errorCode;
        public String errorDescription;
    }
    @AuraEnabled
    
    public static string TellAmdocs2REMOVETEQOnServiceID(String Id) {
        String returnStatus = 'Success';
        try{
            list<Tenant_Equipment__c> T = [select id, Amdocs_Tenant_ID__c, Smart_Card__c, Name, Location__c, Action__c, Opportunity__c 
                                           from Tenant_Equipment__c where Amdocs_Tenant_ID__c = :id  AND Action__c = 'DupeREMOVE' AND Location__c = 'D'];
            System.debug('DEBUG 2 - RESULTS of the LIST lookup to the Tenant Equipment object' +T); 
            
            
            list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
            System.debug('DEBUG 1 - RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
            
            JSONGenerator jsonObj = JSON.createGenerator(true);
            if(T.size() > 0) {      
                jsonObj.writeStartObject();          
                jsonObj.writeFieldName('ImplUpdateTVRestInput');              
                jsonObj.writeStartObject();                  
                jsonObj.writeStringField('orderActionType', 'CH');                  
                jsonObj.writeStringField('reasonCode', 'CREQ');                   
                jsonObj.writeFieldName('equipmentList'); 
                jsonObj.writeStartArray();    
                for(Integer i = 0; i < T.size(); i++){ 
                    jsonObj.writeStartObject(); 
                    jsonObj.writeStringField('action', 'REMOVE'); 
                    jsonObj.writeStringField('receiverID', T[i].Name); 
                    jsonObj.writeStringField('smartCardID', T[i].Smart_Card__c);                       
                    jsonObj.writeEndObject();                    
                } 
                jsonObj.writeEndArray(); 
                jsonObj.writeEndObject(); 
                jsonObj.writeEndObject(); 
                String finalJSON = jsonObj.getAsString();            
                System.debug('DEBUG 5 - Full Request finalJSON : ' +(jsonObj.getAsString()));
                //if (!Test.isRunningTest()){ 
                    HttpRequest request = new HttpRequest(); 
                    String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+T[0].Amdocs_Tenant_ID__c+'/updateTV?sc=SS&lo=EN&ca=SF'; 
                    request.setEndPoint(endpoint); 
                    request.setBody(jsonObj.getAsString()); 
                    request.setTimeout(101000); 
                    request.setHeader('Content-Type', 'application/json'); 
                    request.setMethod('POST'); 
                    String authorizationHeader = A[0].UXF_Token__c; 
                    request.setHeader('Authorization', authorizationHeader);      
                    HttpResponse response = new HTTP().send(request); 
                    if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { 
                        String strjson = response.getbody();
                        System.debug(strjson); 
                        JSONParser parser = JSON.createParser(strjson); 
                        parser.nextToken(); 
                        parser.nextToken(); 
                        parser.nextToken(); 
                        person obj = (person)parser.readValueAs( person.class); 
                        if( obj.equipmentList != null ) { 
                            for(Integer i = 0; i < obj.equipmentList.size(); i++){              
                                System.debug(response.toString());
                                System.debug('STATUS:'+response.getStatus());
                                System.debug('STATUS_CODE:'+response.getStatusCode());
                                System.debug(response.getBody());    
                                System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                                System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
                                System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
                                System.debug('DEBUG 3 ======== obj.equipmentList: ' + obj.equipmentList);
                                System.debug('DEBUG 3.5 ======== obj.equipmentList: ' + obj.equipmentList[i].receiverId);
                            }
                        }
                        System.debug('DEBUG 4 ======== obj.spsList: ' + obj.spsList);
                        System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
                        System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
                        
                        // Tenant Bulk Equipment
                        if(( obj.responseStatus == 'SUCCESS' || obj.responseStatus == 'ERROR') && T.size() > 0 ) { 
                            if(obj.equipmentList[0].receiverID != null) { 
                                list<string> TreceverIds = new list<string>();
                                for(cls_equipmentList cct : obj.equipmentList) {
                                    if(cct.receiverID != null) {
                                        TreceverIds.add(cct.receiverID);
                                        System.debug('DEBUG 8 ======== cc.receiverID: ' + cct.receiverID);
                                    }
                                }
                                if(TreceverIds != null) {               
                                    List<Tenant_Equipment__c> allTEQ = [SELECT Id, Action__c, Name FROM Tenant_Equipment__c WHERE Name in :TreceverIds ];
                                    if( allTEQ.Size() > 0 ) {                   
                                        for(Tenant_Equipment__c currentTEQ : allTEQ){                   
                                            if( obj.responseStatus == 'SUCCESS') {
                                                if( currentTEQ.Action__c == 'DupeREMOVE'){ 
                                                    currentTEQ.Action__c = 'Removed';
                                                }
                                            }
                                            if( obj.responseStatus == 'ERROR') {
                                                if( currentTEQ.Action__c == 'DupeREMOVE') { 
                                                    //Updated by Mahim - Reverting the Action back to Active - Start
                                                    //currentTEQ.Action__c = 'REMOVE - FAILED';                       
                                                    currentTEQ.Action__c = 'Active';                       
                                                    //Updated by Mahim - End
                                                }
                                            }
                                            if(obj.informationMessages != Null) {                      
                                                currentTEQ.AmDocs_Transaction_Description__c='UPDATE EQUIPMENT ERROR ' +obj.informationMessages[0].errorDescription;                      
                                                currentTEQ.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode;                  
                                            }
                                            else if(obj.informationMessages == Null) {                      
                                                currentTEQ.AmDocs_Transaction_Description__c='UPDATE EQUIPMENT SUBSCRIBER ' +obj.responseStatus;                     
                                                currentTEQ.AmDocs_Transaction_Code__c=obj.responseStatus;                 
                                            }
                                            else if(obj.responseStatus == Null) {                      
                                                currentTEQ.AmDocs_Transaction_Description__c='UPDATE EQUIPMENT ERROR Request Rejected';                      
                                                currentTEQ.AmDocs_Transaction_Code__c='ERROR';                 
                                            }            
                                        }
                                        update allTEQ;           
                                    }        
                                }            
                            }    
                        }    
                    }
                    //Updated by Mahim - Start
                    //else if (response.getStatusCode() <= 200 && response.getStatusCode() >= 600) {
                    else if (response.getStatusCode() <= 200 || response.getStatusCode() >= 600) {
                        //Updated by Mahim - End
                        
                        //Added by Mahim - Incase the Action is not updated, reverting the Action back to Active - Start
                        list<Tenant_Equipment__c> RevertTEAction = new list<Tenant_Equipment__c>();
                        list<Tenant_Equipment__c> checkTEData = [select id, Action__c from Tenant_Equipment__c 
                                                                 where Amdocs_Tenant_ID__c = :id  AND Action__c = 'DupeREMOVE'];
                        for(Tenant_Equipment__c TE: checkTEData){
                            TE.Action__c = 'Active';
                            RevertTEAction.add(TE);
                        }
                        if(RevertTEAction.size()>0)
                            Update RevertTEAction;
                        //Added by Mahim - End
                    }    
                //}    
            }
            return returnStatus;
        }
        catch(Exception e) {
            returnStatus = 'The following exception has occurred: ' + e.getMessage();
            return returnStatus;
        }
    }
}