@isTest
private class AmDocs_OPSCalloutTrigger_Test3 {
  //Implement mock callout tests here
  
   @testSetup static void testSetupdata(){

    // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;
        
    // Create CustomerMaster URL
        CustomerMaster__c CM = new CustomerMaster__c();
            cm.EndPoint__c = 'https://business-api-gateway.dish.com/amdocs-api-gateway';
        insert CM;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'PR';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = 'jerry.clifft@dish.com';
            ctc1.Role__c = 'Billing Contact (PR)';
            ctc1.AccountId = acct1.Id;
        insert ctc1;
         
       Opportunity o11 = new Opportunity();
            o11.Name = 'Test Opp 11';
            o11.AccountId = acct1.Id;
            o11.LeadSource='Test Place';
            o11.CloseDate=system.Today();
            o11.StageName='Closed Won';
            o11.AmDocs_tenantType__c = 'Incremental';
            o11.Zip__c='78664';
            o11.Smartbox_Leased__c = false;
            o11.AmDocs_ServiceID__c = '123456';
            o11.Mute_Disconnect__c = '';
        insert o11;

       Opportunity o12 = new Opportunity();
            o12.Name = 'Test Opp 12';
            o12.AccountId = acct1.Id;
            o12.LeadSource='Test Place';
            o12.CloseDate=system.Today();
            o12.StageName='Closed Won';
            o12.AmDocs_tenantType__c = 'Incremental';
            o12.Zip__c='78664';
            o12.Smartbox_Leased__c = false;
            o12.AmDocs_ServiceID__c = '123456';
            o12.Mute_Disconnect__c = '';
        insert o12;
        
       Opportunity o13 = new Opportunity();
            o13.Name = 'Test Opp 13';
            o13.AccountId = acct1.Id;
            o13.LeadSource='Test Place';
            o13.CloseDate=system.Today();
            o13.StageName='Closed Won';
            o13.AmDocs_tenantType__c = 'Incremental';
            o13.Zip__c='78664';
            o13.Smartbox_Leased__c = false;
            o13.AmDocs_ServiceID__c = '123456';
            o13.Mute_Disconnect__c = '';
        insert o13;
        
       Opportunity o14 = new Opportunity();
            o14.Name = 'Test Opp 14';
            o14.AccountId = acct1.Id;
            o14.LeadSource='Test Place';
            o14.CloseDate=system.Today();
            o14.StageName='Closed Won';
            o14.AmDocs_tenantType__c = 'Incremental';
            o14.Zip__c='78664';
            o14.Smartbox_Leased__c = false;
            o14.AmDocs_ServiceID__c = '123456';
            o14.Mute_Disconnect__c = '';
            o14.Username__c='jerry.clifft@dish.com';
            o14.Password2__c='Password';
            o14.partyId__c='123';
            o14.LoginId__c='123';
        insert o14;

       Opportunity o15 = new Opportunity();
            o15.Name = 'Test Opp 15';
            o15.AccountId = acct1.Id;
            o15.LeadSource='Test Place';
            o15.CloseDate=system.Today();
            o15.StageName='Closed Won';
            o15.AmDocs_tenantType__c = 'Incremental';
            o15.Zip__c='78664';
            o15.Smartbox_Leased__c = false;
            o15.AmDocs_ServiceID__c = '123456';
            o15.Mute_Disconnect__c = '';
            o15.Username__c='jerry.clifft@dish.com';
            o15.Password2__c='Password';
            o15.partyId__c='123';
            o15.LoginId__c='123';
        insert o15;
        
       Opportunity o16 = new Opportunity();
            o16.Name = 'Test Opp 16';
            o16.AccountId = acct1.Id;
            o16.LeadSource='Test Place';
            o16.CloseDate=system.Today();
            o16.StageName='Closed Won';
            o16.AmDocs_tenantType__c = 'Incremental';
            o16.Zip__c='78664';
            o16.Smartbox_Leased__c = false;
            o16.AmDocs_ServiceID__c = '123456';
            o16.Mute_Disconnect__c = '';
            o16.Username__c='jerry.clifft@dish.com';
            o16.Password2__c='Password';
            o16.partyId__c='123';
            o16.LoginId__c='123';
        insert o16;
        
       Opportunity o17 = new Opportunity();
            o17.Name = 'Test Opp 17';
            o17.AccountId = acct1.Id;
            o17.LeadSource='Test Place';
            o17.CloseDate=system.Today();
            o17.StageName='Closed Won';
            o17.AmDocs_tenantType__c = 'Incremental';
            o17.Zip__c='78664';
            o17.Smartbox_Leased__c = false;
            o17.AmDocs_ServiceID__c = '123456';
            o17.Mute_Disconnect__c = '';
            o17.Username__c='jerry.clifft@dish.com';
            o17.Password2__c='Password';
            o17.partyId__c='123';
            o17.LoginId__c='123';
        insert o17;
        
       Opportunity o18 = new Opportunity();
            o18.Name = 'Test Opp 18';
            o18.AccountId = acct1.Id;
            o18.LeadSource='Test Place';
            o18.CloseDate=system.Today();
            o18.StageName='Closed Won';
            o18.AmDocs_tenantType__c = 'Incremental';
            o18.Zip__c='78664';
            o18.Smartbox_Leased__c = false;
            o18.AmDocs_ServiceID__c = '123456';
            o18.Mute_Disconnect__c = '';
            o18.Username__c='jerry.clifft@dish.com';
            o18.Password2__c='Password';
            o18.partyId__c='123';
            o18.LoginId__c='123';
        insert o18;
        
     Opportunity opp19 = New Opportunity();
            opp19.Name = 'Test Opp 19';
            opp19.First_Name_of_Property_Representative__c = 'Jerry';
            opp19.Name_of_Property_Representative__c = 'Clifft';
            opp19.Business_Address__c = '1011 Collie Path';
            opp19.Business_City__c = 'Round Rock';
            opp19.Business_State__c = 'TX';
            opp19.Business_Zip_Code__c = '78664';
            opp19.Billing_Contact_Phone__c = '512-383-5201';
            opp19.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp19.LeadSource='Test Place';
            opp19.CloseDate=system.Today();
            opp19.StageName='Closed Won';
            opp19.Smartbox_Leased__c = false;
//            opp19.AmDocs_ServiceID__c = '1111111111';
            opp19.AccountId = acct1.Id;
        insert opp19;
        
        Opportunity opp20 = New Opportunity();
            opp20.Name = 'Test Opp 20';
            opp20.First_Name_of_Property_Representative__c = 'Jerry';
            opp20.Name_of_Property_Representative__c = 'Clifft';
            opp20.Business_Address__c = '1011 Collie Path';
            opp20.Business_City__c = 'Round Rock';
            opp20.Business_State__c = 'TX';
            opp20.Business_Zip_Code__c = '78664';
            opp20.Billing_Contact_Phone__c = '512-383-5201';
            opp20.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp20.LeadSource='Test Place';
            opp20.CloseDate=system.Today();
            opp20.StageName='Closed Won';
            opp20.Smartbox_Leased__c = false;
            opp20.AmDocs_ServiceID__c = '1111111111';
            opp20.AccountId = acct1.Id;
            opp20.AmDocs_tenantType__c = 'Digital Bulk';
            opp20.Amdocs_CustomerID__c = '123';
            opp20.System_Type__c='QAM';
            opp20.Category__c='FTG';
            opp20.Address__c='address';
            opp20.City__c='Austin';
            opp20.State__c='TX';
            opp20.Zip__c='78664';
            opp20.Phone__c='5122965581';
            opp20.AmDocs_Property_Sub_Type__c='Hotel/Motel';
        insert opp20;
        
    Pricebook2 pb1 = new Pricebook2();
            pb1.Name='Test PB2';
            pb1.IsActive=false;
        insert pb1;
        
        pb1.IsActive=true;
        update pb1;
        
       Opportunity opp21 = New Opportunity();
            opp21.Name = 'Test Opp 21';
            opp21.IsActiveAmdocs2__c = TRUE;
            opp21.First_Name_of_Property_Representative__c = 'Jerry';
            opp21.Name_of_Property_Representative__c = 'Clifft';
            opp21.Business_Address__c = '1011 Collie Path';
            opp21.Business_City__c = 'Round Rock';
            opp21.Business_State__c = 'TX';
            opp21.Business_Zip_Code__c = '786664';
            opp21.Billing_Contact_Phone__c = '512-383-5201';
            opp21.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp21.CloseDate=system.Today();
            opp21.StageName='Closed Won';
            opp21.Smartbox_Leased__c = false;
            opp21.AmDocs_SiteID__c = '22222';
            opp21.AccountId = acct1.Id;
            opp21.AmDocs_tenantType__c = 'Digital Bulk';
            opp21.Amdocs_CustomerID__c = '123';
            opp21.Amdocs_BarID__c = '456';
            opp21.Amdocs_FAID__c = '789';
            opp21.System_Type__c='QAM';
            opp21.Category__c='FTG';
            opp21.Address__c='address';
            opp21.City__c='Austin';
            opp21.State__c='TX';
            opp21.Zip__c='78664';
            opp21.Phone__c='5122965581';
            opp21.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp21.Pricebook2Id=pb1.id;
            opp21.Number_of_Units__c=100;
            opp21.Smartbox_Leased__c=false;
            opp21.RecordTypeId='012600000005ChE';
        insert opp21;

       Opportunity opp22 = New Opportunity();
            opp22.Name = 'Test Opp 22';
            opp22.IsActiveAmdocs2__c = TRUE;
            opp22.First_Name_of_Property_Representative__c = 'Jerry';
            opp22.Name_of_Property_Representative__c = 'Clifft';
            opp22.Business_Address__c = '1011 Collie Path';
            opp22.Business_City__c = 'Round Rock';
            opp22.Business_State__c = 'TX';
            opp22.Business_Zip_Code__c = '786664';
            opp22.Billing_Contact_Phone__c = '512-383-5201';
            opp22.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp22.CloseDate=system.Today();
            opp22.StageName='Closed Won';
            opp22.Smartbox_Leased__c = false;
            opp22.AmDocs_SiteID__c = '22222';
            opp22.AccountId = acct1.Id;
            opp22.AmDocs_tenantType__c = 'Digital Bulk';
            opp22.Amdocs_CustomerID__c = '123';
            opp22.Amdocs_BarID__c = '456';
            opp22.Amdocs_FAID__c = '789';
            opp22.System_Type__c='QAM';
            opp22.Category__c='FTG';
            opp22.Address__c='address';
            opp22.City__c='Austin';
            opp22.State__c='TX';
            opp22.Zip__c='78664';
            opp22.Phone__c='5122965581';
            opp22.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp22.Pricebook2Id=pb1.id;
            opp22.Number_of_Units__c=100;
            opp22.Smartbox_Leased__c=false;
            opp22.RecordTypeId='012600000005ChE';
        insert opp22;
    
        Product2 p1 = new Product2();
            p1.Name='AT120 Test';
            p1.IsActive=true;
            p1.Family='Core';
            p1.Category__c='Core';
            p1.Amdocs_ProductID__c = '123456789';
        insert p1;

        Id pricebookId = Test.getStandardPricebookId();
    
        PricebookEntry standardPrice = new PricebookEntry(
           Pricebook2Id = pricebookId, Product2Id = p1.Id,
           UnitPrice = 10000, IsActive = true);
       insert standardPrice;

       Pricebookentry pbe1 = new pricebookentry();
            pbe1.Product2Id=p1.Id;
            pbe1.Pricebook2Id=pb1.Id;
            pbe1.UnitPrice=100;
            pbe1.IsActive=true;
        insert pbe1;

        OpportunityLineItem oli1 = New OpportunityLineItem();
            oli1.OpportunityId = opp21.id;
            oli1.PriceBookEntryId=pbe1.id;
            oli1.Quantity=1;
            oli1.UnitPrice=1;
            oli1.Action__c='ADD';
        insert oli1;
         
    }
  
  
static testMethod void AmDocs_CalloutClass_BulkMuteDC_Test1(){
   Opportunity con16 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 16' Limit 1];
            con16.CM_UpdateFromCustomerMaster__c = TRUE;
            update con16;            
   Opportunity con17 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 17' Limit 1];
            con17.CM_AddSecurityPin__c = TRUE;
            update con17;
   Opportunity con18 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 18' Limit 1];
            con18.CM_Create_Login__c = TRUE;
            update con18;
    Opportunity con19 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 19' Limit 1];
            con19.AmDocs_CreateCustomer__c = TRUE;
            update con19;
    Opportunity con20 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 20' Limit 1];
            con20.AmDocs_CreateSite__c = TRUE;
            update con20;
    Opportunity con21 = [Select Id FROM Opportunity WHERE Name = 'Test Opp 21' Limit 1];
            con21.AmDocs_CreateBulk__c = TRUE;
            update con21;
            
//            con21.CM_Create_Customer__c = TRUE;
//            update con21;
    
    Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con1.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con2.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con3.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con4.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con5.Id); 
//                AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(con6.Id); 
            Test.stopTest(); 
        }
}