public class EquipmentEdit_LEX {
    public Equipment__c[] equip = new Equipment__c[0];
    @AuraEnabled
    public static Equipment__c[] getEquipments(id oppId) {
        
        System.debug('SA:: '+oppId);
        Equipment__c[] equip = [SELECT ID, Name,Action__c, Remove_Equipment__c, Receiver_S__c, Receiver_Model__c, Channel__c, Programming__c,CaseID__c, Location__c,  Statis__c FROM Equipment__c WHERE Opportunity__c = :oppId/*ApexPages.currentPage().getParameters().get('Id')*/ ORDER BY Equipment__c.Name ];
        return equip;
    }
    @AuraEnabled
    public static List<Equipment__c> saveRecords(String strEquipData) {
 		List<Equipment__c> recordsToUpdate=new List<Equipment__c>(); 
        List<Equipment__c> equipData=(List<Equipment__c>)JSON.deserialize(strEquipData, List<Equipment__c>.class);
        System.debug('equipData::'+equipData);
        for(Equipment__c anEquip:equipData) { recordsToUpdate.add(anEquip); 
        }
        System.debug(recordsToUpdate.size());
        if(recordsToUpdate.size()>0) {	
            update recordsToUpdate;
        }
         recordsToUpdate.clear();
        for(Equipment__c anEquip:equipData) { 
            if(anEquip.Remove_Equipment__c==true)
            { 
                anEquip.Remove_Equipment__c=false;
                
            }
            recordsToUpdate.add(anEquip);
        }
        return recordsToUpdate;
    }
    @AuraEnabled
    public static String deleteRecords(String strEquipData) {
        System.debug('SA:: Save records Method called');
 		List<Equipment__c> recordsToDelete=new List<Equipment__c>(); 
        List<Equipment__c> equipData=(List<Equipment__c>)JSON.deserialize(strEquipData, List<Equipment__c>.class);
        System.debug('equipData::'+equipData);
        for(Equipment__c anEquip:equipData) { if(anEquip.Remove_Equipment__c==true){recordsToDelete.add(anEquip);}}
        System.debug(recordsToDelete.size());
        if(recordsToDelete.size()>0){ delete recordsToDelete; }

        return 'SUCCESS';
    }
    
    //Milestone# MS-001252 - 30Jun20 - Start
    @AuraEnabled
    public static userAccessCheck.userAccessCheckResponse getUserAccess(){
        userAccessCheck.userAccessCheckResponse resp = new userAccessCheck.userAccessCheckResponse();
        list<Custom_Button_Access_Detail__mdt> objectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c='Edit Equipment' Limit 1];
        
        if(objectFieldDetail.size()>0)
        	resp=userAccessCheck.getCurrentUserAccess(objectFieldDetail[0].Object_Name__c, objectFieldDetail[0].Field_Name__c);
        else{
            resp.currentUserHaveAccess = false;
            resp.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }
        
        return resp;
    }
    //Milestone# MS-001252 - End
}