@isTest (seeAllData=true)
Public class Sites_Controller_SFLCase_test{

    public static testMethod void Sites_Controller_SFLCreateOpp_test(){
        Account a = new Account();
            a.Name = 'Test Account';
            a.Programming__c = 'Starter Pack';
            a.phone = '(303) 555-5555';
            a.Misc_Code_SIU__c = 'N123';
            a.Zip__c = '80221';
            a.Equipment_Options__c = '1-722k';
        insert a;
        
//       Opportunity o = new Opportunity();
//           o.LeadSource='Salesforce Lite'; 
//           o.Property_Type__c='Private'; 
//           o.RecordTypeId='0126000000053vu'; 
//           o.BTVNA_Request_Type__c='Installation/Activation';
//           o.misc_code_siu__c='N123'; 
//           o.Restricted_Programming__c='True'; 
//           o.CloseDate=system.Today(); 
//           o.StageName='Closed Won';
//       insert o; 
       
       Case c = new Case(
//           Opportunity__c=o.Id,
           Account_Name__c=a.Id,
           Status='Form Sumitted',
           Origin='BTVNA Portal',
           Property_Type__c='Public',
           RecordTypeId='0126000000017LQ',
           RequestedAction__c='Activation',
           Requested_Actvation_Date_Time__c=system.Today(),
           Bulk_Load__c=True);
     
      PageReference pageRef = Page.Sites_SFLCase;
      Test.setCurrentPage(pageRef);
      Sites_Controller_SFLCase controller = new Sites_Controller_SFLCase();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Private');
    System.currentPagereference().getParameters().put('prog','True');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Installation/Activation');
//    System.currentPagereference().getParameters().put('oppid',o.id);
    
    ApexPages.StandardController sc = new ApexPages.standardController(c);       
    Sites_Controller_SFLCase RUSC = new Sites_Controller_SFLCase (sc);
   
  RUSC.createSFLCase();

  }
  
  }