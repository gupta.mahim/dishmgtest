@isTest
private class CMaster_CalloutClassAcct_FrgtUsrnm_Test {

    @testSetup static void testSetupdata(){
   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
          acct1.AmDocs_CustomerID__c = 'Chewbacca101';
        insert acct1;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = '';
            ctc1.Role__c = 'Billing Contact';
            ctc1.AccountId = acct1.Id;
          ctc1.Password__c = 'Sling11!@';
        insert ctc1;
        
        // Create a U.S. based contact
        Contact ctc2 = New Contact();
            ctc2.FirstName = 'Brad';
            ctc2.LastName = 'Clawson';
            ctc2.Phone = '512-383-5201';
            ctc2.Email = 'jerry.test@dish.com';
            ctc2.Role__c = 'Billing Contact';
            ctc2.AccountId = acct1.Id;
          ctc2.Password__c = 'Sling11!@';
          ctc2.dishCustomerId__c = 'DISH775768547075';
          ctc2.Username__c = 'jerry.test@dish.com';
          ctc2.partyId__c = '5b8436484f94675bdd9c6fbf';
          //ctc2.ExtRefNum__c  = 'Chewbacca101';
        insert ctc2;
        
        CustomerMaster__c cm =new CustomerMaster__c();
          cm.EndPoint__c = 'https://test-api-gateway.dish.com/amdocs-api-gateway';
        insert cm;
        
    }
    
    static testMethod void testCreateCustomerLogin1(){
        Contact con1 = [select id from Contact where firstName ='Jerry' limit 1];
        Contact con2 = [select id from Contact where firstName ='Brad' limit 1];
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '[{"id":"5b8555154f94675bddc44c07","encryptionKeyName":null,"partyType":"PERSON","customerPartyRoles":[{"dishCustomerId":"DISH927856833553","partyRoles":["CUSTOMER_OWNER"]}],"emailContacts":[{"emailPosition":1,"emailAddress":"ME@TEST.COM"}],"phoneContacts":null,"securityInfo":null,"person":{"firstName":"TEST","middleName":null,"lastName":"JOE OPPORTUNITY","salutation":null,"ssnLast4":null},"logins":[{"loginId":"db21c46a-4a5e-4c9a-bb9b-8ddb370d8395","domain":"DISH","userName":"TEST@DISH.COM","status":"ACTIVE"}]}]',
                                                  null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
          CMaster_CalloutClassAcctFrgtUsername.CMasterCalloutFrgtUsername(con1.Id); 
          CMaster_CalloutClassAcctFrgtUsername.CMasterCalloutFrgtUsername(con2.Id); 
        Test.stopTest(); 
    }
    static testMethod void testCreateCustomerLogin2(){
        Contact con1 = [select id from Contact where firstName ='Jerry' limit 1];
        Contact con2 = [select id from Contact where firstName ='Brad' limit 1];
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(300,
                                                 'Complete',
                                                 '[{"id":"5b8555154f94675bddc44c07","encryptionKeyName":null,"partyType":"PERSON","customerPartyRoles":[{"dishCustomerId":"DISH927856833553","partyRoles":["CUSTOMER_OWNER"]}],"emailContacts":[{"emailPosition":1,"emailAddress":"ME@TEST.COM"}],"phoneContacts":null,"securityInfo":null,"person":{"firstName":"TEST","middleName":null,"lastName":"JOE OPPORTUNITY","salutation":null,"ssnLast4":null},"logins":[{"loginId":"db21c46a-4a5e-4c9a-bb9b-8ddb370d8395","domain":"DISH","userName":"TEST@DISH.COM","status":"ACTIVE"}]}]',
                                                  null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
          CMaster_CalloutClassAcctFrgtUsername.CMasterCalloutFrgtUsername(con1.Id); 
          CMaster_CalloutClassAcctFrgtUsername.CMasterCalloutFrgtUsername(con2.Id); 
        Test.stopTest(); 
    }
    
}