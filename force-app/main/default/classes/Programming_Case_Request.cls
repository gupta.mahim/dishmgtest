public class Programming_Case_Request {
  private List<opportunityLineItem> Prods;
  private Case ca;
    public Programming_Case_Request(ApexPages.StandardController controller) {
    this.ca= (Case)controller.getRecord();

    }

     
  public List<opportunityLineItem> getprods() {
       Case cas = [SELECT ID, Opportunity__r.id from Case where id = :ca.id];
       if (cas.Opportunity__c == null)
       return null;
        
       
       prods = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, Status__c, CaseID__c
       FROM opportunityLineItem WHERE CaseID__c = :ca.id AND OpportunityId = :cas.Opportunity__r.id AND (Status__c='Add Requested' OR Status__c='Remove Requested') ORDER BY Status__c];
         


        return prods;
        
    }

    
}