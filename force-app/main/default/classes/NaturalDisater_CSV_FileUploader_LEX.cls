public class NaturalDisater_CSV_FileUploader_LEX
{
    public static String[] filelines = new String[]{};
    public static List<Natural_Disaster__c> accstoupload;
    
    @AuraEnabled
    public static List<Natural_Disaster__c> ReadFile(String strNameFile,String oppId)
    {
        System.debug('strNameFile '+strNameFile);
        filelines = strNameFile.split('\n');
        List<Natural_Disaster__c> lexAccstoupload = new List<Natural_Disaster__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            Natural_Disaster__c a = new Natural_Disaster__c();
            a.ServiceID__c = inputvalues[0];
            a.Request_Type__c = inputvalues[1];       
            a.Account__c = oppId;
           
            lexAccstoupload.add(a);         
        }
        System.debug('accstoupload='+lexAccstoupload);     
        
        return lexAccstoupload ;
    }  
    
    @AuraEnabled
    public static void save(String lexAccstoupload)
    {
        System.debug('Save:: lexAccstoupload '+lexAccstoupload);
        List<Natural_Disaster__c> equipData=(List<Natural_Disaster__c>)JSON.deserialize(lexAccstoupload, List<Natural_Disaster__c>.class);
        try{
            insert equipData;
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());    
        }
    }
    
}