@isTest
public class AddEquipmentfromLEXTest {
    
 public static testMethod void testAddEquipment_lex(){
     
     Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
     List <Equipment__c> myEq = new List <Equipment__c>();
     Equipment__c anEquip=new Equipment__c();   
        anEquip.Name='Test';
        anEquip.Receiver_S__c='ATest';
        anEquip.Receiver_Model__c='311';
        myEq.add(anEquip);
     String str = JSON.serialize(myEq);
     System.debug('Converted JSON :: '+str);
     AddEquipmentfromLEX.save(str, newOpp.id);


}
}