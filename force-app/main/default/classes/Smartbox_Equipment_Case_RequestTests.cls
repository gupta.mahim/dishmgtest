@isTest
Public class Smartbox_Equipment_Case_RequestTests{
    public static testMethod void testSmartbox_Equipment_Case_Request(){

PageReference pageRef = Page.case_request;
Test.setCurrentPageReference(pageRef);

Opportunity newOpp = new Opportunity (name='XYZ Organization', StageName='Closed Won', CloseDate=Date.Today());
insert newOpp;

Smartbox__c myEq = new Smartbox__c (
CAID__c = 'JRC123',
Chassis_Serial__c = '10251973',
Serial_Number__c = '07112006',
SmartCard__c = '07252010',
Status__c = 'Activation Requested',
Type_of_Equipment__c = 'Media Server',
Part_Number__c = 'RB1234567',
Opportunity__c=newopp.id);
insert myEq;

Case mycas = new Case (Status='Form Submitted', Origin='PRM', Opportunity__c=newopp.id);
insert mycas;

ApexPages.StandardController sc = new ApexPages.standardController(mycas);

Smartbox_Equipment_Case_Request myPageCon = new Smartbox_Equipment_Case_Request(sc);

myPageCon.getequip();
}
}