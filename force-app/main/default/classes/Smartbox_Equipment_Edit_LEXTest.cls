@IsTest(SeeAllData=true)
public class Smartbox_Equipment_Edit_LEXTest {
    public static testMethod void testSmartboxEquip() {
        
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        
        Document lstDoc = [select id,name,Body from Document where name = 'eqtest'];
        String fileContent=lstDoc.Body.toString();
        List<Smartbox__c> lexAccstoupload = Smartbox_Equipment_Edit_LEX.getSmartboxItems(newOpp.id);
        String str = JSON.serialize(lexAccstoupload);
        Smartbox_Equipment_Edit_LEX.saveRecords(str);
        Smartbox_Equipment_Edit_LEX.getUserAccess();
    }
    
}