public class GetOpportunityRecords {
@AuraEnabled
    public static list<OppData>getOppRecords(string id,string CaseId,string CasePSAId)
    {
       
        List<OppData> OppDatalist=new List<OppData>();
        List<opportunity>opplist= new list<Opportunity>();
       
        oppdata oppobj= new oppData();
        //Milestone: MS-001092 - Mahim - Added Distributor field in below query
        opplist=[Select Name,Account.Name,Business_Zip_Code__c,Legal_Name__c,IsSystemTypeSB__c,RecordType.Name,CloseDate,Category__c,Locals_Provider__c,
                 AmDocs_Property_Sub_Type__c,AmDocs_tenantType__c,Address__c,City__c,Zip__c,State__c,Main_Head_End_Account__r.Name,Distributor__c,
                 Main_Head_Shared_Physical_Plant__c,Property_Signer_s_Email__c,	Shared_Head_End__c,	Property_Signer_s_Name__c,Business_State__c,	
                 Business_Phone_Number__c,Business_City__c,	Business_Legal_Name__c,Business_Address__c, AmDocs_SiteID__c,AmDocs_BarID__c,
                 Amdocs_CustomerID__c,ThirdPartyBiller__c,	First_Name_of_Property_Representative__c,Name_of_Property_Representative__c,
                 Billing_Contact_Phone__c,Billing_Contact_Email__c,Smartbox_Leased__c,Number_of_smartboxes_used__c from opportunity where id=:id];
        
        //Milestone: MS-001092 - Mahim - Start
        Schema.DescribeFieldResult fieldResult = Opportunity.Distributor__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry v : values) {
            if(opplist[0].Distributor__c == v.getValue())
                opplist[0].Distributor__c = v.getLabel();
        }
        //Milestone: MS-001092 - Mahim - End
        
        oppobj.opp=opplist[0];
        
        Oppobj.TElist=[Select Name,Smart_Card__c,Action__c from Tenant_Equipment__c where opportunity__c=:id];
        Oppobj.HElist=[Select Name,	Receiver_S__c,Action__c from equipment__c where opportunity__c=:id];
        Oppobj.sblist=[Select 	Name,CAID__c,Action__c from Smartbox__c where opportunity__c=:id];
        Oppobj.opplineitm=[SELECT product2id,Description,Product_Name__c,Action__c,	CaseID__c,Family__c,Amdocs_Caption2__c,Amdocs_ProductID__c, Quantity, UnitPrice, TotalPrice, PricebookEntry.Name, PricebookEntry.Product2.Family FROM 
        OpportunityLineItem where opportunity.id=:id];
        Oppobj.CaseNum=[Select CaseNumber,RecordType.Name from Case where (id=: CaseId OR id=:CasePSAId) And  opportunity__c=:id];
        system.debug('Caseinfo'+Oppobj.CaseNum);
        oppDatalist.add(oppobj);
        return OppDatalist;
    }
   
      public  class OppData
    {
        @Auraenabled
        public opportunity opp{
            get;
            set;
        }
         @Auraenabled
        public list<Tenant_Equipment__C>TElist{
            get;
            set;
        }
        @Auraenabled
        public list<Equipment__C>HElist{
            get;
            set;
        }
        @Auraenabled
        public list<Smartbox__C>sblist{
            get;
            set;
        }
           @Auraenabled
        public list<opportunitylineitem>opplineitm{
            get;
            set;
        }
          
           @Auraenabled
        public lIST<CASE> CaseNum{
            get;
            set;
        }
       
    
    }
    
}