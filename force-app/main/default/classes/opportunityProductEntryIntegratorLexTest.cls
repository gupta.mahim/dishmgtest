@isTest (seeAllData=true)
public class opportunityProductEntryIntegratorLexTest {
    public static testMethod void testopportunityProductEntry_Integrator(){
    PageReference pageRef = Page.equipment_add_smatv;
Test.setCurrentPageReference(pageRef);

        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];

        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true);
        insert pb;
        Product2 prod = new Product2(Name = 'Anti-infectives 2007', Family = 'Best Practices', IsActive = true);
        insert prod;

        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;

        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert pbe;
        Opportunity opp = new Opportunity(Name = 'Test Syndicated 2010', Type = 'Syndicated - New', StageName = 'Planning', CloseDate = system.today(), weighted_average__c=100, Number_of_Units__c=200, Seaonsal_Property__c=true);
        insert opp;
        OpportunityLineItem oli = new OpportunityLineItem(opportunityId = opp.Id, pricebookentryId = '01u6000000A3Kc9AAF', Quantity = 1, UnitPrice = 7500, Description = '2007 CMR #4 - Anti-Infectives');
        insert oli;  

Case mycas = new Case (Status='Form Submitted', Origin='PRM', Opportunity__c=opp.id);
insert mycas;

ApexPages.StandardController sc = new ApexPages.standardController(opp);

opportunityProductEntry_Integrator_Lex oPEE = new opportunityProductEntry_Integrator_Lex(sc);

        if(UserInfo.isMultiCurrencyOrganization())
            System.assert(oPEE.getChosenCurrency()!='');
        else
            System.assertEquals(oPEE.getChosenCurrency(),'');
           
            Integer startCount = oPEE.ShoppingCart.size();
        system.assert(startCount>0);
            
            oPEE.searchString = 'michaelforce is a hip cat';
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()==0);
        
        
                oPEE.searchString = oli.PricebookEntry.Name;
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()>0); 
        
                oPEE.onSave();
        
                for(OpportunityLineItem o : oPEE.ShoppingCart){
            o.quantity = 5;
            o.unitprice = 300;
        }
        oPEE.onSave();
        
          opportunityLineItem[] oli2 = [select Id from opportunityLineItem where OpportunityId = :oli.OpportunityId];
        system.assert(oli2.size()==startCount);
        
     
        
        // final quick check of cancel button
        System.assert(oPEE.onCancel()!=null);
                pageRef = Page.opportunityProductRedirect;
        pageRef.getParameters().put('Id',oli2[0].Id);
        Test.setCurrentPageReference(pageRef);

        // load the extension and confirm that redirect function returns something
opportunityProductRedirectExtension oPRE = new opportunityProductRedirectExtension(new ApexPages.StandardController(oli2[0]));
        System.assert(oPRE.redirect()!=null);
             

oPEE.priceBookCheck();
oPEE.addToShoppingCart();
}
}