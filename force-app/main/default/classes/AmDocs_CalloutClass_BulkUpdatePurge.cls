/*
* Author: Jerry Clifft
* Test class name: AmDocs_CalloutClass_BulkUpdatePurgeTest
* Description: This class is used to Purge Head-end and Smartbox.
*              Takes the opportunity Id and receiver numbers that should be purged.
*              Collects all Equipment with the provided receiver numbers, and Action Active.
*              Formats the JSON request data setting Action to 'REMOVE' and pass it over to Amdocs at /updateBulk URL.
*              Handles the sucess response by updatig receivers to action 'Removed'
*              Updates the API response information on Opportunity record and generates API Log records.'
* Note: This class is directly invoked from Guide Me disconnect screen and it disconnects all Active receivers matching the receiver numbers.
*       This class should not be invoked from triggers.               
*/

public class AmDocs_CalloutClass_BulkUpdatePurge
{
    @AuraEnabled

    public static void AmDocsMakeCalloutBulkUpdateAll(String Id,List<String> receiverNumbers) {
    
 list<Opportunity> O = [select id, Name, No_Sports__c, NLOS_Locals__c, AmDocs_tenantType__c, Number_of_Units__c, Smartbox_Leased__c, AmDocs_SiteID__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, AmDocs_BarID__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c from Opportunity where Id = :id ];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);
     
 list<Equipment__c> E = [select id, Receiver_S__c, Name, Location__c, Action__c, Opportunity__c from Equipment__c where Opportunity__c = :id AND Name in:receiverNumbers  AND ( Action__c = 'ADD' OR Action__c = 'Active' ) AND Location__c = 'C'];
     System.debug('RESULTS of the LIST lookup to the Equipment object' +E);
     
 list<Tenant_Equipment__c> T = [select id, Smart_Card__c, Name, Address__c, City__c, State__c, Zip_Code__c, Unit__c, Phone_Number__c, Email_Address__c, Customer_First_Name__c, Customer_Last_Name__c, Location__c, Action__c, Opportunity__c from Tenant_Equipment__c where Opportunity__c = :id AND Name in:receiverNumbers AND Action__c = 'Active' AND Location__c = 'D'];
     System.debug('RESULTS of the LIST lookup to the Tenant Equipment object' +T);     
     
list<Smartbox__c> S1 = [select id, Action__c, Location__c, CAID__c, Opportunity__c, Type_of_Equipment__c, AmDocs_No_Send__c, Second_ProCam__c, Serial_Number__c,  SmartCard__c, Chassis_Serial__c, Smartbox_Leased__c, id__c from Smartbox__c where Opportunity__c = :id AND CAID__c in:receiverNumbers AND  ( Action__c = 'ADD' OR Action__c = 'Active' ) AND AmDocs_No_Send__c  = false ];
     System.debug('RESULTS of the LIST lookup to the Smartbox object' +S1);

list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
  if(O.size() > 0) {
      jsonObj.writeStartObject();
          jsonObj.writeFieldName('ImplUpdateBulkRestInput');
              jsonObj.writeStartObject();
                  jsonObj.writeStringField('orderActionType', 'CH');
              jsonObj.writeStringField('reasonCode', 'CREQ');

                // This is EQUIPMENT - Centeralized ONLY
                jsonObj.writeFieldName('equipmentList');
                    jsonObj.writeStartArray();   
                    for(Integer i = 0; i < E.size(); i++){
                        jsonObj.writeStartObject();
                            jsonObj.writeStringField('action', 'REMOVE');
                                system.debug( 'DEBUG JSON 2 : REMOVE' +E[i].Action__c);
                            jsonObj.writeStringField('receiverID', E[i].Name);
                                system.debug( 'DEBUG JSON 3 :' +E[i].Name);
                            jsonObj.writeStringField('smartCardID', E[i].Receiver_S__c);
                                system.debug( 'DEBUG JSON 4 :' +E[i].Receiver_S__c);
                            jsonObj.writeStringField('location', E[i].Location__c);
                                system.debug( 'DEBUG JSON 4 :' +E[i].Location__c);
                       jsonObj.writeEndObject();
                    }
                  
                    // This is TENANT Equipment
                    if(O[0].AmDocs_tenantType__c != 'Chewbacca') {
                    for(Integer i = 0; i < T.size(); i++){
                        jsonObj.writeStartObject();
                           jsonObj.writeStringField('action', 'REMOVE');
                               system.debug( 'DEBUG JSON 5 :PURGE');
                           jsonObj.writeStringField('receiverID', T[i].Name);
                               system.debug( 'DEBUG JSON 6 :' +T[i].Name);
                           jsonObj.writeStringField('smartCardID', T[i].Smart_Card__c);
                               system.debug( 'DEBUG JSON 7 :' +T[i].Smart_Card__c);
                           jsonObj.writeStringField('location', T[i].Location__c);
                               system.debug( 'DEBUG JSON 8 :' +T[i].Location__c);
                           jsonObj.writeEndObject();
                           }
                       
                    }
                 
                    // This is SMARTBOX Equipment (CAIDS/Smartbox)
                    for(Integer i = 0; i < S1.size(); i++){
                        if(S1[i].CAID__c != Null) {
                            jsonObj.writeStartObject();
                                jsonObj.writeStringField('action', 'REMOVE');
                                    system.debug( 'DEBUG JSON 17 :PURGE');
                                jsonObj.writeStringField('location', S1[i].Location__c);
                                    system.debug( 'DEBUG JSON 18 :' +S1[i].Location__c);
                                jsonObj.writeStringField('receiverID', S1[i].CAID__c);
                                    system.debug( 'DEBUG JSON 19 :' +S1[i].CAID__c);
                                jsonObj.writeStringField('smartCardID', S1[i].SmartCard__c);
                                    system.debug( 'DEBUG JSON 20 :' +S1[i].SmartCard__c);
                            jsonObj.writeEndObject();
                            }
                    }

                    // This is SMARTBOX Equipment (NON-CAIDS/Smartbox)
                    for(Integer i = 0; i < S1.size(); i++){
                          if(S1[i].Serial_Number__c != Null && S1[i].AmDocs_No_Send__c == false && S1[i].Second_ProCam__c == false && S1[i].Action__c != 'SWAP' ) {
                            jsonObj.writeStartObject();
                                jsonObj.writeStringField('action', 'REMOVE');
                                jsonObj.writeStringField('location', S1[i].Location__c);
                                jsonObj.writeStringField('chassisSerialNumber', S1[i].Chassis_Serial__c);
// Removed 4-15-2020            if(S1[i].Type_Of_Equipment__c != '16-slot chassis')
                                if(!S1[i].Type_Of_Equipment__c.contains ('hassi'))  // Added 5-15-2020
                                {
                                    jsonObj.writeStringField('serialNumber', S1[i].Serial_Number__c);
                                }
                                if(S1[i].Action__c != 'REMOVE') {
                                if(S1[i].Smartbox_Leased__c == true && S1[i].Type_Of_Equipment__c == '16-slot chassis') { jsonObj.writeBooleanField('leaseInd', true); }
                                if(S1[i].Smartbox_Leased__c == false && S1[i].Type_Of_Equipment__c == '16-slot chassis') {
                                    jsonObj.writeBooleanField('leaseInd', false);
                                }
                                }
                            jsonObj.writeEndObject();
                            }
                    }
                    jsonObj.writeEndArray();

                jsonObj.writeEndObject();
            jsonObj.writeEndObject();
        String finalJSON = jsonObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));

        HttpRequest request = new HttpRequest();
            String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateBulk?sc=SS&lo=EN&ca=SF'; request.setEndPoint(endpoint);
            request.setBody(jsonObj.getAsString());
            request.setTimeout(101000);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json'); request.setHeader('User-Agent', 'SFDC-Callout/45.0');
            request.setMethod('POST');
            String authorizationHeader = A[0].UXF_Token__c;
            request.setHeader('Authorization', authorizationHeader);
            
        HttpResponse response = new HTTP().send(request);
            if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { 
                String strjson = response.getbody();JSONParser parser = JSON.createParser(strjson); 
                    parser.nextToken();parser.nextToken();parser.nextToken(); 
                    person obj = (person)parser.readValueAs( person.class); 
                if( obj.equipmentList != null ) { 
                    for(Integer i = 0; i < obj.equipmentList.size(); i++){
                        System.debug(response.toString());
                        System.debug('STATUS:'+response.getStatus());
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug(response.getBody());    
                        System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                        System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID); 
                        System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus); 
                        System.debug('DEBUG 3 ======== obj.equipmentList: ' + obj.equipmentList);
                        System.debug('DEBUG 3.5 ======== obj.equipmentList: ' + obj.equipmentList[i].receiverId);
                }
            }
            System.debug('DEBUG 4 ======== obj.spsList: ' + obj.spsList);
            System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
            System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);

            // Head-End - Ceteral Equipment (No Smartbox)        
                

                updateTERecords(O[0],obj,receiverNumbers,T);
                
                updateSmartboxRecords(obj,receiverNumbers,S1);
                
                recordAPIResponse(O[0],response,obj);                
            }
            else if (response.getStatusCode() <= 200 || response.getStatusCode() >= 600){recordAPIResponse(O[0],response,null);}
         }
     }
     //Update Head End Equipmentrecords based on API response
     //Collect receiver ids, SOQL the records based on collected Ids and update the action to Removed
     private static void updateHERecords(Opportunity opp,Person obj,List<String> receiverNumbers,List<Equipment__c> E)
     {
         list<string> receverIds = new list<string>(); 
         if(obj.responseStatus == 'SUCCESS' && E.size() > 0 )
         { 
                if(obj.equipmentList[0].receiverID != null)
                {
                    for(cls_equipmentList cc0 : obj.equipmentList){if(cc0.receiverID != null){receverIds.add(cc0.receiverID);}}
                    if(receverIds != null)
                    {
                        List<Equipment__c> allEQ = [SELECT Id, Action__c, Name FROM Equipment__C WHERE Name in :receiverNumbers and Action__c='Active']; 
                        if( allEQ.Size() > 0 ){for(Equipment__c currentEQ : allEQ){currentEQ.Action__c = 'Removed';}update allEQ;}
                    }    
                }
         }
     }
     //Update Tenant Equipmentrecords based on API response
     //Collect receiver ids, SOQL the records based on collected Ids and update the action to Removed
     private static void updateTERecords(Opportunity opp,Person obj,List<String> receiverNumbers,List<Tenant_Equipment__c> T)
     {
         list<string> TreceverIds = new list<string>();
         if(opp.AmDocs_tenantType__c != 'Chewbacca')
         {
             if(obj.responseStatus == 'SUCCESS' && T.size() > 0 )
             {
                 if(obj.equipmentList[0].receiverID != null)
                 {
                     for(cls_equipmentList cct : obj.equipmentList){if(cct.receiverID != null){TreceverIds.add(cct.receiverID);}}
                     if(TreceverIds != null)
                     {
                         List<Tenant_Equipment__c> allTEQ = [SELECT Id, Action__c, Name FROM Tenant_Equipment__c WHERE Name in :receiverNumbers and Action__c='Active'];
                         if( allTEQ.Size() > 0 ){for(Tenant_Equipment__c currentTEQ : allTEQ){currentTEQ.Action__c = 'Removed';}update allTEQ;}
                     }
                 }
             }
          }
     }
     //update Smartbox records based on API response
     //Collect chassis serial number, receiver number and receiver id
     //SOQL the records based on collected Ids and update the action to Removed
     private static void updateSmartboxRecords(Person obj,List<String> receiverNumbers,List<Smartbox__c> S1)
     {
         list<string> SBoxRVIds = new list<string>();
         list<string> SBoxCHIds = new list<string>();
         list<string> SBoxBLIds = new list<string>();
         if(obj.responseStatus == 'SUCCESS' && S1.size() > 0)
         { 
            if(obj.equipmentList[0].chassisSerialNumber != null || obj.equipmentList[0].receiverID != null || obj.equipmentList[0].serialNumber != null )
            {
                
                for(cls_equipmentList cc : obj.equipmentList)
                {
                    if(cc.chassisSerialNumber != null && cc.serialNumber == null)SBoxCHIds.add(cc.chassisSerialNumber);
                    if(cc.serialNumber != null)SBoxBLIds.add(cc.serialNumber);
                    if(cc.receiverID != null)SBoxRVIds.add(cc.receiverID );
                }
                List<Smartbox__c> allCHEQ = [SELECT Id, Action__c, Name, Chassis_Serial__c, Serial_Number__c, CAID__c FROM Smartbox__c WHERE  (Chassis_Serial__c in :SBoxCHIds OR Serial_Number__c in :SBoxBLIds OR CAID__c in :receiverNumbers) and Action__c='Active']; 
                if(allCHEQ.Size() > 0 ){for(Smartbox__c currentCHEQ : allCHEQ){currentCHEQ.Action__c = 'Removed';}update allCHEQ;}
            }
        }
     }
     //Record the API response on API Log and Opportunity record
     //Display appropriate message for both Error and Success scenarions
     
     private static void recordAPIResponse(Opportunity oppRec,HttpResponse response,Person obj)
     {
         API_Log__c apiLogRec= new API_Log__c();
         
         String jsonResponse=response.getBody();
         
         String status='API Status- UPDATE BULK ' +String.valueOf(response.getStatusCode());
         apiLogRec.Object__c='Opportunity';
         apiLogRec.Status__c=String.valueOf(response.getStatusCode());
         apiLogRec.API__c='Bulk Update';
         apiLogRec.User__c=UserInfo.getUsername();
         apiLogRec.Results__c=jsonResponse;
         apiLogRec.Record__c=oppRec.Id;
         apiLogRec.ServiceID__c=oppRec.AmDocs_ServiceID__c;
         
         insert apiLogRec;
         
         Opportunity newOppRef= new Opportunity();
             
         newOppRef.id=oppRec.Id;
         if(obj!=null && obj.orderID!=null)newOppRef.AmDocs_Order_ID__c=obj.orderID;
         newOppRef.API_Status__c=String.valueOf(response.getStatusCode());
        
         if(obj!=null)newOppRef.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus +(obj.informationMessages != Null?' ' +obj.informationMessages[0].errorDescription:'');
         if(obj!=null)newOppRef.AmDocs_Transaction_Code__c=obj.informationMessages != Null?obj.informationMessages[0].errorCode:obj.responseStatus;
        
         if(obj!=null && obj.responseStatus == Null){newOppRef.AmDocs_Transaction_Description__c='UPDATE BULK ERROR Request Rejected';newOppRef.AmDocs_Transaction_Code__c='ERROR';}
        
         update newOppRef;
     }
     public class person {
    public String serviceID;
    public String ownerServiceId;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_componentList[] componentList;
    public cls_spsList[] spsList;
    public cls_informationMessages[] informationMessages;
    public cls_ImplUpdateBulkRestOutput [] ImplUpdateBulkRestOutput;
}

class cls_ImplUpdateBulkRestOutput{
    public String serviceID;
    public String orderID;
    public String responseStatus;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String location;
    public String smartCardID;
    public String leaseInd;
    public String chassisSerialNumber;
    public String serialNumber;
}

class cls_componentList {
    public String action;
    public String caption;
}

class cls_spsList {
    public String action;
    public String caption;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}
}