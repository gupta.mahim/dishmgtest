public class AmDocs_CalloutClass_EQResend {

public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_informationMessages[] informationMessages;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String smartCardID;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}

    @future(callout=true)

    public static void AmDocsMakeCalloutEqResend(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];
     
list<Equipment__c> E = [select id, Receiver_S__c, Name, Location__c, Action__c, Opportunity__c from Equipment__c where Id = :id AND Location__c = 'C' AND Action__c = 'Send Trip / HIT'];
     System.debug('RESULTS of the LIST lookup to the Equipment object' +E);

String SO = [select Opportunity__c from Equipment__c where Id = :id LIMIT 1 ].Opportunity__c ;
     System.debug('RESULTS of the LIST SO String to the Opp object ' +SO);

list<Opportunity> O = [select id, AmDocs_ServiceID__c, Name from Opportunity where Id = :SO LIMIT 1 ];
     System.debug('RESULTS of the LIST lookup to the Opp object S From SOs ID ' +O);
     
list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the AmDocs Login object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplResendEquipmentRestInput');
        jsonObj.writeStartObject();
            jsonObj.writeStringField('orderActionType', 'RD');
            jsonObj.writeFieldName('equipmentList');
            jsonObj.writeStartArray();   
                for(Integer i = 0; i < E.size(); i++){
                    jsonObj.writeStartObject();
                        jsonObj.writeStringField('action', 'RESEND');
                        jsonObj.writeStringField('receiverID', E[i].Name);
                        jsonObj.writeStringField('smartCardID', E[i].Receiver_S__c);
                        jsonObj.writeStringField('location', 'C');
                    jsonObj.writeEndObject();
                }
            jsonObj.writeEndArray();
        jsonObj.writeEndObject();
    jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG 0 ======== finalJSON: ' + finalJSON);
   
if (!Test.isRunningTest()){ HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/resendEquipment?sc=SS&lo=EN&ca=SF'; request.setEndPoint(endpoint); request.setBody(jsonObj.getAsString());  request.setHeader('Content-Type', 'application/json'); request.setMethod('POST'); String authorizationHeader = A[0].UXF_Token__c ; request.setHeader('Authorization', authorizationHeader);  HttpResponse response = new HTTP().send(request); if (response.getStatusCode() >= 200 && response.getStatusCode() <= 499) { String strjson = response.getbody();  JSONParser parser = JSON.createParser(strjson);  parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);
                    System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                    System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
                    System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
                    System.debug('DEBUG 3 ======== obj.equipmentList: ' + obj.equipmentList);
                    System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
                    System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
        
   Equipment__c sbc = new Equipment__c(); {       sbc.id=id;       sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now();       sbc.AmDocs_Order_ID__c=obj.orderID;       sbc.API_Status__c=String.valueOf(response.getStatusCode());       if(obj.OrderID != Null && obj.equipmentList[0].action == 'RESEND') {            sbc.action__c='Active'; sbc.AmDocs_Transaction_Description__c='TRIP/HIT COMPLETED';            sbc.AmDocs_Transaction_Code__c='SUCCESS';        }
     
     if(obj.informationMessages != Null) {          sbc.action__c='Active';          sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription;          sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode;      }     update sbc; System.debug('sbc'+sbc); }   
   
            API_Log__c apil2 = new API_Log__c();{              apil2.Record__c=id;             apil2.Object__c='Equipment';             apil2.Status__c='SUCCESS';             apil2.Results__c=strjson;             apil2.API__c='Trip/Resend';             apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;             apil2.User__c=UserInfo.getUsername();             insert apil2;  }        }
   
     else if (response.getStatusCode() <= 200 || response.getStatusCode() >= 499) {        String strjson = response.getbody();

         Equipment__c sbc = new Equipment__c(); {         sbc.id=id;          sbc.AmDocs_Transaction_Code__c='SUCCESS';          sbc.action__c='Trip / Hit Failed';         sbc.API_Status__c=String.valueOf(response.getStatusCode());          sbc.AmDocs_Transaction_Description__c=String.valueOf(response.getbody());           update sbc; }
         
         API_Log__c apil2 = new API_Log__c();{              apil2.Record__c=id;             apil2.Object__c='Equipment';             apil2.Status__c='ERROR';             apil2.Results__c=strjson;             apil2.API__c='Trip/Resend';             apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;             apil2.User__c=UserInfo.getUsername();             insert apil2;  }         }
      }
   }
}