public class Sites_Controler_LongHaul_PropE {

    public Sites_Controler_LongHaul_PropE() {
    }

public Opportunity opp {get; set;}
public Case cas {get;set;}
public List <pricebookEntry> prods;
public opportunityLineItem[] shoppingCart {get;set;}
public priceBookEntry[] prods2 {get;set;}
public String equip {get;set;}
public string error {get; set;}
public String promo {get;set;}

           
   public Sites_Controler_LongHaul_PropE (ApexPages.StandardController controller) {   
   opp = new Opportunity(AccountId=ApexPages.currentPage().getParameters().get('acctId'), 
   Property_Status__c='Inactive', 
   LeadSource='Long Haul Portal', 
   Property_Type__c=ApexPages.currentPage().getParameters().get('cat'), 
   RecordTypeId='0126000000017Tj', 
   BTVNA_Request_Type__c=ApexPages.currentPage().getParameters().get('type'), 
   misc_code_siu__c=ApexPages.currentPage().getParameters().get('siu'), 
   Restricted_Programming__c=ApexPages.currentPage().getParameters().get('prog'), 
   CloseDate=system.Today(), 
   StageName='Closed Won');
   
   account a = [SELECT ID, Name, Promotion__c, Programming__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];

equip=a.Equipment_Options__c;
promo=a.Promotion__c;
   }     
      
public List<pricebookEntry> getprods() {
if(ApexPages.currentPage().getParameters().get('prog') == 'True'){
account a = [SELECT ID, Name, Promotion__c, Programming__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];

String[] progs = a.programming__c.split(';');

 prods = [select Id, UnitPrice, Name from pricebookEntry where Name = :progs and Pricebook2Id = '01s60000000Ia6UAAS'];
   return prods;  
  }
  else
  {
  account a = [SELECT ID, Name, Programming__c, NA_Programming_Discount__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];
 prods = [select Id, UnitPrice, Name from pricebookEntry where Name = ''];
   return prods;
}
}
  public PageReference createOppty(){
  if(opp.Contact_Name__c==null){error='Error: Please Enter Contact Name'; return null;} if(opp.Contact_Phone__c==null){error='Error: Please Enter Contact Phone'; return null;}   if(opp.Contact_Email__c==null){error='Error: Please Enter Contact Email'; return null;}   if(opp.Name==null){error='Error: Please Enter Property Name'; return null;}opp.Password__c=''; opp.Tax_Status__c=''; opp.Promotion__c=''; opp.TVs_Installed__c='No'; opp.Point_of_Entry__c='No'; opp.Landlord_permission__c='No'; opp.Roof_Access__c='No'; opp.Phone__c='(763) 497-3727'; opp.Address__c='6600 Jansen Ave NE'; opp.City__c='Albertville'; opp.State__c='MN'; opp.Zip__c='55301'; opp.Type_of_Business__c='Automotive';    insert opp;    cas = [select Id, CaseNumber, Opportunity__c, Status from Case where Opportunity__c = :opp.id and Status = 'Form Submitted'];  if(ApexPages.currentPage().getParameters().get('cat') == 'Public' ){  return new PageReference('/LongHaul/Sites_LongHaul_PrivateProgram?id=' + opp.id + '&evo=EVO ' + opp.EVO__c + '&foc=FOC ' + opp.FOC__c + '&direct=true' + '&disc=null');
    }
        else         {         return new PageReference('/LongHaul/Sites_LongHaul_PrivateProgram?id=' + opp.id + '&direct=true' + '&disc=null');
        }     
   }
    }