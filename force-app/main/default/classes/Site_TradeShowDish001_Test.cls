@isTest
Public class Site_TradeShowDish001_Test{
    public static testMethod void Site_TradeShowDish001_Test(){
    
    PageReference pageRef = Page.dish001;
    Test.setCurrentPageReference(pageRef);
    
    Lead newlead = new Lead (INS_Lead_Hold__c=true,
    OwnerId='00G600000016h6A',
    RecordTypeId='012600000005D4m',
    LeadSource='Referral',
    Lead_Source_External__c='FRRP',
    Contact_Name__c='TX',
    Email='Jerry.Clifft@Dish.com',
    Number_of_Portfolio_Units__c = '10',
    FirstName = 'Jerry',
    LastName = 'Clifft',
    Company='Property Name',
    Street='Property Address',
    City='Property City',
    State2__c='TX',
    Postalcode='78664',
    Existing_Provider__c='New',
    Phone='5122965581',
    Number_of_Units__c=100,
    Distributor__c='DOW'
    );

  ApexPages.StandardController sc = new ApexPages.standardController(newlead);  
    Site_TradeShowDish001 myPageCon = new Site_TradeShowDish001(sc);
    myPageCon.onsave();
}


}