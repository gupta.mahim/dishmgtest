@IsTest
private class Lead_Type_PUBLIC_TEST {
    static testMethod void validateLeadCategorySelectionPUBLIC() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Airports',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

       insert L;
        
       L = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:L.Id];
       System.debug('Lead Type after trigger fired: ' + L.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', L.sundog_deprm2__Lead_Type__c );
    }
     static testMethod void validateLeadCategorySelectionPUBLIC2() {
        Lead LL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Lobbies',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LL.Id];
       System.debug('Lead Type after trigger fired: ' + LL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC3() {
        Lead LLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Beauty Salon',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLL;
        
       LLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC4() {
        Lead LLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Golf Course',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC5() {
        Lead LLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Bar/Restaurant',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLL;
        
       LLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC6() {
        Lead LLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retirement Community',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLL;
        
       LLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC7() {
        Lead LLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Assisted Living',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLL;
        
       LLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC9() {
        Lead LLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLL;
        
       LLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC10() {
        Lead LLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='RV Park',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLL;
        
       LLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC11() {
        Lead LLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Marinas',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLL;
        
       LLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC12() {
        Lead LLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Casinos',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLL;
        
       LLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC13() {
        Lead LLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Food Service',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLL;
        
       LLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC14() {
        Lead LLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='RV Park',
        Location_of_Service__c ='Food Service',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC15() {
        Lead LLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Marinas',
        Location_of_Service__c ='Food Service',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC16() {
        Lead LLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='RV Park',
        Location_of_Service__c ='Other',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC17() {
        Lead LLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Marinas',
        Location_of_Service__c ='Other',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
static testMethod void validateLeadCategorySelectionPUBLIC18() {
        Lead LLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='University',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLL = [SELECT sundog_deprm2__Lead_Type__c  FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLL.Id];
       System.debug('Lead Type after trigger fired: ' + LLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', LLLLLLLLLLLLLLLLLL.sundog_deprm2__Lead_Type__c );
}
}