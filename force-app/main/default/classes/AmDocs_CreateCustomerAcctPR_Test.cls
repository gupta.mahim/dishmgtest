@isTest
private class AmDocs_CreateCustomerAcctPR_Test {
  //Implement mock callout tests here
  
   @testSetup static void testSetupdata(){

    // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'PR';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = 'jerry.clifft@dish.com';
            ctc1.Role__c = 'Billing Contact (PR)';
            ctc1.AccountId = acct1.Id;
        insert ctc1;
        
   // Create Account 2
        Account acct2 = new Account();
            acct2.Name = 'Test Account 2';
            acct2.ToP__c = 'PCO';
            acct2.Pyscical_Address__c = '1011 Collie Path';
            acct2.City__c = 'Round Rock';
            acct2.State__c = 'PR';
            acct2.Zip__c = '78664';
            acct2.Amdocs_CreateCustomer__c = false;
            acct2.Distributor__c = 'PACE';
            acct2.OE_AR__c = '12345';
        insert acct2;

        // Create a U.S. based contact
        Contact ctc2 = New Contact();
            ctc2.FirstName = 'Jerry';
            ctc2.LastName = 'Clifft';
            ctc2.Phone = '512-383-5201';
            ctc2.Email = 'jerry.clifft@dish.com';
            ctc2.Role__c = 'Billing Contact (PR)';
            ctc2.AccountId = acct2.Id;
        insert ctc2;

   // Create Account 3
        Account acct3 = new Account();
            acct3.Name = 'Test Account 3';
            acct3.ToP__c = 'Direct';
            acct3.Pyscical_Address__c = '1011 Collie Path';
            acct3.City__c = 'Round Rock';
            acct3.State__c = 'PR';
            acct3.Zip__c = '78664';
            acct3.Amdocs_CreateCustomer__c = false;
            acct3.Distributor__c = 'SMS';
            acct3.OE_AR__c = '12345';
        insert acct3;

        // Create a U.S. based contact
        Contact ctc3 = New Contact();
            ctc3.FirstName = 'Jerry';
            ctc3.LastName = 'Clifft';
            ctc3.Phone = '512-383-5201';
            ctc3.Email = 'jerry.clifft@dish.com';
            ctc3.Role__c = 'Billing Contact (PR)';
            ctc3.AccountId = acct3.Id;
        insert ctc3;


   // Create Account 4
        Account acct4 = new Account();
            acct4.Name = 'Test Account 4';
            acct4.ToP__c = 'Retailer';
            acct4.Pyscical_Address__c = '1011 Collie Path';
            acct4.City__c = 'Round Rock';
            acct4.State__c = 'PR';
            acct4.Zip__c = '78664';
            acct4.Amdocs_CreateCustomer__c = false;
            acct4.Distributor__c = '4COM';
            acct4.OE_AR__c = '12345';
        insert acct4;

        // Create a U.S. based contact
        Contact ctc4 = New Contact();
            ctc4.FirstName = 'Jerry';
            ctc4.LastName = 'Clifft';
            ctc4.Phone = '512-383-5201';
            ctc4.Email = 'jerry.clifft@dish.com';
            ctc4.Role__c = 'Billing Contact (PR)';
            ctc4.AccountId = acct4.Id;
        insert ctc4;


        
    }
  
  
  @isTest
  static void Acct1(){
    Account opp = [Select Id FROM Account WHERE Name = 'Test Account 1' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp);  
        System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
  
        String sendThisID = opp.Id;
        System.Debug('DEBUG 2 sendThisID : ' +sendThisID);
  
      // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
      // This causes a fake response to be sent
      // from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClassAcct_CrtCustmrPRVI.AmDocsMakeCalloutCreateCustomer(sendThisID);
      Test.stopTest();    
  
  // Verify that the response received contains fake values        
      opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Account where id =: opp.Id];
//      System.assertEquals('0123456789',opp.AmDocs_CustomerID__c);     
//        System.assertEquals(null,opp.AmDocs_CustomerID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_FAID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }
    @isTest
  static void Acct2(){
    Account opp = [Select Id FROM Account WHERE Name = 'Test Account 2' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp);  
        System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
  
        String sendThisID = opp.Id;
        System.Debug('DEBUG 2 sendThisID : ' +sendThisID);
  
      // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
      // This causes a fake response to be sent
      // from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClassAcct_CrtCustmrPRVI.AmDocsMakeCalloutCreateCustomer(sendThisID);
      Test.stopTest();    
  
  // Verify that the response received contains fake values        
      opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Account where id =: opp.Id];
//      System.assertEquals('0123456789',opp.AmDocs_CustomerID__c);     
//        System.assertEquals(null,opp.AmDocs_CustomerID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_FAID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }
  
  @isTest
  static void Acct3(){
    Account opp = [Select Id FROM Account WHERE Name = 'Test Account 3' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp);  
        System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
  
        String sendThisID = opp.Id;
        System.Debug('DEBUG 2 sendThisID : ' +sendThisID);
  
      // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
      // This causes a fake response to be sent
      // from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClassAcct_CrtCustmrPRVI.AmDocsMakeCalloutCreateCustomer(sendThisID);
      Test.stopTest();    
  
  // Verify that the response received contains fake values        
      opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Account where id =: opp.Id];
//      System.assertEquals('0123456789',opp.AmDocs_CustomerID__c);     
//        System.assertEquals(null,opp.AmDocs_CustomerID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_FAID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }  
   
  @isTest
  static void Acct4(){
    Account opp = [Select Id FROM Account WHERE Name = 'Test Account 4' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp);  
        System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
  
        String sendThisID = opp.Id;
        System.Debug('DEBUG 2 sendThisID : ' +sendThisID);
  
      // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
      // This causes a fake response to be sent
      // from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClassAcct_CrtCustmrPRVI.AmDocsMakeCalloutCreateCustomer(sendThisID);
      Test.stopTest();    
  
  // Verify that the response received contains fake values        
      opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Account where id =: opp.Id];
//      System.assertEquals('0123456789',opp.AmDocs_CustomerID__c);     
//        System.assertEquals(null,opp.AmDocs_CustomerID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_FAID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }  
  
  
  
  
  
  
}