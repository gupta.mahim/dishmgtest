public class AmDocs_CalloutClass_BulkUpdtCoronavirus {

public class person {
    public String serviceID;
    public String ownerServiceId;
    public String orderID;
    public String responseStatus;
    public cls_componentList[] componentList;
    public cls_spsList[] spsList;
    public cls_informationMessages[] informationMessages;
    public cls_ImplUpdateBulkRestOutput [] ImplUpdateBulkRestOutput;
}

class cls_ImplUpdateBulkRestOutput{
    public String serviceID;
    public String orderID;
    public String responseStatus;
}

class cls_componentList {
    public String action;
    public String caption;
}

class cls_spsList {
    public String action;
    public String caption;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}


    @future(callout=true)

    public static void ReduceRateNOW(String Id) {
    
     System.debug('This is the ID that stsarted the process ' +Id);   
    
String SO = [select OpportunityId from OpportunityLineItem where Id = :Id LIMIT 1 ].OpportunityID ;
     System.debug('RESULTS of the LIST SO String to the Opp object ' +SO);

   
 list<Opportunity> O = [select id, Name, No_Sports__c, NLOS_Locals__c, AmDocs_tenantType__c, Number_of_Units__c, Smartbox_Leased__c, AmDocs_SiteID__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, AmDocs_BarID__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c from Opportunity where Id = :SO ];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);

list<OpportunityLineItem> B1 = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_Caption2__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :SO AND Action__c = 'Covid19NOWReducingRate' AND (Family__c != 'Core' AND Family__c != 'Promotion' ) ];
     System.debug('RESULTS of the LIST lookup to the non-Core & non-Promotional OpportunityLineItem records' +B1);
     
list<OpportunityLineItem> P = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_Caption2__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :SO AND Action__c = 'Covid19NOWReducingRate' AND Family__c = 'Promotion' ];
     System.debug('RESULTS of the LIST lookup to the Promotional OpportunityLineItem records' +P);

list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
  if(O.size() > 0) {
      jsonObj.writeStartObject();
          jsonObj.writeFieldName('ImplUpdateBulkRestInput');
              jsonObj.writeStartObject();
                  jsonObj.writeStringField('orderActionType', 'CH');
          jsonObj.writeStringField('reasonCode', 'CREQ');

                // This is a list of all non-promos and non-core (I guess that leaves add-on)   
                    jsonObj.writeFieldName('spsList');
                    jsonObj.writeStartArray();
                    for(Integer i = 0; i < B1.size(); i++){
                        if( B1[0].Amdocs_Caption2__c != null  ) { 
                            jsonObj.writeStartObject(); 
                                jsonObj.writeStringField('action', 'ADD'); 
                                jsonObj.writeStringField('caption', B1[i].Amdocs_Caption2__c); 
                            jsonObj.writeEndObject(); }
                        }
                    jsonObj.writeEndArray();
                    
                    // This should include all promotional things - such as the discount for HBO FTG 3 year.
                    jsonObj.writeFieldName('componentList');
                    jsonObj.writeStartArray();
                    for(Integer i = 0; i < P.size(); i++){                         jsonObj.writeStartObject();                             jsonObj.writeStringField('action', 'ADD');                             jsonObj.writeStringField('caption', P[i].Amdocs_Caption2__c);                             jsonObj.writeBooleanField('isPromotion', P[i].IsPromotion__c);                         jsonObj.writeEndObject();
                    }
                    jsonObj.writeEndArray();
                jsonObj.writeEndObject();
            jsonObj.writeEndObject();
        String finalJSON = jsonObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));

    HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateBulk?sc=SS&lo=EN&ca=SF'; 
        request.setEndPoint(endpoint); 
        request.setBody(jsonObj.getAsString()); 
        request.setTimeout(101000); 
        request.setHeader('Content-Type', 'application/json'); 
        request.setMethod('POST'); 
            String authorizationHeader = A[0].UXF_Token__c; 
        request.setHeader('Authorization', authorizationHeader); 
    
    HttpResponse response = new HTTP().send(request);
        system.debug('This is the full returned response before mods ' +response.getbody()); 
        if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { 
            String strjson = response.getbody(); 
            JSONParser parser = JSON.createParser(strjson); 
                parser.nextToken(); 
                parser.nextToken(); 
                parser.nextToken(); 
                person obj = (person)parser.readValueAs( person.class); 
    
    // Products - Component
    if(obj.responseStatus == 'SUCCESS' && P.Size() > 0) {
// 10-31-2018       if(obj.componentList[0].caption != null){
        if(obj.componentList != null){            list<string> PIds = new list<string>();            if(obj.componentList[0].caption != null){                 for(cls_componentList cl : obj.componentList){                     PIds.add(cl.caption); }                }                List<OpportunityLineItem> allCOLI = [SELECT Id, Action__c, Name, AmDocs_Caption2__c, Amdocs_ProductID__c, OpportunityID FROM OpportunityLineItem WHERE OpportunityID = : SO AND AmDocs_Caption2__c in :PIds ];                if( allCOLI.Size() > 0 ) {                     for(OpportunityLineItem currentCOLI : allCOLI){                         if( currentCOLI.Action__c == 'Covid19NOWReducingRate'){                             currentCOLI.Action__c = 'Active';                         }                    } update allCOLI;
            }
        }
    } // End of " if(obj.responseStatus == 'SUCCESS' && P.size() > 0 ) "

    // Products - SPS
    if(obj.responseStatus == 'SUCCESS' && B1.size() > 0) {         for(Integer i = 0; i < B1.size(); i++){             OpportunityLineItem bu = new OpportunityLineItem();                 bu.id=B1[i].id;  if(B1[i].Action__c == 'Covid19NOWReducingRate') {                    bu.Action__c = 'Active';                }             update bu;
         }
    } // End of " if(obj.responseStatus == 'SUCCESS' && P.size() > 0 ) "

    
    Opportunity sbc = new Opportunity(); { 
    sbc.id=SO; 
    sbc.AmDocs_Order_ID__c=obj.orderID; 
    sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
    if(obj.informationMessages != Null) {     sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus + ' ' +obj.informationMessages[0].errorDescription;     sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
    }        
    else if(obj.informationMessages == Null) { 
    sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus; sbc.AmDocs_Transaction_Code__c=obj.responseStatus;     }        else if(obj.responseStatus == Null) {     sbc.AmDocs_Transaction_Description__c='UPDATE BULK ERROR Request Rejected';     sbc.AmDocs_Transaction_Code__c='ERROR';
     }        
      else if ( obj.informationMessages != null) {       if (obj.informationMessages.size() > 1)            
       {
       sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode + ' ' +obj.informationMessages[1].errorCode;        sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription + '\n\n' +obj.informationMessages[1].errorDescription +'\n\n';}               else if (obj.informationMessages.size() < 2) {sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode;        sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription;
       }  
       }  
       update sbc; 
       }
        System.debug('sbc'+sbc);

    API_Log__c apil2 = new API_Log__c();{
     apil2.Record__c=SO;      
      apil2.Object__c='Opportunity';       
      apil2.Status__c='SUCCESS';       
      apil2.Results__c=strjson;       
      apil2.API__c='Covid-19';       
      apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;      
       apil2.User__c=UserInfo.getUsername(); 
         insert apil2;  }
 }
        else if (response.getStatusCode() <= 200 && response.getStatusCode() >= 600) {         String strjson = response.getbody();         Opportunity sbc = new Opportunity();{ sbc.id=id; sbc.API_Status__c='API ERROR - UPDATE BULK ' +String.valueOf(response.getStatusCode()); update sbc; }         API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id;       apil2.Object__c='Opportunity';       apil2.Status__c='ERROR';       apil2.Results__c=strjson;       apil2.API__c='Covid-19';       apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;       apil2.User__c=UserInfo.getUsername();   insert apil2;  }
         }
        
        }
    }
}