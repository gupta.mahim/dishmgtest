public class AmDocs_CalloutClass_BulkUpdateAll {

public class person {
    public String serviceID;
    public String ownerServiceId;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_componentList[] componentList;
    public cls_spsList[] spsList;
    public cls_informationMessages[] informationMessages;
    public cls_ImplUpdateBulkRestOutput [] ImplUpdateBulkRestOutput;
}

class cls_ImplUpdateBulkRestOutput{
    public String serviceID;
    public String orderID;
    public String responseStatus;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String location;
    public String smartCardID;
    public String leaseInd;
    public String chassisSerialNumber;
    public String serialNumber;
}

class cls_componentList {
    public String action;
    public String caption;
}

class cls_spsList {
    public String action;
    public String caption;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}


    @future(callout=true)

    public static void AmDocsMakeCalloutBulkUpdateAll(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

 list<Opportunity> O = [select id, Name, No_Sports__c, NLOS_Locals__c, AmDocs_tenantType__c, Number_of_Units__c, Smartbox_Leased__c, AmDocs_SiteID__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, AmDocs_BarID__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c from Opportunity where Id = :id ];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);
     
 list<Equipment__c> E = [select id, Receiver_S__c, Name, Location__c, Action__c, Opportunity__c from Equipment__c where Opportunity__c = :id  AND ( Action__c = 'ADD' OR Action__c = 'REMOVE' OR Action__c = 'SWAP' OR Action__c = 'Enable' OR Action__c = 'Disable' ) AND Location__c = 'C'];
     System.debug('RESULTS of the LIST lookup to the Equipment object' +E);
     
 list<Tenant_Equipment__c> T = [select id, Smart_Card__c, Name, Address__c, City__c, State__c, Zip_Code__c, Unit__c, Phone_Number__c, Email_Address__c, Customer_First_Name__c, Customer_Last_Name__c, Location__c, Action__c, Opportunity__c from Tenant_Equipment__c where Opportunity__c = :id  AND Action__c = 'ADD' AND Location__c = 'D'];
     System.debug('RESULTS of the LIST lookup to the Tenant Equipment object' +T);     
     
list<Smartbox__c> S1 = [select id, Action__c, Location__c, CAID__c, Opportunity__c, Type_of_Equipment__c, AmDocs_No_Send__c, Second_ProCam__c, Serial_Number__c,  SmartCard__c, Chassis_Serial__c, Smartbox_Leased__c, id__c from Smartbox__c where Opportunity__c = :id AND  ( Action__c = 'ADD' OR Action__c = 'REMOVE' OR Action__c = 'SWAP' OR Action__c = 'Enable' OR Action__c = 'Disable' ) AND AmDocs_No_Send__c  = false ];
     System.debug('RESULTS of the LIST lookup to the Smartbox object' +S1);

list<OpportunityLineItem> B1 = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_Caption2__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :id AND ( Action__c = 'ADD' OR Action__c = 'REMOVE' ) AND (Family__c != 'Core' AND Family__c != 'Promotion' ) ];
     System.debug('RESULTS of the LIST lookup to the non-Core & non-Promotional OpportunityLineItem records' +B1);
     
list<OpportunityLineItem> C = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_ProductID__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :id AND Action__c = 'ADD' AND Family__c = 'Core' Order By CreatedDate DESC LIMIT 1];
     System.debug('RESULTS of the LIST lookup to the Core OpportunityLineItem records' +C);
     
list<OpportunityLineItem> P = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_Caption2__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :id AND ( Action__c = 'ADD' OR Action__c = 'REMOVE' ) AND Family__c = 'Promotion' ];
     System.debug('RESULTS of the LIST lookup to the Promotional OpportunityLineItem records' +P);

list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
  if(O.size() > 0) {
      jsonObj.writeStartObject();
          jsonObj.writeFieldName('ImplUpdateBulkRestInput');
              jsonObj.writeStartObject();
                  for(Integer i = 0; i < C.size(); i++){ jsonObj.writeStringField('newProductOfferingID', C[i].Amdocs_ProductID__c);
                          system.debug( 'DEBUG JSON 1 :' +C[i].Amdocs_ProductID__c);
                  }
                  jsonObj.writeStringField('orderActionType', 'CH');
          jsonObj.writeStringField('reasonCode', 'CREQ');

                // This is EQUIPMENT - Centeralized ONLY
                jsonObj.writeFieldName('equipmentList');
                    jsonObj.writeStartArray();   
                    for(Integer i = 0; i < E.size(); i++){
                        jsonObj.writeStartObject();
                            jsonObj.writeStringField('action', E[i].Action__c);
                                system.debug( 'DEBUG JSON 2 :' +E[i].Action__c);
                            jsonObj.writeStringField('receiverID', E[i].Name);
                                system.debug( 'DEBUG JSON 3 :' +E[i].Name);
                            jsonObj.writeStringField('smartCardID', E[i].Receiver_S__c);
                                system.debug( 'DEBUG JSON 4 :' +E[i].Receiver_S__c);
                            jsonObj.writeStringField('location', E[i].Location__c);
                                system.debug( 'DEBUG JSON 4 :' +E[i].Location__c);
                       jsonObj.writeEndObject();
                    }
                  
                    // This is TENANT Equipment
                    if(O[0].AmDocs_tenantType__c != 'Chewbacca') {
                    for(Integer i = 0; i < T.size(); i++){
                        jsonObj.writeStartObject();
                           jsonObj.writeStringField('action', T[i].Action__c);
                               system.debug( 'DEBUG JSON 5 :' +T[i].Action__c);
                           jsonObj.writeStringField('receiverID', T[i].Name);
                               system.debug( 'DEBUG JSON 6 :' +T[i].Name);
                           jsonObj.writeStringField('smartCardID', T[i].Smart_Card__c);
                               system.debug( 'DEBUG JSON 7 :' +T[i].Smart_Card__c);
                           jsonObj.writeStringField('location', T[i].Location__c);
                               system.debug( 'DEBUG JSON 8 :' +T[i].Location__c);
                           jsonObj.writeStringField('address', T[i].Address__c);
                               system.debug( 'DEBUG JSON 9 :' +T[i].Address__c);
                           jsonObj.writeStringField('city', T[i].City__c);
                               system.debug( 'DEBUG JSON 10 :' +T[i].City__c);
                           jsonObj.writeStringField('state', T[i].State__c);
                               system.debug( 'DEBUG JSON 11 :' +T[i].State__c);
                           jsonObj.writeStringField('zipCode', T[i].Zip_Code__c);
                               system.debug( 'DEBUG JSON 12 :' +T[i].Zip_Code__c);
                           jsonObj.writeStringField('unit', T[i].Unit__c);
                               system.debug( 'DEBUG JSON 13 :' +T[i].Unit__c);
                           if(T[i].Phone_Number__c != Null) {
                               jsonObj.writeStringField('phoneNumber', T[i].Phone_Number__c);
                                   system.debug( 'DEBUG JSON 14 :' +T[i].Phone_Number__c);
                           }
                           if(T[i].Email_Address__c != Null) {
                               jsonObj.writeStringField('emailAddress', T[i].Email_Address__c);
                                   system.debug( 'DEBUG JSON 15 :' +T[i].Email_Address__c);
                           }
                           if(T[i].Customer_First_Name__c != Null) {
                               jsonObj.writeStringField('customerFirstName', T[i].Customer_First_Name__c);
                                   system.debug( 'DEBUG JSON 16 :' +T[i].Customer_First_Name__c);
                           }
                           if(T[i].Customer_Last_Name__c != Null) {
                               jsonObj.writeStringField('customerLastName', T[i].Customer_Last_Name__c);
                                   system.debug( 'DEBUG JSON 16 :' +T[i].Customer_Last_Name__c);
                           }
                       jsonObj.writeEndObject();
                    }
                    }
                 
                    // This is SMARTBOX Equipment (CAIDS/Smartbox)
                    for(Integer i = 0; i < S1.size(); i++){
                        if(S1[i].CAID__c != Null) {
                            jsonObj.writeStartObject();
                                jsonObj.writeStringField('action', S1[i].Action__c);
                                    system.debug( 'DEBUG JSON 17 :' +S1[i].Action__c);
                                jsonObj.writeStringField('location', S1[i].Location__c);
                                    system.debug( 'DEBUG JSON 18 :' +S1[i].Location__c);
                                jsonObj.writeStringField('receiverID', S1[i].CAID__c);
                                    system.debug( 'DEBUG JSON 19 :' +S1[i].CAID__c);
                                jsonObj.writeStringField('smartCardID', S1[i].SmartCard__c);
                                    system.debug( 'DEBUG JSON 20 :' +S1[i].SmartCard__c);
                            jsonObj.writeEndObject();
                            }
                    }

                    // This is SMARTBOX Equipment (NON-CAIDS/Smartbox)
                    for(Integer i = 0; i < S1.size(); i++){
//                        if(S1[i].Serial_Number__c != Null && S1[i].CAID__c == Null && S1[i].AmDocs_No_Send__c == false) {
                          if(S1[i].Serial_Number__c != Null && S1[i].AmDocs_No_Send__c == false && S1[i].Second_ProCam__c == false && S1[i].Action__c != 'SWAP' ) {
                            jsonObj.writeStartObject();
                                jsonObj.writeStringField('action', S1[i].Action__c);
                                jsonObj.writeStringField('location', S1[i].Location__c);
                                jsonObj.writeStringField('chassisSerialNumber', S1[i].Chassis_Serial__c);
// Removed 4-15-2020            if(S1[i].Type_Of_Equipment__c != '16-slot chassis')
                                if(!S1[i].Type_Of_Equipment__c.contains ('hassi'))  // Added 5-15-2020
                                {
                                    jsonObj.writeStringField('serialNumber', S1[i].Serial_Number__c);
                                }
                                if(S1[i].Action__c != 'REMOVE') {
                                if(S1[i].Smartbox_Leased__c == true && S1[i].Type_Of_Equipment__c == '16-slot chassis') { jsonObj.writeBooleanField('leaseInd', true); }
                                if(S1[i].Smartbox_Leased__c == false && S1[i].Type_Of_Equipment__c == '16-slot chassis') {
                                    jsonObj.writeBooleanField('leaseInd', false);
                                }
                                }
                            jsonObj.writeEndObject();
                            }
                    }
                    jsonObj.writeEndArray();

                    // This is a list of all non-promos and non-core (I guess that leaves add-on)   
                    jsonObj.writeFieldName('spsList');
                    jsonObj.writeStartArray();
                    for(Integer i = 0; i < B1.size(); i++){
                        if( B1[0].Amdocs_Caption2__c != null  ) { jsonObj.writeStartObject(); jsonObj.writeStringField('action', B1[i].Action__c); jsonObj.writeStringField('caption', B1[i].Amdocs_Caption2__c); jsonObj.writeEndObject(); }
                    }
                    jsonObj.writeEndArray();
                    
                    // This should include all promotional things - such as the discount for HBO FTG 3 year.
                    jsonObj.writeFieldName('componentList');
                    jsonObj.writeStartArray();
                    for(Integer i = 0; i < P.size(); i++){ jsonObj.writeStartObject(); jsonObj.writeStringField('action', P[i].Action__c); jsonObj.writeStringField('caption', P[i].Amdocs_Caption2__c); jsonObj.writeBooleanField('isPromotion', P[i].IsPromotion__c); jsonObj.writeEndObject();
                    }
                    jsonObj.writeEndArray();
                jsonObj.writeEndObject();
            jsonObj.writeEndObject();
        String finalJSON = jsonObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));

if (!Test.isRunningTest()){ HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateBulk?sc=SS&lo=EN&ca=SF'; request.setEndPoint(endpoint); request.setBody(jsonObj.getAsString()); request.setTimeout(101000); request.setHeader('Content-Type', 'application/json'); request.setMethod('POST'); String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader);request.setHeader('Accept', 'application/json'); request.setHeader('User-Agent', 'SFDC-Callout/45.0'); HttpResponse response = new HTTP().send(request); if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { String strjson = response.getbody(); JSONParser parser = JSON.createParser(strjson); parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class); if( obj.equipmentList != null ) { for(Integer i = 0; i < obj.equipmentList.size(); i++){
                 System.debug(response.toString());
                 System.debug('STATUS:'+response.getStatus());
                 System.debug('STATUS_CODE:'+response.getStatusCode());
                 System.debug(response.getBody());    
        // Make calls to nextToken()  // to point to the second // start object marker.
        // Retrieve the Person object from the JSON string.
        System.debug('DEBUG 0 ======== 1st STRING: ' + strjson); System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID); System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus); System.debug('DEBUG 3 ======== obj.equipmentList: ' + obj.equipmentList);
        System.debug('DEBUG 3.5 ======== obj.equipmentList: ' + obj.equipmentList[i].receiverId);
            }
        }
        System.debug('DEBUG 4 ======== obj.spsList: ' + obj.spsList); System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages); System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);





// Head-End - Ceteral Equipment (No Smartbox)        
    if(obj.responseStatus == 'SUCCESS' && E.size() > 0 ){ if(obj.equipmentList[0].receiverID != null){ list<string> receverIds = new list<string>(); for(cls_equipmentList cc0 : obj.equipmentList){ if(cc0.receiverID != null){ receverIds.add(cc0.receiverID); } } if(receverIds != null) { List<Equipment__c> allEQ = [SELECT Id, Action__c, Name FROM Equipment__C WHERE Name in :receverIds ]; if( allEQ.Size() > 0 ) { for(Equipment__c currentEQ : allEQ){ if( currentEQ.Action__c == 'ADD' || currentEQ.Action__c == 'SWAP'){ currentEQ.Action__c = 'Active'; } else if( currentEQ.Action__c == 'REMOVE'){ currentEQ.Action__c = 'Removed'; } } update allEQ;
    }
}    
    } // End of " if(obj.equipmentList[0].receiverID != null) "
} // End of " if(obj.responseStatus == 'SUCCESS' && E.size() > 0 ) "


    // Tenant Bulk Equipment
    if(O[0].AmDocs_tenantType__c != 'Chewbacca') { if(obj.responseStatus == 'SUCCESS' && T.size() > 0 ){ if(obj.equipmentList[0].receiverID != null){ list<string> TreceverIds = new list<string>(); for(cls_equipmentList cct : obj.equipmentList){ if(cct.receiverID != null){ TreceverIds.add(cct.receiverID);
                        System.debug('DEBUG 8 ======== cc.receiverID: ' + cct.receiverID);
    }
}
   // For all Central Head End Equipment (No Smartbox)        
   if(TreceverIds != null) { List<Tenant_Equipment__c> allTEQ = [SELECT Id, Action__c, Name FROM Tenant_Equipment__c WHERE Name in :TreceverIds ]; if( allTEQ.Size() > 0 ) { for(Tenant_Equipment__c currentTEQ : allTEQ){ if( currentTEQ.Action__c == 'ADD'){ currentTEQ.Action__c = 'Active'; } else if( currentTEQ.Action__c == 'REMOVE'){ currentTEQ.Action__c = 'Removed'; } } update allTEQ;
   }
}    
        } // End of " if(obj.equipmentList[0].receiverID != null) "
    } // End of " if(obj.responseStatus == 'SUCCESS' && E.size() > 0 ) "
    }
    
    // Smartbox Equipment
    if(obj.responseStatus == 'SUCCESS' && S1.size() > 0 ){ if(obj.equipmentList[0].chassisSerialNumber != null || obj.equipmentList[0].receiverID != null || obj.equipmentList[0].serialNumber != null ){    list<string> SBoxCHIds = new list<string>(); list<string> SBoxBLIds = new list<string>(); list<string> SBoxRVIds = new list<string>(); for(cls_equipmentList cc : obj.equipmentList){ if(cc.chassisSerialNumber != null && cc.serialNumber == null) { SBoxCHIds.add(cc.chassisSerialNumber); }    if(cc.serialNumber != null) { SBoxBLIds.add(cc.serialNumber); } if(cc.receiverID != null)   { SBoxRVIds.add(cc.receiverID );  } }    List<Smartbox__c> allCHEQ = [SELECT Id, Action__c, Name, Chassis_Serial__c, Serial_Number__c, CAID__c FROM Smartbox__c WHERE  (Chassis_Serial__c in :SBoxCHIds OR Serial_Number__c in :SBoxBLIds OR CAID__c in :SBoxRVIds )]; if( allCHEQ.Size() > 0 ) { for(Smartbox__c currentCHEQ : allCHEQ){ if( currentCHEQ.Action__c == 'ADD' || currentCHEQ.Action__c == 'SWAP'){ currentCHEQ.Action__c = 'Active'; } else if( currentCHEQ.Action__c == 'REMOVE'){ currentCHEQ.Action__c = 'Removed'; }   }  update allCHEQ;
            }
        } // End of " if(obj.equipmentList[0].receiverID != null) " 
    } // End of " if(obj.responseStatus == 'SUCCESS' && T "
    
    // Products - Component
    if(obj.responseStatus == 'SUCCESS' && P.Size() > 0) {
// 10-31-2018       if(obj.componentList[0].caption != null){
        if(obj.componentList != null){
            list<string> PIds = new list<string>();
            if(obj.componentList[0].caption != null){ 
                for(cls_componentList cl : obj.componentList){ 
                    PIds.add(cl.caption); }
                }
                List<OpportunityLineItem> allCOLI = [SELECT Id, Action__c, Name, AmDocs_Caption2__c, Amdocs_ProductID__c, OpportunityID FROM OpportunityLineItem WHERE OpportunityID = : Id AND AmDocs_Caption2__c in :PIds ];
                if( allCOLI.Size() > 0 ) { 
                    for(OpportunityLineItem currentCOLI : allCOLI){ 
                        if( currentCOLI.Action__c == 'ADD'){ 
                            currentCOLI.Action__c = 'Active'; 
                        }
                        else if( currentCOLI.Action__c == 'REMOVE'){
                            currentCOLI.Action__c = 'Removed'; 
                       } 
                   } update allCOLI;
            }
        }
    } // End of " if(obj.responseStatus == 'SUCCESS' && P.size() > 0 ) "

    // Products - SPS
    if(obj.responseStatus == 'SUCCESS' && B1.size() > 0) { for(Integer i = 0; i < B1.size(); i++){ OpportunityLineItem bu = new OpportunityLineItem(); bu.id=B1[i].id;  if(B1[i].Action__c == 'ADD') {bu.Action__c = 'Active';} if(B1[i].Action__c == 'REMOVE') {bu.Action__c = 'Removed';} update bu;
         }
    } // End of " if(obj.responseStatus == 'SUCCESS' && P.size() > 0 ) "

    // Products - CORE
    if(obj.responseStatus == 'SUCCESS') { for(Integer i = 0; i < C.size(); i++) { OpportunityLineItem cu = new OpportunityLineItem(); cu.id=C[i].id; if(C[i].Action__c == 'ADD') {cu.Action__c = 'Active'; } else if(C[i].Action__c == 'REMOVE') {cu.Action__c = 'Removed'; } update cu;
                  }
    } // End of " if(obj.responseStatus == 'SUCCESS' && P.size() > 0 ) "
    
//    Opportunity sbc = new Opportunity(); sbc.id=id; sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); sbc.AmDocs_Order_ID__c=obj.orderID; sbc.API_Status__c=String.valueOf(response.getStatusCode()); if(obj.informationMessages != Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus + ' ' +obj.informationMessages[0].errorDescription; sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; }        else if(obj.informationMessages == Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus; sbc.AmDocs_Transaction_Code__c=obj.responseStatus; }        else if(obj.responseStatus == Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK ERROR Request Rejected'; sbc.AmDocs_Transaction_Code__c='ERROR';}         else if ( obj.informationMessages != null) { if (obj.informationMessages.size() > 1)            {sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode + ' ' +obj.informationMessages[1].errorCode; sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription + '\n\n' +obj.informationMessages[1].errorDescription +'\n\n';}        else if (obj.informationMessages.size() < 2) {sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode; sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription;}    }  update sbc;
    Opportunity sbc = new Opportunity(); { sbc.id=id; sbc.AmDocs_Order_ID__c=obj.orderID; sbc.API_Status__c=String.valueOf(response.getStatusCode()); if(obj.informationMessages != Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus + ' ' +obj.informationMessages[0].errorDescription; sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; }        else if(obj.informationMessages == Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus; sbc.AmDocs_Transaction_Code__c=obj.responseStatus; }        else if(obj.responseStatus == Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK ERROR Request Rejected'; sbc.AmDocs_Transaction_Code__c='ERROR';}         else if ( obj.informationMessages != null) { if (obj.informationMessages.size() > 1)            {sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode + ' ' +obj.informationMessages[1].errorCode; sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription + '\n\n' +obj.informationMessages[1].errorDescription +'\n\n';}        else if (obj.informationMessages.size() < 2) {sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode; sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription;}  }  update sbc; }
        System.debug('sbc'+sbc);

    API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id;       apil2.Object__c='Opportunity';       apil2.Status__c='SUCCESS';       apil2.Results__c=strjson;       apil2.API__c='Bulk Update';       apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;       apil2.User__c=UserInfo.getUsername();   insert apil2;  }
 }
        else if (response.getStatusCode() <= 200 && response.getStatusCode() >= 600) {
         String strjson = response.getbody();
         Opportunity sbc = new Opportunity();{ sbc.id=id; sbc.API_Status__c='API ERROR - UPDATE BULK ' +String.valueOf(response.getStatusCode()); update sbc; }
         API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id;       apil2.Object__c='Opportunity';       apil2.Status__c='ERROR';       apil2.Results__c=strjson;       apil2.API__c='Bulk Update';       apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;       apil2.User__c=UserInfo.getUsername();   insert apil2;  }
         }
        
        }
    }
    }
}