public class CMaster_CalloutClass_CreateCustomerTAPWU {

public class person {
    public String loginId;
}

    @future(callout=true)

    public static void CMasterCalloutCreateCustomerTAPWU(String Id) {

 list<Tenant_Account__c> C = [select Id, API_ID__c, LoginId__c, Username__c, Password__c from Tenant_Account__c where Id =:id Limit 1];    
        if( C.size() < 1 || ((C[0].Username__c == '' || C[0].Username__c == Null) || (C[0].Password__c == '' || C[0].Password__c == Null )) || (C[0].LoginId__c == '' || C[0].LoginId__c == Null)) {
            Tenant_Account__c sbc = new Tenant_Account__c();
                sbc.id=id;
                sbc.API_Status__c='Error';
                sbc.Amdocs_Transaction_Code__c='Missing Information';
//                sbc.Amdocs_Transaction_Description__c='TA Password Update requires the Username, Password, and Login ID. Your data: Username = ' +C[0].Username__c + ' Password = ' +C[0].Password__c + ' LoginId = ' +C[0].LoginId__c;

                if( (C[0].Username__c == '' || C[0].Username__c == Null )){
                    sbc.Amdocs_Transaction_Description__c='Update/Set DISH Anywhere password requires the Username, Password, and a DISH Anywhere Login. Your data is missing the Username';
                }
                if( (C[0].Password__c == '' || C[0].Password__c == Null )){
                    sbc.Amdocs_Transaction_Description__c='Update/Set DISH Anywhere password requires the Username, Password, and a DISH Anywhere Login. Your data is missing the Password';
                }
                if( (C[0].LoginId__c == '' || C[0].LoginId__c == Null )){
                    sbc.Amdocs_Transaction_Description__c='Update/Set DISH Anywhere password requires the Username, Password, and a DISH Anywhere Login. Your data is missing a registered DISH Anywhere Login';
                }
            update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Tenant_Account__c sbc = new Tenant_Account__c();
                        sbc.id=id;
                        sbc.API_Status__c='Error';
                        sbc.Amdocs_Transaction_Code__c='Missing End Point';
                        sbc.Amdocs_Transaction_Description__c='ERROR: CMaster_CalloutClass_TAPassword Update is missing a valid ENDPOINT';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);
                
                JSONGenerator jsonObj = JSON.createGenerator(true);
                    jsonObj.writeStartObject();
//                            jsonObj.writeStringField('domain', 'DISH');
//                            if(C[0].PartyId__c != '' && C[0].PartyId__C != Null) {
//                                jsonObj.writeStringField('partyId', C[0].PartyId__c);
//                            }
//                            if(C[0].Username__c != '' && C[0].Username__c != Null) {
//                                jsonObj.writeStringField('userName', C[0].Username__c);
//                            }
                            if(C[0].Password__c != Null && C[0].Password__c != '') {
                                jsonObj.writeStringField('password', C[0].Password__c);
                            }
                            jsonObj.writeBooleanField('passwordChangeRequired', false);
                            jsonObj.writeBooleanField('passwordChangedByAdmin', true);
                            jsonObj.writeBooleanField('passwordReuseRuleOutput', true);
                        jsonObj.writeEndObject();
    
                String finalJSON = jsonObj.getAsString();
                    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
      
                HttpRequest request = new HttpRequest();
                    String endpoint = CME[0].EndPoint__c+'/cm-registration/login/';
                    request.setEndPoint(endpoint+C[0].LoginId__c);
                    request.setBody(jsonObj.getAsString());
                    request.setHeader('Content-Type', 'application/json');
                    request.setHeader('Customer-Facing-Tool', 'Salesforce');
                    request.setHeader('User-ID', UserInfo.getUsername());
                    request.setMethod('PUT');
                    request.setTimeout(101000);
                        System.debug('DEBUG Get UserInfo: ' +UserInfo.getUsername());   
     
                    HttpResponse response = new HTTP().send(request);
                        System.debug(response.toString());
                        System.debug('STATUS:'+response.getStatus());
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug(response.getBody());

                        if (response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());   

                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());


JSONParser parser = JSON.createParser(strjson); person obj = (person)parser.readValueAs( person.class);

                       
                                Tenant_Account__c sbc = new Tenant_Account__c();{
                                    sbc.id=id;
                                    sbc.API_Status__c='SUCCESS';
                                    sbc.Amdocs_Transaction_Description__c='DISH Anywhere PASSWORD updated!';
                                    sbc.loginId__c=obj.loginId;
                                    update sbc;
                                }
                                API_Log__c apil = new API_Log__c();{
                                    apil.Record__c=id;
                                    apil.Object__c='Tenant Account';
                                    apil.Status__c='SUCCESS';
                                    apil.Results__c=response.getBody();
                                    apil.API__c='TA Subscriber Login Password Updated';
                                    apil.User__c=UserInfo.getUsername();
                                insert apil;
                                }
                        }

                        if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());

                                Tenant_Account__c sbc2 = new Tenant_Account__c();{
                                    sbc2.id=id;
                                    sbc2.API_Status__c='ERROR';
                                    sbc2.Amdocs_Transaction_Description__c=response.getBody();
                                    update sbc2;
                                }
                                API_Log__c apil2 = new API_Log__c();{
                                    apil2.Record__c=id;
                                    apil2.Object__c='Tenant Account';
                                    apil2.Status__c='ERROR';
                                    apil2.Results__c=response.getBody();
                                    apil2.API__c='TA Subscriber Login Password Update';
                                    apil2.User__c=UserInfo.getUsername();
                                insert apil2;
                                }
                }
            }
        }
    }
}