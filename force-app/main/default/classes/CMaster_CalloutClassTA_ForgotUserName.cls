public class CMaster_CalloutClassTA_ForgotUserName {

public class person {
    Public cls_Search [] Search;
}

class cls_Search {
  Public cls_logins [] logins;
  Public cls_phoneContacts [] phoneContacts;
}
class cls_logins {
    public String userName;
    public String loginId;
} 
class cls_phoneContacts {
    public String phoneNumber;
}

    
    @future(callout=true)
    
    public static void CMasterCalloutSearchTAFrgtUserName(String Id) {

 list<Tenant_Account__c> C = [select Id,  First_Name__c, Last_Name__c, Email_Address__c, API_ID__c, Phone_Number__c, AmDocs_ServiceID__c, PIN__c, dishCustomerId__c, partyId__c from Tenant_Account__c where Id =:id Limit 1];    
//        if( C.size() < 1 || ((C[0].First_Name__c == '' || C[0].First_Name__c == Null) || (C[0].Last_Name__c == '' || C[0].Last_Name__c == Null )) || ( C[0].API_ID__c == '' || C[0].API_ID__c == Null)) {
           if( C.size() < 1 || ( C[0].API_ID__c == '' || C[0].API_ID__c == Null)) {
            Tenant_Account__c sbc = new Tenant_Account__c();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Amdocs_Transaction_Code__c='';
//                sbc.Amdocs_Transaction_Description__c='CMaster -TA Customer Forgot Username-, CM needs requires the Customer ID (Customer APID), or First & Last Name of the billing contact along with thier Email and PIN/Last4 Number. Your data: First Name = ' + C[0].First_Name__c + ', LastName = ' +C[0].Last_Name__c + ', Email = ' + C[0].Email_Address__c + ', Pin # = ' + C[0].PIN__c + ', AP-ID = ' +C[0].API_ID__c;
//                if( (C[0].First_Name__c == '' || C[0].First_Name__c == Null )) {
//                    sbc.Amdocs_Transaction_Description__c='DISH Anywhere Forgot/FIND Username requires the Customer APID, First Name, and Last Name. Your data is missing the First Name';
//                }
//                if( (C[0].Last_Name__c == '' || C[0].Last_Name__c == Null )) {
//                    sbc.Amdocs_Transaction_Description__c='DISH Anywhere Forgot/FIND Username requires the Customer APID, First Name, and Last Name. Your data is missing the Last Name';
//                }
//                if( (C[0].Email_Address__c == '' || C[0].Email_Address__c == Null)) {
//                    sbc.Amdocs_Transaction_Description__c='DISH Anywhere Forgot/FIND Username requires the Customer APID, First Name, Last Name, and Email Address. Your data is missing the Email Address';
//                }
                if( (C[0].API_ID__c == '' || C[0].API_ID__c == Null )) {
                    sbc.Amdocs_Transaction_Description__c='DISH Anywhere Forgot/FIND Username requires the Customer APID. Your data is missing the APID';
                }
            update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Tenant_Account__c sbc = new Tenant_Account__c();
                        sbc.id=id;
                        sbc.API_Status__c='';
                        sbc.Amdocs_Transaction_Code__c='';
                        sbc.Amdocs_Transaction_Description__c='ERROR: CMaster_CalloutClassOPP_Search is missing a valid ENDPOINT';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);            

        HttpRequest request = new HttpRequest();
            String endpoint = CME[0].Endpoint__c+'/cm-search';
            request.setEndPoint(endpoint +'/customerParties?extRefNum='+C[0].API_ID__c);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Customer-Facing-Tool', 'Salesforce');
            request.setHeader('User-ID', UserInfo.getUsername());
            request.setMethod('GET');
            request.setTimeout(101000);
     
            HttpResponse response = new HTTP().send(request);
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
                System.debug('STATUS_CODE:'+response.getStatusCode());

                String strjson1 = response.getbody();
                String strjson = '{ "Search": '+strjson1 +'}';
//                String strjson = response.getbody();

                System.debug('DEBUG Get body:  ' +response.getBody());
                
           if(response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
               System.debug('DEBUG 0 ======== strjson: ' + strjson);

        
           JSONParser parser = JSON.createParser(strjson);
           parser.nextToken();
           person obj = (person)parser.readValueAs( person.class); 
 
                Tenant_Account__c sbc = new Tenant_Account__c(); {
                   sbc.id=id;
                   sbc.API_Status__c='SUCCESS';
                   sbc.Amdocs_Transaction_Description__c='Search for the DISH Anywhere Username was successfull!';
                   if ( obj.Search[0].logins != Null) {
                       sbc.Username__c=obj.Search[0].logins[0].userName; sbc.LoginId__c=obj.Search[0].logins[0].loginId;
                   }
                   if ( obj.Search[0].phoneContacts != Null && ( C[0].Phone_Number__c == Null || C[0].Phone_Number__c == '' )) {
                       sbc.Phone_Number__c=obj.Search[0].phoneContacts[0].phoneNumber;
                   }
               update sbc;
               }
               API_Log__c apil = new API_Log__c();{
                   apil.Record__c=id;
                   apil.Object__c='Tenant Account';
                   apil.Status__c='SUCCESS';
                   apil.Results__c=response.getBody();
                   apil.API__c='Search Subscriber Username';
                   apil.User__c=UserInfo.getUsername();
               insert apil;
               }
           }

           if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600){
               System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
               System.debug('DEBUG 0 ======== strjson: ' + strjson);
               System.debug('DEBUG Get body:  ' +response.getBody());
               Tenant_Account__c sbc2 = new Tenant_Account__c(); {
                   sbc2.id=id;
                   sbc2.API_Status__c='ERROR';
                   sbc2.Amdocs_Transaction_Description__c='Search for DISH Anywhere Username failed: ' +response.getBody();
               update sbc2;
               }
               API_Log__c apil2 = new API_Log__c();{
                   apil2.Record__c=id;
                   apil2.Object__c='Tenant Account';
                   apil2.Status__c='ERROR';
                   apil2.Results__c=response.getBody();
                   apil2.API__c='Search Subscriber Username';
                   apil2.User__c=UserInfo.getUsername();
               insert apil2;
               }
            }
        }
    }
}}