@isTest
private class Sites_Controler_MuteDCHome_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
    Account account_Obj = new Account(Name = 'Name368', CS_Revenue_Share__c = false, Call_Center_Outsourced__c = false, Send_Leads__c = false, Direct_Portal_Code__c = 'Direc254', Allow_Programming_Changes__c = false);
    Insert account_Obj; 
    test.stopTest();
  }
  static testMethod void test_getNameQuery_UseCase1(){
    List<Account> account_Obj  =  [SELECT Name,Direct_Portal_Code__c from Account];
    System.assertEquals(true,account_Obj.size()>0);
    Sites_Controler_MuteDCHome obj01 = new Sites_Controler_MuteDCHome();
    obj01.executeSearch = 'test data';
    obj01.nameQuery = 'test data';
    obj01.accounts = account_Obj;
    obj01.a = account_Obj;
    obj01.acct = account_Obj[0];
    obj01.error = 'test data';
    obj01.getNameQuery();
  }
}