/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 22 July 2020
Created for: Milestone# MS-001252
Description: Test class for userAccessCheck.apxc
-------------------------------------------------------------*/

@IsTest
public class userAccessCheckTest {
    public static testMethod void testMethod1() {
        List<userAccessCheck.FlowInput> ListOfFlowInputs = new List<userAccessCheck.FlowInput>();
        userAccessCheck.FlowInput FlowInpObj = new userAccessCheck.FlowInput();
        FlowInpObj.buttonName = 'Test';
        ListOfFlowInputs.add(FlowInpObj);
        
        List<userAccessCheck.Flowoutput> checkAccess = userAccessCheck.partnerUserAccessCheck(ListOfFlowInputs);
        system.assertEquals(checkAccess[0].userHaveAccess , false);
        
        userAccessCheck.userAccessCheckResponse getAccess = userAccessCheck.getCurrentUserAccess('Test','Test');
        system.assertEquals(getAccess.currentUserHaveAccess , false);
    }
}