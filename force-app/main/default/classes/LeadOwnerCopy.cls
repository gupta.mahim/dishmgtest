@isTest public with sharing class 
LeadOwnerCopy { static testMethod void testOwnerCopy() {
 string U1 = '00560000000nTD3';

        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');
        
        insert L;
        
       L = [SELECT Status FROM Lead WHERE Id =:L.Id];
       System.debug('Status after trigger fired: ' + L.Status);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Open', L.Status);
 
 } }