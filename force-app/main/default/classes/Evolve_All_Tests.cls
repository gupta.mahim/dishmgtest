@isTest
private class Evolve_All_Tests { 
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.LeadSource='Test Place';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.APID__c='123';
            opp1.UUID__c='0iZZf1C5ONQ9CwWV5YbDhAcjdbHU9uNH';
        insert opp1;
        
        Smartbox__c sb = new Smartbox__c();
            sb.Opportunity__c = opp1.Id;
            sb.CAID__c = 'R345678912';
            sb.SmartCard__c = 'S345678912';
            sb.Location__c = 'C';
            sb.Action__c = 'ADD';
            sb.Serial_Number__c = 'LALPFB001327';
            sb.Chassis_Serial__c = 'LALPFZ00277A';
            sb.AmDocs_No_Send__c = false;
            sb.NoTrigger__c = true;
        insert sb;

        Smartbox__c sb2 = new Smartbox__c();
            sb2.Opportunity__c = opp1.Id;
            sb2.Location__c = 'C';
            sb2.Action__c = 'ADD';
            sb2.Serial_Number__c = 'LALPFZ00277A';
            sb2.Chassis_Serial__c = 'LALPFZ00277A';
            sb2.AmDocs_No_Send__c = false;
            sb2.NoTrigger__c = true;
        insert sb2;
        
        Evolve__c ev1 = new Evolve__c();
            ev1.Smartbox__c = sb2.Id;
            ev1.HREF__c = 'HREF123';
            ev1.MAC_Address__c = '11:BB:22:CC:33';
        insert ev1;
        
    }



  static testMethod void Evolve_PostReBootTest_1(){
    Evolve__c con1 = [Select Id FROM Evolve__c WHERE MAC_Address__c = '11:BB:22:CC:33' Limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"Empty":{"Empty":"Empty"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
                Evolve_POSTReboot.MakeCallout(con1.Id); 
            Test.stopTest(); 
        }

  static testMethod void Evolve_PostReBootTest_2(){
    Evolve__c con1 = [Select Id FROM Evolve__c WHERE MAC_Address__c = '11:BB:22:CC:33' Limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(400,
                                                 'Complete',
                                                 '{"Empty":{"Empty":"Empty"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
                Evolve_POSTReboot.MakeCallout(con1.Id); 
            Test.stopTest(); 
        }
        
 static testMethod void Evolve_GetEvolveEquipmentTest_1(){
    Smartbox__c con1 = [Select Id FROM Smartbox__c WHERE Serial_Number__c = 'LALPFZ00277A' Limit 1];
       Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"devices":[{"device_uid":"ABCDEF_98:93:CC:A7:58:E2","Online":"false","body":{"HOST_MAC":"98:93:CC:A7:58:E1","HOST_IP":"","ROOM":"Room-885","ETH_MAC":"98:93:CC:A7:58:E2","WIFI_MAC":"30:A9:DE:D9:91:32","CONNECTION_TYPE":"Ethernet","ETH_IP":"192.168.1.155","WIFI_IP":"","SCAN_TYPE":"QAM","VERSION":"1.1.1.4","SERIAL_NUM":"LGLTEC03977K??D?????????????????????????"}}]}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
                Evolve_GETEvolves.MakeCallout(con1.Id); 
            Test.stopTest(); 
        }
 static testMethod void Evolve_GetEvolveEquipmentTest_2(){
    Smartbox__c con1 = [Select Id FROM Smartbox__c WHERE Serial_Number__c = 'LALPFZ00277A' Limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(300,
                                                 'Complete',
                                                 '{"devices":[{"device_uid":"ABCDEF_98:93:CC:A7:58:E2","Online":"false","body":{"HOST_MAC":"98:93:CC:A7:58:E1","HOST_IP":"","ROOM":"Room-885","ETH_MAC":"98:93:CC:A7:58:E2","WIFI_MAC":"30:A9:DE:D9:91:32","CONNECTION_TYPE":"Ethernet","ETH_IP":"192.168.1.155","WIFI_IP":"","SCAN_TYPE":"QAM","VERSION":"1.1.1.4","SERIAL_NUM":"LGLTEC03977K??D?????????????????????????"}}]}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
                Evolve_GETEvolves.MakeCallout(con1.Id); 
            Test.stopTest(); 
        }
 static testMethod void Evolve_GetEvolveEquipmentTest_3(){
    Smartbox__c con1 = [Select Id FROM Smartbox__c WHERE Serial_Number__c = 'LALPFZ00277A' Limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(404,
                                                 'Complete',
                                                 '{"devices":[{"device_uid":"ABCDEF_98:93:CC:A7:58:E2","Online":"false","body":{"HOST_MAC":"98:93:CC:A7:58:E1","HOST_IP":"","ROOM":"Room-885","ETH_MAC":"98:93:CC:A7:58:E2","WIFI_MAC":"30:A9:DE:D9:91:32","CONNECTION_TYPE":"Ethernet","ETH_IP":"192.168.1.155","WIFI_IP":"","SCAN_TYPE":"QAM","VERSION":"1.1.1.4","SERIAL_NUM":"LGLTEC03977K??D?????????????????????????"}}]}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
                Evolve_GETEvolves.MakeCallout(con1.Id); 
            Test.stopTest(); 
        }
  static testMethod void Evolve_PostRemoveTest_1(){
    Evolve__c con1 = [Select Id FROM Evolve__c WHERE MAC_Address__c = '11:BB:22:CC:33' Limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(400,
                                                 'Complete',
                                                 '{"Empty":{"Empty":"Empty"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
                Evolve_RemoveDevice.MakeCallout(con1.Id); 
            Test.stopTest(); 
        }
  static testMethod void Evolve_PostRemoveTest_2(){
    Evolve__c con1 = [Select Id FROM Evolve__c WHERE MAC_Address__c = '11:BB:22:CC:33' Limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"Empty":{"Empty":"Empty"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
                Evolve_RemoveDevice.MakeCallout(con1.Id); 
            Test.stopTest(); 
        }        

}