public class Verizon_CalloutClass_CreateAccount {

    @future(callout=true)

    public static void makeCalloutCreateAccount(String Id) {

// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];
    list<Verizon__c> VZ = [select id, VID__c, M2M_Token__c, VZ_M2M_Token__c, IS_M2M_Token__c, IS_VZ_Token__c, CreatedDate from Verizon__c 
                           where VID__c = :id AND IS_M2M_Token__c = true ORDER BY CreatedDate DESC LIMIT 1 ];
        System.debug('RESULTS of the LIST lookup to the Verizon object' +VZ);
     
    list<Verizon__c> VZ2 = [select id, VID__c, M2M_Token__c, VZ_M2M_Token__c, IS_M2M_Token__c, IS_VZ_Token__c, CreatedDate from Verizon__c 
                            where VID__c = :id AND IS_VZ_Token__c = true ORDER BY CreatedDate DESC LIMIT 1 ];
        System.debug('RESULTS of the LIST lookup to the Verizon object' +VZ2);
     
    list<Smartbox__c> SB1 = [select id, Verizon_ServiceState__c, kind__c, Opportunity__c, ModemID__c from Smartbox__c 
                             where Id = :id AND ModemID__c != '' LIMIT 1 ];
        System.debug('RESULTS of the LIST lookup to the Verizon object' +SB1);

    list<Opportunity> OP1 = [select id, Zip__c, Address__c, City__c, State__c,  CSG_Account_Number__c from Opportunity 
                             where Id = :SB1[0].Opportunity__c LIMIT 1 ];
        System.debug('RESULTS of the LIST lookup to the Verizon object' +SB1);
        
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
    jsonObj.writeStringField('accountName', 'DISH NETWORK SRVCS');
    jsonObj.writeFieldName('devices');
    jsonObj.writeStartArray();
        jsonObj.writeStartObject();
        jsonObj.writeFieldName('deviceIds');
        jsonObj.writeStartArray();
            jsonObj.writeStartObject();
                jsonObj.writeStringField('id', SB1[0].ModemID__c);
                jsonObj.writeStringField('kind', SB1[0].kind__c);
            jsonObj.writeEndObject();
        jsonObj.writeEndArray();            
        jsonObj.writeEndObject();
    jsonObj.writeEndArray();
    jsonObj.writeStringField('mdnZipCode', OP1[0].Zip__c);
    jsonObj.writeStringField('servicePlan', 'M2M');
    jsonObj.writeFieldName('primaryPlaceOfUse');
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('address');
            jsonObj.writeStartObject();
                jsonObj.writeStringField('addressLine1', OP1[0].Address__c);
                jsonObj.writeStringField('city', OP1[0].City__c);
                jsonObj.writeStringField('country', 'USA');
                jsonObj.writeStringField('state', OP1[0].State__c);
                jsonObj.writeStringField('zip', OP1[0].Zip__c);
            jsonObj.writeEndObject();
        jsonObj.writeFieldName('customerName');
            jsonObj.writeStartObject();
                jsonObj.writeStringField('firstname', 'Smartbox');
                jsonObj.writeStringField('lastname', OP1[0].CSG_Account_Number__c);
                jsonObj.writeStringField('middlename', 'None');
                jsonObj.writeStringField('title', 'Mr');
            jsonObj.writeEndObject();
                jsonObj.writeStringField('publicIpRestriction', 'Restricted');
    jsonObj.writeEndObject();
        
        if (!Test.isRunningTest()) {    HttpRequest request = new HttpRequest();        String endpoint = 'https://thingspace.verizon.com/api/m2m/v1/devices/actions/activate';        request.setEndPoint(endpoint);        request.setBody(jsonObj.getAsString());        request.setHeader('Content-Type', 'application/json');        request.setMethod('POST');        String authorizationHeader = 'Bearer ' +VZ[0].M2M_Token__c ;        String VZHeader = VZ2[0].VZ_M2M_Token__c ;        request.setHeader('VZ-M2M-Token', VZHeader);           request.setHeader('Authorization', authorizationHeader);            HttpResponse response = new HTTP().send(request);            System.debug(response.toString());            System.debug('STATUS:'+response.getStatus());            System.debug('STATUS_CODE:'+response.getStatusCode());            System.debug(response.getBody());        if (response.getStatusCode() == 200) {        Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());        List<String> vztoken = new List<String>();            vztoken.addAll(results.keySet());             System.debug('Received the following vztoken info: ' +vztoken);           for (Object Verizon: vztoken) {           System.debug(vztoken);           Verizon__c sbc = new Verizon__c();           sbc.name=String.valueOf(results.values());           sbc.vid__c=id;           sbc.Smartbox__c=id;           sbc.response_code__c=String.valueOf(results.values());           sbc.Action__c='Suspend';           insert sbc;
            System.debug('=== all keys in the map: ' + results.keySet());
            System.debug('=== all values in the map (as a List): ' + results.values());
            System.debug('=== all values in the map (as a List): ' + results.size());
            

            
         System.debug('sbc'+sbc);
        }
    }        
}
}
}