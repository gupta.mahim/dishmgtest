public class AmDocs_CalloutClass_BulkHESCSwap {

public class person {
    public String serviceID;
    public String ownerServiceId;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_informationMessages[] informationMessages;
    public cls_ImplUpdateBulkRestOutput [] ImplUpdateBulkRestOutput;
}

class cls_ImplUpdateBulkRestOutput{
    public String serviceID;
    public String orderID;
    public String responseStatus;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String location;
    public String smartCardID;
    public String leaseInd;
    public String chassisSerialNumber;
    public String serialNumber;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}


    @future(callout=true)

    public static void AmDocsMakeCalloutBulkHESCSwap(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

list<Equipment__c> E = [select id, Receiver_S__c, Name, Location__c, Action__c, Opportunity__c from Equipment__c where Id = :id AND Location__c = 'C' AND Action__c = 'SWAP PENDING'];
     System.debug('RESULTS of the LIST lookup to the Equipment object' +E);

String SO = [select Opportunity__c from Equipment__c where Id = :id LIMIT 1 ].Opportunity__c ;
     System.debug('RESULTS of the LIST SO String to the Opp object ' +SO);

list<Opportunity> O = [select id, AmDocs_ServiceID__c, Name from Opportunity where Id = :SO LIMIT 1 ];
     System.debug('RESULTS of the LIST lookup to the Opp object S From SOs ID ' +O);
          
 list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
  if(O.size() > 0) {
      jsonObj.writeStartObject();
          jsonObj.writeFieldName('ImplUpdateBulkRestInput');
              jsonObj.writeStartObject();
                  jsonObj.writeStringField('orderActionType', 'CH');
                  jsonObj.writeStringField('reasonCode', 'CREQ');

                // This is EQUIPMENT - Centeralized ONLY
                jsonObj.writeFieldName('equipmentList');
                    jsonObj.writeStartArray();   
                        for(Integer i = 0; i < E.size(); i++){
                            jsonObj.writeStartObject();
                                jsonObj.writeStringField('action', 'SWAP');
                                    system.debug( 'DEBUG JSON 2 : SWAP');
                                jsonObj.writeStringField('receiverID', E[i].Name);
                                    system.debug( 'DEBUG JSON 3 :' +E[i].Name);
                                jsonObj.writeStringField('smartCardID', E[i].Receiver_S__c);
                                    system.debug( 'DEBUG JSON 4 :' +E[i].Receiver_S__c);
                                jsonObj.writeStringField('location', E[i].Location__c);
                                    system.debug( 'DEBUG JSON 4 :' +E[i].Location__c);
                           jsonObj.writeEndObject();
                        }
                    jsonObj.writeEndArray();                  
                jsonObj.writeEndObject();
            jsonObj.writeEndObject();
        }
        String finalJSON = jsonObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));

if (!Test.isRunningTest()){ HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateBulk?sc=SS&lo=EN&ca=SF'; request.setEndPoint(endpoint); request.setBody(jsonObj.getAsString()); request.setTimeout(101000); request.setHeader('Content-Type', 'application/json'); request.setMethod('POST'); String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader); HttpResponse response = new HTTP().send(request); if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { String strjson = response.getbody(); JSONParser parser = JSON.createParser(strjson); parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class); if( obj.equipmentList != null ) { for(Integer i = 0; i < obj.equipmentList.size(); i++){
                 System.debug(response.toString());
                 System.debug('STATUS:'+response.getStatus());
                 System.debug('STATUS_CODE:'+response.getStatusCode());
                 System.debug(response.getBody());    
        System.debug('DEBUG 0 ======== 1st STRING: ' + strjson); System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID); System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus); System.debug('DEBUG 3 ======== obj.equipmentList: ' + obj.equipmentList);
        System.debug('DEBUG 3.5 ======== obj.equipmentList: ' + obj.equipmentList[i].receiverId);
            }
        }
        System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages); System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);



    Equipment__c sbc = new Equipment__c(); sbc.id=id; sbc.AmDocs_Order_ID__c=obj.orderID; sbc.API_Status__c=String.valueOf(response.getStatusCode());
        if(obj.orderID != Null) {sbc.Action__c = 'Active';}
        else if(obj.orderID == Null) {sbc.Action__c = 'SWAP FAILED';}
        if(obj.informationMessages != Null) { sbc.AmDocs_Transaction_Description__c='SMARTCARD SWAP ' +obj.responseStatus + ' ' +obj.informationMessages[0].errorDescription; sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; }
        else if(obj.informationMessages == Null) { sbc.AmDocs_Transaction_Description__c='SMARTCARD SWAP ' +obj.responseStatus; sbc.AmDocs_Transaction_Code__c=obj.responseStatus; }
        else if(obj.responseStatus == Null) { sbc.AmDocs_Transaction_Description__c='SMARTCARD SWAP ERROR Request Rejected'; sbc.AmDocs_Transaction_Code__c='ERROR';}
        update sbc; System.debug('sbc'+sbc); }
        else if (response.getStatusCode() <= 200 && response.getStatusCode() >= 600) { Equipment__c sbc = new Equipment__c(); sbc.id=id; sbc.API_Status__c='API ERROR - UPDATE BULK ' +String.valueOf(response.getStatusCode()); update sbc; }
        }
    }
}