public with sharing class DisplayMapController {
    
    @AuraEnabled (cacheable=true)
    public static List<Account> getAccountLocations(String cityNameInitial){
        String str = cityNameInitial + '%';
        return [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry 
                FROM Account 
                WHERE BillingCity LIKE :str];       
    }
    
}