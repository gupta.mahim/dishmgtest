global class ValidateTEAddressBatch implements Database.Batchable<sObject>, Database.AllowsCallouts 
{
    global String Query='';
    global ValidateTEAddressBatch(String q)
    {
        query=q;
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);

    }
    global void execute(Database.BatchableContext ctx, List<SObject> records)
    {
        ValidateAdressCtrl.validateTenantEquipBulk(records);
    }
    global void finish(Database.BatchableContext ctx)
    {
        
    }
}