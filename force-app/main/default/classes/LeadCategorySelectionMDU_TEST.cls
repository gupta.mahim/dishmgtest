@IsTest
private class LeadCategorySelectionMDU_TEST {
    static testMethod void validateLeadCategorySelectionMDU() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Apartment/Condo/Townhome',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

       insert L;
        
       L = [SELECT Category__c FROM Lead WHERE Id =:L.Id];
       System.debug('Category after trigger fired: ' + L.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L.Category__c);
    }
     static testMethod void validateLeadCategorySelectionMDU2() {
        Lead LL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Planned Unit Development',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL = [SELECT Category__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Category after trigger fired: ' + LL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LL.Category__c);
}

static testMethod void validateLeadCategorySelectionMDU4() {
        Lead LLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='University',
        Location_of_Service__c='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Category after trigger fired: ' + LLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionMDU5() {
        Lead LLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='School',
        Location_of_Service__c='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLL;
        
       LLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
     //  System.assertEquals('MDU', LLLLL.Category__c);
}

static testMethod void validateLeadCategorySelectionMDU7() {
        Lead L7= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Assisted Living',
        Location_of_Service__c ='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert L7;
        
       L7= [SELECT Category__c FROM Lead WHERE Id =:L7.Id];
       System.debug('Category after trigger fired: ' + L7.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L7.Category__c);
}
static testMethod void validateLeadCategorySelectionMDU8() {
        Lead L8= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Assisted Living',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert L8;
        
       L8= [SELECT Category__c FROM Lead WHERE Id =:L8.Id];
       System.debug('Category after trigger fired: ' + L8.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L8.Category__c);
}
static testMethod void validateLeadCategorySelectionMDU9() {
        Lead L9 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retirement Community',
        Location_of_Service__c ='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert L9;
        
       L9= [SELECT Category__c FROM Lead WHERE Id =:L9.Id];
       System.debug('Category after trigger fired: ' + L9.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L9.Category__c);
}
static testMethod void validateLeadCategorySelectionMDU10() {
        Lead L10 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retirement Community',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert L10;
        
       L10= [SELECT Category__c FROM Lead WHERE Id =:L10.Id];
       System.debug('Category after trigger fired: ' + L10.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L10.Category__c);
}
}