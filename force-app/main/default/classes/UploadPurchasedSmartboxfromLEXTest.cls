@IsTest
public class UploadPurchasedSmartboxfromLEXTest {
    public static testMethod void testReadFile() {
        
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        
        //Milestone# MS-001252 - 3Aug20 - Remove SeeAllData=True from this test class. Updated the below code accordingly  - Start
        //Document lstDoc = [select id,name,Body from Document where name = 'eqtest'];
        //String fileContent=lstDoc.Body.toString();
        //List<Smartbox__c> lexAccstoupload = UploadPurchasedSmartboxfromLEX.ReadFile(fileContent,newOpp.id);

        String equipData;
        list<String> equipTemp = new List<String>(2);
        equipTemp[0] = 'Receiver #,SmartCard #,Receiver Model,Programming Package,Channel Name,Channel Number,Satellite,Satellite Channel,Output Channel';
        equipTemp[1] = 'R32155533221,S321321321321,311,Essentials,HGTV,4136,118.7,123,52';
        
        for(String eD : equipTemp){
            equipData = equipData + eD +  '\n' ;  
        }
        
        List<Smartbox__c> lexAccstoupload = UploadPurchasedSmartboxfromLEX.ReadFile(equipData,newOpp.id);
        //Milestone# MS-001252 - End
        
        String str = JSON.serialize(lexAccstoupload);
        UploadPurchasedSmartboxfromLEX.save(str);
        
        UploadPurchasedSmartboxfromLEX.getUserAccess();
    }
}