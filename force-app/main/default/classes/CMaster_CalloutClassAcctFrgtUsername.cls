public class CMaster_CalloutClassAcctFrgtUsername {


public class person {
    Public cls_Search [] Search;
}

class cls_Search {
  Public cls_logins [] logins;
}
class cls_logins {
    public String userName;
    public String loginId;
} 

    
    @future(callout=true)
    
    public static void CMasterCalloutFrgtUsername(String Id) {

    list<Contact> C = [select Id, FirstName, LastName, Role__c, Email, Phone, ExtRefNum__c, Pin__c, dishCustomerId__c, partyId__c from Contact where Id =:id Limit 1];    
        if( C.size() < 1 || ((C[0].LastName == '' || C[0].LastName == Null) && (C[0].FirstName == '' || C[0].FirstName == Null )) || (C[0].Email == '' || C[0].Email == Null) || (C[0].Role__c != 'Billing Contact' && C[0].Role__c != 'Billing Contact (PR)' ) || ( C[0].ExtRefNum__c == '' || C[0].ExtRefNum__c == Null)) {
            Contact sbc = new contact();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Transaction_Code__c='';
//                sbc.Transaction_Description__c='Search Contact Username, CM requires the Customer ID (ExtRefNum), or First & Last Name, Email and the role must contain Billing Contact. Your data: ExtRefnum = ' +C[0].ExtRefNum__c  + ', First Name = ' + C[0].FirstName + ', LastName = ' +C[0].LastName + ', Email = ' + C[0].Email + ', Phone # = ' + C[0].Phone + ', Contact Role = ' +C[0].Role__c;

        if( (C[0].LastName == '' || C[0].LastName == Null )) {
            sbc.Transaction_Description__c='The search for contact Username requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Your data is missing the Last Name';
        }
        if( (C[0].FirstName == '' || C[0].FirstName == Null )) {
            sbc.Transaction_Description__c='The search for contact Username requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Your data is missing the First Name';
        }
        if( (C[0].Email == '' || C[0].Email == Null )) {
            sbc.Transaction_Description__c='The search for contact Username requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Your data is missing the Email Address';
        }
        if( (C[0].Role__c != 'Billing Contact' && C[0].Role__c != 'Billing Contact (PR)' )) {
            sbc.Transaction_Description__c='The search for contact Username requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Role is not Billing Contact';
        }
        if( (C[0].ExtRefNum__c == '' || C[0].ExtRefNum__c == Null)) {
            sbc.Transaction_Description__c='The search for contact Username requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Your data is missing Customer ID';
        }
        update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

        list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
            System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);
            
            System.debug('DEBUG 0 ' +C[0].Id);
            System.debug('DEBUG 1 ' +C[0].FirstName);
            System.debug('DEBUG 2 ' +C[0].LastName);
            System.debug('DEBUG 3 ' +C[0].Email);
            System.debug('DEBUG 4 ' +C[0].Role__c);
            System.debug('DEBUG 5 ' +C[0].Phone);
            System.debug('DEBUG 6 ' +C[0].Pin__c);
            System.debug('DEBUG 7 ' +C[0].ExtRefNum__c);
            System.debug('DEBUG 8 ' +C[0].dishCustomerId__c);
            System.debug('DEBUG 9 ' +C[0].partyId__c);

        HttpRequest request = new HttpRequest();
            String endpoint = CME[0].Endpoint__c+'/cm-search';
            request.setEndPoint(endpoint +'/customerParties?extRefNum='+C[0].ExtRefNum__c);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Customer-Facing-Tool', 'Salesforce');
            request.setHeader('User-ID', UserInfo.getUsername());
            request.setMethod('GET');
            request.setTimeout(101000);

            HttpResponse response = new HTTP().send(request);
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
                System.debug('STATUS_CODE:'+response.getStatusCode());
                System.debug('DEBUG Get body:  ' +response.getBody());

               String strjson1 = response.getbody();
               String strjson = '{ "Search": '+strjson1 +'}';
//                String strjson = response.getbody();
               System.debug('DEBUG 0 ======== strjson: ' + strjson);
                
           if(response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
       
           JSONParser parser = JSON.createParser(strjson);
           parser.nextToken();
           person obj = (person)parser.readValueAs( person.class); 
           if(obj.Search.size() > 0) {
                Contact sbc = new contact(); {
                   sbc.id=id;
                   sbc.API_Status__c='SUCCESS';
                   sbc.Transaction_Description__c='SEARCH for Username complete, Username found!';
                   sbc.Username__c=obj.Search[0].logins[0].userName;
                   sbc.LoginId__c=obj.Search[0].logins[0].loginId;
               update sbc;
           }
               }
           if(obj.Search.size() < 1) {
                Contact sbc = new contact(); {
                   sbc.id=id;
                   sbc.API_Status__c='SUCCESS';
                   sbc.Transaction_Description__c='SEARCH for Username complete, Username not found!';
                   sbc.Username__c=obj.Search[0].logins[0].userName;
                   sbc.LoginId__c=obj.Search[0].logins[0].loginId;
               update sbc;
           }
               }
               API_Log__c apil = new API_Log__c();{
                   apil.Record__c=id;
                   apil.Object__c='Contact';
                   apil.Status__c='SUCCESS';
                   apil.Results__c=response.getBody();
                   apil.API__c='Search Customer Username';
                   apil.User__c=UserInfo.getUsername();
               insert apil;
               }
           }

           if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600){
               System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
               System.debug('DEBUG 0 ======== strjson: ' + strjson);
               System.debug('DEBUG Get body:  ' +response.getBody());
               Contact sbc = new contact(); { 
                   sbc.id=id; sbc.API_Status__c='ERROR';
                   sbc.Transaction_Description__c='SEARCH Customer Username Not Found! ' +response.getBody();
               update sbc;
               }
               API_Log__c apil = new API_Log__c();{ 
                   apil.Record__c=id;
                   apil.Object__c='Contact'; 
                   apil.Status__c='ERROR'; 
                   apil.Results__c=response.getBody();
                   apil.API__c='Search Customer Username';
                   apil.User__c=UserInfo.getUsername();
               insert apil;
               }
           }
        }
    }
}