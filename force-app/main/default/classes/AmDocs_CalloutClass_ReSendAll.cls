public class AmDocs_CalloutClass_ReSendAll {

public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_informationMessages[] informationMessages;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String smartCardID;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}


    @future(callout=true)

    public static void AmDocsMakeCalloutReSendAll(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

    list<Opportunity> O = [select id, Name, AmDocs_ServiceID__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Reset__c, Category__c, AmDocs_Site_Id__c from Opportunity where Id = :id Limit 1];
        System.debug('RESULTS of the LIST lookup to the Opp object' +O);

 if(O[0].AmDocs_ServiceID__c == '' || O[0].AmDocs_ServiceID__c == Null ){
  
      Opportunity sbc = new Opportunity(); {
          sbc.id=id;
          sbc.API_Status__c='Error';
          sbc.Amdocs_Transaction_Code__c='Error';
          if( (O[0].AmDocs_ServiceID__c == '' || O[0].  AmDocs_ServiceID__c  == Null )) {
              sbc.Amdocs_Transaction_Description__c = 'Action requies a Subscription / Service ID. You are missing: Subscription / Service ID.';
          }
      update sbc;
      }
      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Opportunity';
          apil2.Status__c='ERROR';
          apil2.Results__c='Action requies a Service ID. You are missing: Service ID.';
          apil2.API__c='Trip / Resend ALL';
          apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
      }
  }
  
 if(O[0].AmDocs_ServiceID__c != '' && O[0].AmDocs_ServiceID__c != Null ){        
      Datetime dt1 = System.Now();
      list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateReSendAllTime__c from API_LOG__c where ServiceID__c = :O[0].AmDocs_ServiceID__c AND NextAvailableActivateReSendAllTime__c >= :dt1 Order By NextAvailableActivateReSendAllTime__c DESC Limit 1];
            
       if( (timer.size() > 0 )  || O[0].Reset__c == 'ChewbaccaIsTesting' ) {
           system.debug('DEBUG API LOG Timer size : ' +timer.size());
           system.debug('DEBUG API LOG Info' +timer);
           system.debug('DEBUG timer 1 : ' +dt1);

       Opportunity sbc = new Opportunity(); {
           sbc.id=id; sbc.API_Status__c='Error'; 
           sbc.Amdocs_Transaction_Code__c='Error'; 
           sbc.Amdocs_Transaction_Description__c='You must wait atleast (12) twelve hours between Resend / Trip ALL transactions'; 
            update sbc;
       }
       API_Log__c apil2 = new API_Log__c();{ 
           apil2.Record__c=id; 
           apil2.Object__c='Opportunity';
           apil2.Status__c='ERROR'; 
           apil2.Results__c='You must wait atleast (12) twelve hours between Trip / Resend transactions'; 
           apil2.API__c='Trip / Resend ALL'; 
           apil2.ServiceID__c=O[0].AmDocs_ServiceID__c; 
           apil2.User__c=UserInfo.getUsername(); 
           insert apil2; 
     }
   }   

   else if(timer.size() < 1) {    









    list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
        System.debug('RESULTS of the LIST lookup to the Amdocs Login object' +A);  
     
    JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartObject();
            jsonObj.writeFieldName('ImplResendEquipmentRestInput');

            jsonObj.writeStartObject();
                jsonObj.writeStringField('orderActionType', 'RL');
            jsonObj.writeEndObject();

    jsonObj.writeEndObject();
    
    String finalJSON = jsonObj.getAsString();
    System.debug('Jerry Debug 0 === request: ' + finalJSON);
 
   
    HttpRequest request = new HttpRequest(); 
        String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/resendEquipment?sc=SS&lo=EN&ca=SF'; 
        request.setEndPoint(endpoint); 
        request.setBody(jsonObj.getAsString());  
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('POST'); 
        for(Integer i = 0; i < A.size(); i++){ 
            String authorizationHeader = A[i].UXF_Token__c ; 
            request.setHeader('Authorization', authorizationHeader); 
        }
        
        HttpResponse response = new HTTP().send(request); 
            if (response.getStatusCode() == 200) { 
                String strjson = response.getbody(); 
                JSONParser parser = JSON.createParser(strjson); 
                parser.nextToken(); 
                parser.nextToken(); 
                parser.nextToken(); 
                person obj = (person)parser.readValueAs( person.class);
                System.debug('DEBUG ENDPOINT: ' +endpoint);         
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
                System.debug('STATUS_CODE:'+response.getStatusCode());
                System.debug(response.getBody());
                System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
                System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
                System.debug('DEBUG 3 ======== obj.equipmentList: ' + obj.equipmentList);
                System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
                System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);

                Opportunity sbc = new Opportunity(); {
                    sbc.id=id; 
                    sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
                    sbc.AmDocs_Order_ID__c=obj.orderID; 
                    sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
                    if(obj.OrderID != Null ) { 
                        sbc.AmDocs_Transaction_Description__c='TRIP/HIT COMPLETED'; 
                        sbc.AmDocs_Transaction_Code__c='SUCCESS'; 
                    }
                    if(obj.informationMessages != Null) { 
                        sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription; 
                        sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
                    }
                    update sbc;
                    System.debug('sbc'+sbc); 
                    }
                API_Log__c apil2 = new API_Log__c();{
                    apil2.Record__c=id;
                    apil2.Object__c='Opportunity';
                    apil2.Status__c='Success';
                    apil2.Results__c=strjson + ' ' + system.now();
                    apil2.API__c='Trip / Resend ALL';
                    apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
                    apil2.User__c=UserInfo.getUsername();
                insert apil2;
      }
                }
                else if (response.getStatusCode() != 200) { 
                    Opportunity sbc = new Opportunity(); {
                        sbc.id=id; sbc.API_Status__c=String.valueOf(response.getStatusCode());
                    update sbc;
                    }
                    API_Log__c apil2 = new API_Log__c();{
                        apil2.Record__c=id;
                        apil2.Object__c='Opportunity';
                        apil2.Status__c='ERROR';
                        apil2.Results__c=response.getbody();
                        apil2.API__c='Trip / Resend ALL';
                        apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
                        apil2.User__c=UserInfo.getUsername();
                    insert apil2;
      }
             }
        }
    }
}}