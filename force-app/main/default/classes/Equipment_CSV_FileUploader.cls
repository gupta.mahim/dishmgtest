public class Equipment_CSV_FileUploader 
{
    ApexPages.StandardController stdCtrl;    public Equipment_CSV_FileUploader(ApexPages.StandardController std)    {       stdCtrl=std;    }    public PageReference quicksave()    {        update accstoupload ;        return null;    }  
  
    public PageReference fileAccess() {
        return null;
    }
public Equipment_CSV_FileUploader(){

}

    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Equipment__c> accstoupload;
    
    public Pagereference ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        accstoupload = new List<Equipment__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            Equipment__c a = new Equipment__c();
            a.Name = inputvalues[0];
            a.Receiver_S__c = inputvalues[1];       
            a.Receiver_Model__c = inputvalues[2];
            a.Programming__c = inputvalues[3];
            a.Channel__c = inputvalues[4];
            a.Channel_No__c = inputvalues[5];
            a.Satellite__c = inputvalues[6];
            a.Satellite_Channel__c = inputvalues[7];
            a.Output_Channel__c = inputvalues[8];            
            a.Opportunity__c = ApexPages.currentPage().getParameters().get('oppId');
            a.RecordTypeId = '01260000000Luri';
            
            accstoupload.add(a);         
        }
        try{
              insert accstoupload;
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template');
            ApexPages.addMessage(errormsg);
        }      
                      
        return null ;    }        public List<Equipment__c> getuploadedEquipment()    {        if (accstoupload!= NULL)            if (accstoupload.size() > 0)                return accstoupload;            else                return null;                            else            return null;


     }}