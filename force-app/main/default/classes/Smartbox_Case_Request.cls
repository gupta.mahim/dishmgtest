public class Smartbox_Case_Request {
  private List<Smartbox__c> equip;
  private Case ca;
    public Smartbox_Case_Request(ApexPages.StandardController controller) {
    this.ca= (Case)controller.getRecord();

    }
     
  public List<Smartbox__c> getequip() {
       Case cas = [SELECT ID, Opportunity__r.id from Case where id = :ca.id];
       if (cas.Opportunity__c == null)
       return null;
        
       equip = [SELECT ID, Name, CAID__c, Chassis_Serial__c, Part_Number__c, Remove_Equipment__c, SmartCard__c, Status__c, Type_of_Equipment__c, Serial_Number__c 
       FROM Smartbox__c WHERE Opportunity__c = :cas.Opportunity__r.id AND CaseID__c = :ca.id AND
        (Status__c='Activation Requested' OR 
        Status__c='Drop Requested')
        ORDER BY Status__c];
 
        return equip;
    }
}