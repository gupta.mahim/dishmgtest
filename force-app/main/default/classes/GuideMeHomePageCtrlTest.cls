/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 22 July 2020
Created for: Milestone# MS-001252
Description: Test class for GuideMeHomePageController.apxc
-------------------------------------------------------------*/

@IsTest
public class GuideMeHomePageCtrlTest {
    public static testMethod void testMethod1() {
        GuideMeHomePageController.UserAccessListWrapper userHaveAccess = GuideMeHomePageController.getUserAccess();
        system.assertEquals(userHaveAccess.PropertyAccess.currentUserHaveAccess , true);
    }
}