@isTEST (SeeAllData=true)
private class CaseProducts_Test{
    private static testmethod void testTriggerForCase(){
                opportunityLineItem p = new opportunityLineItem();
                     p.PriceBookEntryId = '01u6000000CAnTdAAL';
                     p.Quantity = 2;
                     p.UnitPrice = 1.5;
                     p.Status__c = 'Add Requested';
                     p.OpportunityId = '0066000000MwCh5';
                   
    insert p;
    
                opportunityLineItem p2 = new opportunityLineItem();
                     p2.PriceBookEntryId = '01u6000000CAnUhAAL';
                     p2.Quantity = 1;
                     p2.UnitPrice = 1.5;
                     p2.Status__c = 'Add Requested';
                     p2.OpportunityId = '0066000000MwCh5';
                   
    insert p2;
    
                opportunityLineItem p3 = new opportunityLineItem();
                     p3.PriceBookEntryId = '01u6000000CAnV3AAL';
                     p3.Quantity = 1;
                     p3.UnitPrice = 1.5;
                     p3.Status__c = 'Add Requested';
                     p3.OpportunityId = '0066000000MwCh5';
                   
    insert p3;
    
                opportunityLineItem p4 = new opportunityLineItem();
                     p4.PriceBookEntryId = '01u6000000CAnTfAAL';
                     p4.Quantity = 1;
                     p4.UnitPrice = 1.5;
                     p4.Status__c = 'Remove Requested';
                     p4.OpportunityId = '0066000000MwCh5';
                     p4.CaseID__c = NULL;
                   
    insert p4;

 Case c = new Case();

                    c.Opportunity__c = '0066000000MwCh5';

                    c.AccountId = '0016000000kGWKW';
                    c.Status = 'Request Completed';
                    c.Origin = 'PRM';
                    c.RequestedAction__c = 'Activation';
                    c.RecordTypeId = '0126000000017LQ';
                    c.Number_of_Accounts__c = 2;
                    c.CSG_Account_Number__c = '82551234567890';
                    
                    c.Requested_Actvation_Date_Time__c = system.Today();
                      insert c;
                      update c;

       }
    
        }