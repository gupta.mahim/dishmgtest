public class CMaster_CalloutClassOPP_Search {

public class person {
    public String dishCustomerId;
    public cls_results [] results;
//    public cls_relations [] relations;
}

class cls_results {
    public String dishCustomerId;
    cls_customerOwner customerOwner;
    cls_relations [] relations;
}
class cls_customerOwner {
    public String Id;
}  
class cls_relations {
    public String extRefNum;
}  
    
    @future(callout=true)
    
    public static void CMasterCalloutSearchOPPCustomer(String Id) {

 list<Opportunity> C = [select Id, APID__c, AmDocs_ServiceID__c, PIN__c, Amdocs_CustomerID__c, dishCustomerId__c, partyId__c from Opportunity where Id =:id Limit 1];    
        if( C.size() < 1 || ((C[0].APID__c == '' || C[0].APID__c == Null)  || ( C[0].AmDocs_ServiceID__c == '' || C[0].AmDocs_ServiceID__c == Null))) {
            Opportunity sbc = new Opportunity();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Amdocs_Transaction_Code__c='';

        if( (C[0].APID__c == '' || C[0].APID__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Customer Search requires the Customer ID, the APID and Service ID. Your data is missing the APID';
        }
        if( (C[0].Amdocs_CustomerID__c == '' || C[0].Amdocs_CustomerID__c == Null )) {sbc.Amdocs_Transaction_Description__c='Customer Search requires the Customer ID, the First Name, and the Last Name of the billing contact. Your data is missing the Subscriber ID';
        }
            update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Opportunity sbc = new Opportunity();
                        sbc.id=id;
                        sbc.API_Status__c='';
                        sbc.Amdocs_Transaction_Code__c='';
                        sbc.Amdocs_Transaction_Description__c='ERROR: CMaster_CalloutClassOPP_Search is missing a valid ENDPOINT';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);            

        HttpRequest request = new HttpRequest();
            String endpoint = CME[0].Endpoint__c+'/cm-search';
            if(C[0].APID__c != '' && C[0].APID__c != Null){
                request.setEndPoint(endpoint +'/customers?extRefNum='+C[0].APID__c);
            }
//            else if(C[0].Amdocs_CustomerID__c == '' || C[0].Amdocs_CustomerID__c == Null){
//                request.setEndPoint(endpoint +'/customers?firstName='+C[0].First_Name_of_Property_Representative__c+'&lastName='+C[0].Name_of_Property_Representative__c+'&Phone='+C[0].Billing_Contact_Phone__c+'&Email='+C[0].Billing_Contact_Email__c+'&maxResultsCount=40');
//            }
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Customer-Facing-Tool', 'Salesforce');
            request.setHeader('User-ID', UserInfo.getUsername());
            request.setHeader('Accept', 'application/json'); 
            request.setHeader('User-Agent', 'SFDC-Callout/45.0'); 
            request.setMethod('GET');
            request.setTimeout(101000);
     
            HttpResponse response = new HTTP().send(request);
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
                System.debug('STATUS_CODE:'+response.getStatusCode());
                String strjson = response.getbody();
                System.debug('DEBUG Get body:  ' +response.getBody());
                
           if(response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
               System.debug('DEBUG 0 ======== strjson: ' + strjson);

        
           JSONParser parser = JSON.createParser(strjson);
           parser.nextToken();
           person obj = (person)parser.readValueAs( person.class);

            if(obj.results[0].relations.size() > 0) {
                Opportunity sbc = new Opportunity(); {
                    sbc.id=id;
                    sbc.API_Status__c='SUCCESS';
                    sbc.Amdocs_Transaction_Description__c='SEARCH Found Customer Information!';
                    sbc.dishCustomerId__c=obj.results[0].dishCustomerId;
                    sbc.partyId__c=obj.results[0].customerOwner.Id;
                    sbc.UUID__c=obj.results[0].relations[0].extRefNum;
                    }
               update sbc;
                }
           else { Opportunity sbc = new Opportunity(); { sbc.id=id; sbc.API_Status__c='SUCCESS';sbc.Amdocs_Transaction_Description__c='SEARCH No Customer Information Found!'; update sbc; }
           }
               API_Log__c apil = new API_Log__c();{
                   apil.Record__c=id;
                   apil.Object__c='Opportunity';
                   apil.Status__c='SUCCESS';
                   apil.Results__c=response.getBody();
                   apil.API__c='Search Customer';
                   apil.User__c=UserInfo.getUsername();
               insert apil;
               }
           }

           if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600){
               System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
               System.debug('DEBUG 0 ======== strjson: ' + strjson);
               System.debug('DEBUG Get body:  ' +response.getBody());
               Opportunity sbc2 = new Opportunity(); {
                   sbc2.id=id;
                   sbc2.API_Status__c='ERROR';
                   sbc2.Amdocs_Transaction_Description__c='Search for Customer Information: ' +response.getBody();
               update sbc2;
               }
               API_Log__c apil2 = new API_Log__c();{
                   apil2.Record__c=id;
                   apil2.Object__c='Opportunity';
                   apil2.Status__c='ERROR';
                   apil2.Results__c=response.getBody();
                   apil2.API__c='Search Customer';
                   apil2.User__c=UserInfo.getUsername();
               insert apil2;
               }
            }
        }
    }
}}