@isTest public class EquipDupeCaseHandlerTest {
    @isTest public static void testCreateCaseForTE()
    {
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        
        List<Tenant_Equipment__c> TE = TestDataFactoryforInternal.createTenantEquipmentBulk(opp);
       // List<Tenant_Equipment__c> TEOld =new List<Tenant_Equipment__c>();
       // TEOld.addAll(TE)
        map<Id,Tenant_Equipment__c>MyMap = new map<Id,Tenant_Equipment__c>(TE);
        List<Tenant_Equipment__c> TE1 = TestDataFactoryforInternal.createTenantEquipmentBulk(opp);
        TE[0].Is_active_elsewhere__c=true;
        update TE;
        
        map<Id,Tenant_Equipment__c>MyMap1 = new map<Id,Tenant_Equipment__c>(TE);
        List<Equipment__c> HE = TestDataFactoryforInternal.createHeEquipmentBulk(opp);
        map<Id,Equipment__c>MyMapEquip = new map<Id,Equipment__c>(HE);
        
        List<Smartbox__c> SMB = TestDataFactoryforInternal.createSBEquipmentBulk(opp);
        map<Id,Smartbox__c>MySMB = new map<Id,Smartbox__c>(SMB);
        
        List<EquipDupeCaseHandler.DupeData>dupeDataList = new List<EquipDupeCaseHandler.DupeData>();
        
        EquipDupeCaseHandler.DupeData dd1= new EquipDupeCaseHandler.DupeData();
        dd1.recordName = 'dummy';
        dd1.CaseName='OverrideAddress';
        dupeDataList.add(dd1);
        
        EquipDupeCaseHandler.DupeData dd2= new EquipDupeCaseHandler.DupeData();
        dd2.recordName = 'dummy';
        dd2.CaseName='Amentity promotion';
        dupeDataList.add(dd2);
       
        
        EquipDupeCaseHandler.DupeData dupeRec = new EquipDupeCaseHandler.DupeData();
       Test.startTest();
      
        EquipDupeCaseHandler.createCaseForTE(MyMap,MyMap1);
        EquipDupeCaseHandler.createCaseForTE(TE);
        EquipDupeCaseHandler.createCaseForHE(MyMapEquip,MyMapEquip);
        EquipDupeCaseHandler.createCaseForSmartbox(MySMB,MySMB);
        EquipDupeCaseHandler.createAddressOverrideCase(dupeDataList);
        EquipDupeCaseHandler.CreateDupeDataTE(TE[0]);
        EquipDupeCaseHandler.CreateDupeDataHE(HE[0]);
        EquipDupeCaseHandler.CreateDupeDataSB(SMB[0]);
        Test.stopTest();
    }

}