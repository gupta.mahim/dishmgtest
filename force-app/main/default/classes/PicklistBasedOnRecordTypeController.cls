public with sharing class PicklistBasedOnRecordTypeController {
   
   @AuraEnabled 
   public static String getPicklistValueBasedonRecordType(String objectAPIName, String fieldAPIName, String recordTypeDeveloperName){
       system.debug('Recordtypetest');
       list<PicklistValue> picklistValues = new list<PicklistValue>();
       
       //get record type Id
       list<RecordType> recordTypes = [Select Id, Name From RecordType  Where SobjectType = :objectAPIName and DeveloperName = :recordTypeDeveloperName limit 1];
       Id recordTypeId = (!recordTypes.isEmpty()) ? recordTypes.get(0).Id : null;
       
       if(recordTypeId != null){
           
           
           String method = 'GET';
          // String endpoint = String.format('/services/data/v43.0/ui-api/object-info/Opportunity/picklist-values/{1}/{2}', new String[]{ objectAPIName, recordTypeId, fieldAPIName });
           
           //HttpRequest request = new HttpRequest();
          //request.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionID());
          //request.setEndpoint('callout:UI_API_Named_Credentials'+endPoint);
          
         // request.setEndpoint('/ui-api/object-info/{objectApiName}/picklist-values/{recordTypeId}/{fieldApiName}');
         Opportunity__c oppsetting=Opportunity__c.getOrgDefaults();

           system.debug('oppsetting'+oppsetting);
           // String endpoint = 'https://dish--lex--c.cs70.visual.force.com';
           String endpoint =oppsetting.Url__c;
         
           endpoint += '/services/data/v44.0';
            endpoint += '/ui-api/object-info/{0}/picklist-values/{1}/{2}';
            endpoint = String.format(endpoint, new String[]{ objectAPIName, recordTypeId, fieldAPIName });
            EncodingUtil.urlEncode(endpoint,'UTF-8');    
             //requ.setMethod(method);
           //system.debug('r'+request);
           HttpRequest req = new HttpRequest();
             req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId()); 
              req.setEndpoint(endpoint);
              req.setMethod('GET');
              Http http = new Http();
             // HTTPResponse res = http.send(req);
           
           HTTPResponse response = (new Http()).send(req);
           system.debug('body:'+response.getBody());
           if(response.getStatusCode() == 200){
               
               
               Map<String,Object> root = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
               system.debug('map'+root);
               if(root.containsKey('values'))
               { 
                  
                   List<Object> picklistVals = (List<Object>)root.get('values');
                   for(Object picklistVal : picklistVals){
                       Map<String,Object> picklistValMap = (Map<String,Object>) picklistVal;
                       picklistValue pickVal = new picklistValue();
                       pickVal.value = (String) picklistValMap.get('value');
                       pickVal.label = (String) picklistValMap.get('label');
                       
                       
                       picklistValues.add(pickVal);
                       system.debug('test'+picklistValues);
                   }
                  
                   
                   
               }
            
       }
        system.debug('test1'+picklistValues);
     
   }
         return JSON.serialize(picklistValues);
   }
   public class PicklistValue{
       public String value {get;set;}
       public String label {get;set;}
        public String valid {get;set;}
       
         public String controlling{get;set;}
       
       
   }
}