@isTest (seeAllData=true)
public class Sites_Controler_MuteDC_MTSnglEntry_Test{

    public static testMethod void Sites_Controler_MuteDC_MuteSnglEntry_Test(){

    PageReference pageRef = Page.Sites_MuteDC_MutesSingleEntry;
        Account a = new Account();
        a.Name = 'Test Account';
        a.Zip__c = '78664';
        insert a;
 
        Mutes__c o = new Mutes__c();
            o.Account_Number__c = '8255250026789123';
            o.Account__c = a.Id;
        insert o;
     
  String nameQuery  = 'Installation/Activation';     
  String error = 'Please enter Account Number.';
        
    pageRef.getParameters().put('acctId',a.Id);
    Test.setCurrentPage(pageRef);        
       
        
 ApexPages.StandardController sc = new ApexPages.standardController(o);       
 Sites_Controler_MuteDC_MuteSingleEntry RUSC = new Sites_Controler_MuteDC_MuteSingleEntry(sc);
   RUSC.Error='Please enter Account Number.';

  }
}