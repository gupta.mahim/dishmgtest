@IsTest
public class NaturalDisater_CSV_FileUploaderLEX_Test {
    
    @isTest
    static void testMethod1(){
        string strNameFile = 'ServiceID1,RequestType1\nServiceID2,RequestType2';
        Opportunity testOpp = New Opportunity();
        testOpp.Name = 'testOpp';
        testOpp.CloseDate=system.Today();
        testOpp.StageName='Closed Won';
        testOpp.RecordTypeId='012600000005ChE';
        insert testOpp;
        
        List<Natural_Disaster__c> ND = NaturalDisater_CSV_FileUploader_LEX.ReadFile(strNameFile, testOpp.Id);
        
        String lexAccstoupload = String.valueOf(JSON.serialize( [Select Id,ServiceID__c,Request_Type__c,Account__c from Natural_Disaster__c limit 1] ));
        NaturalDisater_CSV_FileUploader_LEX.save(lexAccstoupload);
    }
}