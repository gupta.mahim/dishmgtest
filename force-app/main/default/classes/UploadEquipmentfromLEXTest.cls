@IsTest(SeeAllData=true)
public class UploadEquipmentfromLEXTest {
    
    public static testMethod void testReadFile() {
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        Document lstDoc = [select id,name,Body from Document where name = 'eqtest'];
        String fileContent=lstDoc.Body.toString();
        List<Equipment__c> lexAccstoupload = UploadEquipmentfromLEX.ReadFile(fileContent,newOpp.id);
        String str = JSON.serialize(lexAccstoupload);
        UploadEquipmentfromLEX.save(str);
        UploadEquipmentfromLEX.getUserAccess();
    }
}