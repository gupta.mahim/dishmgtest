@IsTest
private class LeadCategorySelectionMDU_Res_TEST {
    static testMethod void validateLeadCategorySelectionMDURes() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='MDU Residential',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

       insert L;
        
       L = [SELECT Category__c FROM Lead WHERE Id =:L.Id];
       System.debug('Category after trigger fired: ' + L.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU Residential', L.Category__c);
    }
}