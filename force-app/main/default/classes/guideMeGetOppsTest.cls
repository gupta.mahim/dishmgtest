/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 6th May 2020
Created for: Milestone# MS-001631
Description: Test class for guideMeGetOpps
-------------------------------------------------------------*/

@isTest
public class guideMeGetOppsTest {
    @isTest Private static void GetOppsDataTest(){
        Opportunity opp = New Opportunity(Name = 'Test', Property_Type__c='MDU Property (1300)', StageName='Deal Re-Work',
                                        Property_Status__c ='Active',CloseDate = system.today(),RecordTypeID = '012600000005Bu3',
                                        Address__c='1011 COLLIE PATH',city__c='Round Rock',State__c='TX',Zip__c='78664',
                                        AmDocs_SiteID__c='1234567',AmDocs_BarID__c='1234567', Digital_Status__c = 'Active', 
                                        AmDocs_tenantType__c ='Individual');
        Test.startTest();
        insert opp; 
        Test.stopTest();
        
        List<guideMeGetOpps.Flowoutput> Result = guideMeGetOpps.GetOppsData();

        System.assertEquals('Test', Result[0].ListofOpps[0].Name );
    }
}