public class GetPropertyDetails 
{
    @AuraEnabled
    public static List<EquipData> getActiveEquips(List<Id> equipIds,String oppId,String taId)
    {
        system.debug('equipIds-'+equipIds);
        system.debug('oppId-'+oppId);
        system.debug('taId-'+taId);
        List<EquipData> equipDataList=new List<EquipData>();
        List<Tenant_Equipment__c> equipList=[select Id,CaseName__r.CaseNumber,Case__c,Equip_Lookup_info__c,Is_active_elsewhere__c,IsOverrideCaseCreated__c from Tenant_Equipment__c where id in:equipIds];
        
        Boolean hasOwnDupe=false;
        Boolean hasOtherDupe=false;
        Boolean hasAdded=false;
        
        for(Tenant_Equipment__c anE:equipList)
        {
            EquipData aData=new EquipData();
            aData.equipId=anE.Id;
            aData.equipName=anE.Equip_Lookup_info__c;
            aData.isActiveElsewhere=anE.Is_active_elsewhere__c;
            
            if(anE.CaseName__c!=null){aData.isOther=true;hasOtherDupe=true;}
            if(anE.CaseName__c!=null)aData.caseDetails=anE.CaseName__r.CaseNumber;
            
            if(anE.Is_active_elsewhere__c && anE.CaseName__c==null)hasOwnDupe=true;
            if(!anE.Is_active_elsewhere__c)hasAdded=true;
            
            equipDataList.add(aData);
        }
        
        if(equipDataList.size()>0)
        {
            equipDataList[0].hasOwnActiveElsewhere=hasOwnDupe;
            equipDataList[0].hasOtherActiveElsewhere=hasOtherDupe;
            equipDataList[0].hasAddedEquip=hasAdded;
            
            List<Opportunity> oppList=[Select Name,Category__c,Status__c,Address__c,City__c,State__c,Zip__c,AmDocs_tenantType__c,AmDocs_Property_Sub_Type__c,AmDocs_SiteID__c from Opportunity where id=:oppId];
            
            if(oppList.size()>0)
            {
                equipDataList[0].oppName=oppList[0].Name;
                equipDataList[0].oppCategory=oppList[0].Category__c;
                equipDataList[0].oppStatus=oppList[0].Status__c;
                equipDataList[0].oppAddress=oppList[0].Address__c;
                equipDataList[0].oppCity=oppList[0].City__c;
                equipDataList[0].oppState=oppList[0].State__c;
                equipDataList[0].oppZip=oppList[0].Zip__c;
                equipDataList[0].oppPropertyType=oppList[0].AmDocs_tenantType__c;
                equipDataList[0].oppPropertySubType=oppList[0].AmDocs_Property_Sub_Type__c ;
                equipDataList[0].oppSiteID=oppList[0].AmDocs_SiteID__c;
            }
            
            List<Tenant_Account__c> taList=[Select First_Name__c,Override_NetQual__c,Last_Name__c,Email_Address__c,Unit__c,Address__c,City__c,State__c,Zip__c  from Tenant_Account__c where id=:taId];
            
            if(taList.size()>0)
            {
                equipDataList[0].taFName=taList[0].First_Name__c;
                equipDataList[0].taLName=taList[0].Last_Name__c;
                equipDataList[0].taEmail=taList[0].Email_Address__c;
                equipDataList[0].taUnit=taList[0].Unit__c;
                equipDataList[0].taAddress=taList[0].Address__c;
                equipDataList[0].taCity=taList[0].City__c;
                equipDataList[0].taState=taList[0].State__c;
                equipDataList[0].taZip=taList[0].Zip__c;
                equipDataList[0].overrideAddress=taList[0].Override_NetQual__c;
            }
        }
        system.debug('equipDataList-'+equipDataList);
        return equipDataList;
    }
    public class EquipData
    {
        @AuraEnabled
        public String equipId{get;set;}
        
        @AuraEnabled
        public String equipName{get;set;}
        
        @AuraEnabled
        public boolean isActiveElsewhere{get;set;}
        
        @AuraEnabled
        public boolean isOther{get;set;}
        
        @AuraEnabled
        public String caseDetails{get;set;}
        
        @AuraEnabled
        public boolean hasOwnActiveElsewhere{get;set;}
        
        @AuraEnabled
        public boolean hasOtherActiveElsewhere{get;set;}
        
        @AuraEnabled
        public boolean hasAddedEquip{get;set;}
        
        @AuraEnabled
        public String oppName{get;set;}
        
        @AuraEnabled
        public String oppSiteID{get;set;}
        
        @AuraEnabled
        public String oppCategory{get;set;}
        
        @AuraEnabled
        public String oppStatus{get;set;}
        
        @AuraEnabled
        public String oppAddress{get;set;}
        
        @AuraEnabled
        public String oppCity{get;set;}
        
        @AuraEnabled
        public String oppState{get;set;}
        
        @AuraEnabled
        public String oppZip{get;set;}
        
        @AuraEnabled
        public String oppPropertyType{get;set;}
        
        @AuraEnabled
        public String oppPropertySubType{get;set;}
        
        @AuraEnabled
        public String taFName{get;set;}
        
        @AuraEnabled
        public String taLName{get;set;}
        
        @AuraEnabled
        public String taAddress{get;set;}
        
        @AuraEnabled
        public String taCity{get;set;}
        
        @AuraEnabled
        public String taState{get;set;}
        
        @AuraEnabled
        public String taZip{get;set;}
        
        @AuraEnabled
        public String taUnit{get;set;}
        
        @AuraEnabled
        public String taEmail{get;set;}
        
        @AuraEnabled
        public Boolean overrideAddress{get;set;}
    }
}