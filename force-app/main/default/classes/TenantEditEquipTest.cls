@IsTest
public class TenantEditEquipTest {
    public static testMethod void testReadFile() {
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        List<Tenant_Equipment__c> lexAccstoupload = TenantEditEquipfromLEX.getEquipments(newOpp.id);
        String str = JSON.serialize(lexAccstoupload);
        TenantEditEquipfromLEX.saveRecords(str);
        TenantEditEquipfromLEX.deleteRecords(str);
        TenantEditEquipfromLEX.getUserAccess();
    }
}