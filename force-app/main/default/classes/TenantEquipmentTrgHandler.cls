public class TenantEquipmentTrgHandler
{
    public static void doBeforeInsert(List<Tenant_Equipment__c> newTEData)
    {
        // The logic derived from teCreateCaseOnEquipDupesTrg  
        EquipDupeHandler.handleTEDupes(newTEData,false);        
        // call the logic for address validation with NetQual
        doAddressValdiate(newTEData,null);
        
    }
    public static void doBeforeUpdate(Map<Id,Tenant_Equipment__c> newTEData,Map<Id,Tenant_Equipment__c> oldTEData)
    {
        doAmDocsBulkTenantCallout(newTEData.values());
        // call the logic for address validation with NetQual
        doAddressValdiate(newTEData.values(),oldTEData.values());
        doMoveEquipment(newTEData.values(),oldTEData.values());
    }
    public static void doMoveEquipment(List<Tenant_Equipment__c> newTEData,List<Tenant_Equipment__c> oldTEData)
    {
        Tenant_Equipment__c newRec=newTEData[0];// The address validation is a manual process and should happen on individual record.
        Tenant_Equipment__c oldRec=Trigger.isUpdate?oldTEData[0]:null;
        
        boolean doMoveReceiver=newRec.Move_Receiver_To__c!=null && (oldRec!=null && oldRec.Move_Receiver_To__c!=newRec.Move_Receiver_To__c);
        if(doMoveReceiver){AmdocsMoveReceiverCtrl.doTEMoveReceiver(newRec.id);}
    }
    public static void doAfterInsert(Map<Id,Tenant_Equipment__c> newTEData,Map<Id,Tenant_Equipment__c> oldTEData)
    {
        // The logic derived from teCreateCaseOnEquipDupesTrg
        EquipDupeCaseHandler.createCaseForTE(oldTEData,newTEData);
    }
    // this method contains logic from teValidateAdressTrg
    private static void doAddressValdiate(List<Tenant_Equipment__c> newTEData,List<Tenant_Equipment__c> oldTEData)
    {
        Tenant_Equipment__c newRec=newTEData[0];// The address validation is a manual process and should happen on individual record.
        Tenant_Equipment__c oldRec=Trigger.isUpdate?oldTEData[0]:null;
        
        Boolean callAPI=newRec.Valid_Address__c==false && (newRec.Invoke_NetQual__c || (oldRec!=null && oldRec.Address__c!=newRec.Address__c ) || (oldRec!=null && oldRec.city__c!=newRec.city__c) || (oldRec!=null && oldRec.State__c!=newRec.State__c) || (oldRec!=null && oldRec.Zip_Code__c!=newRec.Zip_Code__c));
        
        newRec.Invoke_NetQual__c=callAPI;
    }
    // this metod contains the logic from AmDocs_BulkTenantCalloutTrigger
    private static void doAmDocsBulkTenantCallout(List<Tenant_Equipment__c> teData)
    {
        // Processes for SF->AmDocs to call @Future API callout triigers.
        Tenant_Equipment__c S =teData[0];
        system.debug('test'+S.id);
        
        if(S.Action__c == 'Send Trip / HIT' && S.Smart_Card__c != 'S-TEST')
        {S.Action__c = 'Trip / HIT - Pending';S.AmDocs_Transaction_Code__c = '';S.AmDocs_Transaction_Description__c = '';S.AmDocs_FullString_Return__c = '';S.API_Status__c = '';AmDocs_CalloutClass_TNResend.AmDocsMakeCalloutTNResend(S.Id); System.debug('IF 2' +S);}
        else
        
        if(S.Action__c == 'REMOVE' ) { if( S.Tenant_Account__c == Null)
        {S.AmDocs_Transaction_Code__c = '';
         S.AmDocs_Transaction_Description__c = '';  S.AmDocs_FullString_Return__c = '';S.API_Status__c = ''; AmDocs_CalloutClass_BulkDIGITALRemove2.AmDocsMakeCalloutBulkDIGITALRemove2(S.Id);
         System.debug('IF 2' +S); }}
        else
        
        if(S.Tenant_Account__c == Null && S.Action__c == 'Mute' || S.Action__c == 'UnMute' || S.Action__c == 'Non-Pay Disconnect' || S.Action__c == 'Non-Pay Resume'  || S.Action__c == 'Disconnect'  || S.Action__c == 'Re-Connect' || S.Action__c == 'Disconnect Resume'){S.AmDocs_Transaction_Code__c = '';S.AmDocs_Transaction_Description__c = ''; S.AmDocs_FullString_Return__c = ''; S.API_Status__c = ''; AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(S.Id);}
        else
        
        if(S.Action__c == 'SWAP' && S.Tenant_Account__c == Null)
        {S.Action__c = 'SWAP PENDING'; S.AmDocs_Transaction_Code__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_FullString_Return__c = ''; S.API_Status__c = ''; AmDocs_CalloutClass_TESwap.AmDocsMakeCalloutTESwap(S.Id);
         System.debug('IF 2' +S); }
        else{}
    }
    
}