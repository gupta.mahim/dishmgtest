@isTest(SeeAllData=true)
Public class ExecSumFORupdate2_Test{
static testMethod void testExecSumFORupdate(){

Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.Phased_Construction__c = 'No';
                o.CloseDate = system.today();
                o.RecordTypeID = '012600000005Bu3';
            insert o;
            
Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Front_Office_Decision__c = 'In Progress';
          fo.RecordTypeId = '0126000000017rc';
          fo.Phase_Build__c = 'No';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.Lead_Source__c = 'Test';
          fo.Lead_Source_External__c = 'Testing';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'SVP Approved';
           insert ex;
           ex.Status__c = 'EVP Approved';
           update ex;
           
           }
           }