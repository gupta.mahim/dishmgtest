@isTest (seeAllData=true)
private class AmDocs_BulkProductsCalloutTriggerTest{
    public static testMethod void testocc(){
        
        /*Account Acc = New Account(Name='Test Account', RecordtypeId='0126000000018Rc', Email__c='mahim.gupta@dish.com', Misc_Code_SIU__c='S100', Phone='(303)555-5555');
        insert Acc;
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];

        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true);
        insert pb;
        
        Product2 prod = new Product2(Name = 'Anti-infectives 2007', Family = 'Best Practices', IsActive = true);
        insert prod;

        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert pbe;
        
        Opportunity opp = new Opportunity(AccountId=Acc.Id, Name = 'Test Syndicated 2010', Type = 'Syndicated - New', StageName = 'Planning', Contact_Email__c = 'm@dish.com', CloseDate = system.today());
        insert opp;
        
        OpportunityLineItem oli = new OpportunityLineItem(opportunityId = opp.Id, pricebookentryId = pbe.Id, Quantity = 1, UnitPrice = 7500, Description = '2007 CMR #4 - Anti-Infectives');
        insert oli;*/
        
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"test":{"test":"test"}}',null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        User u = [select name,Id from User where Isactive = true and profileId=:[select Id from profile where name='system administrator'].Id  limit 1];
        system.runAs(u){
            //OpportunityLineItem updOLI = [Select Id,Action__c from OpportunityLineItem where OpportunityId = :opp.Id Limit 1];
            OpportunityLineItem updOLI = [select Id,Action__c from OpportunityLineItem limit 1];
			updOLI.Action__c = 'Covid19ReduceRate';
            Update updOLI;
        }
    }
}