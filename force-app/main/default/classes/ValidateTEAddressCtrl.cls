Public class ValidateTEAddressCtrl
{
    public Id current_TE_Id{get;set;}
    public boolean inProgress{get;set;}
    public boolean callNetQual{get;set;}
    public boolean validation_msg_exist{get;set;}
    
    
    public ValidateTEAddressCtrl(ApexPages.StandardController sc)
    {
        inProgress=true;
        current_TE_Id=sc.getRecord().Id;
        //Invoke_NetQual__c
        //Override_NetQual__c
        //Valid_Address__c
        Tenant_Equipment__c current_TE=[select id,Invoke_NetQual__c,Override_NetQual__c,Valid_Address__c, Address_Validation_Message__c  from Tenant_Equipment__c where id=:current_TE_Id];
        boolean invokeNetQual=current_TE.Invoke_NetQual__c;
        boolean overrideNetQual=current_TE.Override_NetQual__c;
        boolean validAddress=current_TE.Valid_Address__c;
        System.debug('invokeNetQual='+invokeNetQual+', overrideNetQual='+overrideNetQual+', validAddress='+validAddress);
        if(overrideNetQual==false && validAddress==false && invokeNetQual==true)
        {
            callNetQual=true;
        }
        else
        {
            callNetQual=false;
        }
        
        String validation_msg = current_TE.Address_Validation_Message__c;
        if (validation_msg != null && validation_msg != ''){
            validation_msg_exist = true;
        }else{
            validation_msg_exist = false;
        }
        
    }
    
    public PageReference invokeNetQual()
    {   
        if(callNetQual)
        {   
            ValidateAdressCtrl.validateTenantEquip(current_TE_Id);
        }
        return null;
    }
    
    public PageReference removeValidationMsg()
    {
        System.debug('Inside "removeValidationMsg" function');
        System.debug('validation_msg_exist : ' + validation_msg_exist + ' && callNetQual : ' + callNetQual);
        if(validation_msg_exist == true && callNetQual == false)
        {
            Tenant_Equipment__c current_TE=[Select Id, Address_Validation_Message__c from Tenant_Equipment__c where Id=:current_TE_Id];
            current_TE.Address_Validation_Message__c = null;
            try{
                update current_TE;
            }catch(Exception e){
                System.debug('Exception message is : ' + e.getMessage());
            }
        }
        return null;
    }
    
}