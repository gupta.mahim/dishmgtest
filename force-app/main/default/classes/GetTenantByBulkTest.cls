/*   This test class is doing code coverage of the following classes.
*    1-    GetTenantByBulkCtrl
*    2-    AmDocs_CalloutClass_GetTenantByBulk
*/
@isTest
public class GetTenantByBulkTest {
    public static  string responsebody='{"ImplGetTenantByBulkRestOutput":{"tenantList":[{"serviceId":"724136","productStatus":"Active","apId":"100799638","receiverList":[{"receiverId":"R1941318686","smartCardId":"S1941318686"}]},{"serviceId":"724138","productStatus":"Active","apId":"100799652","receiverList":[{"receiverId":"R1941318666","smartCardId":"S1941318666"}]}],"bulkStatus":"Active","status":"Success"}}';
    //Test method to check the Api response with Status Code 100
    @istest Public static Void GetTenantBulkStatusCode100()
    {
        Opportunity Opp= TestDataFactoryforInternal.createOpportunity();
        opp.AmDocs_ServiceID__c='12345';
        update opp;
        
        Tenant_Equipment__c Te= TestDataFactoryforInternal.createTenantEquipment(Opp);
       
        
        GetTenantByBulkCtrl.UpdateOppApiStatus(opp.id); 
        Opportunity Opp1= TestDataFactoryforInternal.createOpportunity();
        opp1.AmDocs_ServiceID__c='12345';
        update opp1;
        GetTenantByBulkCtrl.UpdateOppApiStatus(opp1.id);   
        //set the Mock Response for Amdocs Api 
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(100,'Complete',responsebody, null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.stopTest();
    }
    @istest Public static void getDirectTenantBulkStatusCode100()
    {
        Opportunity Opp= TestDataFactoryforInternal.createOpportunity();
        opp.AmDocs_ServiceID__c='12345';
        update opp;
        
        Tenant_Equipment__c Te= TestDataFactoryforInternal.createTenantEquipment(Opp);
        
        
        //set the Mock Response for Amdocs Api 
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(100,'Complete',responsebody, null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        GetTenantByBulkCtrl.UpdateDirectOppApiStatus(opp.id); 
        Test.stopTest();
    }
    //Test method to check the Api response with Status Code 200
    @istest Public static Void GetTenantBulkStatusCode200()
    {
        Opportunity Opp= TestDataFactoryforInternal.createOpportunity();
        opp.AmDocs_ServiceID__c='12345';
        update opp;
        GetTenantByBulkCtrl.UpdateOppApiStatus(opp.id); 
        
        Tenant_Equipment__c Te= TestDataFactoryforInternal.createTenantEquipment(Opp);
        Te.Name='R1941318686';
        update Te;
        Tenant_Equipment__c Te1= TestDataFactoryforInternal.createTenantEquipment(Opp);
        
        
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Complete',responsebody, null);
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.stopTest();
        
    }
    
    
}