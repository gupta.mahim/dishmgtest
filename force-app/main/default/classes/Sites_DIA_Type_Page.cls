public class Sites_DIA_Type_Page {
public String nameQuery {get; set;}
 public string error {get; set;}


    public Sites_DIA_Type_Page(ApexPages.StandardController controller) {

    }
    
        public String getNameQuery() {

        return null;
    }
        public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('-Select-','-Select-'));
            options.add(new SelectOption('Public','Yes'));
            options.add(new SelectOption('Private','No'));
            return options;
        }
    
    
 public PageReference Next(){
 if(nameQuery == '-Select-'){
  error='Please select a request type.';
 return null;
}
else
{
 return new PageReference('/apex/Sites_DIA_Form?acctId=' + ApexPages.currentPage().getParameters().get('acctId') + 
 '&prog='+ ApexPages.currentPage().getParameters().get('prog') + 
 '&cat=' + nameQuery + 
 '&siu=' + ApexPages.currentPage().getParameters().get('siu') + 
 '&type=' + 'Site Survey' + 
 '&pro=' + ApexPages.currentPage().getParameters().get('pro') +
 '&disc=' + ApexPages.currentPage().getParameters().get('disc'));
}

}
}