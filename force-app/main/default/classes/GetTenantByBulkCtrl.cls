public class GetTenantByBulkCtrl
{
    @future(callout=true)
    public static void UpdateOppApiStatus(String objId)
    {    
        Map<String,Tenant_Equipment__C> oppEquips=new Map<String,Tenant_Equipment__C>();
        Map<String,String> taForTEs=new Map<String,String>();
        Map<Id,Tenant_Account__c> mapTA=new Map<Id,Tenant_Account__c>();
        
        Set<String> resEquipReceivers=new Set<String>();
        
        //Get Current Opp record for updating the bulk status field
        Opportunity anObj=[select Id,Property_Status__c,API_Status__c,Amdocs_Transaction_Code__c,Amdocs_Transaction_Description__c from Opportunity where id=:objId];
        system.debug('Opptest'+anObj.id);
        List<Tenant_Equipment__C> Tenantlst=[Select Name,Tenant_Account__c from Tenant_Equipment__c where Opportunity__c=:anObj.id ];

        // convert list into Maps
        for(Tenant_Equipment__C anEquip:Tenantlst){oppEquips.put(anEquip.Name,anEquip);if(anEquip.Tenant_Account__c!=null){taForTEs.put(anEquip.Name,anEquip.Tenant_Account__c);}}
        
        if(taForTEs.size()>0){mapTA=new Map<Id,Tenant_Account__c>([select Id,Action__c from Tenant_Account__c where id in:taForTEs.values()]);}
        
        //List<Tenant_Account__c> taList=[Select Id from Tenant_Account__c where];
        AmDocs_CalloutClass_GetTenantByBulk.BulkResponseWrapper wrapperResult=AmDocs_CalloutClass_GetTenantByBulk.AmDocsMakeCalloutGetTenantByBulk(objId);
        // list<BulkTenantResponseInfo.ReceiverList> reclst=new list<BulkTenantResponseInfo.ReceiverList>();
        processResponse(wrapperResult,anObj,oppEquips,mapTA);                
        
        
    }
    public static void UpdateDirectOppApiStatus(String objId)
    {    
        Map<String,String> taForTEs=new Map<String,String>();
        Map<Id,Tenant_Account__c> mapTA=new Map<Id,Tenant_Account__c>();
        Map<String,Tenant_Equipment__C> oppEquips=new Map<String,Tenant_Equipment__C>();
        
        //Get Current Opp record for updating the bulk status field
        Opportunity anObj=[select Id,Property_Status__c,API_Status__c,Amdocs_Transaction_Code__c,Amdocs_Transaction_Description__c from Opportunity where id=:objId];
        system.debug('Opptest'+anObj.id);
        List<Tenant_Equipment__C> Tenantlst=[Select Id,Name,Tenant_Account__c from Tenant_Equipment__c where Opportunity__c=:anObj.id ];

        // convert list into Maps
        for(Tenant_Equipment__C anEquip:Tenantlst){oppEquips.put(anEquip.Name,anEquip);if(anEquip.Tenant_Account__c!=null){taForTEs.put(anEquip.Name,anEquip.Tenant_Account__c);}}
        
        if(taForTEs.size()>0){mapTA=new Map<Id,Tenant_Account__c>([select Id,Action__c from Tenant_Account__c where id in:taForTEs.values()]);}
        
        //List<Tenant_Account__c> taList=[Select Id from Tenant_Account__c where];
        AmDocs_CalloutClass_GetTenantByBulk.BulkResponseWrapper wrapperResult=AmDocs_CalloutClass_GetTenantByBulk.AmDocsMakeCalloutGetTenantByBulk(objId);
        // list<BulkTenantResponseInfo.ReceiverList> reclst=new list<BulkTenantResponseInfo.ReceiverList>();
        processResponse(wrapperResult,anObj,oppEquips,mapTA);        
        
        
    }
    private static void processResponse(AmDocs_CalloutClass_GetTenantByBulk.BulkResponseWrapper wrapperResult,Opportunity anObj,Map<String,Tenant_Equipment__C> oppEquips,Map<Id,Tenant_Account__c> mapTA)
    {
        List<Tenant_Equipment__C> newEquipList=new List<Tenant_Equipment__C>();
        Map<Id,Tenant_Equipment__c> teEquiplsttoupdate=new Map<Id,Tenant_Equipment__c>();
        Map<Id,Tenant_Account__c> taEquiplsttoupdate=new Map<Id,Tenant_Account__c>();
        
        List<Tenant_Equipment__c> teEquiplsttoupInsert=new list<Tenant_Equipment__c>();
        List<Tenant_Account__c> taEquiplsttoupInsert=new list<Tenant_Account__c>();
        
        Set<String> resEquipReceivers=new Set<String>();
        
        
        AmDocs_CalloutClass_GetTenantByBulk.ImplGetTenantByBulkRestOutput scrubResult=null;
        system.debug('wrapper'+wrapperResult);
        if(wrapperResult!=null)
        {
            scrubResult=wrapperResult.ImplGetTenantByBulkRestOutput;}
        system.debug('scrub'+scrubResult);
        if(scrubResult!=null)
        {
            //check the bulkstatus field from response
            if(String.isNotBlank(scrubResult.status) && scrubResult.status.equalsIgnoreCase('Success'))
            {
                
                if(scrubResult.tenantList!=null && scrubResult.tenantList.size() > 0)
                {
                    //Loop for tenantlist Objects
                    for(AmDocs_CalloutClass_GetTenantByBulk.TenantList rec: scrubResult.tenantList){
                        if(rec!=null && rec.receiverList!=null && rec.receiverList.size()>0)
                        {    //Loop for Receiverlist Objects
                            for(AmDocs_CalloutClass_GetTenantByBulk.ReceiverList receiver: rec.receiverList)
                            {
                                String resRecId=receiver.receiverId;
                                String Smartcardid=receiver.SmartCardid;
                                resEquipReceivers.add(resRecId);
                                Tenant_Equipment__C resEquip=oppEquips.get(resRecId);
                                //Update opp equip from response data
                                
                                System.debug('resRecId=['+resRecId+'],Smartcardid=['+Smartcardid+']');
                                
                                
                                if(resEquip!=null)
                                {
                                    System.debug('apid=['+rec.apid+'],productstatus=['+rec.productstatus+']');
                                    resEquip.API_ID__c=rec.apid;
                                    resEquip.Action__c=rec.productstatus;
                                    resEquip.Amdocs_Tenant_ID__c=rec.Serviceid;  
                                    
                                    if(!teEquiplsttoupdate.containsKey(resEquip.Id)){teEquiplsttoupdate.put(resEquip.Id,resEquip);}
                                    
                                    
                                    if(resEquip.Tenant_Account__c!=null)
                                    {
                                        Tenant_Account__c equipTA=mapTA.get(resEquip.Tenant_Account__c);
                                        
                                        equipTA.Action__c=rec.productstatus;
                                        if(!taEquiplsttoupdate.containsKey(equipTA.Id)){taEquiplsttoupdate.put(equipTA.Id,equipTA);}
                                        
                                    }
                                }
                                //Create new equip from response data if does not exist on opp
                                else       
                                {
                                    Tenant_Equipment__C newEquip=new Tenant_Equipment__C();
                                    newEquip.Name=resRecId;
                                    newEquip.Smart_Card__c=Smartcardid;
                                    newEquip.API_ID__c=rec.apid;
                                    newEquip.Action__c=rec.productstatus;
                                    newEquip.Opportunity__c=  anObj.id;
                                    newEquip.Amdocs_Tenant_ID__c=rec.Serviceid;
                                    teEquiplsttoupInsert.add(newEquip);   
                                    // newEquipList.add(newEquip);   
                                }            
                            }
                        }
                    }
                    //Remove opp equip which are not in response
                    for(String oppReceiver:oppEquips.keyset())
                    {
                        if(!resEquipReceivers.contains(oppReceiver))
                        {
                            Tenant_Equipment__C remEquip=oppEquips.get(oppReceiver);
                            remEquip.Action__c='Removed';
                            // remEquipList.add(remEquip);
                            if(!teEquiplsttoupdate.containsKey(remEquip.Id)){teEquiplsttoupdate.put(remEquip.Id,remEquip);}
                               
                        }
                    }
                }
            }    
        }
        //update the Existing Equipment and insert the new Equipment
        //System.debug('Records to update='+Equiplsttoupdate);
        if(teEquiplsttoupdate.size()>0)
        {
            List<Tenant_Equipment__c> lstEquiplsttoupdate = new List<Tenant_Equipment__c>();
            lstEquiplsttoupdate.addAll(teEquiplsttoupdate.values());
            update lstEquiplsttoupdate;    
        }
        if(taEquiplsttoupdate.size()>0)
        {
            List<Tenant_Account__c> lstEquiplsttoupdate = new List<Tenant_Account__c>();
            lstEquiplsttoupdate.addAll(taEquiplsttoupdate.values());
            update lstEquiplsttoupdate;    
        }
        //System.debug('Records to insert='+EquiplsttoupInsert);
        if(teEquiplsttoupInsert.size()>0)
        {
            Insert teEquiplsttoupInsert;    
        }
        if(taEquiplsttoupInsert.size()>0)
        {
            Insert taEquiplsttoupInsert;    
        }
    }
}