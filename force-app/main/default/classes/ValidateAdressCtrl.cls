/*
* Author: M Fazal Ur Rehman
* Description: The controller responsible for validatig the address against Adress Scrub API and populates validated address infromation 
* on target object 
*/
public class ValidateAdressCtrl
{
    private static Map<String,String> matchCodes=new Map<String,String>{
    '4000' =>'Address not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.',
    '4002' =>'Address not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.',
    '4004' =>'ZIP Code not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.',
    '4006' =>'Street not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.',
    '4001' =>'Address not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.',
    '4008' =>'Address not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.',
    '4003' =>'City, State, ZIP not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.',
    '4005' =>'City not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.',
    '4007' =>'Street not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.'
    };
    public static void validateOppty(Id objId)
    {
        Opportunity anObj=[select Id,Address__c,city__c,Valid_Address__c,State__c,Zip__c from Opportunity where id=:objId];
        AddressScrubRequestInfo requestInfo=prepareReuest((String)anObj.get('Address__c'),(String)anObj.get('city__c'),(String)anObj.get('State__c'),(String)anObj.get('Zip__c'));
        
        if(requestInfo!=null)
        {
            AddressScrubResponseInfo scrubResult=AddressScrubCallout.invoke(requestInfo,false);
            
            if(scrubResult!=null){boolean changeFound=false;if(scrubResult.addressChangeIndicators.line1==true){anObj.Address__c=scrubResult.scrubbedAddress.line1;changeFound=true;}                if(scrubResult.addressChangeIndicators.city==true){anObj.city__c=scrubResult.scrubbedAddress.city;changeFound=true;}                if(scrubResult.addressChangeIndicators.state==true){anObj.State__c=scrubResult.scrubbedAddress.state;changeFound=true;}                if(scrubResult.addressChangeIndicators.zipCode==true){anObj.Zip__c=scrubResult.scrubbedAddress.zipCode;changeFound=true;}                                anObj.Valid_Address__c=true;                anObj.Invoke_NetQual__c=false;                anObj.Valid_Address_picklist__c='Address Valid';                anObj.Address_Validation_Message__c=changeFound?'Address validation has modified your property address. Please refresh this page and verify that the updated address is correct.':'The property address had been verified.';            }
            else{anObj.Invoke_NetQual__c=false;anObj.Address_Validation_Message__c='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address checkbox.';                anObj.Valid_Address_picklist__c='Address Invalid';           }
            if(scrubResult!=null && matchCodes.containsKey(scrubResult.addressMatchCode)){anObj.Valid_Address__c=false;anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Invalid';anObj.Address_Validation_Message__c=matchCodes.get(scrubResult.addressMatchCode);}
            update anObj;
        }else{anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Invalid';anObj.Address_Validation_Message__c='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address checkbox.';
        update anObj;}
    }
    
    public static void validateTenantEquip(Id objId)
    {
        Tenant_Equipment__c anObj=[select Id,Address__c,city__c,Valid_Address__c,State__c,Zip_Code__c from Tenant_Equipment__c where id=:objId];
        AddressScrubRequestInfo requestInfo=prepareReuest((String)anObj.get('Address__c'),(String)anObj.get('city__c'),(String)anObj.get('State__c'),(String)anObj.get('Zip_Code__c'));
        
        if(requestInfo!=null)
        {
            AddressScrubResponseInfo scrubResult=AddressScrubCallout.invoke(requestInfo,false);
            
            if(scrubResult!=null){boolean changeFound=false;if(scrubResult.addressChangeIndicators.line1==true){anObj.Address__c=scrubResult.scrubbedAddress.line1;changeFound=true;}                if(scrubResult.addressChangeIndicators.city==true){anObj.city__c=scrubResult.scrubbedAddress.city;changeFound=true;}                if(scrubResult.addressChangeIndicators.state==true){anObj.State__c=scrubResult.scrubbedAddress.state;changeFound=true;}                if(scrubResult.addressChangeIndicators.zipCode==true){anObj.Zip_Code__c=scrubResult.scrubbedAddress.zipCode;changeFound=true;}                                anObj.Valid_Address__c=true;                anObj.Invoke_NetQual__c=false;                anObj.Valid_Address_picklist__c='Address Valid';                anObj.Address_Validation_Message__c=changeFound?'Address validation has modified your property address. Please refresh this page and verify that the updated address is correct.':'The property address has been verified.';                            }            else            {                anObj.Invoke_NetQual__c=false;                anObj.Address_Validation_Message__c='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address  checkbox.';                anObj.Valid_Address_picklist__c='Address Invalid';            }            if(scrubResult!=null && matchCodes.containsKey(scrubResult.addressMatchCode)){anObj.Valid_Address__c=false;anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Invalid';anObj.Address_Validation_Message__c=matchCodes.get(scrubResult.addressMatchCode);}
            update anObj;
        }else{anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Invalid';anObj.Address_Validation_Message__c='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address  checkbox..';update anObj;}
    }
    public static void validateTenantEquipBulk(List<Tenant_Equipment__c> lstObj)
    {
        //List<Tenant_Equipment__c> lstObj=[select Id,Address__c,city__c,Valid_Address__c,State__c,Zip_Code__c from Tenant_Equipment__c where id=:objIds];
        List<Tenant_Equipment__c> recordsToUpdate=new List<Tenant_Equipment__c>();
        for(Tenant_Equipment__c anObj:lstObj)
        {
            AddressScrubRequestInfo requestInfo=prepareReuest((String)anObj.get('Address__c'),(String)anObj.get('city__c'),(String)anObj.get('State__c'),(String)anObj.get('Zip_Code__c'));
        
            if(requestInfo!=null)
            {
                AddressScrubResponseInfo scrubResult=AddressScrubCallout.invoke(requestInfo,false);
                
                if(scrubResult!=null){boolean changeFound=false;if(scrubResult.addressChangeIndicators.line1==true){anObj.Address__c=scrubResult.scrubbedAddress.line1;changeFound=true;}                    if(scrubResult.addressChangeIndicators.city==true){anObj.city__c=scrubResult.scrubbedAddress.city;changeFound=true;}                    if(scrubResult.addressChangeIndicators.state==true){anObj.State__c=scrubResult.scrubbedAddress.state;changeFound=true;}                    if(scrubResult.addressChangeIndicators.zipCode==true){anObj.Zip_Code__c=scrubResult.scrubbedAddress.zipCode;changeFound=true;}                    anObj.Valid_Address_picklist__c='Address Valid'; anObj.Valid_Address__c=true; anObj.Invoke_NetQual__c=false;                    anObj.Address_Validation_Message__c=changeFound?'Address validation has modified your property address. Please refresh this page and verify that the updated address is correct.':'The property address has been verified.';                }                else                {                    anObj.Invoke_NetQual__c=false;                    anObj.Address_Validation_Message__c='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address  checkbox.';                    anObj.Valid_Address_picklist__c='Address Invalid';                }
                if(scrubResult!=null && matchCodes.containsKey(scrubResult.addressMatchCode)){anObj.Valid_Address__c=false;anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Invalid';anObj.Address_Validation_Message__c=matchCodes.get(scrubResult.addressMatchCode);}
                recordsToUpdate.add(anObj);
            }    
        }
        if(recordsToUpdate.size()>0)
        {
            update recordsToUpdate;
        }
    }
    @Future(callout=true)
    public static void validateTenantEquips(List<Id> objIds)
    {
        List<Tenant_Equipment__c> lstObj=[select Id,Address__c,city__c,Valid_Address__c,State__c,Zip_Code__c from Tenant_Equipment__c where id=:objIds];
        system.debug('lstObj - '+lstObj);
        List<Tenant_Equipment__c> recordsToUpdate=new List<Tenant_Equipment__c>();
        for(Tenant_Equipment__c anObj:lstObj)
        {
            AddressScrubRequestInfo requestInfo=prepareReuest((String)anObj.get('Address__c'),(String)anObj.get('city__c'),(String)anObj.get('State__c'),(String)anObj.get('Zip_Code__c'));
        system.debug('requestInfo - '+requestInfo);
            if(requestInfo!=null)
            {
                AddressScrubResponseInfo scrubResult=AddressScrubCallout.invoke(requestInfo,false);
                
                if(scrubResult!=null){
                    boolean changeFound=false;
                    if(scrubResult.addressChangeIndicators.line1==true){anObj.Address__c=scrubResult.scrubbedAddress.line1;changeFound=true;}                    
                    if(scrubResult.addressChangeIndicators.city==true){anObj.city__c=scrubResult.scrubbedAddress.city;changeFound=true;}                    
                    if(scrubResult.addressChangeIndicators.state==true){anObj.State__c=scrubResult.scrubbedAddress.state;changeFound=true;}                    
                    if(scrubResult.addressChangeIndicators.zipCode==true){anObj.Zip_Code__c=scrubResult.scrubbedAddress.zipCode;changeFound=true;}                    
                    anObj.Valid_Address_picklist__c='Address Valid'; anObj.Valid_Address__c=true;anObj.Invoke_NetQual__c=false;anObj.Address_Validation_Message__c=changeFound?'Address validation has modified your property address. Please refresh this page and verify that the updated address is correct.':'The property address has been verified.';                
                }                
                else{
                    anObj.Invoke_NetQual__c=false;
                    anObj.Address_Validation_Message__c='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address  checkbox.';                    
                    anObj.Valid_Address_picklist__c='Address Invalid';                
                }
                if(scrubResult!=null && matchCodes.containsKey(scrubResult.addressMatchCode)){anObj.Valid_Address__c=false;anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Invalid';anObj.Address_Validation_Message__c=matchCodes.get(scrubResult.addressMatchCode);
                }
                recordsToUpdate.add(anObj);
            }    
        }
        if(recordsToUpdate.size()>0)
        {
            update recordsToUpdate;
        }
    }
    public static void validateTenantAccount(Id objId)
    {
        Tenant_Account__c anObj=[select Id,Address__c,city__c,Valid_Address__c,State__c,Zip__c from Tenant_Account__c where id=:objId];
        AddressScrubRequestInfo requestInfo=prepareReuest((String)anObj.get('Address__c'),(String)anObj.get('city__c'),(String)anObj.get('State__c'),(String)anObj.get('Zip__c'));
        
        if(requestInfo!=null)
        {
            AddressScrubResponseInfo scrubResult=AddressScrubCallout.invoke(requestInfo,false);
            
            if(scrubResult!=null){boolean changeFound=false;
                if(scrubResult.addressChangeIndicators.line1==true){anObj.Address__c=scrubResult.scrubbedAddress.line1;changeFound=true;}                
                if(scrubResult.addressChangeIndicators.city==true){anObj.city__c=scrubResult.scrubbedAddress.city;changeFound=true;}                
                if(scrubResult.addressChangeIndicators.state==true){anObj.State__c=scrubResult.scrubbedAddress.state;changeFound=true;}                
                if(scrubResult.addressChangeIndicators.zipCode==true){anObj.Zip__c=scrubResult.scrubbedAddress.zipCode;changeFound=true;}                                
                anObj.Valid_Address__c=true;anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Valid';anObj.Address_Validation_Message__c=changeFound?'Address validation has modified your property address. Please refresh this page and verify that the updated address is correct.':'The property address has been verified.';
            }            
            else{
                anObj.Address_Validation_Message__c='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address  checkbox.';
                anObj.Invoke_NetQual__c=false;
                anObj.Valid_Address_picklist__c='Address Invalid'; 
            }
            if(scrubResult!=null && matchCodes.containsKey(scrubResult.addressMatchCode)){anObj.Valid_Address__c=false;anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Invalid';anObj.Address_Validation_Message__c=matchCodes.get(scrubResult.addressMatchCode);}
            update anObj;
        }
        else{anObj.Invoke_NetQual__c=false;anObj.Valid_Address_picklist__c='Address Invalid';anObj.Address_Validation_Message__c='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address  checkbox.';update anObj;
        }
    }
    @invocableMethod(label='Validate Address')
    public static List<FlowOutputs> validateAddressFlow(List<FlowInputs> request)
    {
        List<FlowOutputs> results=new List<FlowOutputs>();
        FlowInputs inputs=request[0];
        AddressScrubRequestInfo requestInfo=prepareReuest(inputs.street,inputs.city,inputs.state,inputs.zip);
        AddressScrubResponseInfo scrubResult=AddressScrubCallout.invoke(requestInfo,false);
        
        FlowOutputs outputs=new FlowOutputs();
        outputs.street=inputs.street;
        outputs.city=inputs.city;
        outputs.state=inputs.state;
        outputs.zip=inputs.zip;
                    
        if(scrubResult!=null){
            system.debug('test1');
            system.debug('scrubResult1 - '+ scrubResult);
            boolean changeFound=false;            
            if(scrubResult.addressChangeIndicators.line1==true){outputs.street=scrubResult.scrubbedAddress.line1;changeFound=true;}            
            if(scrubResult.addressChangeIndicators.city==true){outputs.city=scrubResult.scrubbedAddress.city;changeFound=true;}            
            if(scrubResult.addressChangeIndicators.state==true){outputs.state=scrubResult.scrubbedAddress.state;changeFound=true;}            
            if(scrubResult.addressChangeIndicators.zipCode==true){outputs.zip=scrubResult.scrubbedAddress.zipCode;changeFound=true;}
            
            outputs.addressValid=true;outputs.addressModified=true;outputs.errorMessage=changeFound?'Address validation has modified your property address. Please verify that the updated address is correct':'The property address has been verified.';        
            
            //Milestone# MS-001233 & MS-001306 - Mahim - Start
            outputs.InvokeNetQual=false;outputs.ValidAddressPicklist='Address Valid'; 
            //Milestone# MS-001233 & MS-001306 - End
        }        
        else{          
            system.debug('test2'); 
            system.debug('scrubResult2 - '+ scrubResult);
            outputs.errorMessage='Address validation failed, please provide a valid address and resubmit or you can also proceed by checking the Override Address checkbox.';            
            outputs.addressModified=false;            
            outputs.addressValid=false;
            //Milestone# MS-001233 & MS-001306 - Mahim - Start
            outputs.InvokeNetQual=false;                
            outputs.ValidAddressPicklist=''; 
            //Milestone# MS-001233 & MS-001306 - End
        }        
        if(scrubResult!=null && matchCodes.containsKey(scrubResult.addressMatchCode)){
            outputs.addressModified=false;outputs.addressValid=false;outputs.errorMessage=matchCodes.get(scrubResult.addressMatchCode);
            //Milestone# MS-001233 & MS-001306 - Mahim - Start
            outputs.InvokeNetQual=false;outputs.ValidAddressPicklist=''; 
            //Milestone# MS-001233 & MS-001306 - End
        }        
        system.debug('outputs - '+ outputs);
        results.add(outputs);
        
        return results;
    }
    private static AddressScrubRequestInfo prepareReuest(String line1,String city,String state,String zipCode)
    {
        if(String.isEmpty(line1) || String.isEmpty(city) || String.isEmpty(state) || String.isEmpty(zipCode)) return null;
        
        AddressScrubRequestInfo requestInfo=new AddressScrubRequestInfo();
        AddressScrubRequestInfo.address addInfo=new AddressScrubRequestInfo.address();
        
        requestInfo.address=addInfo;
        
        addInfo.line1=line1;
        addInfo.city=city;
        addInfo.state=state;
        addInfo.zipCode=zipCode;
                
        return requestInfo;
    }
    public class FlowInputs
    {
        @InvocableVariable
        public string street;
        
        @InvocableVariable
        public string city;
        
        @InvocableVariable
        public string state;
        
        @InvocableVariable
        public string zip;
        
        @InvocableVariable
        public string country;
    }
    public class FlowOutputs
    {
        @InvocableVariable
        public string street;
        
        @InvocableVariable
        public string city;
        
        @InvocableVariable
        public string state;
        
        @InvocableVariable
        public string zip;
        
        @InvocableVariable
        public string country;
        
        @InvocableVariable
        public boolean hasError;
        
        @InvocableVariable
        public boolean addressValid;
        
        @InvocableVariable
        public boolean addressModified;
        
        @InvocableVariable
        public string errorMessage;
       
        //Milestone# MS-001233 & MS-001306 - Mahim - Start
        @InvocableVariable
        public boolean InvokeNetQual;
        
        @InvocableVariable
        public string ValidAddressPicklist; 
        //Milestone# MS-001233 & MS-001306 End
    }
}