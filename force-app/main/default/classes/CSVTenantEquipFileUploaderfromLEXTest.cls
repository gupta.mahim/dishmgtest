@IsTest(SeeAllData=true)
public class CSVTenantEquipFileUploaderfromLEXTest {

    public static testMethod void testReadFile() {
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        list<Document> lstDoc = new list<Document>();
        String fileContent;
        lstDoc = [select id,name,Body from Document where name = 'tenantEquip' Limit 1];
        if(lstDoc.size() > 0)
        	fileContent=lstDoc[0].Body.toString();
        else
            fileContent='Test';
        List<Tenant_Equipment__c> lexAccstoupload1 = CSVTenantEquipFileUploaderfromLEX.ReadFile(fileContent,newOpp.id);
        List<Tenant_Equipment__c> lexAccstoupload = new List<Tenant_Equipment__c>();
        for(Tenant_Equipment__c TE: lexAccstoupload1){
            TE.Phone_Number__c = '1234567890';
            lexAccstoupload.add(TE);
        }
        
        String str = JSON.serialize(lexAccstoupload);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        CSVTenantEquipFileUploaderfromLEX.save(str);
        Test.stopTest();
        
        //CSVTenantEquipFileUploaderfromLEX.invokeAddressValidation();
        
        CSVTenantEquipFileUploaderfromLEX.getStateOptions();
        
        CSVTenantEquipFileUploaderfromLEX.getUserAccess();
    }
    
}