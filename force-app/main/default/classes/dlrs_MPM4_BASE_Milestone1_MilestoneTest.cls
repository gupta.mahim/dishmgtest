/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_MPM4_BASE_Milestone1_MilestoneTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_MPM4_BASE_Milestone1_Milea4OTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new MPM4_BASE__Milestone1_Milestone__c());
    }
}