@IsTest
private class LeadCategorySelectionFTG_TEST {

static testMethod void validateLeadCategorySelectionFTG2() {
        Lead LL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL= [SELECT Category__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Category after trigger fired: ' + LL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LL.Category__c);
}

static testMethod void validateLeadCategorySelectionFTG3() {
        Lead LLL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hospitals',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLL;
        
       LLL= [SELECT Category__c FROM Lead WHERE Id =:LLL.Id];
       System.debug('Category after trigger fired: ' + LLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLL.Category__c);
}
static testMethod void validateLeadCategorySelectionFTG4() {
        Lead LLLL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL= [SELECT Category__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Category after trigger fired: ' + LLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionFTG7() {
        Lead LLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Casinos',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLL;
        
       LLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionFTG8() {
        Lead LLLLLLLL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Prisons',
        Location_of_Service__c ='Cells',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLL;
        
       LLLLLLLL= [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionFTG9() {
        Lead LLLLLLLLL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLL;
        
       LLLLLLLLL= [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLLLLLLLL.Category__c);
}
}