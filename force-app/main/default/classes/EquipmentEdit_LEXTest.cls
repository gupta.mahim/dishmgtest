@IsTest(SeeAllData=true)
public class EquipmentEdit_LEXTest {
    
    public static testMethod void testReadFile() {
        
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        
        Document lstDoc = [select id,name,Body from Document where name = 'eqtest'];
        String fileContent=lstDoc.Body.toString();
        List<Equipment__c> lexAccstoupload = EquipmentEdit_LEX.getEquipments(newOpp.id);
        String str = JSON.serialize(lexAccstoupload);
        EquipmentEdit_LEX.saveRecords(str);
        EquipmentEdit_LEX.deleteRecords(str);
        EquipmentEdit_LEX.getUserAccess();
    }
}