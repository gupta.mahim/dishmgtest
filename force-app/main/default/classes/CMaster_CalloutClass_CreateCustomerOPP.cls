public class CMaster_CalloutClass_CreateCustomerOPP {

public class person {
    public String orderID;
    public String dishCustomerId;
    public String partyId;
}

    @future(callout=true)

    public static void CMasterCalloutCreateCustomerOPP(String Id) {

 list<Opportunity> C = [select Id,  First_Name_of_Property_Representative__c, Amdocs_CustomerID__c, Name_of_Property_Representative__c, Billing_Contact_Email__c, Billing_Contact_Phone__c, AmDocs_ServiceID__c, PIN__c, dishCustomerId__c, partyId__c from Opportunity where Id =:id Limit 1];    
        if( C.size() < 1 || ((C[0].First_Name_of_Property_Representative__c == '' || C[0].First_Name_of_Property_Representative__c == Null) || (C[0].Name_of_Property_Representative__c == '' || C[0].Name_of_Property_Representative__c == Null )) || (C[0].Billing_Contact_Email__c == '' || C[0].Billing_Contact_Email__c == Null) || (C[0].PIN__c == '' || C[0].PIN__c == Null ) || ( C[0].Amdocs_CustomerID__c == '' || C[0].Amdocs_CustomerID__c == Null)) {
            Opportunity sbc = new Opportunity();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Amdocs_Transaction_Code__c='';
//                sbc.Amdocs_Transaction_Description__c='Create Customer requires the Customer ID (Customer ID), First & Last Name of the billing contact along with thier Email andPIN/Last4 Number. Your data: First Name = ' + C[0].First_Name_of_Property_Representative__c + ', LastName = ' +C[0].Name_of_Property_Representative__c + ', Email = ' + C[0].Billing_Contact_Email__c + ', Pin # = ' + C[0].PIN__c + ', Customer ID = ' +C[0].Amdocs_CustomerID__c;

        if( (C[0].First_Name_of_Property_Representative__c == '' || C[0].First_Name_of_Property_Representative__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Register Customer requires the Customer ID, First Name & Last Name of the billing, Email Address, and Customer PIN. Your data is missing the First Name';
        }
        if( (C[0].Name_of_Property_Representative__c == '' || C[0].Name_of_Property_Representative__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Register Customer requires the Customer ID, First Name & Last Name of the billing, Email Address, and Customer PIN. Your data is missing the Last Name';
        }
        if( (C[0].Billing_Contact_Email__c == '' || C[0].Billing_Contact_Email__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Register Customer requires the Customer ID, First Name & Last Name of the billing, Email Address, and Customer PIN. Your data is missing the Email Address';
        }
        if( (C[0].PIN__c == '' || C[0].PIN__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Register Customer requires the Customer ID, First Name & Last Name of the billing, Email Address, and Customer PIN. Your data is missing the Customer PIN';
        }
        if( ( C[0].Amdocs_CustomerID__c == '' || C[0].Amdocs_CustomerID__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Register Customer requires the Customer ID, First Name & Last Name of the billing, Email Address, and Customer PIN. Your data is missing the Customer ID';
        }
            update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Opportunity sbc = new Opportunity();
                        sbc.id=id;
                        sbc.API_Status__c='';
                        sbc.Amdocs_Transaction_Code__c='';
                        sbc.Amdocs_Transaction_Description__c='ERROR: CMaster_CalloutClass_CreateCustomerCTC is missing a valid ENDPOINT';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);
                
                JSONGenerator jsonObj = JSON.createGenerator(true);
                    jsonObj.writeStartObject();
                        jsonObj.writeFieldName('party');
                            jsonObj.writeStartObject();
                                if(C[0].Billing_Contact_Email__c != ''&& C[0].Billing_Contact_Email__c != Null) {
                                    jsonObj.writeStringField('emailAddress', C[0].Billing_Contact_Email__c);
                                }
//                                if(C[0].Billing_Contact_Phone__c != ''&& C[0].Billing_Contact_Phone__c != Null) {
//                                    jsonObj.writeStringField('phone', C[0].Billing_Contact_Phone__c);
//                                }
                                if(C[0].First_Name_of_Property_Representative__c != '' && C[0].First_Name_of_Property_Representative__c != Null) {
                                    jsonObj.writeStringField('firstName', C[0].First_Name_of_Property_Representative__c);
                                }
                                if(C[0].Name_of_Property_Representative__c != '' && C[0].Name_of_Property_Representative__c != Null) {
                                    jsonObj.writeStringField('lastName', C[0].Name_of_Property_Representative__c);
                                }
                                jsonObj.writeStringField('type', 'PERSON');
                                if(C[0].PIN__c != Null || C[0].PIN__c != '') {
                                    jsonObj.writeStringField('ssnLast4', C[0].PIN__c);
                                }
                              jsonObj.writeEndObject();
                        jsonObj.writeFieldName('relations');
                            jsonObj.writeStartArray();
                                jsonObj.writeStartObject();
                                    jsonObj.writeStringField('extRefNum', C[0].Amdocs_CustomerID__c);
                                    jsonObj.writeStringField('providerId', 'AMDOCS-CUS');
                                jsonObj.writeEndObject();
                            jsonObj.writeEndArray();
                       jsonObj.writeEndObject();
    
                    String finalJSON = jsonObj.getAsString();
                        System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
      
                    HttpRequest request = new HttpRequest();
                        String endpoint = CME[0].Endpoint__c+'/cm-customer/customer';
                         request.setEndPoint(endpoint);
                         request.setBody(jsonObj.getAsString());
                         request.setHeader('Content-Type', 'application/json');
                         request.setHeader('Customer-Facing-Tool', 'Salesforce');
                         request.setHeader('User-ID', UserInfo.getUsername());
                         request.setMethod('POST');
                         request.setTimeout(101000);
                         System.debug('DEBUG Get UserInfo: ' +UserInfo.getUsername());   
     
                    HttpResponse response = new HTTP().send(request);
                        System.debug(response.toString());
                        System.debug('STATUS:'+response.getStatus());
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug(response.getBody());

                        if (response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());   

                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());


JSONParser parser = JSON.createParser(strjson); person obj = (person)parser.readValueAs( person.class);

                                Opportunity sbc = new Opportunity(); {
                                    sbc.id=id;
                                    sbc.API_Status__c='SUCCESS';
                                    sbc.AmDocs_Transaction_Description__c='The customer has been registered - Please Create a Login for the customer.';
                                    sbc.dishCustomerId__c=obj.dishCustomerId;
                                    sbc.partyId__c=obj.partyId;
                                    update sbc;
                                }

                               API_Log__c apil = new API_Log__c();{
                                   apil.Record__c=id;
                                   apil.Object__c='Opportunity';
                                   apil.Status__c='SUCCESS';
                                   apil.Results__c=response.getBody();
                                   apil.API__c='Create Customer';
                                   apil.User__c=UserInfo.getUsername();
                               insert apil;
                               }
                           }

                        if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600 || response.getStatusCode() == Null){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());
                                
                                Opportunity sbc2 = new Opportunity(); {
                                    sbc2.id=id;
                                    sbc2.API_Status__c='ERROR';
                                    sbc2.AmDocs_Transaction_Description__c='The customer could not be regisitered. The system says: ' +response.getBody();
                                    update sbc2;
                                }

                               API_Log__c apil2 = new API_Log__c();{
                                   apil2.Record__c=id;
                                   apil2.Object__c='Opportunity';
                                   apil2.Status__c='ERROR';
                                   apil2.Results__c=response.getBody();
                                   apil2.API__c='Create Customer';
                                   apil2.User__c=UserInfo.getUsername();
                               insert apil2;
                               }
                }
            }
        }
    }
}