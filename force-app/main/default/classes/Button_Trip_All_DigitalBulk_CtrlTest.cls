/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 14 Sept 2020
Created for: Milestone# MS-001252
Description: Test class for Button_Trip_All_DigitalBulk_Ctrl.apxc
-------------------------------------------------------------*/

@IsTest
public class Button_Trip_All_DigitalBulk_CtrlTest {
    public static testMethod void testMethod1() {
        
        Account acct1 = new Account();
        acct1.Name = 'Test Account 1';
        acct1.ToP__c = 'Integrator';
        acct1.Pyscical_Address__c = '1011 Collie Path';
        acct1.City__c = 'Round Rock';
        acct1.State__c = 'PR';
        acct1.Zip__c = '78664';
        acct1.Amdocs_CreateCustomer__c = false;
        acct1.Distributor__c = 'PACE';
        acct1.OE_AR__c = '12345';
        insert acct1;
        
        Opportunity o1 = new Opportunity();
        o1.Name = 'Test Opp 1';
        o1.AccountId = acct1.Id;
        o1.LeadSource='Test Place';
        o1.CloseDate=system.Today();
        o1.StageName='Closed Won';
        o1.AmDocs_tenantType__c = 'Incremental';
        o1.Zip__c='78664';
        o1.Smartbox_Leased__c = false;
        o1.Mute_Disconnect__c = '';
        o1.AmDocs_ServiceID__c = '1111111111';
        insert o1;
        
        //Create a new instance of standard controller
        ApexPages.StandardController sc = new ApexPages.standardController(o1);
        Button_Trip_All_DigitalBulk_Ctrl controller = new Button_Trip_All_DigitalBulk_Ctrl(sc);
    }
}