/*
* Author: M Fazal Ur Rehman
* Description: Main callout that prepares the Adress Scrub API request and parse the response
* 
*/
public class AddressScrubCallout
{
    public static String testResponse='{\"addressChangeIndicators\": {\"line1\": true,\"line2\": false,\"city\": false,\"zipCode\": false,\"state\": false},'
    +'\"scrubbedAddress\": {\"line1\": \"1011 COLLIE PATH\",\"line2\": null,\"city\": \"ROUND ROCK\",\"county\": \"WILLIAMSON COUNTY\",'
    +'\"state\": \"TX\",\"zipCode\": \"78664\",\"zipCodeExtension\": \"3443\"},\"pushAddressPossible\": false,\"addressMatchCode\": \"0\", \"_status\": ['
    +'{\"statusCode\": null,\"message\": \"success\"}]}';
    
    public static AddressScrubResponseInfo invoke(AddressScrubRequestInfo requestInfo,boolean isTest)
    {
        AddressScrubResponseInfo addressScrubResponse=null;
        
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        String endPointURL=AddressScrubURL__c.getOrgDefaults().URL__c;
        System.debug('Address Scrub End Point:'+endPointURL);
        String requestURL=endPointURL+'?sessionId='+UserInfo.getUserId()+'&agentId=Salesforce';
        //requestURL=requestURL+state+'/'+city+'.json';
        req.setEndpoint(requestURL);
        req.setMethod('POST');
        req.setHeader('Accept','application/json');
        req.setHeader('User-Agent','SFDC-Callout/44.0');
        //req.setHeader('sessionId','SFDC123');
        req.setHeader('Content-type','application/json');    
        //
        String jsonBody = json.serialize(requestInfo);
        
        System.debug('Address Request:::'+jsonBody);

        req.setBody(jsonBody);
        req.setHeader('Content-length',string.valueOf(jsonBody.length()));
        
        String addressResult=getResult(req,isTest);
        
        if(isSuccess(addressResult))addressScrubResponse=(AddressScrubResponseInfo)JSON.deserialize(addressResult, AddressScrubResponseInfo.class);
        
        System.debug('Address Response Object:::'+addressScrubResponse);
        
        return addressScrubResponse;

    }
    private static String getResult(HttpRequest req,boolean isTest)
    {
        String addressResult=testResponse;
        
         Http h = new Http();
        // Send the request, and return a response
        if(!isTest){HttpResponse res = h.send(req);addressResult=res.getBody();System.debug('Address Response:::'+res);}
        
        
        System.debug('Address Response String:::'+addressResult);
        return addressResult;
    }
    private static boolean isSuccess(String resBody)
    {
        boolean isSuccess=false;
        
        String statusCode='';
        String statusMessage='';
        
        JSONParser parser = JSON.createParser(resBody);
        String fieldName= '';
        String fieldValue='';
        System.JSONToken token;

        while((token = parser.nextToken()) != null) {
            // Parse the object
            if ((token = parser.getCurrentToken()) != JSONToken.END_OBJECT) {
                fieldName= parser.getText();
                
                if (token == JSONToken.FIELD_Name) {
                    token=parser.nextToken();
                    fieldValue=parser.getText();
                    
                    if(fieldName=='statusCode'){statusCode=fieldValue;}
                    if(fieldName=='message'){statusMessage=fieldValue;}
                }
            }
        }
        if(statusMessage=='success'){isSuccess=true;}
        System.debug('Status Code='+statusCode+' Status Message='+statusMessage);
        
        return isSuccess;
    }
}