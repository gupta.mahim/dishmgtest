Public class Verizon_CalloutClass_SMS_Up120_4G {

    @future(callout=true)

    public static void makeCalloutSMS_Up120(String Id) {

 list<Verizon__c> VZ = [select id, VID__c, M2M_Token__c, VZ_M2M_Token__c, IS_M2M_Token__c, IS_VZ_Token__c, CreatedDate from Verizon__c 
                         where VID__c = :id AND IS_M2M_Token__c = true ORDER BY CreatedDate DESC LIMIT 1 ];
     System.debug('RESULTS of the LIST lookup to the Verizon object' +VZ);
     
 list<Verizon__c> VZ2 = [select id, VID__c, M2M_Token__c, VZ_M2M_Token__c, IS_M2M_Token__c, IS_VZ_Token__c, CreatedDate from Verizon__c 
                          where VID__c = :id AND IS_VZ_Token__c = true ORDER BY CreatedDate DESC LIMIT 1 ];
     System.debug('RESULTS of the LIST lookup to the Verizon object' +VZ2);
     
list<Smartbox__c> SB1 = [select id, Verizon_ServiceState__c, kind__c, ModemID__c, ID_4G__c, ICCID__c from Smartbox__c 
                          where Id = :id AND ModemID__c != '' LIMIT 1 ];
     System.debug('RESULTS of the LIST lookup to the Verizon object' +SB1);

JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
    jsonObj.writeFieldName('deviceIds');
    jsonObj.writeStartArray();
            jsonObj.writeStartObject();
                jsonObj.writeStringField('id', SB1[0].ModemID__c);
                jsonObj.writeStringField('kind', SB1[0].kind__c);
            jsonObj.writeEndObject();
            jsonObj.writeStartObject();
                jsonObj.writeStringField('id', SB1[0].ID_4g__c);
                jsonObj.writeStringField('kind', SB1[0].ICCID__c);
            jsonObj.writeEndObject();
    jsonObj.writeEndArray();       
            jsonObj.writeStringField('smsMessage', 'UP120');
    jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();

        if (!Test.isRunningTest())
{
    HttpRequest request = new HttpRequest();
    
       // String endpoint = 'http://putsreq.com/a1L6RNKCeO0fvf68ENZO';
       String endpoint = 'https://thingspace.verizon.com/api/m2m/v1/sms';      request.setEndPoint(endpoint);        request.setBody(jsonObj.getAsString());        request.setHeader('Content-Type', 'application/json');        request.setMethod('POST');        String authorizationHeader = 'Bearer ' +VZ[0].M2M_Token__c ;        String VZHeader = VZ2[0].VZ_M2M_Token__c ;        request.setHeader('VZ-M2M-Token', VZHeader);           request.setHeader('Authorization', authorizationHeader);            HttpResponse response = new HTTP().send(request);            System.debug(response.toString());            System.debug('STATUS:'+response.getStatus());            System.debug('STATUS_CODE:'+response.getStatusCode());            System.debug(response.getBody());        if (response.getStatusCode() == 200) {         Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                    
                    
                    // If the request is successful, parse the JSON response.
                                              // Deserialize the JSON string into collections of primitive data types.

            System.debug('=== all keys in the map: ' + results.keySet());
            System.debug('=== all values in the map (as a List): ' + results.values());
            System.debug('=== all values in the map (as a List): ' + results.size());
            
         // Cast the values in the 'blades' key as a list
        List<String> vztoken = new List<String>();            vztoken.addAll(results.keySet());             System.debug('Received the following vztoken info: ' +vztoken);           for (Object Verizon: vztoken) {           System.debug(vztoken);           Verizon__c sbc = new Verizon__c();           sbc.name=String.valueOf(results.values());           sbc.vid__c=id;           sbc.Smartbox__c=id;           sbc.response_code__c=String.valueOf(results.values());           sbc.Action__c='SMS Up120';           insert sbc;
            
         System.debug('sbc'+sbc);
        }
    }        
}
}
}