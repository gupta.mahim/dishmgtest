public class Sites_Controller_SFLPrivateProducts {

    public Opportunity theOpp {get;set;}
    public Account theAcc {get;set;}
    public Case cas {get;set;}
    public String searchString {get;set;}
    public opportunityLineItem[] shoppingCart {get;set;}
    public priceBookEntry[] AvailableProducts {get;set;}
    public priceBookEntry[] Starter {get;set;}
    public priceBookEntry[] NewsandE {get;set;}
    public priceBookEntry[] Music {get;set;}
    public priceBookEntry[] Sports {get;set;}
    public priceBookEntry[] Intl {get;set;}
    public Pricebook2 theBook {get;set;} 
    public Boolean refreshPage {get; set;} 
 
    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public Decimal Total {get;set;}
    
    public Boolean overLimit {get;set;}
    public Boolean multipleCurrencies {get; set;}
    
    private Boolean forcePricebookSelection = false;
    
    private opportunityLineItem[] forDeletion = new opportunityLineItem[]{};
    

    public Sites_Controller_SFLPrivateProducts (ApexPages.StandardController controller) {
    cas = [select Id, CaseNumber, AccountId, Opportunity__c, Status from Case where Opportunity__c = :ApexPages.currentPage().getParameters().get('id') and Status = 'Form Submitted']; 
    theAcc = [select Id, Email__c from Account where Id = :cas.AccountId];
    
    
 multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
 
         if(multipleCurrencies)
            theOpp = database.query('select Id, Name, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = \'' + controller.getRecord().Id + '\' limit 1');
        else
           theOpp = [select Id, Name, Pricebook2Id, PriceBook2.Name, Contact_Name__c, weighted_average__c, Contact_Email__c, BTVNA_Request_Type__c from Opportunity where Id = :controller.getRecord().Id limit 1];
          
        // If products were previously selected need to put them in the "selected products" section to start with
        shoppingCart = [select Id, Quantity, TotalPrice, UnitPrice, Sub_Total__c, Description, PriceBookEntryId, PriceBookEntry.Name, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.PriceBook2Id from opportunityLineItem where OpportunityId=:theOpp.Id];

        // Check if Opp has a pricebook associated yet
        if(theOpp.Pricebook2Id == null){
            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where Id = '01s600000001w1DAAQ'];
            if(activepbs.size() == 2){
                forcePricebookSelection = true;
                theBook = new Pricebook2();
            }
            else{
                theBook = activepbs[0];
            }
        }
        else{
            theBook = theOpp.Pricebook2;
        }
        
        if(!forcePricebookSelection)
            updateAvailableList();
            updateMusicList();
            updateNewsandEList();
            updateSportsList();
            updateIntlList();
            updateStarterList();
    }
    
    // this is the 'action' method on the page
     
     public String getChosenCurrency(){
    
        if(multipleCurrencies)
            return (String)theOpp.get('CurrencyIsoCode');
        else
            return '';
    }
    
    public void updateAvailableList() {
    
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
         
     
        if(multipleCurrencies)
            qstring += ' and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';
        
        // note that we are looking for the search string entered by the user in the name OR description
        // modify this to search other fields if desired
        if(searchString!=null){
            qString+= ' and (Product2.Name like \'%' + searchString + '%\' or Product2.Description like \'%' + searchString + '%\')';
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }
        
      
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AvailableProducts = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(AvailableProducts.size()==101){
            AvailableProducts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
    }
 
 
     public void updateMusiclist() {
    
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
         
      
            qString+= ' and (Product2.Family = \'' + 'Music' + '\')';
            


        
        Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }
        
      
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        Music = database.query(qString);
        
        if(Music.size()==101){
            Music.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
    }
    
         public void updateNewsandEList() {
    
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
        qString+= ' and (Product2.Family = \'' + 'Add On Packages' + '\')';
        Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        system.debug('qString:' +qString);        
        NewsandE = database.query(qString);
        }
    
             public void updateSportsList() {
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
        qString+= ' and (Product2.Family = \'' + 'Sports' + '\')';

        Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        system.debug('qString:' +qString);        
        Sports = database.query(qString);
        }
 
              public void updateIntlList() {
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
        qString+= ' and (Product2.Family = \'' + 'International' + '\')';
        Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        system.debug('qString:' +qString);        
        Intl = database.query(qString);
        }
 
               public void updateStarterList() {
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
        qString+= ' and (Product2.Family = \'' + 'Basic' + '\')';
        Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }
        
        qString+= ' limit 101';
        system.debug('qString:' +qString);        
        Starter = database.query(qString);
        }
 
 
 
        
    public void addToShoppingCart(){
    
        // This function runs when a user hits "select" button next to a product
 for(PricebookEntry d :AvailableProducts){
             if((String)d.Id==toSelect){
                 
                      shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice,
                      Quantity=1, Status__c='Add Requested'));

                           break;
              
                           
            }
        }
        
        
    }
    

     



    public PageReference removeFromShoppingCart(){
    
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
    
        Integer count = 0;
    
        for(opportunityLineItem d : shoppingCart){
            if((String)d.PriceBookEntryId==toUnselect){
            
                if(d.Id!=null)
                    d.Status__c='Remove Requested';
                    forDeletion.add(d);
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }
    
    public PageReference onSave(){
    
        // If previously selected products are now removed, we need to delete them
        if(forDeletion.size()>0)
            Update (forDeletion);

    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(shoppingCart.size()>0)
                upsert(shoppingCart);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
//Messaging.reserveSingleEmailCapacity(2);         
//Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
//String[] toAddresses = new String[] {theOpp.Contact_Email__c};

//if(ApexPages.currentPage().getParameters().get('direct')== 'true'){ 
//String[] ccAddresses = new String[] {theAcc.Email__c}; 
//mail.setccAddresses(ccAddresses);
//}

//mail.setToAddresses(toAddresses);

//mail.setReplyTo('salesforceadmin@dish.com');
//mail.setSenderDisplayName('Dish Business Support');
//mail.setSubject('Your ' + theOpp.BTVNA_Request_Type__c + ' request has been received for ' + theOpp.Name +'.');
//mail.setBccSender(false);
//mail.setUseSignature(false);
//mail.setPlainTextBody('Dear ' + theOpp.Contact_Name__c + 'Please be informed that your request for DISH service for' + theOpp.Name +'is currently being processed. Your confirmation number is '+ cas.CaseNumber + '. You will receive a call within 48 hours to schedule your ' + theOpp.BTVNA_Request_Type__c);
//mail.setHtmlBody('Dear ' + theOpp.Contact_Name__c + ',<br/>Please be informed that your request for DISH service for <b>' + theOpp.Name +'</b> is currently being processed. Your confirmation number is <b>'+ cas.CaseNumber + '</b>. You will receive a call within 48 hours to schedule your ' + theOpp.BTVNA_Request_Type__c);
//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 
 if(ApexPages.currentPage().getParameters().get('direct')== 'true'){        
        
      return new PageReference('/Salesforcelite/Sites_SFLThanks?casid='+cas.CaseNumber);    
        
        }
        else
        {
        
      return new PageReference('/Salesforcelite/Sites_SFLThanks?casid='+cas.CaseNumber); 
        }          
     
 }
    
    public PageReference onCancel(){
  refreshPage=true;

  return null;
    }
    
    public PageReference changePricebook(){
    
        // This simply returns a PageReference to the standard Pricebook selection screen
        // Note that is uses retURL parameter to make sure the user is sent back after they choose
    
        // This simply returns a PageReference to the standard Pricebook selection screen
        // Note that is uses retURL parameter to make sure the user is sent back after they choose
    
        PageReference ref = new PageReference('/Salesforcelite/Sites_SFLThanks');        ref.getParameters().put('id',theOpp.Id);         ref.getParameters().put('retURL','/Salesforcelite/Sites_SFLThanks?id=' + theOpp.Id);                return ref;
    }
}