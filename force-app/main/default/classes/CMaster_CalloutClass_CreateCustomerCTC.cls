public class CMaster_CalloutClass_CreateCustomerCTC {

public class person {
    public String orderID;
    public String dishCustomerId;
    public String partyId;
}

    @future(callout=true)

    public static void CMasterCalloutCreateCustomerCTC(String Id) {

 list<Contact> C = [select Id, FirstName, LastName, Role__c, Email, Phone, ExtRefNum__c, Pin__c, dishCustomerId__c, partyId__c from Contact where Id =:id Limit 1];    
        if( C.size() < 1 || ((C[0].LastName == '' || C[0].LastName == Null) && (C[0].FirstName == '' || C[0].FirstName == Null )) || (C[0].Email == '' || C[0].Email == Null) || (C[0].Role__c != 'Billing Contact' && C[0].Role__c != 'Billing Contact (PR)' ) || ( C[0].ExtRefNum__c == '' || C[0].ExtRefNum__c == Null)) {
            Contact sbc = new contact();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Transaction_Code__c='';
//                sbc.Transaction_Description__c='Create CMaster -Customer-, CM needs requires the Customer ID (ExtRefNum), or First & Last Name, Email and the role must contain Billing Contact. Your data: ExtRefnum = ' +C[0].ExtRefNum__c  + ', First Name = ' + C[0].FirstName + ', LastName = ' +C[0].LastName + ', Email = ' + C[0].Email + ', Phone # = ' + C[0].Phone + ', Contact Role = ' +C[0].Role__c;
        if( (C[0].LastName == '' || C[0].LastName == Null )) {
            sbc.Transaction_Description__c='Register Customer requires the Customer ID (ExtRefNum), or First & Last Name, Email and the role must contain Billing Contact. Your data is missing the Last Name';
        }
        if( (C[0].FirstName == '' || C[0].FirstName == Null )) {
            sbc.Transaction_Description__c='Register Customer requires the Customer ID (ExtRefNum), or First & Last Name, Email and the role must contain Billing Contact. Your data is missing the First Name';
        }
        if( (C[0].Email == '' || C[0].Email == Null )) {
            sbc.Transaction_Description__c='Register Customer requires the Customer ID (ExtRefNum), or First & Last Name, Email and the role must contain Billing Contact. Your data is missing the Email Address';
        }
        if( (C[0].Role__c != 'Billing Contact' && C[0].Role__c != 'Billing Contact (PR)' )) {
            sbc.Transaction_Description__c='Register Customer requires the Customer ID (ExtRefNum), or First & Last Name, Email and the role must contain Billing Contact. The Contact must have a role of Billing Contact';
        }
        if( (C[0].ExtRefNum__c == '' || C[0].ExtRefNum__c == Null )) {
            sbc.Transaction_Description__c='Register Customer requires the Customer ID (ExtRefNum), or First & Last Name, Email and the role must contain Billing Contact. Your data is missing the Customer ID';
        }
        update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Contact sbc = new contact();
                        sbc.id=id;
                        sbc.API_Status__c='';
                        sbc.Transaction_Code__c='';
                        sbc.Transaction_Description__c='ERROR: CMaster_CalloutClass_CreateCustomerCTC is missing a valid ENDPOINT';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);
                
                JSONGenerator jsonObj = JSON.createGenerator(true);
                    jsonObj.writeStartObject();
                        jsonObj.writeFieldName('party');
                            jsonObj.writeStartObject();
                                if(C[0].Email != ''&& C[0].Email != Null) {
                                    jsonObj.writeStringField('emailAddress', C[0].Email);
                                }
                                if(C[0].FirstName != '' && C[0].FirstName != Null) {
                                    jsonObj.writeStringField('firstName', C[0].FirstName);
                                }
                                if(C[0].LastName != '' && C[0].LastName != Null) {
                                    jsonObj.writeStringField('lastName', C[0].LastName);
                                }
                                jsonObj.writeStringField('type', 'PERSON');
                                if(C[0].PIN__c != Null && C[0].PIN__c != '') {
                                    jsonObj.writeStringField('ssnLast4', C[0].PIN__c);
                                }
                                if(C[0].PIN__c == Null || C[0].PIN__c == '') {
//                                    jsonObj.writeStringField('ssnLast4', C[0].PIN__C);
                                }
                              jsonObj.writeEndObject();

                        jsonObj.writeFieldName('relations');
                            jsonObj.writeStartArray();
                                jsonObj.writeStartObject();
                                    jsonObj.writeStringField('extRefNum', C[0].ExtRefNum__c);
                                    jsonObj.writeStringField('providerId', 'AMDOCS-CUS');
                                jsonObj.writeEndObject();
                            jsonObj.writeEndArray();
                       jsonObj.writeEndObject();
    
                    String finalJSON = jsonObj.getAsString();
                        System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
      
                    HttpRequest request = new HttpRequest();
                        String endpoint = CME[0].Endpoint__c+'/cm-customer/customer';
                         request.setEndPoint(endpoint);
                         request.setBody(jsonObj.getAsString());
                         request.setHeader('Content-Type', 'application/json');
                         request.setHeader('Customer-Facing-Tool', 'Salesforce');
                         request.setHeader('User-ID', UserInfo.getUsername());
                         request.setMethod('POST');
                         request.setTimeout(101000);
                             System.debug('DEBUG Get UserInfo: ' +UserInfo.getUsername());   
     
                    HttpResponse response = new HTTP().send(request);
                        System.debug(response.toString());
                        System.debug('STATUS:'+response.getStatus());
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug(response.getBody());

                        if (response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());   

                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());


JSONParser parser = JSON.createParser(strjson); person obj = (person)parser.readValueAs( person.class);

                            if(obj.dishCustomerId == Null) {
                                Contact sbc = new contact(); {
                                    sbc.id=id;
                                    sbc.API_Status__c='SUCCESS';
                                    sbc.Transaction_Description__c='Customer Registered FAILED! The system says: ' +response.getBody();
                                    sbc.dishCustomerId__c=obj.dishCustomerId;
                                    sbc.partyId__c=obj.partyId;
                                    update sbc;
                                }
                            }
                            if(obj.partyId != Null) {
                                Contact sbc = new contact(); {
                                    sbc.id=id;
                                    sbc.API_Status__c='SUCCESS';
                                    sbc.Transaction_Description__c='Customer Registered Success!';
                                    sbc.dishCustomerId__c=obj.dishCustomerId;
                                    sbc.partyId__c=obj.partyId;
                                    update sbc;
                                }
                            }
                               API_Log__c apil = new API_Log__c();{
                                   apil.Record__c=id;
                                   apil.Object__c='Contact';
                                   apil.Status__c='SUCCESS';
                                   apil.Results__c=response.getBody();
                                   apil.API__c='Create Customer';
                                   apil.User__c=UserInfo.getUsername();
                               insert apil;
                               }
                           }

                        if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600 || response.getStatusCode() == Null){
                            System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
                            String strjson = response.getbody();
                                System.debug('DEBUG 0 ======== strjson: ' + strjson);
                                System.debug('DEBUG Get body:  ' +response.getBody());
                                
                                Contact sbc2 = new contact(); {
                                    sbc2.id=id;
                                    sbc2.API_Status__c='ERROR';
                                    sbc2.Transaction_Description__c='Register the Customer Failed, the system says: ' +response.getBody();
                                    update sbc2;
                                }

                               API_Log__c apil2 = new API_Log__c();{
                                   apil2.Record__c=id;
                                   apil2.Object__c='Contact';
                                   apil2.Status__c='ERROR';
                                   apil2.Results__c=response.getBody();
                                   apil2.API__c='Create Customer';
                                   apil2.User__c=UserInfo.getUsername();
                               insert apil2;
                               }
                }
            }
        }
    }
}