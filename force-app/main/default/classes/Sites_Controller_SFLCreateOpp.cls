public class Sites_Controller_SFLCreateOpp {

    public Sites_Controller_SFLCreateOpp() {
    }

public Opportunity opp {get; set;}
public Case cas {get;set;}
public List <pricebookEntry> prods;
public opportunityLineItem[] shoppingCart {get;set;}
public priceBookEntry[] prods2 {get;set;}
public String equip {get;set;}
public string error {get; set;}
public String promo {get;set;}

           
   public Sites_Controller_SFLCreateOpp (ApexPages.StandardController controller) {   
   opp = new Opportunity(AccountId=ApexPages.currentPage().getParameters().get('acctId'), 
   Property_Status__c='Inactive', 
   LeadSource='Salesforce Litel', 
   Property_Type__c=ApexPages.currentPage().getParameters().get('cat'), 
   RecordTypeId='0126000000017Tj', 
   BTVNA_Request_Type__c=ApexPages.currentPage().getParameters().get('type'), 
   misc_code_siu__c=ApexPages.currentPage().getParameters().get('siu'), 
   Restricted_Programming__c=ApexPages.currentPage().getParameters().get('prog'), 
   CloseDate=system.Today(), 
   StageName='Closed Won');
   
   account a = [SELECT ID, Name, Promotion__c, Programming__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];

equip=a.Equipment_Options__c;
promo=a.Promotion__c;
   }     
      
public List<pricebookEntry> getprods() {
if(ApexPages.currentPage().getParameters().get('prog') == 'True'){
account a = [SELECT ID, Name, Promotion__c, Programming__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];

String[] progs = a.programming__c.split(';');

 prods = [select Id, UnitPrice, Name from pricebookEntry where Name = :progs and Pricebook2Id = '01s60000000Ia6UAAS'];
   return prods;  
  }
  else
  {
  account a = [SELECT ID, Name, Programming__c, NA_Programming_Discount__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];
 prods = [select Id, UnitPrice, Name from pricebookEntry where Name = ''];
   return prods;
}
}
  public PageReference createOppty(){
  if(opp.Contact_Name__c==null){error='Error: Please Enter Contact Name'; return null;}
  if(opp.Contact_Phone__c==null){error='Error: Please Enter Contact Phone'; return null;}
  if(opp.Contact_Email__c==null){error='Error: Please Enter Contact Email'; return null;}
  if(opp.Name==null){error='Error: Please Enter Property Name'; return null;}
  if(opp.Phone__c==null){error='Error: Please Enter Property Phone'; return null;}
  if(opp.Address__c==null){error='Error: Please Enter Address'; return null;}
  if(opp.City__c==null){error='Error: Please Enter City'; return null;}
  if(opp.State__c==null){error='Error: Please Enter State'; return null;}
  if(opp.Zip__c==null){error='Error: Please Enter Zip Code'; return null;}
  if(opp.HID__c==null){error='Error: Please Enter HID'; return null;}
  if(opp.GID__c==null){error='Error: Please Enter GID'; return null;}
  if(opp.Tax_Employer_ID_Number__c==null){error='Error: Please TAX ID Number'; return null;}
  if(opp.EVO__C==null && ApexPages.currentPage().getParameters().get('cat') == 'Public'){error='Error: Please Enter EVO'; return null;}
  if(opp.FOC__C==null && ApexPages.currentPage().getParameters().get('cat') == 'Public'){error='Error: Please Enter FOC'; return null;}
  if(opp.Total_Number_of_TVs__c==null){error='Error: Please Enter Number of TVs'; return null;} 
  if(opp.Type_of_Business__c==null){error='Error: Please Enter Type of Business'; return null;}



opp.TVs_Installed__c='No';




    insert opp;
     cas = [select Id, CaseNumber, Opportunity__c, Status from Case where Opportunity__c = :opp.id and Status = 'Form Submitted'];
     






  if(ApexPages.currentPage().getParameters().get('cat') == 'Public' )   {       return new PageReference('/Salesforcelite/Sites_SFLPublicProducts?id=' + opp.id + '&evo=EVO ' + opp.EVO__c + '&foc=FOC ' + opp.FOC__c + '&direct=true' + '&disc=null'
                                                            + '&acp=' + ApexPages.currentPage().getParameters().get('acp') );
  }
        else         {         return new PageReference('/Salesforcelite/Sites_SFLPrivateProducts?id=' + opp.id
                                                            + '&acp=' + ApexPages.currentPage().getParameters().get('acp')
                                                            + '&direct=true' 
                                                            + '&disc=null' 
                                                         );
        }     
   }
    }