public class AmDocs_CalloutClass_SBResend {

public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_informationMessages[] informationMessages;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String smartCardID;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}

    @future(callout=true)

    public static void AmDocsMakeCalloutSBResend(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];
  
list<Smartbox__c> S1 = [select id, Action__c, Location__c, CAID__c, Opportunity__c, Serial_Number__c,  SmartCard__c, Chassis_Serial__c, id__c from Smartbox__c where Id = :id AND Action__c = 'Trip / HIT Pending'];
    System.debug('RESULTS of the LIST lookup to the Smartbox object' +S1);

String SO = [select Opportunity__c from Smartbox__c where Id = :id LIMIT 1 ].Opportunity__c ;
     System.debug('RESULTS of the LIST SO String to the Opp object ' +SO);

list<Opportunity> O = [select id, AmDocs_ServiceID__c, Name from Opportunity where Id = :SO LIMIT 1 ];
     System.debug('RESULTS of the LIST lookup to the Opp object S From SOs ID ' +O);
     
list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the AmDocs Login object' +A);  
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
    jsonObj.writeFieldName('ImplResendEquipmentRestInput');
        jsonObj.writeStartObject();
        
        jsonObj.writeStringField('orderActionType', 'RD');
        jsonObj.writeFieldName('equipmentList');
        jsonObj.writeStartArray();   

          for(Integer i = 0; i < S1.size(); i++){
                jsonObj.writeStartObject();
                jsonObj.writeStringField('action', 'RESEND');
                   if(S1[i].CAID__c != Null) {
                       jsonObj.writeStringField('receiverID', S1[i].CAID__c);
                   }
                   if(S1[i].SmartCard__c != Null) {
                       jsonObj.writeStringField('smartCardID', S1[i].SmartCard__c);
                   }
                   if(S1[i].CAID__c != Null) {
                      jsonObj.writeStringField('location', 'C');
                   }
               jsonObj.writeEndObject();
                }
                
        jsonObj.writeEndArray();
        jsonObj.writeEndObject();
    jsonObj.writeEndObject();
    
String finalJSON = jsonObj.getAsString();
        System.debug('DEBUG finalJSON: ' +finalJSON);
           
if (!Test.isRunningTest()){ HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/resendEquipment?sc=SS&lo=EN&ca=SF'; request.setEndPoint(endpoint); request.setBody(jsonObj.getAsString()); request.setHeader('Content-Type', 'application/json'); request.setMethod('POST'); String authorizationHeader = 'Bearer ' +A[0].UXF_Token__c ; HttpResponse response = new HTTP().send(request); if (response.getStatusCode() == 200) { String strjson = response.getbody(); JSONParser parser = JSON.createParser(strjson); parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);
        
    Smartbox__c sbc = new Smartbox__c(); sbc.id=id; sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); sbc.AmDocs_Order_ID__c=obj.orderID; sbc.API_Status__c=String.valueOf(response.getStatusCode()); if(obj.OrderID != Null && obj.equipmentList[0].action == 'RESEND') { sbc.action__c='Active'; sbc.AmDocs_Transaction_Description__c='TRIP/HIT COMPLETED'; sbc.AmDocs_Transaction_Code__c='SUCCESS'; } if(obj.informationMessages != Null) { sbc.Action__c = 'Active'; sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription;  sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; } update sbc;
        System.debug('sbc'+sbc);
    }
    else if (response.getStatusCode() != 200) { Smartbox__c sbc = new Smartbox__c(); sbc.id=id; sbc.API_Status__c=String.valueOf(response.getStatusCode()); sbc.AmDocs_Transaction_Description__c=String.valueOf(response.getStatus()); update sbc;
    }
   }
   }
}