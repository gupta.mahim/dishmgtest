public class CSVTenantEquipFileUploaderfromLEX {
    public static String[] filelines = new String[]{};
        public static List<Tenant_Equipment__c> accstoupload;
    @AuraEnabled
    public static List<Tenant_Equipment__c> ReadFile(String strNameFile, String opportunityId)
    {
        filelines = strNameFile.split('\n');
        List<Tenant_Equipment__c> lexAccstoupload = new List<Tenant_Equipment__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
            
            Tenant_Equipment__c a = new Tenant_Equipment__c();
            a.Name = inputvalues[0];
            a.Smart_Card__c = inputvalues[1];
            a.Unit__c = inputvalues[2];
            a.Address__c = inputvalues[3];
            a.City__c = inputvalues[4];
            a.State__c = inputvalues[5];
            a.Zip_Code__c = inputvalues[6];
            a.Customer_First_Name__c = inputvalues[7];
            a.Customer_Last_Name__c = inputvalues[8];
            a.Phone_Number__c = inputvalues[9];
            a.Email_Address__c = inputvalues[10];
            a.Opportunity__c = opportunityId;
            
            lexAccstoupload.add(a);         
        }
        
        
        return lexAccstoupload ;    }
    
    public static void invokeAddressValidation(List<Tenant_Equipment__c> lstObj)
    {
        List<Id> teIdList=new List<Id>();
        String teIds='(';
        for(Tenant_Equipment__c te:lstObj)
        {
            teIds+='\''+te.id+'\',';teIdList.add(te.id);
        }
        teIds=teIds.removeEnd(',')+')';
        String query='select Id,Address__c,city__c,Valid_Address__c,State__c,Zip_Code__c from Tenant_Equipment__c where id in '+teIds;
        ValidateTEAddressBatch batch=new ValidateTEAddressBatch(query);
        if(lstObj.size()>100){Database.executeBatch(batch,100);}else{ValidateAdressCtrl.validateTenantEquips(teIdList);}
        
        System.debug('Batch requested:::'); 
    }
    @AuraEnabled
    public static void save(String lexAccstoupload)
    {
        System.debug('Save:: lexAccstoupload '+lexAccstoupload);
        List<Tenant_Equipment__c> equipData=(List<Tenant_Equipment__c>)JSON.deserialize(lexAccstoupload, List<Tenant_Equipment__c>.class);
        try{ insert equipData;
            invokeAddressValidation(equipData);}
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());    
        }
    }
    
    //Milestone# MS-000026 -- Mahim -- Start
    @AuraEnabled
    public static List<string> getStateOptions()
    {  
        List<String> pickListValuesList= new List<String>();
		Schema.DescribeFieldResult fieldResult = Opportunity.State__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			pickListValuesList.add(pickListVal.getLabel());
		}     
		return pickListValuesList; 
    }
    //Milestone# MS-000026 -- Mahim -- End
    
    //Milestone# MS-001252 - 30Jun20 - Start
    @AuraEnabled
    public static userAccessCheck.userAccessCheckResponse getUserAccess(){
        userAccessCheck.userAccessCheckResponse resp = new userAccessCheck.userAccessCheckResponse();
        list<Custom_Button_Access_Detail__mdt> objectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c='Upload Tenant Equipment' Limit 1];
        
        if(objectFieldDetail.size()>0)
        	resp=userAccessCheck.getCurrentUserAccess(objectFieldDetail[0].Object_Name__c, objectFieldDetail[0].Field_Name__c);
        else{
            resp.currentUserHaveAccess = false;
            resp.noAccessMsg ='Test1';// System.Label.CustomButtonNoAccessMsg;
        }
        
        return resp;
    }
    //Milestone# MS-001252 - End
}