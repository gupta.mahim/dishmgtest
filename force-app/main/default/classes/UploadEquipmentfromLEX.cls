public class UploadEquipmentfromLEX {
    
    
    public static String[] filelines = new String[]{};
    public static List<Equipment__c> accstoupload;
    
    /*
    @AuraEnabled
    public static void init(String oppId){
        Opportunity opp=[select id from Opportunity where id=:oppId];
    }
    */
    
    @AuraEnabled
    public static List<Equipment__c> ReadFile(String strNameFile,String oppId)
    {
        System.debug('strNameFile '+strNameFile);
        filelines = strNameFile.split('\n');
        List<Equipment__c> lexAccstoupload = new List<Equipment__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            Equipment__c a = new Equipment__c();
            a.Name = inputvalues[0];
            a.Receiver_S__c = inputvalues[1];       
            a.Receiver_Model__c = inputvalues[2];
            a.Programming__c = inputvalues[3];
            a.Channel__c = inputvalues[4];
            a.Channel_No__c = inputvalues[5];
            a.Satellite__c = inputvalues[6];
            a.Satellite_Channel__c = inputvalues[7];
            a.Output_Channel__c = inputvalues[8];            
            a.Opportunity__c = oppId;
            a.RecordTypeId = '01260000000Luri';
            
            lexAccstoupload.add(a);         
        }
        System.debug('accstoupload='+lexAccstoupload);     
        
        return lexAccstoupload ;
    }  
    
    @AuraEnabled
    public static void save(String lexAccstoupload)
    {
        System.debug('Save:: lexAccstoupload '+lexAccstoupload);
        List<Equipment__c> equipData=(List<Equipment__c>)JSON.deserialize(lexAccstoupload, List<Equipment__c>.class);
        try{
            insert equipData;
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());    
        }
    }
    
    //Milestone# MS-001252 - 30Jun20 - Start
    @AuraEnabled
    public static userAccessCheck.userAccessCheckResponse getUserAccess(){
        userAccessCheck.userAccessCheckResponse resp = new userAccessCheck.userAccessCheckResponse();
        list<Custom_Button_Access_Detail__mdt> objectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c='Upload Equipment' Limit 1];
        
        if(objectFieldDetail.size()>0)
        	resp=userAccessCheck.getCurrentUserAccess(objectFieldDetail[0].Object_Name__c, objectFieldDetail[0].Field_Name__c);
        else{
            resp.currentUserHaveAccess = false;
            resp.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }
        
        return resp;
    }
    //Milestone# MS-001252 - End
}