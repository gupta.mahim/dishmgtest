public class NA_Type_Page {
public String nameQuery {get; set;}
 public string error {get; set;}


    public NA_Type_Page(ApexPages.StandardController controller) {

    }
    
        public String getNameQuery() {

        return null;
    }
        public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('-Select-','-Select-'));
            options.add(new SelectOption('Installation/Activation','Installation/Activation'));
            options.add(new SelectOption('Site Survey','Site Survey'));
            return options;
        }
    
    
 public PageReference Next(){
 if(nameQuery == '-Select-'){
  error='Please select a request type.';
 return null;
}
else
{
 return new PageReference('/apex/NA_Form?acctId=' + ApexPages.currentPage().getParameters().get('acctId') + 
 '&prog='+ ApexPages.currentPage().getParameters().get('prog') + 
 '&cat='+ ApexPages.currentPage().getParameters().get('cat') + 
 '&siu=' + ApexPages.currentPage().getParameters().get('siu') + 
 '&type=' + nameQuery + 
 '&pro=' + ApexPages.currentPage().getParameters().get('pro') +
 '&disc=' + ApexPages.currentPage().getParameters().get('disc'));
}

}
}