public class GetBulkEquipment
{
    public static string OpportunityName;
    
    @AuraEnabled
    public static List<BulkEquipData> initBulkTenantEquipment(string OpptyId)
    {
        List<BulkEquipData> bulkEquips=new List<BulkEquipData>();
                
        List<Tenant_Equipment__c> bulkEquipRecords= [Select Id,Equip_Info__c,Equip_Lookup_Info__c,Tenant_Account__c,Opportunity__c,
        	Amdocs_Tenant_ID__c,Customer_First_Name__c,Customer_Last_Name__c,Phone_Number__c,Address__c,Unit__c,Is_active_elsewhere__c,
            Name, Smart_Card__c,Action__c from Tenant_Equipment__c where Action__c='Active' and 
            Tenant_Account__c=null and Amdocs_Tenant_ID__c != null and Opportunity__c = :OpptyId Order By Amdocs_Tenant_ID__c limit 1000];
        Opportunity OppName = [Select Name from Opportunity where Id = : OpptyId];
        OpportunityName = String.valueof(OppName.Name);
        system.debug('bulkEquipRecords-'+bulkEquipRecords);
        for(Tenant_Equipment__c equipRec:bulkEquipRecords){bulkEquips.add(new BulkEquipData(equipRec));}
        system.debug('bulkEquips-'+bulkEquips);       
        return bulkEquips;
    }
    public class BulkEquipData
    {
        public BulkEquipData(Tenant_Equipment__c cBulkEquip)
        {
            this.bulkEquip=cBulkEquip;
            this.isSelected=false;
            this.Equip_Info_Updated = cBulkEquip.Name;
            
            if(cBulkEquip.Smart_Card__c != '' && cBulkEquip.Smart_Card__c != null)
                	this.Equip_Info_Updated = this.Equip_Info_Updated+' | '+cBulkEquip.Smart_Card__c;
            if(cBulkEquip.Action__c != '' && cBulkEquip.Action__c != null)
                	this.Equip_Info_Updated = this.Equip_Info_Updated+' | '+cBulkEquip.Action__c;
            //if(cBulkEquip.Opportunity__r.Name != '' && cBulkEquip.Opportunity__r.Name != null)
            //    	this.Equip_Info_Updated = this.Equip_Info_Updated+' | '+cBulkEquip.Opportunity__r.Name;
            
            if(OpportunityName != '' && OpportunityName != null)
                	this.Equip_Info_Updated = this.Equip_Info_Updated+' | '+OpportunityName;
            if(cBulkEquip.Amdocs_Tenant_ID__c != '' && cBulkEquip.Amdocs_Tenant_ID__c != null)
                	this.Equip_Info_Updated = this.Equip_Info_Updated+' | '+cBulkEquip.Amdocs_Tenant_ID__c;
            
            //if(cBulkEquip.Is_active_elsewhere__c){                
            String CustomerName;
            if(cBulkEquip.Customer_First_Name__c != null && cBulkEquip.Customer_Last_Name__c == null)
                CustomerName = cBulkEquip.Customer_First_Name__c;
            if (cBulkEquip.Customer_First_Name__c == null && cBulkEquip.Customer_Last_Name__c != null)
                CustomerName = cBulkEquip.Customer_Last_Name__c;
            if (cBulkEquip.Customer_First_Name__c != null && cBulkEquip.Customer_Last_Name__c != null)
                CustomerName = cBulkEquip.Customer_First_Name__c + ' ' + cBulkEquip.Customer_Last_Name__c;
            
            
            if(CustomerName != null)
                this.Equip_Info_Updated = this.Equip_Info_Updated +' | '+CustomerName;
            if(cBulkEquip.Phone_Number__c != '' && cBulkEquip.Phone_Number__c != null)
                this.Equip_Info_Updated = this.Equip_Info_Updated+' | '+cBulkEquip.Phone_Number__c;
            if(cBulkEquip.Address__c != '' && cBulkEquip.Address__c != null)
                this.Equip_Info_Updated = this.Equip_Info_Updated+' | '+cBulkEquip.Address__c;
            if(cBulkEquip.Unit__c != '' && cBulkEquip.Unit__c != null)
                this.Equip_Info_Updated = this.Equip_Info_Updated+' | '+cBulkEquip.Unit__c;
            //}
            
            this.tenantId = cBulkEquip.Amdocs_Tenant_ID__c; //MS-001665 - 11May20 - Added
        }
        
        @AuraEnabled
        public Tenant_Equipment__c bulkEquip{get;set;}
        
        @AuraEnabled
        public boolean isSelected{get;set;}
        
        @AuraEnabled
        public string Equip_Info_Updated{get;set;}
        
        //MS-001665 - 11May20 - Added - Start
        @AuraEnabled
        public string tenantId{get;set;}
        //MS-001665 - End
        
    }
    @AuraEnabled
    public static List<BulkEquipData> searchBulkTenantEquipment(string OpptyId,String searchText)
    {
        List<BulkEquipData> bulkEquips=new List<BulkEquipData>();
        List<Tenant_Equipment__c> bulkEquipRecords= new List<Tenant_Equipment__c>(); 
        
        String qString = 'Select Id,Equip_Info__c,Equip_Lookup_Info__c,Tenant_Account__c,Opportunity__c, Amdocs_Tenant_ID__c,'
            +' Customer_First_Name__c,Customer_Last_Name__c,Phone_Number__c,Address__c,Unit__c,Is_active_elsewhere__c,Name,Action__c,'
            +' Smart_Card__c from Tenant_Equipment__c where Action__c=\'Active\' and Tenant_Account__c=null'
            +' and Amdocs_Tenant_ID__c != null and Opportunity__c = \'' + OpptyId + '\' ';
        
        if(!String.IsEmpty(searchText)){
            qString+= ' and (Name like \'%'+searchText+'%\' OR Smart_Card__c like \'%'+searchText+'%\' OR Amdocs_Tenant_ID__c  like \'%'+searchText+'%\' OR ';
            qString+= ' Customer_First_Name__c like \'%'+searchText+'%\' OR Customer_Last_Name__c like \'%'+searchText+'%\' OR Phone_Number__c like \'%'+searchText+'%\' OR ';
            qString+= ' Address__c like \'%'+searchText+'%\' OR Unit__c like \'%'+searchText+'%\' )';
        }
        qString+= ' Order By Amdocs_Tenant_ID__c limit 1000';
        system.debug('qString - '+qString);
        bulkEquipRecords= database.query(qString);
        
        for(Tenant_Equipment__c equipRec:bulkEquipRecords){bulkEquips.add(new BulkEquipData(equipRec));}
        
        return bulkEquips;
    }
}