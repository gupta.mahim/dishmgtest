@IsTest(SeeAllData=true)
public class UploadLeasedSmartboxfromLEXTest {
    public static testMethod void testReadFile() {
        
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        Document lstDoc = [select id,name,Body from Document where name = 'eqtest'];
        String fileContent=lstDoc.Body.toString();
        List<Smartbox__c> lexAccstoupload = UploadLeasedSmartboxfromLEX.ReadFile(fileContent,newOpp.id);
        String str = JSON.serialize(lexAccstoupload);
        UploadLeasedSmartboxfromLEX.save(str);
        
        UploadLeasedSmartboxfromLEX.getUserAccess();
    }
}