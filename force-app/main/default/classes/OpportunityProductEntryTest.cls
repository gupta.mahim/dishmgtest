@IsTest(SeeAllData=true)
public class OpportunityProductEntryTest {
	
     public static testMethod void createPricebookEntry() {
      
       
       Opportunity newOppRecTypeTest1 = TestDataFactoryforInternal.createOpportunity(1); 
   	   boolean recordType = OpportunityProductEntry.getNoPriceStage(newOppRecTypeTest1.id);
       Opportunity newOppRecTypeTest2 = TestDataFactoryforInternal.createOpportunity(2); 
   	   recordType = OpportunityProductEntry.getNoPriceStage(newOppRecTypeTest2.id);  
       Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();  
       PricebookEntry[] pricebook = OpportunityProductEntry.updateAvailableList(newOpp.id);
       opportunityLineItem[] opplineItems = OpportunityProductEntry.getOppLineItems(newOpp.id);
       String priceBookEntryID = pricebook.get(0).id+'';
       String oppID = newOpp.id +'';
       opportunityLineItem[] opplnitm = OpportunityProductEntry.createOppLineItem(JSON.serialize(pricebook),JSON.serialize(opplineItems), priceBookEntryID , newOpp.id);
       List<opportunityLineItem>  removeList = opplnitm;
       opportunityLineItem   itmToRmv = removeList.get(0);
       removeList.clear();
       removeList=OpportunityProductEntry.removeFromShoppingCart(JSON.serialize(opplnitm), itmToRmv.id,JSON.serialize(removeList), oppID);
       OpportunityProductEntry.saveRecords(JSON.serialize(opplnitm),JSON.serialize(removeList));   
  }
}