@isTest
private class AmDocs_CalloutClass_TNEMuteDC_Test{
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Incremental';
            opp1.AmDocs_ServiceID__c='012';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;
        
        Tenant_Account__c ta = new Tenant_Account__c();
            ta.Address__c ='1011 Coliie Path';
            ta.City__c = 'Round Rock';
            ta.State__c = 'TX';
            ta.Zip__c = '78664';
            ta.Unit__c = '1A';
            ta.Phone_Number__c = '5123835201';
            ta.Email_Address__c = 'jerry.clifft1111111111111@dish.com';
            ta.First_Name__c = 'Jerry';
            ta.Last_Name__c = 'Clifft';
            ta.Type__c = 'Incremental';
            ta.Opportunity__c = opp1.id;
        insert ta;
        
       Tenant_Equipment__c te = new Tenant_Equipment__c();
            te.Opportunity__c = opp1.Id;
            te.Name = 'R234567819';
            te.Smart_Card__c = 'S1234567891';
            te.Location__c = 'D';
            te.Action__c = 'Mute';
            te.Address__c ='1011 Coliie Path';
            te.City__c = 'Round Rock';
            te.State__c = 'TX';
            te.Zip_Code__c = '78664';
            te.Unit__c = '1A';
            te.Phone_Number__c = '5123835201';
            te.Email_Address__c = 'jerry.clifft@dish.com';
            te.Customer_First_Name__c = 'Jerry';
            te.Customer_Last_Name__c = 'Clifft';
            te.Tenant_Account__c = ta.id;
        insert te;

       Tenant_Equipment__c te2 = new Tenant_Equipment__c();
            te2.Opportunity__c = opp1.Id;
            te2.Name = 'R345678190';
            te2.Smart_Card__c = 'S1234567891';
            te2.Location__c = 'D';
            te2.Action__c = 'Mute';
            te2.Address__c ='1011 Coliie Path';
            te2.City__c = 'Round Rock';
            te2.State__c = 'TX';
            te2.Zip_Code__c = '78664';
            te2.Unit__c = '1A';
            te2.Phone_Number__c = '5123835201';
            te2.Email_Address__c = 'jerry.clifft@dish.com';
            te2.Customer_First_Name__c = 'Jerry';
            te2.Customer_Last_Name__c = 'Clifft';
            te2.Amdocs_Tenant_ID__c  = '1234';
        insert te2;
        
        Tenant_Equipment__c te3 = new Tenant_Equipment__c();
            te3.Opportunity__c = opp1.Id;
            te3.Name = 'R456781901';
            te3.Smart_Card__c = 'S1234567891';
            te3.Location__c = 'D';
            te3.Action__c = 'TEST';
            te3.Address__c ='1011 Coliie Path';
            te3.City__c = 'Round Rock';
            te3.State__c = 'TX';
            te3.Zip_Code__c = '78664';
            te3.Unit__c = '1A';
            te3.Phone_Number__c = '5123835201';
            te3.Email_Address__c = 'jerry.clifft@dish.com';
            te3.Customer_First_Name__c = 'Jerry';
            te3.Customer_Last_Name__c = 'Clifft';
            te3.Amdocs_Tenant_ID__c  = '1234';
            te3.Reset__c = 'ChewbaccaIsTesting';
        insert te3;
        
  }
  

 static testMethod void Acct1(){
    Tenant_Equipment__c opp = [Select Id FROM Tenant_Equipment__c WHERE Name = 'R345678190' Limit 1];
    Tenant_Equipment__c opp2 = [Select Id FROM Tenant_Equipment__c WHERE Name = 'R234567819' Limit 1];

        string id = opp.id;
        string id2 = opp2.id;
                
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(ID);
            AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(ID2);
      Test.stopTest(); 
    }
    
    static testMethod void Acct2(){
    Tenant_Equipment__c opp = [Select Id FROM Tenant_Equipment__c WHERE Name = 'R345678190' Limit 1];
    Tenant_Equipment__c opp2 = [Select Id FROM Tenant_Equipment__c WHERE Name = 'R234567819' Limit 1];

        string id = opp.id;
        string id2 = opp2.id;
                
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(ID);
            AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(ID2);
      Test.stopTest(); 
    }
    
    static testMethod void Acct3(){
    Tenant_Equipment__c opp = [Select Id FROM Tenant_Equipment__c WHERE Name = 'R345678190' Limit 1];
    Tenant_Equipment__c opp2 = [Select Id FROM Tenant_Equipment__c WHERE Name = 'R234567819' Limit 1];
    Tenant_Equipment__c opp3 = [Select Id FROM Tenant_Equipment__c WHERE Name = 'R456781901' Limit 1];

        string id = opp.id;
        string id2 = opp2.id;
        string id3 = opp3.id;
                
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(ID);
            AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(ID2);
    //        AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(ID3);
      Test.stopTest(); 
    }
    
    static testMethod void Acct4(){
    Tenant_Equipment__c opp3 = [Select Id FROM Tenant_Equipment__c WHERE Name = 'R456781901' Limit 1];

        string id3 = opp3.id;
                
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(900,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_TNEMuteDC.AmDocsMakeCalloutTNEMuteDC(ID3);
      Test.stopTest(); 
    }
}