public class AmDocs_CalloutClass_TNEMuteDC {

public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public String status;
    public cls_informationMessages[] informationMessages;
}
class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}



    @future(callout=true)

    public static void AmDocsMakeCalloutTNEMuteDC(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

    list<Tenant_Equipment__c> O = [select id, Name, Amdocs_Tenant_ID__c, Reset__c, Action__c from Tenant_Equipment__c where Id = :id AND ( Action__c = 'Disconnect' OR Action__c = 'Re-Connect' OR Action__c = 'Mute' OR Action__c = 'UnMute' OR Action__c = 'Non-Pay Disconnect' OR Action__c = 'Non-Pay Resume' OR Action__c = 'TEST') ];
        System.debug('RESULTS of the LIST lookup to the Opp object' +O);
        
    if(O[0].Amdocs_Tenant_ID__c == '' || O[0].Amdocs_Tenant_ID__c == Null ){
  
      Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
          sbc.id=id;
          sbc.API_Status__c='Error';
          sbc.Amdocs_Transaction_Code__c='Error';
          if( (O[0].Amdocs_Tenant_ID__c == '' || O[0].  Amdocs_Tenant_ID__c == Null )) {
              sbc.Amdocs_Transaction_Description__c= O[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          }
      update sbc;
      }
      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Tenant Equipment';
          apil2.Status__c='ERROR';
          apil2.Results__c=O[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          apil2.API__c='Update Unit';
          apil2.ServiceID__c=O[0].Amdocs_Tenant_ID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
  }
 
  
  }
  if(O[0].Amdocs_Tenant_ID__c != '' && O[0].Amdocs_Tenant_ID__c != Null ){        
      Datetime dt1 = System.Now();
      list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :O[0].Amdocs_Tenant_ID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1];
 
      if( timer.size() > 0 || O[0].Reset__c == 'ChewbaccaIsTesting' ) {
         system.debug('DEBUG API LOG Timer size : ' +timer.size());
         system.debug('DEBUG API LOG Info' +timer);
         system.debug('DEBUG timer 1 : ' +dt1);

        Tenant_Equipment__c sbc = new Tenant_Equipment__c(); { sbc.id=id; sbc.API_Status__c='Error';  sbc.Amdocs_Transaction_Code__c='Error'; sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; update sbc; }
        API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id; apil2.Object__c='Tenant Equipment'; apil2.Status__c='ERROR'; apil2.Results__c='You must wait atleast (2) two minutes between transactions'; apil2.API__c='Update Unit'; apil2.ServiceID__c=O[0].AmDocs_Tenant_ID__c; apil2.User__c=UserInfo.getUsername(); insert apil2; 
     }
   }   

  else if(timer.size() < 1) {

  list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
      System.debug('RESULTS of the LIST lookup to the Amdocs Login object' +A);
      System.debug('DEBUG UXF_Token' +A[0].UXF_Token__c);

JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
    jsonObj.writeFieldName('ImplUpdateProductStatusInput');
//    jsonObj.writeStartArray();
        jsonObj.writeStartObject();
      
        for(Integer i = 0; i < O.size(); i++){
            // Cease
            if( O[i].Action__c == 'Non-Pay Disconnect') { jsonObj.writeStringField('type', 'CE'); }
            // Reestablish
            if( O[i].Action__c == 'Non-Pay Resume') { jsonObj.writeStringField('type', 'ES'); }
            // Regular Disconnect
            if( O[i].Action__c == 'Disconnect') { jsonObj.writeStringField('type', 'CE'); }
            // Re-Connect
            if( O[i].Action__c == 'Re-Connect') { jsonObj.writeStringField('type', 'ES'); }
            // Suspend
            if( O[i].Action__c == 'Mute') {
                jsonObj.writeStringField('type', 'SU');
            }
            // Resume
            if( O[i].Action__c == 'UnMute') { jsonObj.writeStringField('type', 'RS'); }
            jsonObj.writeStringField('reasonCode', 'CREQ');
         }   
        jsonObj.writeEndObject();
//    jsonObj.writeEndArray();
    jsonObj.writeEndObject();
    
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
        
// if (!Test.isRunningTest()){
    HttpRequest request = new HttpRequest();  
        String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].Amdocs_Tenant_ID__c+'/updateProductStatus?sc=SS&lo=EN&ca=SF'; 
        request.setEndPoint(endpoint); 
        request.setBody(jsonObj.getAsString()); 
        request.setHeader('Content-Type', 'application/json'); 
        request.setMethod('POST'); 
        String authorizationHeader = A[0].UXF_Token__c; 
        request.setHeader('Authorization', authorizationHeader); 
        request.setTimeout(101000); 
        
        HttpResponse response = new HTTP().send(request); 
            if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) {  
            String strjson = response.getbody(); 
            JSONParser parser = JSON.createParser(strjson); 
            parser.nextToken(); parser.nextToken(); parser.nextToken(); 
            person obj = (person)parser.readValueAs( person.class);
                System.debug('DEBUG Authorization Header :' +A[0].UXF_Token__c);
                System.debug(response.toString());
                System.debug('STATUS_CODE:'+response.getStatusCode());
                System.debug(response.getBody());
                System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
                System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
                System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
                System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
                System.debug('DEBUG 7 ======== obj.Status: ' + obj.status);

    Tenant_Equipment__c sbc = new Tenant_Equipment__c();
        sbc.id=id; 
        sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
        sbc.API_Status__c=String.valueOf(response.getStatusCode()); 

        if(obj.orderID != null) { 
            sbc.AmDocs_Order_ID__c=obj.orderID; 
            sbc.Action__c = O[0].Action__c+ ' ' + 'Successful'; 
            sbc.AmDocs_Transaction_Code__c='SUCCESS'; 
            sbc.AmDocs_Transaction_Description__c=O[0].Action__c + ' SUCCESSFUL'; 
        }
        else if(obj.orderID == null) {
            sbc.Action__c = O[0].Action__c+ ' ' + 'Failed'; 
            if(obj.informationMessages != Null) { 
                sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
                sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription; 
            }
            else if(obj.informationMessages == Null) {
                sbc.Action__c = O[0].Action__c+ ' ' + 'Failed'; 
                sbc.AmDocs_Transaction_Code__c='ERROR'; 
                sbc.AmDocs_Transaction_Description__c=O[0].Action__c + ' ERROR something went wrong, please contact your Salesforce Admin';             
            }
        }
        else{} update sbc; 
        
        API_Log__c apil2 = new API_Log__c();{ 
            apil2.Record__c=id; 
            apil2.Object__c='Tenant Equipment'; 
            apil2.Status__c=obj.responseStatus; 
            apil2.Results__c=strjson; 
            apil2.API__c=O[0].Action__c; 
            apil2.ServiceID__c=O[0].AmDocs_Tenant_ID__c; 
            apil2.User__c=UserInfo.getUsername(); 
        insert apil2; 
     }
        
        }
    else if (response.getStatusCode() == Null || O[0].Reset__c == 'ChewbaccaIsTesting') { 
        Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
            sbc.id=id;  
            sbc.Action__c = O[0].Action__c+ ' ' + 'Time Out'; 
            sbc.API_Status__c=String.valueOf(response.getStatusCode());
            sbc.AmDocs_Transaction_Description__c=O[0].Action__c + ' ERROR - Missing Response Code';
        update sbc; }
        
        API_Log__c apil2 = new API_Log__c();{ 
            apil2.Record__c=id; 
            apil2.Object__c='Tenant Equipment'; 
            apil2.Status__c='Error'; 
            apil2.Results__c=(response.getBody()); 
            apil2.API__c=O[0].Action__c; 
            apil2.ServiceID__c=O[0].AmDocs_Tenant_ID__c; 
            apil2.User__c=UserInfo.getUsername(); 
        insert apil2; 
     }
    }
  }
}
}
}