@isTest
private class AmDocs_Trigger_TNResend_Test{
    //Implement mock callout tests here
  
static testMethod void AmDocs_Trigger_TNResend_Test1() {

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c (
            UXF_Token__c = 'UXFToken0123456789');

        insert aml;

        
   // Create Account 1
        Account acct1 = new Account (
            Name = 'Test Account 1',
            ToP__c = 'Integrator',
            Pyscical_Address__c = '1011 Collie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip__c = '78664',
            Amdocs_CreateCustomer__c = false,
            Distributor__c = 'PACE',
            OE_AR__c = '12345',
            Amdocs_CustomerID__c = '123',
            Amdocs_BarID__c = '456',
            Amdocs_FAID__c = '789');
        insert acct1;

        Opportunity opp1 = New Opportunity(
            Name = 'Test Opp 123456789 Testing opp1',
            First_Name_of_Property_Representative__c = 'Jerry',
            Name_of_Property_Representative__c = 'Clifft',
            Business_Address__c = '1011 Collie Path',
            Business_City__c = 'Round Rock',
            Business_State__c = 'TX',
            Business_Zip_Code__c = '78664',
            Billing_Contact_Phone__c = '512-383-5201',
            Billing_Contact_Email__c = 'jerry.clifft@dish.com',
            CloseDate=system.Today(),
            StageName='Closed Won',
            Smartbox_Leased__c = false,
            AmDocs_ServiceID__c = '1111111111',
            AmDocs_SiteID__c = '22222',
            AccountId = acct1.Id,
            AmDocs_tenantType__c = 'Digital Bulk',
            Amdocs_CustomerID__c = '123',
            Amdocs_BarID__c = '456',
            Amdocs_FAID__c = '789',
            System_Type__c='QAM',
            Category__c='FTG',
            Address__c='address',
            City__c='Austin',
            State__c='TX',
            Zip__c='78664',
            Phone__c='5122965581',
            AmDocs_Property_Sub_Type__c='Hotel/Motel',
            Number_of_Units__c=100,
            RecordTypeId='012600000005ChE');
        insert opp1;
              
        Equipment__c et = new Equipment__c(
            Opportunity__c = opp1.Id,
            Name = 'R123456789',
            Receiver_S__c = 'S-TEST',
            Location__c = 'C',
            Action__c = 'Send Trip / HIT');
        insert et;
        
       Tenant_Equipment__c te = new Tenant_Equipment__c(
            Reset__c = 'ChewbaccaIsTesting',
            Opportunity__c = opp1.Id,
            Action__c = 'Send Trip / HIT',
            Name = 'R234567819',
            Smart_Card__c = 'S-TEST',
            Location__c = 'D',
            Address__c ='1011 Coliie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip_Code__c = '78664',
            Unit__c = '1A',
            Phone_Number__c = '5123835201',
            Email_Address__c = 'jerry.clifft@dish.com',
            Customer_First_Name__c = 'Jerry',
            Customer_Last_Name__c = 'Clifft');
        insert te;
        update te;

        Smartbox__c sb = new Smartbox__c(
            Opportunity__c = opp1.Id,
            CAID__c = 'R345678912',
            SmartCard__c = 'S-TEST',
            Location__c = 'C',
            Action__c = 'Send Trip / HIT',
            Serial_Number__c = 'LALPSC011652',
            Chassis_Serial__c = 'LALPFB001327',
            NoTrigger__c = True);
        insert sb;
        
        Tenant_Equipment__c te2 = new Tenant_Equipment__c(
            Opportunity__c = opp1.Id,
            Action__c = 'REMOVE',
            Name = 'R234567819',
            Smart_Card__c = 'S-TEST',
            Location__c = 'D',
            Address__c ='1011 Coliie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip_Code__c = '78664',
            Unit__c = '1A',
            Phone_Number__c = '5123835201',
            Email_Address__c = 'jerry.clifft@dish.com',
            Customer_First_Name__c = 'Jerry',
            Customer_Last_Name__c = 'Clifft');
        insert te2;
        
        Tenant_Equipment__c te3 = new Tenant_Equipment__c(
            Opportunity__c = opp1.Id,
            Action__c = 'Mute',
            Name = 'R234567819',
            Smart_Card__c = 'S-TEST',
            Location__c = 'D',
            Address__c ='1011 Coliie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip_Code__c = '78664',
            Unit__c = '1A',
            Phone_Number__c = '5123835201',
            Email_Address__c = 'jerry.clifft@dish.com',
            Customer_First_Name__c = 'Jerry',
            Customer_Last_Name__c = 'Clifft');
        insert te3;
  }
}