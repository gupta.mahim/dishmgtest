@isTest
private class AmDocs_CalloutClass_TNAResendALL_Test{
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;
        
        Tenant_Account__c ta1 = new Tenant_Account__c();
            ta1.Address__c ='1011 Coliie Path';
            ta1.City__c = 'Round Rock';
            ta1.State__c = 'TX';
            ta1.Zip__c = '78664';
            ta1.Unit__c = '1';
            ta1.Phone_Number__c = '5123835205';
            ta1.Email_Address__c = 'jerry.clifft5555@dish.com';
            ta1.First_Name__c = 'Jerry';
            ta1.Last_Name__c = 'Clifft';
            ta1.Type__c = 'Individual';
            ta1.Opportunity__c = opp1.id;
            ta1.AmDocs_ServiceID__c = '123';
        insert ta1;
              
        Tenant_Account__c ta2 = new Tenant_Account__c();
            ta2.Address__c ='1011 Coliie Path';
            ta2.City__c = 'Round Rock';
            ta2.State__c = 'TX';
            ta2.Zip__c = '78664';
            ta2.Unit__c = '2';
            ta2.Phone_Number__c = '5123835205';
            ta2.Email_Address__c = 'jerry.clifft5555@dish.com';
            ta2.First_Name__c = 'Jerry';
            ta2.Last_Name__c = 'Clifft';
            ta2.Type__c = 'Individual';
            ta2.Opportunity__c = opp1.id;
        insert ta2;

        Tenant_Account__c ta3 = new Tenant_Account__c();
            ta3.Address__c ='1011 Coliie Path';
            ta3.City__c = 'Round Rock';
            ta3.State__c = 'TX';
            ta3.Zip__c = '78664';
            ta3.Unit__c = '3';
            ta3.Phone_Number__c = '5123835205';
            ta3.Email_Address__c = 'jerry.clifft5555@dish.com';
            ta3.First_Name__c = 'Jerry';
            ta3.Last_Name__c = 'Clifft';
            ta3.Type__c = 'Individual';
            ta3.Opportunity__c = opp1.id;
            ta3.AmDocs_ServiceID__c = '123';
            ta3.Reset__c = 'ChewbaccaIsTesting';
        insert ta3;



} 
  
    static testMethod void AmDocs_CalloutClass_TNResend_Test1(){
        Tenant_Account__c con1 = [select id from Tenant_Account__c where Unit__c = '1' limit 1];
        Tenant_Account__c con2 = [select id from Tenant_Account__c where Unit__c = '2' limit 1];
        Tenant_Account__c con3 = [select id from Tenant_Account__c where Unit__c = '3' limit 1];
        
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_TNAReSendAll.AmDocsMakeCalloutTNAReSendAll(con1.Id); 
            AmDocs_CalloutClass_TNAReSendAll.AmDocsMakeCalloutTNAReSendAll(con2.Id); 
            AmDocs_CalloutClass_TNAReSendAll.AmDocsMakeCalloutTNAReSendAll(con3.Id);                         
        Test.stopTest(); 
    }

    static testMethod void AmDocs_CalloutClass_TNResend_Test2(){
        Tenant_Account__c con1 = [select id from Tenant_Account__c where Unit__c = '1' limit 1];
        Tenant_Account__c con2 = [select id from Tenant_Account__c where Unit__c = '2' limit 1];
        Tenant_Account__c con3 = [select id from Tenant_Account__c where Unit__c = '3' limit 1];
                
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(400,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_TNAReSendAll.AmDocsMakeCalloutTNAReSendAll(con1.Id); 
            AmDocs_CalloutClass_TNAReSendAll.AmDocsMakeCalloutTNAReSendAll(con2.Id);
            AmDocs_CalloutClass_TNAReSendAll.AmDocsMakeCalloutTNAReSendAll(con3.Id); 
        Test.stopTest(); 
    }
}