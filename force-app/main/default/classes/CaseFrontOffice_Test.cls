@isTest(SeeAllData=true)
Public class CaseFrontOffice_Test{
static testMethod void testCaseFrontOffice(){
 
   Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        
        insert a;
        
    Opportunity o = new Opportunity(
        Name = 'Test Opp',
        LeadSource='Portal',
        RecordTypeId='0126000000053vu',
        misc_code_siu__c='N123',
        Restricted_Programming__c='True',
        Number_of_Units__c=1,
        CloseDate=system.Today(),
        StageName='Closed Won');
    
        insert o;
 
   Case c = new Case();
       c.Opportunity__c = o.Id;
       c.AccountId = a.Id;
       c.Status = 'Form Submitted';
       c.Origin = 'PRM';
       c.RequestedAction__c = 'Activation';
       c.Requested_Actvation_Date_Time__c = system.Today();
       
       insert c;
       update c;
       

       
   Front_Office_Request__c f = new Front_Office_Request__c();
       f.Account__c = a.Id;
       f.Opportunity__c = o.Id;
       f.RecordTypeId = '0126000000017k7';
       f.OwnerId = '00G60000001Evwv';

       insert f;
       update f;
       
       
   }
                    
static testMethod void testCaseFrontOffice2(){
 
   Account a2 = new Account();
        a2.Name = 'Test Account';
        a2.Programming__c = 'Starter Pack';
        a2.phone = '(303) 555-5555';
        a2.Misc_Code_SIU__c = 'N123';
        a2.Zip__c = '80221';
        a2.Equipment_Options__c = '1-722k';
        
        insert a2;
        
    Opportunity o2 = new Opportunity(
        Name = 'Test Opp',
        LeadSource='Portal',
        RecordTypeId='0126000000053vu',
        misc_code_siu__c='N123',
        Restricted_Programming__c='True',
        Number_of_Units__c=1,
        CloseDate=system.Today(),
        StageName='Closed Won');
    
        insert o2;
 
   Case c2 = new Case();
       c2.Opportunity__c = o2.Id;
       c2.AccountId = a2.Id;
       c2.Status = 'Form Submitted';
       c2.Origin = 'Dead';
       c2.RequestedAction__c = 'Activation';
       c2.Requested_Actvation_Date_Time__c = system.Today();
       
       insert c2;
       update c2;
       

       
   Front_Office_Request__c f2 = new Front_Office_Request__c();
       f2.Account__c = a2.Id;
       f2.Opportunity__c = o2.Id;
       f2.RecordTypeId = '0126000000017k7';
       f2.OwnerId = '00G60000001Evwv';

       insert f2;
       update f2;
       
       }
       }