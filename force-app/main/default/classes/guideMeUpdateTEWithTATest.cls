@isTest
public class guideMeUpdateTEWithTATest {
    @isTest Private static void testMethod1(){
        Opportunity Opp = TestDataFactoryForInternal.createOpportunity();
        Tenant_Account__c TA = TestDataFactoryForInternal.createTenantAccount(Opp);
        list<Tenant_Equipment__c> lstTE =  TestDataFactoryForInternal.createTenantEquipmentBulk(Opp);
        list<string> lstTEIds = new list<string>();
        for(Tenant_Equipment__c TE: lstTE){
            lstTEIds.add(string.valueOf(TE.Id));
        }
        List<guideMeUpdateTEWithTA.FlowInput> TEObjLst = new list <guideMeUpdateTEWithTA.FlowInput>();
        guideMeUpdateTEWithTA.FlowInput TeObj = new guideMeUpdateTEWithTA.FlowInput();
        TeObj.iVTEIds = lstTEIds; 
        TeObj.iVTAId = string.valueOf(TA.Id);
        TEObjLst.add(TeObj);
        guideMeUpdateTEWithTA.UpdateTEWithTA(TEObjLst);
    }
}