public class Evolve_GETEvolves {

public class person {
    public cls_devices[] devices;
}

class cls_devices{
    public String device_uid;
    public String Online;
    public cls_body body;
}

class cls_body{
    public String ROOM;
    public String ETH_MAC;
    public String WIFI_MAC;
    public String CM_MAC;
    public String CONNECTION_TYPE;
    public String ETH_IP;
    public String WIFI_IP;
    public String CM_IP;
    public String SCAN_TYPE;
    public String VERSION;
    public String SERIAL_NUM;
}

    @future(callout=true)

    public static void makeCallout(String Id) {
    
    list<Smartbox__c> S = [select id, Opportunity__r.AmDocs_ServiceID__c, UUID_Opp__c from Smartbox__c where Id=:Id];
        System.debug('RESULTS of the LIST lookup to the Smartbox object' +S);
        
    String SID = S[0].Id;
        
        list<Evolve__c> E = [select id, Location__c from Evolve__c where Smartbox__c =:Id AND Location__c = Null];
        System.debug('RESULTS of the LIST lookup to the Evolve object' +E);        

    HttpRequest request = new HttpRequest();
      String endpoint = 'https://uxikj70uyh.execute-api.us-west-2.amazonaws.com/dev/v1/devices/'+S[0].UUID_Opp__c +'*';    
//        String endpoint = 'https://i5nquk5eik.execute-api.us-east-2.amazonaws.com/dev/v1/devices/'+S[0].UUID_Opp__c +'*';
//        https://i5nquk5eik.execute-api.us-east-2.amazonaws.com/dev/v1/devices/ABCDEF_*        
//        String endpoint = 'https://wytp2gunxk.execute-api.us-east-1.amazonaws.com/dev/evolve/'+S[0].UUID_Opp__c;
//        String endpoint = 'https://webhook.site/e714da84-646a-453a-9dd0-9a3061d197c7';
        request.setEndPoint(endpoint);
        request.setMethod('GET');
        request.setHeader('Content-Type', 'application/json'); 
        request.setTimeout(101000);
        request.setHeader('Accept', 'application/json'); 
        request.setHeader('User-Agent', 'SFDC-Callout/45.0');         
        
    
        HttpResponse response = new HTTP().send(request); 
            if (response.getStatusCode() == 200) { 
                String strjson = response.getbody();
                    System.debug('DEBUG 1 === RESPONSE json string: '   + strjson);
                 JSONParser parser = JSON.createParser(strjson);
                 String shortStrJson = strjson.abbreviate(1000);
                     parser.nextToken(); 
                     person obj = (person)parser.readValueAs( person.class);
//                         System.debug('DEBUG 1.1 === Device List Size: '   + obj.devices.size());

        List<Evolve__c> evList = new List<Evolve__c>();
        for(Integer i = 0; i < obj.devices.size(); i++){
            if( obj.devices.size() >0){
                if( obj.devices[0] != Null ) {
                    Evolve__c sbc = new Evolve__c();
                        sbc.Smartbox__c=Id;
                        sbc.Online__c=obj.devices[i].Online;
                        sbc.href__c = obj.devices[i].device_uid;
                        if( obj.devices[i].body != Null ) {
                            sbc.Location__c = obj.devices[i].body.ROOM;
                            sbc.Ethernet_MAC__c = obj.devices[i].body.ETH_MAC;
                            sbc.WiFi_MAC__c= obj.devices[i].body.WIFI_MAC;
                            sbc.CM_MAC__c = obj.devices[i].body.CM_MAC;
                            sbc.Connection_Type__c = obj.devices[i].body.CONNECTION_TYPE;
                            sbc.Ethernet_IP__c = obj.devices[i].body.ETH_IP;
                            sbc.WIFI_IP__c = obj.devices[i].body.WIFI_IP;
                            sbc.CM_IP__c = obj.devices[i].body.CM_IP;
                            sbc.Scan_Type__c = obj.devices[i].body.SCAN_TYPE;
                            sbc.Version__c = obj.devices[i].body.VERSION;
                            sbc.Serial_Number__c = obj.devices[i].body.SERIAL_NUM;
                        }
                        sbc.Status_Last_Checked__c=System.Now();
//                    insert sbc;
//                      upsert sbc sbc.href__c;
                      
                      evList.add(sbc);
                          System.debug('DEBUG 2 === evList: '   + evList[i]);

                }
//                      upsert evList evList.href__c;
            }
        }

       if( obj.devices.size() >0){
           if( obj.devices[0] != Null ) {
                upsert evList evList.href__c;
                    System.debug('DEBUG 3 === evList.Upsert: '   + evList[0]);
                }
            }

       Smartbox__c sbc = new Smartbox__c(); {
           sbc.Id=Id;
           sbc.AmDocs_Transaction_Code__c = 'SUCCESS';
           sbc.AmDocs_Transaction_Description__c = 'Success: Get Evolve Equipment';
       upsert sbc;
       }

      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;       
          apil2.Object__c='Smartbox';
          apil2.Status__c='SUCCESS';       
          apil2.Results__c=shortStrJson;
          apil2.API__c='Get Evolve Equipment';       
          apil2.ServiceID__c=S[0].Opportunity__r.AmDocs_ServiceID__c;       
          apil2.User__c=UserInfo.getUsername();   
      insert apil2;  }
 }

//         for(Evolve__c DelE:E) {} DELETE E;


     else if (response.getStatusCode() == 404 ) { 
                String strjson = response.getbody();
                    System.debug('DEBUG 1 === RESPONSE json string: '   + strjson);
                 String shortStrJson = strjson.abbreviate(1000);

                    Smartbox__c sbc = new Smartbox__c(); {
                        sbc.Id=Id;
                        sbc.AmDocs_Transaction_Code__c = 'Not Found';
                        sbc.AmDocs_Transaction_Description__c = 'Error: Get Evolve' +shortStrJson;
                     upsert sbc;
                     }

                  API_Log__c apil3 = new API_Log__c();{
                      apil3.Record__c=id;       
                      apil3.Object__c='Smartbox';
                      apil3.Status__c='SUCCESS';       
                      apil3.Results__c=shortStrJson;
                      apil3.API__c='Get Evolve Equipment';       
                      apil3.ServiceID__c=S[0].Opportunity__r.AmDocs_ServiceID__c;       
                      apil3.User__c=UserInfo.getUsername();   
                  insert apil3; 
                  }
             }
             
     else if (response.getStatusCode() != 404 ) { 
                String strjson = response.getbody();
                    System.debug('DEBUG 1 === RESPONSE json string: '   + strjson);
                 String shortStrJson = strjson.abbreviate(1000);

                    Smartbox__c sbc = new Smartbox__c(); {
                        sbc.Id=Id;
                        sbc.AmDocs_Transaction_Code__c = 'API ERROR';
                        sbc.AmDocs_Transaction_Description__c = 'Error: Get Evolve' +shortStrJson;
                     upsert sbc;
                     }

                  API_Log__c apil3 = new API_Log__c();{
                      apil3.Record__c=id;       
                      apil3.Object__c='Smartbox';
                      apil3.Status__c='ERROR';       
                      apil3.Results__c=shortStrJson;
                      apil3.API__c='Get Evolve Equipment';       
                      apil3.ServiceID__c=S[0].Opportunity__r.AmDocs_ServiceID__c;       
                      apil3.User__c=UserInfo.getUsername();   
                  insert apil3; 
                  }
             }
        }
    }