@isTest
private class AmDocs_CalloutClass_CreateTV_Test{
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Incremental';
            opp1.AmDocs_ServiceID__c='012';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;

Opportunity opp2 = New Opportunity();
            opp2.Name = 'Test Opp2 123456789 Testing opp1';
            opp2.First_Name_of_Property_Representative__c = 'Jerry';
            opp2.Name_of_Property_Representative__c = 'Clifft';
            opp2.Business_Address__c = '1011 Collie Path';
            opp2.Business_City__c = 'Round Rock';
            opp2.Business_State__c = 'TX';
            opp2.Business_Zip_Code__c = '78664';
            opp2.Billing_Contact_Phone__c = '512-383-5201';
            opp2.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp2.CloseDate=system.Today();
            opp2.StageName='Closed Won';
            opp2.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp2.AmDocs_SiteID__c = '22222';
            opp2.AccountId = acct1.Id;
            opp2.AmDocs_tenantType__c = 'Individual';
            opp2.AmDocs_ServiceID__c='012';
            opp2.Amdocs_CustomerID__c = '123';
            opp2.Amdocs_BarID__c = '456';
            opp2.Amdocs_FAID__c = '789';
            opp2.System_Type__c='QAM';
            opp2.Category__c='FTG';
            opp2.Address__c='address';
            opp2.City__c='Austin';
            opp2.State__c='TX';
            opp2.Zip__c='78664';
            opp2.Phone__c='5122965581';
            opp2.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp2.Number_of_Units__c=100;
            opp2.Smartbox_Leased__c=false;
            opp2.RecordTypeId='012600000005ChE';
        insert opp2;

        Tenant_Account__c ta = new Tenant_Account__c();
            ta.Address__c ='1011 Coliie Path';
            ta.City__c = 'Round Rock';
            ta.State__c = 'TX';
            ta.Zip__c = '78664';
            ta.Unit__c = '1A';
            ta.Phone_Number__c = '5123835201';
            ta.Email_Address__c = 'jerry.clifft1111111111111@dish.com';
            ta.First_Name__c = 'Jerry';
            ta.Last_Name__c = 'Clifft';
            ta.Type__c = 'Individual';
            ta.Opportunity__c = opp2.id;
        insert ta;

        Tenant_Account__c ta5 = new Tenant_Account__c();
            ta5.Address__c ='1011 Coliie Path';
            ta5.City__c = 'Round Rock';
            ta5.State__c = 'TX';
            ta5.Zip__c = '78664';
            ta5.Unit__c = '101';
            ta5.Phone_Number__c = '5123835205';
            ta5.Email_Address__c = 'jerry.clifft5555@dish.com';
            ta5.First_Name__c = 'Jerry';
            ta5.Last_Name__c = 'Clifft';
            ta5.Type__c = 'Individual';
            ta5.Opportunity__c = opp2.id;
//            ta5.AmDocs_ServiceID__c = '123';
        insert ta5;
        
        Tenant_Account__c ta6 = new Tenant_Account__c();
            ta6.Address__c ='1011 Coliie Path';
            ta6.City__c = 'Round Rock';
            ta6.State__c = 'TX';
            ta6.Zip__c = '78664';
            ta6.Unit__c = '101';
            ta6.Phone_Number__c = '5123835205';
            ta6.Email_Address__c = 'jerry.clifft5556@dish.com';
            ta6.First_Name__c = 'Jerry';
            ta6.Last_Name__c = 'Clifft';
            ta6.Type__c = 'Individual';
            ta6.Opportunity__c = opp2.id;
//            ta6.AmDocs_ServiceID__c = '123';
            ta6.PPV__c=TRUE;
        insert ta6;

        Tenant_Account__c ta7 = new Tenant_Account__c();
            ta7.Address__c ='1011 Coliie Path';
            ta7.City__c = 'Round Rock';
            ta7.State__c = 'TX';
            ta7.Zip__c = '78664';
            ta7.Unit__c = '101';
            ta7.Phone_Number__c = '5123835205';
            ta7.Email_Address__c = 'jerry.clifft5557@dish.com';
            ta7.First_Name__c = 'Jerry';
            ta7.Last_Name__c = 'Clifft';
            ta7.Type__c = 'Individual';
            ta7.Opportunity__c = opp2.id;
            ta7.AmDocs_ServiceID__c = '123';
        insert ta7;
        
        Tenant_Account__c ta11 = new Tenant_Account__c();
            ta11.Address__c ='1011 Coliie Path';
            ta11.City__c = 'Round Rock';
            ta11.State__c = 'TX';
            ta11.Zip__c = '78664';
            ta11.Unit__c = '1A';
            ta11.Phone_Number__c = '5123835201';
            ta11.Email_Address__c = 'jerry.clifft1111111111111blah@dish.com';
            ta11.First_Name__c = 'Jerry';
            ta11.Last_Name__c = 'Clifft';
            ta11.Type__c = 'Individual';
            ta11.Opportunity__c = opp2.id;
            ta11.AmDocs_ServiceID__c = Null;
        insert ta11;

        
        Tenant_Account__c ta2 = new Tenant_Account__c();
            ta2.Address__c ='1011 Coliie Path';
            ta2.City__c = 'Round Rock';
            ta2.State__c = 'TX';
            ta2.Zip__c = '78664';
            ta2.Unit__c = '1A';
            ta2.Phone_Number__c = '5123835201';
            ta2.Email_Address__c = 'jerry.clifft2222222222222@dish.com';
            ta2.First_Name__c = 'Jerry';
            ta2.Last_Name__c = 'Clifft';
            ta2.Type__c = 'Incremental';
            ta2.Opportunity__c = opp2.id;
        insert ta2;

        Tenant_Account__c ta3 = new Tenant_Account__c();
            ta3.Address__c ='1011 Coliie Path';
            ta3.City__c = 'Round Rock';
            ta3.State__c = 'TX';
            ta3.Zip__c = '78664';
            ta3.Unit__c = '1A';
            ta3.Phone_Number__c = '5123835201';
            ta3.Email_Address__c = 'jerry.clifftABC@dish.com';
            ta3.First_Name__c = 'Jerry';
            ta3.Last_Name__c = 'Clifft';
            ta3.Type__c = 'Individual';
            ta3.Opportunity__c = opp2.id;
        insert ta3;
        
       Tenant_Equipment__c te = new Tenant_Equipment__c();
            te.Opportunity__c = opp1.Id;
            te.Name = 'R223456789';
            te.Smart_Card__c = 'S1234567891';
            te.Amdocs_Tenant_ID__c = Null;
            te.Location__c = 'D';
            te.Action__c = 'ADD';
            te.Address__c ='1011 Coliie Path';
            te.City__c = 'Round Rock';
            te.State__c = 'TX';
            te.Zip_Code__c = '78664';
            te.Unit__c = '1A';
            te.Phone_Number__c = '5123835201';
            te.Email_Address__c = 'jerry.clifft@dish.com';
            te.Customer_First_Name__c = 'Jerry';
            te.Customer_Last_Name__c = 'Clifft';
            te.Tenant_Account__c = ta.id;
        insert te;
        
        Tenant_Equipment__c te2 = new Tenant_Equipment__c();
            te2.Opportunity__c = opp2.Id;
            te2.Name = 'R223456789';
            te2.Smart_Card__c = 'S1234562891';
            te2.Location__c = 'D';
            te2.Action__c = 'ADD';
            te2.Address__c ='1011 Coliie Path';
            te2.City__c = 'Round Rock';
            te2.State__c = 'TX';
            te2.Zip_Code__c = '78664';
            te2.Unit__c = '1A';
            te2.Phone_Number__c = '5123835201';
            te2.Email_Address__c = 'jerry.clifft@dish.com';
            te2.Customer_First_Name__c = 'Jerry';
            te2.Customer_Last_Name__c = 'Clifft';
            te2.Tenant_Account__c = ta2.id;
        insert te2;

        Tenant_Pricebook__c tb = new Tenant_Pricebook__c();
            tb.Name = 'Prcebook Name';
            tb.Pricebook_Name__c = 'Prcebook Name';
        insert tb;

        Tenant_Product__c tp = new Tenant_Product__c();
          tp.Active__c = True;
          tp.AmDocs_Incremental_Caption__c = '123';
          tp.AmDocs_Incremental_ProductID__c = '123';
          tp.AmDocs_Individual_Caption__c = '123';
          tp.AmDocs_Individual_ProductID__c = '123';
          tp.Family__c = 'Core';
          tp.Product_Name__c = 'Test Product 1';
          tp.Name = 'Test Product 1';
        insert tp;

        Tenant_ProductEntry__c tpe = new Tenant_ProductEntry__c();
            tpe.Name = 'Test Product 1';
            tpe.Active__c = True;
            tpe.Tenant_Pricebook__c = tb.id;
            tpe.Tenant_Product__c = tp.id;
        insert tpe;
        
        Tenant_Product_Line_Item__c TOli = new Tenant_Product_Line_Item__c ();
            TOli.Tenant_Account__c = ta11.id;
            TOli.Action__c = 'ADD';
            TOli.Tenant_ProductEntry__c = tpe.id;
        insert TOli;
        
        Tenant_Product_Line_Item__c TOlita5 = new Tenant_Product_Line_Item__c ();
            TOlita5.Tenant_Account__c = ta5.id;
            TOlita5.Action__c = 'ADD';
            TOlita5.Tenant_ProductEntry__c = tpe.id;
        insert TOlita5;
        
        Tenant_Product_Line_Item__c TOlir = new Tenant_Product_Line_Item__c ();
            TOlir.Tenant_Account__c = ta.id;
            TOlir.Action__c = 'ADD';
            TOlir.Tenant_ProductEntry__c = tpe.id;
        insert TOlir;
  
        Tenant_Product_Line_Item__c TOli2 = new Tenant_Product_Line_Item__c ();
            TOli2.Tenant_Account__c = ta2.id;
            TOli2.Action__c = 'ADD';
            TOli2.Tenant_ProductEntry__c = tpe.id;
        insert TOli2;
        
        Tenant_Product__c tp2 = new Tenant_Product__c();
          tp2.Active__c = True;
          tp2.AmDocs_Incremental_Caption__c = '456';
          tp2.AmDocs_Incremental_ProductID__c = '456';
          tp2.AmDocs_Individual_Caption__c = '456';
          tp2.AmDocs_Individual_ProductID__c = '456';
          tp2.Family__c = 'Add-On';
          tp2.Product_Name__c = 'Test SPS Product';
          tp2.Name = 'Test SPS Product';
        insert tp2;

        Tenant_ProductEntry__c tpe2 = new Tenant_ProductEntry__c();
            tpe2.Name = 'Test SPS Product';
            tpe2.Active__c = True;
            tpe2.Tenant_Pricebook__c = tb.id;
            tpe2.Tenant_Product__c = tp2.id;
        insert tpe2;
        
        Tenant_Product_Line_Item__c TOli2a = new Tenant_Product_Line_Item__c ();
            TOli2a.Tenant_Account__c = ta11.id;
            TOli2a.Action__c = 'ADD';
            TOli2a.Tenant_ProductEntry__c = tpe2.id;
        insert TOli2a;
        
        Tenant_Product_Line_Item__c TOli5a = new Tenant_Product_Line_Item__c ();
            TOli5a.Tenant_Account__c = ta5.id;
            TOli5a.Action__c = 'ADD';
            TOli5a.Tenant_ProductEntry__c = tpe2.id;
        insert TOli5a;
        
        Tenant_Product_Line_Item__c TOli3 = new Tenant_Product_Line_Item__c ();
            TOli3.Tenant_Account__c = ta11.id;
            TOli3.Action__c = 'REMOVE';
            TOli3.Tenant_ProductEntry__c = tpe2.id;
        insert TOli3;
  
        Tenant_Product_Line_Item__c TOli4 = new Tenant_Product_Line_Item__c ();
            TOli4.Tenant_Account__c = ta3.id;
            TOli4.Action__c = 'ADD';
            TOli4.Tenant_ProductEntry__c = tpe2.id;
        insert TOli4;

        Tenant_Product__c tp4 = new Tenant_Product__c();
          tp4.Active__c = True;
          tp4.AmDocs_Incremental_Caption__c = '789';
          tp4.AmDocs_Incremental_ProductID__c = '789';
          tp4.AmDocs_Individual_Caption__c = '789';
          tp4.AmDocs_Individual_ProductID__c = '789';
          tp4.Family__c = 'Promotion';
          tp4.Product_Name__c = 'Test Promotion Product';
          tp4.Name = 'Test Promotion Product';
          tp4.Amdocs_IsPromotion__c = True;
          
        insert tp4;

        Tenant_ProductEntry__c tpe4 = new Tenant_ProductEntry__c();
            tpe4.Name = 'Test Promotion Product';
            tpe4.Active__c = True;
            tpe4.Tenant_Pricebook__c = tb.id;
            tpe4.Tenant_Product__c = tp4.id;
        insert tpe4;
        
        Tenant_Product_Line_Item__c TOli4a = new Tenant_Product_Line_Item__c ();
            TOli4a.Tenant_Account__c = ta11.id;
            TOli4a.Action__c = 'ADD';
            TOli4a.Tenant_ProductEntry__c = tpe4.id;
        insert TOli4a;
        
        Tenant_Product_Line_Item__c TOli5a2 = new Tenant_Product_Line_Item__c ();
            TOli5a2.Tenant_Account__c = ta5.id;
            TOli5a2.Action__c = 'ADD';
            TOli5a2.Tenant_ProductEntry__c = tpe4.id;
        insert TOli5a2;
        
        Tenant_Product_Line_Item__c TOli4r = new Tenant_Product_Line_Item__c ();
            TOli4r.Tenant_Account__c = ta11.id;
            TOli4r.Action__c = 'REMOVE';
            TOli4r.Tenant_ProductEntry__c = tpe4.id;
        insert TOli4r;
        
        Tenant_Equipment__c teq1 = new Tenant_Equipment__c();
            teq1.Name = 'R223456789';
            teq1.Smart_Card__c = 'S023456789';
            teq1.Action__c = 'ADD';
            teq1.Address__c = '1011 Collie Path';
            teq1.City__c = 'Round Rock';
            teq1.State__c = 'TX';
            teq1.Zip_Code__c = '78664';
            teq1.Location__c = 'D';
            teq1.Opportunity__c = opp2.id;
            teq1.Unit__c = '1';
            teq1.Tenant_Account__c = ta.Id;
         insert teq1;
         
        Tenant_Equipment__c teq2 = new Tenant_Equipment__c();
            teq2.Name = 'R1949086366';
            teq2.Smart_Card__c = 'S123456789';
            teq2.Action__c = 'ADD';
            teq2.Address__c = '1011 Collie Path';
            teq2.City__c = 'Round Rock';
            teq2.State__c = 'TX';
            teq2.Zip_Code__c = '78664';
            teq2.Location__c = 'D';
            teq2.Opportunity__c = opp2.id;
            teq2.Unit__c = '1';
            teq2.Tenant_Account__c = ta5.Id;
         insert teq2;
         
        Tenant_Equipment__c teq6 = new Tenant_Equipment__c();
            teq6.Name = 'R223456789';
            teq6.Smart_Card__c = 'S123456786';
            teq6.Action__c = 'ADD';
            teq6.Address__c = '1011 Collie Path';
            teq6.City__c = 'Round Rock';
            teq6.State__c = 'TX';
            teq6.Zip_Code__c = '78664';
            teq6.Location__c = 'D';
            teq6.Opportunity__c = opp2.id;
            teq6.Unit__c = '1';
            teq6.Tenant_Account__c = ta6.Id;
         insert teq6;

  }

  static testMethod void Acct1(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft1111111111111@dish.com' Limit 1];
    Tenant_Account__c opp2 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft2222222222222@dish.com' Limit 1];
    Tenant_Account__c opp3 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft5557@dish.com' Limit 1];

        string id = opp.id;
        string id2 = opp2.id;
        string id3 = opp3.id;
                
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID2);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID3);
        Test.stopTest(); 
    }  

  static testMethod void Acct01(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft5555@dish.com' Limit 1];

        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"ERROR","spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }  
  
 

  static testMethod void Acct4(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifftABC@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(301,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"ERROR","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }  

    static testMethod void Acct5(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft2222222222222@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(301,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"ERROR","ownerServiceId":"1335840","backendErrorCode":"1007","backendErrorMessage":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }  
  
    static testMethod void Acct6(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifftABC@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(301,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"responseStatus":"ERROR","ownerServiceId":"1335840"}}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }
    static testMethod void Acct7(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft1111111111111blah@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(301,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantServiceID":"1037992","responseStatus":"ERROR","ownerServiceId":"1335840", "orderID":"1234"}}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }  
    static testMethod void Acct8(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft1111111111111blah@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantServiceID":"1037992","responseStatus":"SUCCESS","ownerServiceId":"1335840","equipmentList":[{"action":"ADD","receiverID":"R223456789","smartCardID":"S2346380122"}], "orderID":"1234"}}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }
    static testMethod void Acct9(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft2222222222222@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(301,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"blah1":"1037992","responseStatus":"SUCCESS","blah2":"1234","informationMessages":[{"errorCode":"1007","errorDescription":"Unable to do whatever"}],"equipmentList":[{"action":"ADD","receiverID":"R223456789","smartCardID":"S2346380122"}], "blah3":"1234"}}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }
    static testMethod void Acct10(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft1111111111111@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(301,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"responseStatus":"ERROR","ownerServiceId":"1335840"}}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }
    static testMethod void Acct11(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft5556@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantServiceID":"1037992","responseStatus":"SUCCESS","ownerServiceId":"1335840","equipmentList":[{"action":"ADD","receiverID":"R223456789","smartCardID":"S2346380122"}], "orderID":"1234"}}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(ID);
        Test.stopTest(); 
    }
    static testMethod void Acct12(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft5556@dish.com' Limit 1];
        string id = opp.id;
        
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantServiceID":"1037992","responseStatus":"SUCCESS","ownerServiceId":"1335840","equipmentList":[{"action":"ADD","receiverID":"R223456789","smartCardID":"S2346380122"}], "orderID":"1234"}}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_CreateUnit.createUnit(ID);
        Test.stopTest(); 
    }
}