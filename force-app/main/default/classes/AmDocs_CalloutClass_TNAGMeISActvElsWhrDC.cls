/*-------------------------------------------------------------
Author: Jerry Clifft
Created on: 5th March 2020
Created for: Milestone# MS-000022
Description: To DISCONNECT when Tenant Account != NULL
-------------------------------------------------------------*/

public class AmDocs_CalloutClass_TNAGMeISActvElsWhrDC {
    
    public class person {
        public String serviceID;
        public String orderID;
        public String responseStatus;
        public String status;
        public cls_informationMessages[] informationMessages;
        public cls_serviceStatusOutput serviceStatusOutput;    
        
    }
    class cls_informationMessages {
        public String errorCode;
        public String errorDescription;
    }
    class cls_serviceStatusOutput {
        public String status;
        public String errorCode;
        public String errorDescription;
    }
    
    
    @AuraEnabled
    
    public static string AmDocsMakeCalloutTNAMuteDC(String Id) {
        // Added by Mahim - Start
        String returnStatus = 'Success';
        list<String> receiversForActiveElseWhereUpdate = new list<String>();
        list<Tenant_Equipment__c> teForActiveElseWhereUpdate = new list<Tenant_Equipment__c>();
        list<Tenant_Equipment__c> teWithActiveElseWhereTrue = new list<Tenant_Equipment__c>();
        // Added by Mahim - End
        try{
            list<Tenant_Account__c> T = [select id, Name, Mute_Disconnect__c, Reset__c, Action__c, AmDocs_ServiceID__c from Tenant_Account__c 
                                         where Id = :id ];
            System.debug('RESULTS of the LIST lookup to the Tenant Account object' +T);
            
            if(T[0].AmDocs_ServiceID__c == '' || T[0].AmDocs_ServiceID__c == Null ){
                
                Tenant_Account__c sbc = new Tenant_Account__c(); {
                    sbc.id=id;
                    sbc.API_Status__c='Error';
                    sbc.Amdocs_Transaction_Code__c='Error';
                    //Added by Mahim - Start
                    if(T[0].Action__c == 'DupeDisconnect'){ 
                        sbc.Action__c='Active'; 
                    } 
                    //Added by Mahim - End
                    if( (T[0].AmDocs_ServiceID__c == '' || T[0].  AmDocs_ServiceID__c  == Null )) {
                        sbc.Amdocs_Transaction_Description__c = T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
                    }
                    update sbc;
                }
                API_Log__c apil2 = new API_Log__c();{
                    apil2.Record__c=id;
                    apil2.Object__c='Tenant Account';
                    apil2.Status__c='ERROR';
                    apil2.Results__c=T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
                    apil2.API__c=T[0].Action__c;
                    apil2.ServiceID__c=T[0].AmDocs_ServiceID__c;
                    apil2.User__c=UserInfo.getUsername();
                    apil2.User_Error__c=true;
                    insert apil2;
                }
            }
            
            if(T[0].AmDocs_ServiceID__c != '' && T[0].AmDocs_ServiceID__c != Null ){ 
                Datetime dt1 = System.Now();
                list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :T[0].AmDocs_ServiceID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1]; 
                
                if( (timer.size() > 0 )  || T[0].Reset__c == 'ChewbaccaIsTesting' ) {
                    system.debug('DEBUG API LOG Timer size : ' +timer.size());
                    system.debug('DEBUG API LOG Info' +timer);
                    system.debug('DEBUG timer 1 : ' +dt1);
                    
                    Tenant_Account__c sbc = new Tenant_Account__c(); {
                        sbc.id=id; sbc.API_Status__c='Error'; 
                        sbc.Amdocs_Transaction_Code__c='Error'; 
                        sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions';
                        //Added by Mahim - Start
                        if(T[0].Action__c == 'DupeDisconnect'){ 
                            sbc.Action__c='Active'; 
                        } 
                        //Added by Mahim - End
                        update sbc;
                    }
                    API_Log__c apil2 = new API_Log__c();{ 
                        apil2.Record__c=id; 
                        apil2.Object__c='Tenant Account'; 
                        apil2.Status__c='ERROR'; 
                        apil2.Results__c='You must wait atleast (2) two minutes between transactions'; 
                        apil2.API__c=T[0].Action__c; 
                        apil2.ServiceID__c=T[0].AmDocs_ServiceID__c; 
                        apil2.User__c=UserInfo.getUsername(); 
                        apil2.User_Error__c=true;
                        insert apil2; 
                    }
                }   
                
                else if(timer.size() < 1) {
                    //Milestone #MS-001612 - Updated by Mahim - Start
                    //list<Tenant_Equipment__c> TE = [select id,name, Amdocs_Tenant_ID__c, Unit__c, Action__c, Opportunity__c from Tenant_Equipment__c where Tenant_Account__c = :id  AND Location__c = 'D'];
                    list<Tenant_Equipment__c> TE = [select id,name, Amdocs_Tenant_ID__c, Unit__c, Action__c, Opportunity__c from Tenant_Equipment__c where Tenant_Account__c = :id  AND Location__c = 'D' AND Action__c='DupeDisconnect'];
                    System.debug('RESULTS of the LIST lookup to the Tenant Equipment object' +TE);
                    
                    //list<Tenant_Product_Line_Item__c> TOli = [select id, Action__c from Tenant_Product_Line_Item__c where Tenant_Account__c = :id ];
                    list<Tenant_Product_Line_Item__c> TOli = [select id, Action__c from Tenant_Product_Line_Item__c where Tenant_Account__c = :id AND Action__c='DupeDisconnect'];
                    System.debug('RESULTS of the LIST lookup to the Core OpportunityLineItem records for TOli ' +TOli);
                    //Milestone #MS-001612 - End
                    
                    list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
                    System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);
                    
                    JSONGenerator jsonObj = JSON.createGenerator(true);
                    jsonObj.writeStartObject();
                    jsonObj.writeFieldName('ImplUpdateProductStatusInput');
                    jsonObj.writeStartObject();
                    if( T[0].Action__c == 'DupeDisconnect') { jsonObj.writeStringField('type', 'CE');}
                    jsonObj.writeStringField('reasonCode', 'CREQ');
                    jsonObj.writeEndObject();
                    jsonObj.writeEndObject();
                    String finalJSON = jsonObj.getAsString();
                    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
                    
                    HttpRequest request = new HttpRequest();
                    String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+T[0].AmDocs_ServiceID__c+'/updateProductStatus?sc=SS&lo=EN&ca=SF'; 
                    request.setEndPoint(endpoint); 
                    request.setBody(jsonObj.getAsString());
                    request.setHeader('Content-Type', 'application/json');
                    request.setMethod('POST');
                    String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader);
                    
                    HttpResponse response = new HTTP().send(request);
                    if (response.getStatusCode() == 200) { 
                        String strjson = response.getbody();
                        String TruncatedStrJson = strjson.abbreviate(1000); 
                        JSONParser parser = JSON.createParser(strjson); 
                        parser.nextToken(); 
                        parser.nextToken(); 
                        parser.nextToken(); 
                        person obj = (person)parser.readValueAs( person.class);
                        System.debug('DEBUG Full Response strjson : ' +strjson);
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug('DEBUG Response Body' +response.getBody());
                        if(TE.size() > 0){
                            if(obj.orderID != null) { 
                                for(Integer i = 0; i < TE.size(); i++){ 
                                    Tenant_Equipment__c teu = new Tenant_Equipment__c(); 
                                    teu.id=TE[i].id; 
                                    if(T[0].Action__c == 'DupeDisconnect'){ 
                                        teu.Action__c='Disconnected'; 
                                        receiversForActiveElseWhereUpdate.add(TE[i].Name);
                                    } 
                                    update teu; 
                                }
                            }
                            //Added by Mahim - Start
                            else{
                                for(Integer i = 0; i < TE.size(); i++){ 
                                    Tenant_Equipment__c teu = new Tenant_Equipment__c(); 
                                    teu.id=TE[i].id; 
                                    if(T[0].Action__c == 'DupeDisconnect'){ 
                                        teu.Action__c='Active'; 
                                    } 
                                    update teu; 
                                }
                            }
                            //Added by Mahim - End
                        }
                        
                        //Milestone #MS-001326 - Mahim - Start
                        //Update the ActiveElseWhere field of new equipment to false if the disconnect/remove is successful
                        if(receiversForActiveElseWhereUpdate.size()>0){
                            teWithActiveElseWhereTrue = [Select Id,Is_active_elsewhere__c,Where_it_s_active_elsewhere__c From Tenant_Equipment__c 
                                                         Where name in: receiversForActiveElseWhereUpdate And Is_active_elsewhere__c = true And Action__c='Add'];
                            for(Tenant_Equipment__c TEq: teWithActiveElseWhereTrue){
                                TEq.Is_active_elsewhere__c = false;
                                TEq.Where_it_s_active_elsewhere__c = null;
                                teForActiveElseWhereUpdate.add(TEq);
                            }
                            if(teForActiveElseWhereUpdate.size()>0){
                                Update teForActiveElseWhereUpdate;
                            }
                        }
                        //Milestone #MS-001326 - End
                        
                        if(TOli.size() > 0 && obj.orderID != null) { 
                            for(Integer i = 0; i < TOli.size(); i++) { 
                                Tenant_Product_Line_Item__c tpu = new Tenant_Product_Line_Item__c();{
                                    tpu.id=TOli[i].id; 
                                    if(T[0].Action__c == 'DupeDisconnect'){ 
                                        tpu.Action__c='Disconnected';
                                    }
                                    update tpu;
                                }
                            }
                        }
                        
                        Tenant_Account__c sbc = new Tenant_Account__c(); {
                            sbc.id=id; 
                            sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
                            sbc.API_Status__c=String.valueOf(response.getStatusCode());
                            if(obj.orderID != null) { 
                                sbc.AmDocs_Order_ID__c=obj.orderID; 
                                if(T[0].Action__c == 'DupeDisconnect'){
                                    sbc.Action__c='Disconnected'; 
                                }
                                sbc.AmDocs_Transaction_Description__c=T[0].Action__c + ' SUCCESSFUL'; 
                                sbc.AmDocs_Transaction_Code__c='SUCCESS';
                            }
                            if(obj.orderID == null && obj.informationMessages != null) { 
                                //Added by Mahim - Start
                                if(T[0].Action__c == 'DupeDisconnect'){
                                    sbc.Action__c='Active'; 
                                }
                                //Added by Mahim - End
                                sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription;                   
                                sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
                            }
                            if(obj.orderID == null && obj.informationMessages == null) {  
                                //Added by Mahim - Start
                                if(T[0].Action__c == 'DupeDisconnect'){
                                    sbc.Action__c='Active'; 
                                }
                                //Added by Mahim - End
                                sbc.AmDocs_Transaction_Description__c=obj.serviceStatusOutput.errorDescription;                    
                                sbc.AmDocs_Transaction_Code__c=obj.serviceStatusOutput.errorCode; 
                            }
                            
                            
                            update sbc;
                        }
                        API_Log__c apil2 = new API_Log__c();{
                            apil2.Record__c=id;
                            apil2.Object__c='Tenant Account';
                            apil2.Status__c='SUCCESS';
                            apil2.Results__c=TruncatedStrJson;
                            apil2.API__c=T[0].Action__c;
                            apil2.ServiceID__c=T[0].Amdocs_ServiceID__c;
                            apil2.User__c=UserInfo.getUsername();
                            insert apil2;
                        }
                    }
                    
                    else if(response.getStatusCode() != 200) {         
                        String strjson = response.getbody();        
                        String TruncatedStrJson = strjson.abbreviate(1000);
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug('DEBUG Response Body' +response.getBody());
                        
                        Tenant_Account__c sbc = new Tenant_Account__c(); {              
                            sbc.id=id;               
                            sbc.API_Status__c=String.valueOf(response.getStatusCode());               
                            sbc.AmDocs_Transaction_Description__c=T[0].Action__c + ' ERROR : ' +TruncatedStrJson;
                            //Added by Mahim - Start
                            if(T[0].Action__c == 'DupeDisconnect'){
                                sbc.Action__c='Active'; 
                            }
                            //Added by Mahim - End
                            update sbc; 
                        }
                        
                        //Added by Mahim - Start
                        for(Integer i = 0; i < TE.size(); i++){ 
                            Tenant_Equipment__c teu = new Tenant_Equipment__c(); 
                            teu.id=TE[i].id; 
                            if(T[0].Action__c == 'DupeDisconnect'){ 
                                teu.Action__c='Active'; 
                            } 
                            update teu; 
                        }
                        //Added by Mahim - End
                        
                        API_Log__c apil2 = new API_Log__c();{               
                            apil2.Record__c=id;                
                            apil2.Object__c='Tenant Account';               
                            apil2.Status__c='ERROR';                
                            apil2.Results__c=TruncatedStrJson;                
                            apil2.API__c=T[0].Action__c;                
                            apil2.ServiceID__c=T[0].Amdocs_ServiceID__c;               
                            apil2.User__c=UserInfo.getUsername();            
                            insert apil2;
                        }
                    }
                }
            }
            return returnStatus;
        }
        catch(Exception e) {
            returnStatus = 'The following exception has occurred: ' + e.getMessage();
            return returnStatus;
        }
    }
}