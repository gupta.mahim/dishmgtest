public class relRecordTilesController {
    @AuraEnabled
   public static List<tileWrapper> getResults(id currentID, string objectName, string lookupField,
    string field1, string field2, string field3, string field4, string field5, string field6, string field7, string field8, string field9) {
    System.debug('getResults starting...');

    //add in query fields if they are not null
    string queryfields;
    if(!String.isBlank(field1)){queryfields = ', ' +field1.trim();}
    if(!String.isBlank(field2)){queryfields = queryfields+ ', ' +field2.trim();}
    if(!String.isBlank(field3)){queryfields = queryfields+ ', ' +field3.trim();}
    if(!String.isBlank(field4)){queryfields = queryfields+ ', ' +field4.trim();}
    if(!String.isBlank(field5)){queryfields = queryfields+ ', ' +field5.trim();}
    if(!String.isBlank(field6)){queryfields = queryfields+ ', ' +field6.trim();}
    if(!String.isBlank(field7)){queryfields = queryfields+ ', ' +field7.trim();}
    if(!String.isBlank(field8)){queryfields = queryfields+ ', ' +field8.trim();}
    if(!String.isBlank(field9)){queryfields = queryfields+ ', ' +field9.trim();}
    string nameField=objectName=='Case'?'CaseNumber':'name';
    nameField=objectName=='OpportunityLineItem'?'Product_Name__c':nameField;
       //build the string we will query
    string queryString = 'SELECT id, '+nameField+' ' +queryfields+ ' FROM ' +objectName+ ' WHERE ' +lookupField+ '=' +'\''+currentID+'\' limit 5';
    system.debug('queryOutput = '+queryString);
    //get the data into a list
    List<sObject> queryOutput = new List<sObject>();
    queryOutput = database.query(queryString);
    system.debug('queryOutput = ' + queryOutput);

    //Iterate over the list and put the values into a wrapper class defined below
    List<tileWrapper> returnList = new List<tileWrapper>();
    If(queryOutput.size()>0)
    {
    for(sObject s: queryOutput)
    {
        
        tileWrapper tile = new tileWrapper();
        tile.recID = String.valueOf(s.id);
        if(objectName=='Case')tile.recName = String.valueOf(s.get('CaseNumber'));
        if(objectName=='OpportunityLineItem')tile.recName = String.valueOf(s.get('Product_Name__c'));
        if(objectName!='OpportunityLineItem' && objectName!='Case')tile.recName =String.valueOf(s.get('name'));
        
    
        //.get(fieldName).getDescribe().getLabel();
        if(!String.isBlank(field1) && s.get(field1.trim())!=null && String.valueOf(s.get(field1.trim())) != null ){tile.field1 = String.valueOf(s.get(field1.trim()));}
        if(!String.isBlank(field2) && s.get(field2.trim())!=null &&  String.valueOf(s.get(field2.trim())) != null ){tile.field2 = String.valueOf(s.get(field2.trim()));}
        if(!String.isBlank(field3) && s.get(field3.trim())!=null &&  String.valueOf(s.get(field3.trim())) != null ){tile.field3 = String.valueOf(s.get(field3.trim()));}
        if(!String.isBlank(field4) && s.get(field4.trim())!=null &&  String.valueOf(s.get(field4.trim())) != null ){tile.field4 = String.valueOf(s.get(field4.trim()));}
        if(!String.isBlank(field5) && s.get(field5.trim())!=null &&  String.valueOf(s.get(field5.trim())) != null ){tile.field5 = String.valueOf(s.get(field5.trim()));}
        if(!String.isBlank(field6) && s.get(field6.trim())!=null &&  String.valueOf(s.get(field6.trim())) != null ){tile.field6 = String.valueOf(s.get(field6.trim()));}
        if(!String.isBlank(field7) && s.get(field7.trim())!=null &&  String.valueOf(s.get(field7.trim())) != null ){tile.field7 = String.valueOf(s.get(field7.trim()));}
        if(!String.isBlank(field8) && s.get(field8.trim())!=null &&  String.valueOf(s.get(field8.trim())) != null ){tile.field8 = String.valueOf(s.get(field8.trim()));}
        if(!String.isBlank(field9) && s.get(field9.trim())!=null &&  String.valueOf(s.get(field9.trim())) != null ){tile.field9 = String.valueOf(s.get(field9.trim()));}
        returnList.add(tile);
    }
    }
    system.debug('returnList = ' + returnList);
    return returnList;        
    
    }


public class tileWrapper{
    
    @AuraEnabled
    public id recID{get;set;}
    @AuraEnabled
    public string recName{get;set;}
    
    @AuraEnabled
    public string field1{get;set;}
    @AuraEnabled
    public string field2{get;set;}
    @AuraEnabled
    public string field3{get;set;}
    @AuraEnabled
    public string field4{get;set;}
    @AuraEnabled
    public string field5{get;set;}
    @AuraEnabled
    public string field6{get;set;}
    @AuraEnabled
    public string field7{get;set;}
    @AuraEnabled
    public string field8{get;set;}
    @AuraEnabled
    public string field9{get;set;}
}
}