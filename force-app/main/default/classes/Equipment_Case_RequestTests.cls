@isTest
Public class Equipment_Case_RequestTests{
    public static testMethod void testEquipment_Case_Request(){

PageReference pageRef = Page.case_request;
Test.setCurrentPageReference(pageRef);

Opportunity newOpp = new Opportunity (name='XYZ Organization', StageName='Closed Won', CloseDate=Date.Today());
insert newOpp;

Equipment__c myEq = new Equipment__c (Name='Joe',
 Receiver_S__c='SJoe', Receiver_Model__c='311', Statis__c='Activation Requested', Programming__c='HBO',
Opportunity__c=newopp.id);
insert myEq;

Case mycas = new Case (Status='Form Submitted', Origin='PRM', Opportunity__c=newopp.id);
insert mycas;

ApexPages.StandardController sc = new ApexPages.standardController(mycas);

Equipment_Case_Request myPageCon = new Equipment_Case_Request(sc);

myPageCon.getequip();
}
}