public class AmDocs_CalloutClass_TESwap {

public class person {
 public String tenantServiceID;
 public String orderID;
 public String responseStatus;
 public cls_informationMessages[] informationMessages;
 public cls_equipmentList[] equipmentList;
}

class cls_informationMessages {
 public String errorCode;
 public String errorDescription;
}

class cls_equipmentList {
 public string action;
 public string receiverID;
}


 @future(callout=true)

 public static void AmDocsMakeCalloutTESwap(String Id) {
 
list<Tenant_Equipment__c> T = [select id, Amdocs_Tenant_ID__c, Reset__c, Smart_Card__c, Name, Address__c, City__c, State__c, Zip_Code__c, Unit__c, Phone_Number__c, Email_Address__c, Customer_First_Name__c, Customer_Last_Name__c, Location__c, Action__c, Opportunity__c from Tenant_Equipment__c where Id = :id  AND Action__c = 'SWAP PENDING' AND Location__c = 'D' AND Tenant_Account__c = ''];
  System.debug('RESULTS of the LIST lookup to the Tenant Equipment object' +T);  

  if(T[0].Amdocs_Tenant_ID__c == '' || T[0].Amdocs_Tenant_ID__c == Null ){
  
      Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
          sbc.id=id;
          sbc.API_Status__c='Error';
          sbc.Amdocs_Transaction_Code__c='Error';
          if( (T[0].Amdocs_Tenant_ID__c == '' || T[0].  Amdocs_Tenant_ID__c == Null )) {
              sbc.Amdocs_Transaction_Description__c = T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          }
      update sbc;
      }
      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Tenant Equipment';
          apil2.Status__c='ERROR';
          apil2.Results__c=T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          apil2.API__c='SWAP';
          apil2.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
  }
 
  
  }
  if(T[0].Amdocs_Tenant_ID__c != '' && T[0].Amdocs_Tenant_ID__c != Null ){        
      Datetime dt1 = System.Now();
      list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :T[0].Amdocs_Tenant_ID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1];
            
       if( (timer.size() > 0 )  || T[0].Reset__c == 'ChewbaccaIsTesting' ) {
           system.debug('DEBUG API LOG Timer size : ' +timer.size());
           system.debug('DEBUG API LOG Info' +timer);
           system.debug('DEBUG timer 1 : ' +dt1);

       Tenant_Equipment__c sbc = new Tenant_Equipment__c(); { sbc.id=id; sbc.API_Status__c='Error';  sbc.Amdocs_Transaction_Code__c='Error'; sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; update sbc; }
       API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id; apil2.Object__c='Tenant Equipment'; apil2.Status__c='ERROR'; apil2.Results__c='You must wait atleast (2) two minutes between transactions'; apil2.API__c='TRIP/Resend'; apil2.ServiceID__c=T[0].AmDocs_Tenant_ID__c; apil2.User__c=UserInfo.getUsername(); insert apil2; 
     }
   }   

   else if(timer.size() < 1) {

list<AmDocs_Login__c> A = [select id, UserName__c, UserPassword__c, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
  System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);  
  
JSONGenerator jsonObj = JSON.createGenerator(true);
jsonObj.writeStartObject();
    jsonObj.writeFieldName('ImplUpdateTVRestInput');
    jsonObj.writeStartObject();
        jsonObj.writeStringField('orderActionType', 'CH');
        jsonObj.writeStringField('reasonCode', 'CREQ');
        jsonObj.writeFieldName('equipmentList');
        jsonObj.writeStartArray();

            if(T.size() > 0) {
                for(Integer i = 0; i < T.size(); i++){
                    jsonObj.writeStartObject();
                        jsonObj.writeStringField('action', 'SWAP');
                        jsonObj.writeStringField('receiverID', T[i].Name);
                        jsonObj.writeStringField('smartCardID', T[i].Smart_Card__c);
                    jsonObj.writeEndObject();
                }
            }
        jsonObj.writeEndArray();
    jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));


// if (!Test.isRunningTest()){ 
    HttpRequest request = new HttpRequest(); 
        String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+T[0].Amdocs_Tenant_ID__c+'/updateTV?sc=SS&lo=EN&ca=SF'; 
        request.setTimeout(101000); 
        request.setEndPoint(endpoint);
        request.setBody(jsonObj.getAsString());
        request.setHeader('Content-Type', 'application/json'); 
        request.setMethod('POST'); 
        String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader); 
            
    HttpResponse response = new HTTP().send(request); 
        if (response.getStatusCode() == 200 ) { 
            String strjson = response.getbody();
            JSONParser parser = JSON.createParser(strjson);
                parser.nextToken();
                parser.nextToken();
                parser.nextToken();
            person obj = (person)parser.readValueAs( person.class);
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
                System.debug('STATUS_CODE:'+response.getStatusCode());
                System.debug(response.getBody());
                System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.tenantServiceID);
                System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
                System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
  
            Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
                sbc.id=id;
                sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now();
                sbc.AmDocs_Order_ID__c=obj.orderID;
                if (obj.tenantServiceID != Null && obj.orderID != Null) {
                    sbc.Amdocs_Tenant_ID__c=obj.tenantServiceID; 
                    sbc.Action__c='Active'; 
                }
                sbc.API_Status__c=String.valueOf(response.getStatusCode());
                if(obj.informationMessages != Null) {
                    sbc.AmDocs_Transaction_Description__c='SMARTCARD SWAP ERROR ' +obj.informationMessages[0].errorDescription;
                    sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode;
                }
                else if(obj.informationMessages == Null) {  
                    if(obj.tenantServiceID != Null) { 
                        sbc.Amdocs_Tenant_ID__c=obj.tenantServiceID; 
                        sbc.Action__c='Active';
                        sbc.AmDocs_Transaction_Description__c='SMARTCARD SWAP ' +obj.responseStatus;
                        sbc.AmDocs_Transaction_Code__c=obj.responseStatus; 
                    }
                }
                else if(obj.responseStatus == Null) { 
                    sbc.AmDocs_Transaction_Description__c='SMARTCARD SWAP ERROR Request Rejected'; 
                    sbc.AmDocs_Transaction_Code__c='ERROR';
                }
                update sbc;
                }

                if (obj.tenantServiceID != Null && obj.orderID != Null) {                
                    API_Log__c apil2 = new API_Log__c();{
                        apil2.Record__c=id;
                        apil2.Object__c='Tenant Equipment';
                        apil2.Status__c='SUCCESS';
                        apil2.Results__c=strjson;
                        apil2.API__c='SWAP';
                        apil2.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
                        apil2.User__c=UserInfo.getUsername();
                    insert apil2;
                    }
                    }
                
                if (obj.tenantServiceID == Null || obj.orderID == Null) {                
                    API_Log__c apil3 = new API_Log__c();{
                        apil3.Record__c=id;
                        apil3.Object__c='Tenant Equipment';
                        apil3.Status__c='ERROR';
                        apil3.Results__c=response.getBody();
                        apil3.API__c='SWAP';
                        apil3.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
                        apil3.User__c=UserInfo.getUsername();
                    insert apil3;
                    }
                    }
                }
                else if(response.getStatusCode() != 200) { 
                    Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
                        sbc.id=id;
                        sbc.Action__c='SMARTCARD SWAP FAILED';
                        sbc.API_Status__c='API ERROR - SMARTCARD SWAP ' +String.valueOf(response.getStatusCode());
                        sbc.AmDocs_Transaction_Description__c='SMARTCARD SWAP ERROR ' +response.getBody();
                    update sbc; 
                    }
                    
                    API_Log__c apil3 = new API_Log__c();{
                        apil3.Record__c=id;
                        apil3.Object__c='Tenant Equipment';
                        apil3.Status__c='ERROR';
                        apil3.Results__c=response.getBody();
                        apil3.API__c='SWAP';
                        apil3.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
                        apil3.User__c=UserInfo.getUsername();
                    insert apil3;
                    }
                    }
                }
        }
    }
}