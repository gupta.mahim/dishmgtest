public class FO_Programming_Ext {
private List<opportunityLineItem> Prods;
private Front_Office_Request__c fo;
    public FO_Programming_Ext(ApexPages.StandardController controller) {
    this.fo=(Front_Office_Request__c)controller.getRecord();
    }

  public List<opportunityLineItem> getprods() {
       Front_Office_Request__c f = [SELECT ID, Opportunity__r.id from Front_Office_Request__c where id = :fo.id];
       if (f.Opportunity__c == null)
       return null;

       prods = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, Status__c
       FROM opportunityLineItem WHERE OpportunityId = :f.Opportunity__r.id ];
         


        return prods;

}

}