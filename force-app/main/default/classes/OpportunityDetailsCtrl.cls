public class OpportunityDetailsCtrl
{
    @AuraEnabled
    public static PropDetailsResponse loadPropDetails(String oppId)
    {
        //Milestone #MS-001604 - Added Locals_Provider__c field in below field
        Opportunity opp=[Select Id,Name,Category__c,AmDocs_Property_Sub_Type__c,AmDocs_tenantType__c,Status__c,
                         AmDocs_SiteID__c,Address__c,City__c,State__c,Zip__c,AmDocs_ServiceID__c, Locals_Provider__c
                         from Opportunity where id=:oppId];
        
        //Milestone #MS-001604 - Added by Mahim - Start
        PropDetailsResponse resp=new PropDetailsResponse();
        resp.oppDetails = opp;
        
        List<OpportunityLineItem> lstOLI = [SELECT Product_Name__c FROM OpportunityLineItem
                                            WHERE  Action__c IN ('Active','Remove') AND Family__c IN ('Core','Add-On','Promotion')
                                            AND Opportunity.AmDocs_tenantType__c = 'Incremental' AND OpportunityId =:oppId];
        if(lstOLI.size()>0){
            for(Integer i=0; i<lstOLI.size(); i++){
                if(i == 0) {resp.oppProdDetails = lstOLI[i].Product_Name__c;}
                else {resp.oppProdDetails = resp.oppProdDetails + ', ' + lstOLI[i].Product_Name__c;}
            }
        }
        system.debug('resp.oppProdDetails-'+resp.oppProdDetails);
        return resp;
        //Milestone #MS-001604 - End 
    }
    
    //Milestone #MS-001604 - Added by Mahim - Start
    public Class PropDetailsResponse
    {
        @AuraEnabled
        public Opportunity oppDetails{get;set;}
        
        @AuraEnabled
        public String oppProdDetails{get;set;}
    }
    //Milestone #MS-001604 - End
}