({
	doInit : function(component, event, helper)
    {
        helper.searchProds(component, event, helper,true);
    }
    ,onSelectAllChange: function(component, event, helper)
    {
    	helper.handleSelectAllChange(component,event);
    }
    ,onLanguageChange : function(component, event, helper)
    {
        helper.searchProds(component, event, helper,false);
    }
    ,coreSelected : function(component, event, helper)
    {
        var selectedCore = event.getSource().get("v.text");
        component.set("v.isCoreSelected",true);
        var prodFamily=component.get("v.prodFamily");
        var selectedData=component.get("v.selectedData");
        var removedData=component.get("v.removedData");
        var removedDisplayData=component.get("v.removedDisplayData");
        var existingProducts=component.get("v.existingProducts");
        var thisProdData=component.get("v.data");
        var prodEntry={};
        //if(!removedData){removedData=[];}
        removedData=[];
        if(!removedDisplayData){removedDisplayData={};}
        for(var d=0;d<thisProdData.length;d++)
        {
            if(thisProdData[d].productEntry.Id==selectedCore)
            {
                prodEntry=thisProdData[d];
                break;
            }     
        }
        
        for(var ep = 0; ep < existingProducts.length; ep++)
        {
            if(existingProducts[ep].Family__c=='Core')
            {
                if(existingProducts[ep].Tenant_ProductEntry__r.Id!=selectedCore)
                {
                    removedData.push(existingProducts[ep]);
                }
                else
                {
                    for(var rd = 0; rd < removedData.length; rd++)
                    {
                        if(removedData[rd].Tenant_ProductEntry__r.Id==selectedCore)
                        {
                            removedData.splice(rd,1);
                        }
                    }
                }
            }
        }
        var prodEntries=[];
        selectedData={};
        if(prodEntry && prodEntry!=null)
        {
            if(!prodEntry.productEntry.isExisting || prodEntry.productEntry.isExisting==null || prodEntry.productEntry.isExisting==false)
            {
        		prodEntries.push(prodEntry);
        		selectedData[prodFamily]=prodEntries;        
            }
        }
        removedDisplayData[prodFamily]=removedData;
        component.set("v.selectedData",selectedData);
        component.set("v.removedData",removedData);
        component.set("v.removedDisplayData",removedDisplayData);
    }
    ,prodSelected : function(component, event, helper)
    {
        var isSelected=event.getSource().get("v.value");
        var selectedProdId=event.getSource().get("v.text");
        var prodFamily=component.get("v.prodFamily");
        var selectedData=component.get("v.selectedData");
        var removedData=component.get("v.removedData");
        var removedDisplayData=component.get("v.removedDisplayData");
        
        var existingProducts=component.get("v.existingProducts");
        
        var thisProdData=component.get("v.data");
        var prodEntry={};
        
        for(var d=0;d<thisProdData.length;d++)
        {
            if(thisProdData[d].productEntry.Id==selectedProdId)
            {
                prodEntry=thisProdData[d];
                break;
            }     
        }
        var prodEntries=[];
        if(!removedData){removedData=[];}
        if(!removedDisplayData){removedDisplayData={};}
        if(!selectedData){selectedData={};}
        else{prodEntries= selectedData[prodFamily];}
        if(isSelected==true)
        {
            if(prodEntry && prodEntry!=null)
        	{
                if(!prodEntry.productEntry.isExisting || prodEntry.productEntry.isExisting==null || prodEntry.productEntry.isExisting==false)
                {
                    prodEntries.push(prodEntry);
                }
        	}
            
            for(var rd = 0; rd < removedData.length; rd++)
            {
                if(removedData[rd].Tenant_ProductEntry__r.Id==selectedProdId)
                {
                    removedData.splice(rd,1);
                }
            }
        }
        else
        {
            for(var i = 0; i < prodEntries.length; i++)
            { 
               if ( prodEntries[i].productEntry.Id === selectedProdId){prodEntries.splice(i, 1);}
            }
            for(var ep = 0; ep < existingProducts.length; ep++)
            {
                if(existingProducts[ep].Tenant_ProductEntry__r.Id==selectedProdId)
                {
                    removedData.push(existingProducts[ep]);
                }
            }
        }
        selectedData[prodFamily]=prodEntries;
        removedDisplayData[prodFamily]=removedData;
        
        component.set("v.selectedData",selectedData);
        component.set("v.removedData",removedData);
        component.set("v.removedDisplayData",removedDisplayData);
    }
    ,searchProdsOnChange : function(component, event, helper) 
    {
		   helper.searchProds(component, event, helper,false);     
    }
    ,onNext : function(component, event, helper) {        
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber+1);
        helper.buildData(component, helper);
    },
    
    onPrev : function(component, event, helper) {        
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber-1);
        helper.buildData(component, helper);
    },
    
    processMe : function(component, event, helper) {
        component.set("v.currentPageNumber", parseInt(event.target.name));
        helper.buildData(component, helper);
    },
    
    onFirst : function(component, event, helper) {        
        component.set("v.currentPageNumber", 1);
        helper.buildData(component, helper);
    },
    
    onLast : function(component, event, helper) {        
        component.set("v.currentPageNumber", component.get("v.totalPages"));
        helper.buildData(component, helper);
    }
})