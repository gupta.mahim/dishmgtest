({
    helperMethod : function(component) {
        var action;
        
        /*
Method to call the getValidateOpptyAddress method which sets the value of callNetQuals or callBulks in component
which calls the API methods of callBulk and callNetQual functionality.
*/                    
        action = component.get("c.getValidateTAAddress");
        action.setParams({"recId": component.get("v.recordId")
                         });
        console.log('Value of recordID' +component.get("v.recordId"));
        action.setCallback(this, function(response){
            console.log('callBack');
            var state = response.getState();
            var Id = component.get("v.recordId");
            console.log('Id'+Id);
            if (state === "SUCCESS") {
                console.log('SUCCESS2');
                var storeResponse = response.getReturnValue();
                console.log('storeRespponse'+JSON.stringify(storeResponse));
                component.set("v.OverrideNetQual",storeResponse.overRideNetQualWr);
                console.log('callNetQual2'+ storeResponse.callNetQWr);
                console.log('callNetQual3'+ storeResponse.validationMsgWr);
                if((storeResponse.callNetQWr == true)){
                    /*
Method to remove the Validation Message on Tenant Account record after the page reload.
*/                                    
                    var action1 = component.get("c.removeValidationMsg");
                    action1.setParams({"recId": component.get("v.recordId"),
                                       "callNQl": storeResponse.callNetQWr,
                                       "vMsgExist": storeResponse.validationMsgWr
                                      });
                    action1.setCallback(this,function(response2){
                        var state = response2.getState();
                        if(state === 'SUCCESS'){
                            console.log('SUCCESS3');
                            console.log('callNetQual5');
                            var retResponse = response2.getReturnValue();
                            console.log('retResponse'+JSON.stringify(retResponse));
                        }else if (state === "ERROR") {
                            console.log('Error2');
                        }
                    });
                    
                    component.set("v.callNetQuals", storeResponse.callNetQWr);
                    
                    window.setTimeout(function(){
                        console.log('callNetQual3')
                        window.open('/lex/s/detail/'+Id,'_top')      
                    }, 2000);
                    
                    $A.enqueueAction(action1);        
                    
                }     
                
                if((storeResponse.callNetQWr == false) && (storeResponse.validationMsgWr == true)){
                    component.set("v.validationMsgValue",storeResponse.validationMsgValueWr);
                    /*
Method to delete the Custom Setting Record after the page reloadS.
*/                         
                                    var action1 = component.get("c.updateValidationMsg");
                                    action1.setParams({"recId": component.get("v.recordId"),
                                                       "callNQl": storeResponse.callNetQWr,
                                                       "vMsgExist": storeResponse.validationMsgWr
                                                      });
                                    action1.setCallback(this,function(response3){
                                        var state = response3.getState();
                                        if(state === 'SUCCESS'){
                                            console.log('SUCCESS4');
                                            var retResponse = response3.getReturnValue();
                                            console.log('retResponse1'+JSON.stringify(retResponse));
                                            //             component.set("v.validationMsgValue",retResponse1);
                                        }else if (state === "ERROR") {
                                            console.log('Error2');
                                        }
                                    });
                                    $A.enqueueAction(action1);        
                                    
                                }
                
            }
            else{
                var status = response.getError();
                console.log('errorStatus'+JSON.stringify(status));
                console.log('FAILURE');
            }
        });
        $A.enqueueAction(action);
        
    },
    
    
})