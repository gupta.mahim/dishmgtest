({
    /* Calling the server side controller to get the list of record types accessible to the logged in user */
	getRecordTypes : function(component,event) {
        console.log('getRecordTypes Called');
		var action=component.get("c.getRecordTypes");
        action.setParams({
            ObjectApiName : component.get("v.objectApiName")
        });
        
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                console.log(response.getReturnValue());
                var rtlist=response.getReturnValue();
                if(rtlist.length===0){
                	//No recordtype Available
                    var rt={}; 
                    rt.rtName='No Record Type Available';
                    rt.rtId=null;
                    rtlist.push(rt);
                	component.set("v.recordtypeList",rtlist);
                }
                else{
                    //Check for Invalid Api Name
                    if(rtlist[0].incorrectApiName===true){
                        component.set("v.invalidApiName",true);
                    }
                    else{
                        component.set("v.recordtypeList",rtlist);
                    }
                }
            }
            else{
                console.log('State: '+state);
                if(state==="ERROR")
                    console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
	},
    
    /* Calling the server side controller to get the Picklist fields dependency map
     * &  on successful response, calling 'getRecordTypes' helper method 
     * to get the list of record types accessible to the logged in user
    */
    getFieldDependencyMap : function(component,event) {
        console.log('getFieldDependencyMap Called');
        var action=component.get("c.getFieldDependencyMap");
        action.setParams({
            ObjectApiName : component.get("v.objectApiName"),
            CfieldApiName : component.get("v.controllingFieldApiName"),
            DfieldApiName : component.get("v.dependentFieldApiName")            
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                console.log(response.getReturnValue());
                component.set("v.fieldDependencMap",response.getReturnValue());
                this.getRecordTypes(component,event);
            }
            else{
                console.log('State: '+state);
                if(state==="ERROR")
                    console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    /* Calling server side controller to get picklist values based on the chosen record type */
    getPicklistValues : function(component,event, fieldApiName,fieldtype){
        console.log(' getPicklistValues Called: '+ fieldtype);
        var action=component.get("c.getPicklistValues");
		action.setParams({
            recordTypeId : component.get("v.selectedRecordTypeId"),
            ObjectApiName : component.get("v.objectApiName"),
            fieldApiName : fieldApiName
        });
        
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                console.log(response.getReturnValue());
                if(fieldtype==='Controlling')
                	component.set("v.controllingPicklistValues",response.getReturnValue());
                if(fieldtype==='Dependent')
                    component.set("v.dependentPicklistValuesCache",response.getReturnValue());
                
                component.set("v.showSpinner",false);
            }
            else{
                console.log('State: '+state);
                if(state==="ERROR")
                    console.log(response.getError());
            }
        });
        
       $A.enqueueAction(action); 
    },
      handleRecordTypeChange : function(component, event, helper) {
      
        //Show spinner
        component.set("v.showSpinner",true);
        
        helper.getPicklistValues(component,event,component.get("v.controllingFieldApiName"),'Controlling');
        helper.getPicklistValues(component,event,component.get("v.dependentFieldApiName"),'Dependent');
        var dependentPicklistValues=[];
        component.set("v.dependentPicklistValues",dependentPicklistValues);
        component.set("v.selectedControllingPicklistValue",null);
         component.set("v.showSpinner",false);
      
        
    }
})