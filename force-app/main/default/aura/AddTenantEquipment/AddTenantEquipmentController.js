({
    init: function (component, event, helper) 
    {
        var oppId=component.get("v.OppId");
        
        var action = component.get("c.initBulkTenantEquipment");
        action.setParams({ "OpptyId" : oppId});
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS")
            {
                var prodResp=response.getReturnValue();
                component.set("v.existingEquips",prodResp);
                
                //MS-001665 - 11May20 - Added - Start
                //getting the unique Tenant ID list
                var flags = [], output = [], i;
                for( i=0; i<prodResp.length; i++) {
                    if( flags[prodResp[i].tenantId]) continue;
                    flags[prodResp[i].tenantId] = true;
                    output.push(prodResp[i].tenantId);
                }
                component.set("v.tenantIdsExistingEquips",output);
                //MS-001665 - End
                
                if(prodResp.length>0)
                    component.set("v.displayExistingEquipTab",true);
                else
                    component.set("v.displayExistingEquipTab",false);
                    
            }});        
        $A.enqueueAction(action);
        var isFromBack = component.get("v.IsBackFromNextScr");
        
        if(isFromBack!='yes')
        {
            
            helper.addTERecords(component, event);
            $A.enqueueAction(component.get('c.ValdiateInp'));
        }
        
        
    },
    
    //MS-001665 - 11May20 - Added - Start
    rdbProdSelected: function(component, event, helper)
    {
        var selectedTenantID=event.getSource().get("v.value");
        console.log('selectedTenantID'+selectedTenantID);
        component.set("v.selectedExistingEquipTenantID",selectedTenantID);
    },
    //MS-001665 - End
    
    prodSelected : function(component, event, helper)
    {
        var isSelected=event.getSource().get("v.value");
        var selectedProdId=event.getSource().get("v.text");
        var thisProdData=component.get("v.existingEquips");
        var oppId=component.get("v.OppId");
        var taId=component.get("v.taId");
        var prodEntries=component.get("v.selectedExistingEquipList");
        if(!prodEntries){prodEntries={};}
        if(thisProdData)
        {
            var prodEntry={};
            for(var d=0;d<thisProdData.length;d++)
            {
                prodEntry={};
                if(thisProdData[d].bulkEquip.Id==selectedProdId)
                {
                    prodEntry=thisProdData[d].bulkEquip;
                    
                    break;
                }     
            }
            if(isSelected==true)
            {
                if(prodEntry && prodEntry!=null)
                {
                    prodEntry.Tenant_Account__c=taId;
                    prodEntry.Opportunity__c=oppId;
                    prodEntries.push(prodEntry);
                    
                }
            }
            else
            {
                for(var i = 0; i < prodEntries.length; i++)
                { 
                    if ( prodEntries[i].Id === selectedProdId){prodEntries.splice(i, 1);}
                }
            }
        } 
        component.set("v.selectedExistingEquipList",prodEntries);
    },
    addRow: function(component, event, helper) {
        helper.addTERecords(component, event);
        //this.ValdiateInp(component, event, helper);
        $A.enqueueAction(component.get('c.ValdiateInp'));
    },
    
    
    ValdiateInpSC : function(component, event, helper) {
        
        var inpCmp= component.get('v.addedEquipList');
        for (var indexVar = 0; indexVar < inpCmp.length; indexVar++) {
            if (inpCmp[indexVar].Smart_Card__c.length < 11) 
            {
                
                component.set("v.Invalidinp",false);
            }
            else
            {
                component.set("v.Invalidinp",true);
            }
        }
    },
    ValdiateInp : function(component, event, helper) {
        
        var inpCmp= component.get('v.addedEquipList');
        for (var indexVar = 0; indexVar < inpCmp.length; indexVar++) {
            if (inpCmp[indexVar].Name.length < 11) 
            {
                
                component.set("v.Invalidinp",false);
            }
            else
            {
                component.set("v.Invalidinp",true);
            }
        }
    },
    
    removeRow: function(component, event, helper) {
        //Get the account list
        var addedEquipList = component.get("v.addedEquipList");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        addedEquipList.splice(index, 1);
        component.set("v.addedEquipList", addedEquipList);
    },
    
    searchEquipOnChange : function(component, event, helper){
        component.set("v.displayExistingEquipTab",true);
        component.set("v.isLoading", true); 
        var oppId=component.get("v.OppId");
        var searchText=component.get("v.searchText");
        if(!searchText){searchText="";}
        var action = component.get("c.searchBulkTenantEquipment");
        action.setParams({ "OpptyId" : oppId, searchText:searchText});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                var prodResp=response.getReturnValue();
                
                //MS-001665 - 11May20 - Added - Start
                //getting the unique Tenant ID list
                var flags = [], output = [], i;
                for( i=0; i<prodResp.length; i++) {
                    if( flags[prodResp[i].tenantId]) continue;
                    flags[prodResp[i].tenantId] = true;
                    output.push(prodResp[i].tenantId);
                }
                component.set("v.tenantIdsExistingEquips",output);
                //console.log('Test-'+ JSON.stringify(component.get("v.tenantIdsExistingEquips")));
                //MS-001665 - End
                
                component.set("v.existingEquips",prodResp);
                component.set("v.isLoading", false); 
            }});        
        $A.enqueueAction(action);
        
        /*var isFromBack = component.get("v.IsBackFromNextScr");
        if(isFromBack!='yes'){
            helper.addTERecords(component, event);
            $A.enqueueAction(component.get('c.ValdiateInp'));
        } */   
    },
    
    //MS-001691 - 29May2020 -Start
    btnNextClick : function(component, event, helper) {       
        var equipList = component.get("v.addedEquipList");
        var i;
        
        for(i=0;i<equipList.length;i++){
            if(equipList[i].Name.length>0 && equipList[i].Name.length != 11){
                alert('Receiver# length must be 11 characters.');
                return false;
            }
            if(equipList[i].Name.length>0 && equipList[i].Name.charAt(0) != 'R' && equipList[i].Name.charAt(0) != 'r'){
                alert('Receiver# must start with letter "R".');
                return false;
            }
            if(equipList[i].Smart_Card__c.length>0 && equipList[i].Smart_Card__c.length != 11){
                alert('Smart Card length must be 11 characters.');
                return false;
            }
            if(equipList[i].Smart_Card__c.length>0 && equipList[i].Smart_Card__c.charAt(0) != 'S' && equipList[i].Smart_Card__c.charAt(0) != 's'){
                alert('Smart Card must start with letter "S".');
                return false;
            }
        }
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    },
    
    btnPreviousClick : function(component, event, helper) {
        var navigate = component.get("v.navigateFlow");
        navigate("BACK");        
    },
    //MS-001691 - End
})