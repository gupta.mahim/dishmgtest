({
    addTERecords: function(component, event) {
        var CheckInvalid = component.get("v.Invalidinp");
        //get the account List from component  
        var addedEquipList = component.get("v.addedEquipList");
        //Add New Account Record
        addedEquipList.push({
            'Name': '',
            'Smart_Card__c': '',
            'Opportunity__c':component.get("v.OppId"),
            'Address__c':component.get("v.address"),
            'City__c':component.get("v.city"),
            'State__c':component.get("v.state"),
            'Zip_Code__c':component.get("v.zipCode"),
            'Unit__c':component.get("v.unit"),
            'Customer_First_Name__c':component.get("v.firstName"),
            'Customer_Last_Name__c':component.get("v.lastName"),
            'Phone_Number__c':component.get("v.phone"),
            'Email_Address__c':component.get("v.email"),
            //Milestone MS-001664 - 2July2020 Commenting out below line. Address cannot be updated if it is already Validated
            //'Valid_Address__c':true,			
            
            
            'Tenant_Account__c':component.get("v.taId")
		}
        );
        component.set("v.addedEquipList", addedEquipList);
		}
})