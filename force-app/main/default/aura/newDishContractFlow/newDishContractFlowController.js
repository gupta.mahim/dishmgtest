({
    init : function (component) {
        // Find the component whose aura:id is "flowData"
        var flow = component.find("flowData");
        var varRecordId=component.get("v.recordId");
         var inputVariables = [
         { name : "recordId", type : "String", value: varRecordId },
         { name : "VarIssCommunity", type : "Boolean", value: true }
         ];
        
        // In that component, start your flow. Reference the flow's API Name.
        flow.startFlow("Create_New_Dish_Contract",inputVariables);
    },
})