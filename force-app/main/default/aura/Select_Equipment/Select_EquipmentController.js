({
    proceedDisconnect1 : function(component, event, helper) 
    {
        var equipData=component.get("v.Equips")
        component.set("v.modalMsg","");
        var disconnectData=[];
        for(var obj=0;obj<equipData.length;obj++)
        {
            if(equipData[obj].disconnect)
            {
                disconnectData.push(equipData[obj]);    
            }
        }
        component.set("v.isModalOpen", true);
        if(disconnectData.length==0)
        {
            component.set("v.modalMsg","Please select equipment to disconnect.");
        }
        if(disconnectData.length==1)
        {
            var aJsonObj=disconnectData[0];
            var action=helper.invokeDisconnect(component,aJsonObj,false,null,null);
            helper.msgStartDisconnect(component,aJsonObj);
            $A.enqueueAction(action);
        }
        else
        {
            var allPromises=[];
            var promiseDelay=10*1000;
            
            for(var dData=0;dData<disconnectData.length;dData++)
            {
                var isWaiting=true;
                if(dData==((disconnectData.length)-1)){isWaiting=false;}
                var dPromise=function(component)
                {
                    var disconnectPromise=new Promise(function (resolve, reject) 
                                                      {
                                                          var aJsonObj=disconnectData[dData];
                                                          var action=helper.invokeDisconnect(component,aJsonObj,isWaiting,resolve, reject);
                                                          
                                                          if(dData>0)
                                                          {
                                                              setTimeout(function()
                                                                         {
                                                                             var hasAmdocsError=component.get("v.amdocsError");
                                                                             //if(hasAmdocsError==false)
                                                                             //{
                                                                             helper.msgStartDisconnect(component,aJsonObj);
                                                                             $A.enqueueAction(action); 
                                                                             //}
                                                                         },promiseDelay);
                                                          }
                                                          else
                                                          {
                                                              helper.msgStartDisconnect(component,aJsonObj);
                                                              $A.enqueueAction(action);
                                                          }
                                                      });
                    return disconnectPromise;
                }
                allPromises.push(dPromise(component));
            }
            Promise.all(allPromises);
        }
    },
    loadEquip : function(component, event, helper) 
    {
        var action = component.get("c.getActiveEquips");
        
        var teIds=component.get("v.TenantActiveProdut");
        var heIds=component.get("v.HEActiveProdut");
        var sbIds=component.get("v.SBActiveProdut");
        
        var teIdsJSON=JSON.stringify(teIds);
        var heIdsJSON=JSON.stringify(heIds);
        var sbIdsJSON=JSON.stringify(sbIds);
        
        action.setParams({
            strEquipIds: teIdsJSON
            ,strHEIds: heIdsJSON
            ,strSBIds: sbIdsJSON
        });
        action.setCallback(this,function(a){
            component.set("v.Equips", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    closeModel: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
    },
    
    //Milestone: MS-000022 - Mahim - Start    
    proceedDisconnectNew : function(component, event, helper) 
    {
        //MS-001649 - 20May20 - Start
        var d = new Date();
        //Adding 2 minutes to the time when Disconnect process started
		var disconnectTime = d.getTime() + 2*60000; 
        component.set('v.strDisconnectTime', disconnectTime.toString());
        //MS-001649 - End
        
        var equipData=component.get("v.Equips")
        component.set("v.modalMsg","");
        var disconnectTEData=[];
        var disconnectHESBData=[];
        for(var obj=0;obj<equipData.length;obj++){
            if(equipData[obj].disconnect){
                if(equipData[obj].isSB == true || equipData[obj].isHE == true){
                    disconnectHESBData.push(equipData[obj]);    
                }
                if(equipData[obj].isTE == true ){
                    disconnectTEData.push(equipData[obj]);    
                }
            }
        }
        
        console.log('disconnectHESBData - '+ JSON.stringify(disconnectHESBData));
        console.log('disconnectTEData - '+ JSON.stringify(disconnectTEData));
        
        component.set("v.isModalOpen", true);
        if(disconnectHESBData.length==0 && disconnectTEData.length==0)
        {
            component.set("v.modalMsg","Please select equipment to disconnect.");
        }
        
        if(disconnectTEData.length>0){
            
            /*to find unique Tenant ID's in the list */
            var flags = [], tenantIDList = []; var receiverNumberList = ' '; var receiverforHESB = ' ';
            for(var i=0;i<disconnectTEData.length;i++){
                if( flags[disconnectTEData[i].TenantID]) continue;
                flags[disconnectTEData[i].TenantID] = true;
                tenantIDList.push(disconnectTEData[i].TenantID);
            }
            console.log('tenantIDList - '+tenantIDList)
            
            var action1 = component.get("c.setActionforTE");
            var jsonData=JSON.stringify(disconnectTEData);
            action1.setParams({lstTEData: jsonData});
            action1.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS"){
                    if(tenantIDList.length==1){
                        for(var j=0;j<disconnectTEData.length;j++){
                            if(tenantIDList[0] == disconnectTEData[j].TenantID){
                                receiverNumberList+=' '+ disconnectTEData[j].receiverNumber;
                                receiverforHESB = disconnectTEData[j].receiverNumber;
                            }
                        }
                        if(tenantIDList[0] != null && tenantIDList[0] != '' && tenantIDList[0] != undefined){
                        	receiverNumberList+=' - (Amdocs Tenant ID: '+ tenantIDList[0]+ ')';
                        }
                        console.log('receiverforHESB - '+ receiverforHESB);
                        //var aJsonObj=disconnectTEData[0];
                        var action2=helper.invokeTEDisconnect(component,tenantIDList[0],receiverforHESB,false,null,null);
                        helper.msgStartDisconnect(component,receiverNumberList,true);
                        $A.enqueueAction(action2);
                    }
                    if(tenantIDList.length>1){
                        var allPromises=[];
                        var promiseDelay=20*1000;
                        
                        for(var dData=0;dData<tenantIDList.length;dData++){
                            receiverNumberList= ' ';
                            //alert('dData & TenantID[dData] - '+dData & tenantIDList[dData]);
                            for(var j=0;j<disconnectTEData.length;j++){
                                if(tenantIDList[dData] == disconnectTEData[j].TenantID){
                                    receiverNumberList+=' '+ disconnectTEData[j].receiverNumber;
                                    receiverforHESB = disconnectTEData[j].receiverNumber;
                                }
                            }
                            if(tenantIDList[dData] != null && tenantIDList[dData] != '' && tenantIDList[dData] != undefined){
                            	receiverNumberList+=' - (Amdocs Tenant ID: '+ tenantIDList[dData]+ ')';
                            }
                            helper.msgStartDisconnect(component,receiverNumberList,true);
                            var isWaiting=true;
                            if(dData==((tenantIDList.length)-1)){isWaiting=false;}
                            var dPromise=function(component){
                                var disconnectPromise=new Promise(
                                    function(resolve, reject){
                                        //var aJsonObj=disconnectTEData[dData];
                                        var action2=helper.invokeTEDisconnect(component,tenantIDList[dData],receiverforHESB,isWaiting,resolve,reject);
                                        
                                        if(dData>0){
                                            setTimeout(function(){
                                                var hasAmdocsError=component.get("v.amdocsError");
                                                //alert('dData - '+dData );
                                                //alert('receiverNumberList - '+receiverNumberList );
                                                //helper.msgStartDisconnect(component,receiverNumberList,true);
                                                $A.enqueueAction(action2); 
                                            },promiseDelay);
                                        }
                                        else{
                                            //alert('dData - '+dData );
                                            //helper.msgStartDisconnect(component,receiverNumberList,true);
                                            $A.enqueueAction(action2);
                                        }
                                    });
                                return disconnectPromise;
                            }
                            allPromises.push(dPromise(component));
                        }
                        Promise.all(allPromises);
                    }
                    /*for(var i=0;i<tenantIDList.length;i++){
                        
                        for(var j=0;j<disconnectTEData.length;j++){
                            if(tenantIDList[i] == disconnectTEData[j].TenantID)
                                receiverNumberList+=' '+ disconnectTEData[j].receiverNumber;
                        }
                        alert('receiverNumberList - '+ receiverNumberList);
                        var action2 = component.get("c.disconnectTENew");
                        action2.setParams({strTenantId: tenantIDList[i]});
                        action2.setCallback(this, function (response) {
                            var state2 = response.getState();
                            if (state2 === "SUCCESS"){
                                var disconnectResp=response.getReturnValue();
                                console.log("disconnectResp-"+ JSON.strigify(disconnectResp)); 
                                var oldLoadMsg=component.get("v.modalMsg");
                                var seperator="";
                                var newLoadMsg="";
                                if(!disconnectResp){
                                    component.set("v.amdocsError",true);
                                    newLoadMsg="<br/>"+"An unexpected error has occurred. Please contact your Salesforce Administrator."; 
                                }
                                if(disconnectResp.apiStatus=="SUCCESS"){
                                    newLoadMsg=oldLoadMsg+"<br/>"+disconnectResp.apiDesc;
                                    //if(isWaiting==true){newLoadMsg+="<br/>Waiting 2 minutes for previous work order to complete...";}
                                }else{
                                    component.set("v.amdocsError",true);
                                    newLoadMsg=oldLoadMsg+"<br/>"+disconnectResp.apiDesc;
                                }
                                component.set("v.modalMsg",newLoadMsg);
                            }else{ 
                                //console.log('Error occured while disconnecting the Tenant Equipment. Error Details: '+ JSON.strigify(response.getError()));
                                var errors = response.getError();
                                console.info(response.getError()[0]);
                                var oldLoadMsg=component.get("v.modalMsg");
                                var newLoadMsg=oldLoadMsg+"<br/>"+response.getError()[0].message;
                                newLoadMsg+="<br/><br/><b>Equipment disconnect process has completed. Please review above logs and proceed.</b>";
                                component.set("v.modalMsg",newLoadMsg);    
                                component.set("v.amdocsError",true);
                            }
                        }); 
                        helper.msgStartDisconnect(component,receiverNumberList,true);
                        $A.enqueueAction(action2);
                    }*/
                }
                else{ 
                    console.log('Error occured while setting the action on Tenant Equipment. Error Details: '+ JSON.strigify(response.getError()));
                }
            });
            $A.enqueueAction(action1);       
        }
        
        if(disconnectHESBData.length>0){
            
            /*to find unique Service ID's in the list */
            var flags = [], serviceIDList = []; var receiverNumberList = ' '; var receiverforTE = ' ';
            for(var i=0;i<disconnectHESBData.length;i++){
                if( flags[disconnectHESBData[i].ServiceID]) continue;
                flags[disconnectHESBData[i].ServiceID] = true;
                serviceIDList.push(disconnectHESBData[i].ServiceID);
            }
            console.log('serviceIDList - '+serviceIDList)
            
            var action1 = component.get("c.setActionforHESB");
            var jsonData=JSON.stringify(disconnectHESBData);
            action1.setParams({lstHESBData: jsonData});
            action1.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS"){
                    if(serviceIDList.length==1){
                        for(var j=0;j<disconnectHESBData.length;j++){
                            if(serviceIDList[0] == disconnectHESBData[j].ServiceID){
                                receiverNumberList+=' '+ disconnectHESBData[j].receiverNumber;
                            	receiverforTE = disconnectHESBData[j].receiverNumber;
                            }
                        }
                        if(serviceIDList[0] != null && serviceIDList[0] != '' && serviceIDList[0] != undefined){
                        	receiverNumberList+=' - (Amdocs Service ID: '+ serviceIDList[0] + ')';
                        }
                        //var aJsonObj=disconnectHESBData[0];
                        var action2=helper.invokeHESBDisconnect(component,serviceIDList[0],receiverforTE,false,null,null);
                        helper.msgStartDisconnect(component,receiverNumberList,true);
                        $A.enqueueAction(action2);
                    }
                    if(serviceIDList.length>1){
                        var allPromises=[];
                        var promiseDelay=20*1000;
                        
                        for(var dData=0;dData<serviceIDList.length;dData++){
                            receiverNumberList= ' ';
                            //alert('dData & serviceIDList[dData] - '+dData & serviceIDList[dData]);
                            for(var j=0;j<disconnectHESBData.length;j++){
                                if(serviceIDList[dData] == disconnectHESBData[j].ServiceID){
                                    receiverNumberList+=' '+ disconnectHESBData[j].receiverNumber;
                                    receiverforTE = disconnectHESBData[j].receiverNumber;
                                }
                            }
                            if(serviceIDList[dData] != null && serviceIDList[dData] != '' && serviceIDList[dData] != undefined){
                            	receiverNumberList+=' - (Amdocs Service ID: '+ serviceIDList[dData] + ')';
                            }
                            helper.msgStartDisconnect(component,receiverNumberList,true);
                            var isWaiting=true;
                            if(dData==((serviceIDList.length)-1)){isWaiting=false;}
                            var dPromise=function(component){
                                var disconnectPromise=new Promise(
                                    function(resolve, reject){
                                        //var aJsonObj=disconnectHESBData[dData];
                                        var action2=helper.invokeHESBDisconnect(component,serviceIDList[dData],receiverforTE,isWaiting,resolve,reject);
                                        //alert('action2- '+ JSON.stringify(action2));
                                        if(dData>0){
                                            setTimeout(function(){
                                                var hasAmdocsError=component.get("v.amdocsError");
                                                //alert('dData - '+dData );
                                                //alert('receiverNumberList - '+receiverNumberList );
                                                //helper.msgStartDisconnect(component,receiverNumberList,true);
                                                $A.enqueueAction(action2); 
                                            },promiseDelay);
                                        }
                                        else{
                                            //alert('dData - '+dData );
                                            //helper.msgStartDisconnect(component,receiverNumberList,true);
                                            $A.enqueueAction(action2);
                                        }
                                    });
                                return disconnectPromise;
                            }
                            allPromises.push(dPromise(component));
                        }
                        Promise.all(allPromises);
                    }
                }
                else{ 
                    console.log('Error occured while setting the action on Tenant Equipment. Error Details: '+ JSON.strigify(response.getError()));
                }
            });
            $A.enqueueAction(action1);       
        }
    },
    //Milestone: MS-000022 - Mahim - End
    
    //MS-001664 - 14May20 - Start
    btnNextClick : function(component, event, helper) {
        //navigate to the next screen
        var response = event.getSource().getLocalId();
        component.set("v.value", response);
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    },
    
    btnPreviousClick : function(component, event, helper) {
        var teIds=component.get("v.AddedEquipmentIds");        
        var teIdsJSON=JSON.stringify(teIds);
        
        var action=component.get("c.deleteAddedEquips");
        action.setParams({strEquipIds: teIdsJSON});
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                var response = event.getSource().getLocalId();
                component.set("v.value", response);
                var navigate = component.get("v.navigateFlow");
                navigate("BACK");
            }
            else{
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    //MS-001664 - End
})