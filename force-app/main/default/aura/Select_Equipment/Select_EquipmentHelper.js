({
	invokeDisconnect : function(component,aJsonObj,isWaiting,resolve,reject)
    {
		console.info("Helper "+aJsonObj.receiverNumber);
        var action = component.get("c.proceedDisconnect");
        var loadDelay=30*1000;
            
        var jsonData=JSON.stringify(aJsonObj);
        action.setParams({strEData: jsonData});
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.info(state+" " + aJsonObj.receiverNumber);
            if (component.isValid() && state === "SUCCESS"){
                //setTimeout(function()
                //{
                   //var recLoadAction = component.get("c.loadRecord");
                   //recLoadAction.setParams({strEData: jsonData});
                   //recLoadAction.setCallback(this, function (response) {
                   //console.info("Load "+response.getState()+" "+aJsonObj.receiverNumber);
                   //if (component.isValid() && response.getState() === "SUCCESS")
                   //{
                     var disconnectResp=response.getReturnValue();
                    console.info("disconnectResp"); 
                	console.info(disconnectResp); 
                	var oldLoadMsg=component.get("v.modalMsg");
                     var seperator="";
                     var newLoadMsg="";
                    if(!disconnectResp)
                    {
                       	component.set("v.amdocsError",true);
                        newLoadMsg="<br/>"+"An unexpected error has occurred. Please contact your Salesforce Administrator."; 
                    }
                	 if(disconnectResp.apiStatus=="SUCCESS")
                     {
                         newLoadMsg=oldLoadMsg+"<br/>"+disconnectResp.apiDesc;
                         if(isWaiting==true){newLoadMsg+="<br/>Waiting 2 minutes for previous work order to complete...";}
                         //if(isWaiting==false){newLoadMsg+="<br/><b>Equipment disconnect process has completed. Please review above logs and proceed.</b>";}
                     }
                     else
                     {
                         component.set("v.amdocsError",true);
                         newLoadMsg=oldLoadMsg+"<br/>"+disconnectResp.apiDesc;
                         //newLoadMsg=oldLoadMsg+"<br/>"+"An unexpected error has occurred. Please contact your Salesforce Administrator.";
                         //if(isWaiting==false){newLoadMsg+="<br/><br/><b>Equipment disconnect process has completed. Please review above logs and proceed.</b>";}
                     }  
                     component.set("v.modalMsg",newLoadMsg);    
                     //console.info(response.getReturnValue());
                     //if(resolve){resolve(response.getReturnValue());}    
                   //}});
                   //console.info("Invoking load action"); 
                   //$A.enqueueAction(recLoadAction);
                 //}, loadDelay);
            }
            else if (component.isValid() && state === "ERROR"){
                var errors = response.getError();
                console.info(response.getError()[0]);
                var oldLoadMsg=component.get("v.modalMsg");
                var newLoadMsg=oldLoadMsg+"<br/>"+response.getError()[0].message;
                if(isWaiting==false){newLoadMsg+="<br/><br/><b>Equipment disconnect process has completed. Please review above logs and proceed.</b>";}
                component.set("v.modalMsg",newLoadMsg);    
                component.set("v.amdocsError",true);
                if(reject){reject(response.getError()[0]);}
            }
        });
        return action;
    },
    msgStartDisconnect : function(component,aJsonObj,isTe)
    {
        var oldMsg=component.get("v.modalMsg");
        var newMsg;
        var seperator="";
        if(oldMsg && oldMsg!=null && oldMsg!=""){seperator="<br/>";}else{oldMsg=""}
        if(isTe)
        	newMsg=oldMsg+seperator+"Disconnecting "+aJsonObj+"...<br/>";
        else
            newMsg=oldMsg+seperator+"Disconnecting "+aJsonObj.receiverNumber+"...<br/>";
        component.set("v.modalMsg",newMsg);
    },
    
    //Milestone: MS-000022 - Mahim - Start
    invokeTEDisconnect : function(component,tenantID,receiverforHESB,isWaiting,resolve,reject)
    {
        console.info("tenantID- "+tenantID);
        var action = component.get("c.disconnectTENew");
        var loadDelay=30*1000;
        
        var jsonData=JSON.stringify(tenantID);
        //action.setParams({strEData: jsonData});
        action.setParams({strTenantId: tenantID, ReceiverforHESB: receiverforHESB});
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.info(state+" " + tenantID);
            if (component.isValid() && state === "SUCCESS"){
                var disconnectResp=response.getReturnValue();
                console.info("disconnectResp"); 
                console.info(disconnectResp); 
                var oldLoadMsg=component.get("v.modalMsg");
                var seperator="";
                var newLoadMsg="";
                if(!disconnectResp || disconnectResp == null || disconnectResp == '')
                {
                    component.set("v.amdocsError",true);
                    newLoadMsg="<br/>"+"An unexpected error has occurred. Please contact your Salesforce Administrator."; 
                }
                if(disconnectResp.apiStatus=="SUCCESS")
                {
                    newLoadMsg=oldLoadMsg+"<br/>"+disconnectResp.apiDesc+"<br/>";
                    if(isWaiting==true){newLoadMsg+="<br/>Waiting for previous work order to complete...";}
                }
                else
                {
                    component.set("v.amdocsError",true);
                    newLoadMsg=oldLoadMsg+"<br/>"+disconnectResp.apiDesc+"<br/>";
                }  
                component.set("v.modalMsg",newLoadMsg);
            }
            else if (component.isValid() && state === "ERROR"){
                var errors = response.getError();
                console.info(response.getError()[0]);
                var oldLoadMsg=component.get("v.modalMsg");
                var newLoadMsg=oldLoadMsg+"<br/>"+response.getError()[0].message;
                if(isWaiting==false){newLoadMsg+="<br/><br/><b>Equipment disconnect process has completed. Please review above logs and proceed.</b>";}
                component.set("v.modalMsg",newLoadMsg);    
                component.set("v.amdocsError",true);
                if(reject){reject(response.getError()[0]);}
            }
        });
        return action;
    },
    
    invokeHESBDisconnect : function(component,serviceID,receiverforTE,isWaiting,resolve,reject)
    {
        console.info("serviceID- "+serviceID);
        var action = component.get("c.disconnectHESBNew");
        action.setParams({strServiceId: serviceID, ReceiverforTE: receiverforTE});
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.info(state+" " + serviceID);
            if (component.isValid() && state === "SUCCESS"){
                var disconnectResp=response.getReturnValue();
                console.info("disconnectResp"); 
                console.info(disconnectResp); 
                var oldLoadMsg=component.get("v.modalMsg");
                var seperator="";
                var newLoadMsg="";
                if(!disconnectResp || disconnectResp == null || disconnectResp == '')
                {
                    component.set("v.amdocsError",true);
                    newLoadMsg="<br/>"+"An unexpected error has occurred. Please contact your Salesforce Administrator."; 
                }
                if(disconnectResp.apiStatus=="SUCCESS")
                {
                    newLoadMsg=oldLoadMsg+"<br/>"+disconnectResp.apiDesc+"<br/>";
                    if(isWaiting==true){newLoadMsg+="<br/>Waiting for previous work order to complete...";}
                }
                else
                {
                    component.set("v.amdocsError",true);
                    newLoadMsg=oldLoadMsg+"<br/>"+disconnectResp.apiDesc+"<br/>";
                }  
                component.set("v.modalMsg",newLoadMsg);
            }
            else if (component.isValid() && state === "ERROR"){
                var errors = response.getError();
                console.info(response.getError()[0]);
                var oldLoadMsg=component.get("v.modalMsg");
                var newLoadMsg=oldLoadMsg+"<br/>"+response.getError()[0].message;
                if(isWaiting==false){newLoadMsg+="<br/><br/><b>Equipment disconnect process has completed. Please review above logs and proceed.</b>";}
                component.set("v.modalMsg",newLoadMsg);    
                component.set("v.amdocsError",true);
                if(reject){reject(response.getError()[0]);}
            }
        });
        return action;
    },
    //Milestone: MS-000022 - Mahim - End
})