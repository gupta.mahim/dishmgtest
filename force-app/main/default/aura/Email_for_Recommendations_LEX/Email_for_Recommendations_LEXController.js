({
    doInit : function (component, event) {
        var id= component.get ("v.recordId");
        var action= component.get("c.getRecommnedContent");
        action.setParams({
            "execId":id
        });
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            console.info(a.getReturnValue());
            //check if result is successfull
            if(state === "SUCCESS")
            {
            var urlEvent = $A.get("e.force:navigateToURL");
            if(urlEvent){
                    urlEvent.setParams({
                      "url": a.getReturnValue(),
                      "isredirect": "true"
                    });
                    urlEvent.fire();
            }else{
                
                sforce.one.navigateToURL(a.getReturnValue(),true);
                
            }
        }
                
           else if (state === "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +errors[0].message);
                    }
                } }
            else {
                    console.log("Unknown error");
                }

        });
      
        $A.enqueueAction(action);
       
    }
})