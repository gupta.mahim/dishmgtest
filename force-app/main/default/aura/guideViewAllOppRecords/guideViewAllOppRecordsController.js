({
    getOppRecordctlr : function(component, event, helper) 
    {
        var action = component.get("c.getOppRecords");
        action.setParams({
           
            id:component.get("v.SelectedOpportunity"),
            CaseId:component.get("v.CaseId"),
            CasePSAId:component.get("v.CaseIdPSA")
            
        });
        action.setCallback(this,function(a){
            component.set("v.oppDatalist", a.getReturnValue());
            
        });
        $A.enqueueAction(action); 
    }
})