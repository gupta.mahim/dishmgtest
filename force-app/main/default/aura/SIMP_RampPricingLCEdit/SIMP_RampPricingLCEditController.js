({
	saveRecordCntrlr : function(component, event, helper) {
          component.find("recordHandler").saveRecord($A.getCallback(function(saveResult) {
              
            //alert(saveResult.state);
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
            	var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Ramp Pricing",
                    "message": "Ramp Pricing Values Saved!",
                    "type" : "success"
                });
                toastEvent.fire();
            	component.set("v.curView", "baseView" );            
            } 
        }));
	},
 
	cancelSaveRecord : function(component, event, helper){
		component.set("v.curView", "baseView" );
	}
})