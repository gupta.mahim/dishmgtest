({
	createObj: function(fieldName, fieldValue) {
        var obj = new Object();
        obj.fieldName = fieldName;
        obj.fieldValue = fieldValue;
        console.log(obj);
        return obj;
    },
    
    refresh : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
})