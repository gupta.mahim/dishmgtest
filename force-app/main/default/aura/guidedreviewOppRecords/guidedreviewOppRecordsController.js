({
    doInit : function(component, event, helper) {
        //Milestone #MS-000971 - Mahim -Start
        
        //if(component.get('v.Propertytype') != 'FTG' || component.get('v.PropertySubType') != 'Hotel/Motel')
        //    component.set("v.HotelPortfolioOwnerId",null);
                
        //if(component.get('v.HotelPortfolioOwnerId') != null){
            var OwnerId = component.get('v.HotelPortfolioOwnerId');  
            console.log('OwnerId_'+OwnerId);
            var action=component.get("c.getHotelPortfolioOwnerName");
            action.setParams({
                HotelPFOwnerId : OwnerId
            });
            action.setCallback(this,function(response){
                var state=response.getState();
                if(state==="SUCCESS"){
                    console.log(response.getReturnValue());
                    component.set("v.HotelPortfolioOwnerName",response.getReturnValue());
                }
                else{
                    if(state==="ERROR")
                        console.log(response.getError());
                }
            });
            $A.enqueueAction(action);
        //}
        //Milestone #MS-000971 - End
        
        //Milestone# MS-001092 - Mahim - Start
        var selectedDistributorAPI = component.get('v.selectedDistributor');  
        var action2=component.get("c.getSelectedDistributorValue");
        action2.setParams({
            selDistAPI : selectedDistributorAPI
        });
        action2.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                component.set("v.selectedDistributorValue",response.getReturnValue());
            }
            else{
                if(state==="ERROR")
                    console.log(response.getError());
            }
        });
        $A.enqueueAction(action2);
        //Milestone# MS-001092 - Mahim - End
        
        
        
    },    
})