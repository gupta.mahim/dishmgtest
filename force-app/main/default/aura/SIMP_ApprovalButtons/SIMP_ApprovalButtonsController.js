({
    submitApprovalJS : function(component, event, helper) {
        var action = component.get("c.submitApprovalQte");
        console.log(action);
        action.setParams({ 
            "appID" : component.get("v.recordId"),
            "reason" : "submit"
        });
        component.set("v.Spinner", true);
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS"){ 
                $A.get('e.force:refreshView').fire(); 
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Quote Sent for Approval",
                    "type" : "success"
                });
                toastEvent.fire();
            }
        }); 
        $A.enqueueAction(action);
    },
    recallApprovalJS : function(component, event, helper) {
        var action = component.get("c.submitApprovalQte");
        console.log(action);
        action.setParams({ 
            "appID" : component.get("v.recordId"),
            "reason" : "recall"
        });
        component.set("v.Spinner", true);
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS"){ 
                $A.get('e.force:refreshView').fire(); 
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Quote Sent for Recall",
                    "type" : "success"
                });
                toastEvent.fire();
            }
        }); 
        $A.enqueueAction(action);
    },
    checkStatus : function(component, event, helper){
        var getQuoteRecType = component.get("c.fetchQuoteName");
        getQuoteRecType.setParams({ 
            "parentId" : component.get("v.recordId"),
            "parentApiName" : "SBQQ__Quote__c",
            "purpose" : "RecType",
        });
        
        getQuoteRecType.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS"){ 
                component.set("v.disableMe",response.getReturnValue());
            }
        }); 
        $A.enqueueAction(getQuoteRecType);
        
        var getQuoteRecApprovalR = component.get("c.fetchQuoteName");
        getQuoteRecApprovalR.setParams({ 
            "parentId" : component.get("v.recordId"),
            "parentApiName" : "SBQQ__Quote__c",
            "purpose" : "Reason",
        });
        
        getQuoteRecApprovalR.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS"){ 
                component.set("v.approvalReason",response.getReturnValue());
            }
        }); 
        $A.enqueueAction(getQuoteRecApprovalR);
    },
    
     hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})