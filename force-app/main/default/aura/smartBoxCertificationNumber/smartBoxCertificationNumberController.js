({
    btnNextClick : function(component, event, helper) {
        console.log('selectedSmartCertNumber' +component.get('v.selectedSmartCertNumber'));
        
        //Milestone# MS-001296 - Mahim - Added condition to check if the logged in user is of World Cinema profile
	
        //Added by Mahim - Do not check Smartbox Certification Number mandatory validation if 
        // selectedSystemType != 'Headend Smartbox' or 'Smartbox + L-Band' ------------------- Start
        if (component.get('v.isWCI') == true || (component.get('v.selectedSystemType') != 'Headend Smartbox' && component.get('v.selectedSystemType') != 'Smartbox + L-Band')){
            //navigate to the next screen
            var response = event.getSource().getLocalId();
            component.set("v.value", response);
            var navigate = component.get("v.navigateFlow");
            navigate("NEXT");
        }
        else{
            if (component.get('v.selectedSystemType') == 'Headend Smartbox' || component.get('v.selectedSystemType') == 'Smartbox + L-Band'){  
                //Added by Mahim ------------------- End
                if (component.get('v.selectedSmartCertNumber') != null && component.get('v.selectedSmartCertNumber') != ''){           
                    //navigate to the next screen
                    var response = event.getSource().getLocalId();
                    component.set("v.value", response);
                    var navigate = component.get("v.navigateFlow");
                    navigate("NEXT");
                } 
                else {
                    component.set("v.recordStatusMessage", "Please enter some valid input. Input is not optional.");
                    component.set("v.isStatusMessage", "errorStatus");
                    //       alert('Please update the invalid form entries and try again.');
                }
            }
        }
    },
    
    btnPreviousClick : function(component, event, helper) {
        var response = event.getSource().getLocalId();
        component.set("v.value", response);
        var navigate = component.get("v.navigateFlow");
        navigate("BACK");        
    },
    
})