({
    addClass : function (element, className) {
        //Global Aura util method for adding a style class from an aura element
        $A.util.addClass(element,className);
    }, 
    
    removeClass : function (element , className) {
        //Global Aura util method for removing a style class from an aura element
        $A.util.removeClass(element,className);
    },
    
    showElement : function (element) {
        //To show an aura html element
        var self = this;
        self.removeClass(element,'slds-hide');
        self.addClass(element,'slds-show');
    },
    
    hideElement : function (element) {
        //To hide an aura html element
        var self = this;
        self.removeClass(element,'slds-show');
        self.addClass(element,'slds-hide');
    },
    searchContent : function (component,searchContent) {
        
        var action = component.get("c.getRecords");
        var sObjectName = component.get("v.sObjectName");
        var displayedFieldName = component.get("v.displayedFieldName");
        
        var valueFieldName = component.get("v.valueFieldName");
        
        var filterField = component.get("v.filterField");
        var filterValue = component.get("v.filterValue");
        var otherFields = component.get("v.otherFields");
        var searchString=component.get("v.searchText");
        var searchWhereClause = component.get("v.displayedFieldName") + " LIKE '%" +
									searchString + "%' and "+filterField+"="+"'"+filterValue+"'";
        
         action.setParams({ "sObjectName" : sObjectName ,
                           "valueFieldName" : valueFieldName,
                           "otherFields" : otherFields,
                           "displayedFieldName" : displayedFieldName,
                           "whereClause" : searchWhereClause
                          });
        
        action.setCallback(this, function(response)
        {
           var resp = response.getReturnValue();
           component.set("v.fetchedRecords",resp);
        });
        $A.enqueueAction(action);
    }
})