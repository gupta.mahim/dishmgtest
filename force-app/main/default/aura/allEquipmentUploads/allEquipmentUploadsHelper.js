({
	saveChanges: function (component, event,helper) 
    {
        component.set("v.isLoading", true);
        var compLeasedSB=component.find("compLeasedSB");
        var compPurchasedSB=component.find("compPurchasedSB");
        var compHE=component.find("compHE");
        var compTE=component.find("compTE");
        console.info("compLeasedSB");
        console.info(compLeasedSB);
        console.info(compPurchasedSB);
        console.info(compHE);
        console.info(compTE);
        
        var leasedSBData='';
        var purchasedSBData='';
        var heData='';
        var teData='';
        
        if(compLeasedSB)
        {
            var uploadedEquipment=compLeasedSB.get("v.uploadedEquipment");
            
            //Milestone# MS-000026 -- Mahim -- Start
            if(uploadedEquipment.length>0){
                for(var i=0; i<uploadedEquipment.length; i++){
                    if((uploadedEquipment[i].Serial_Number__c != '' && uploadedEquipment[i].Serial_Number__c != null) || 
                       (uploadedEquipment[i].Chassis_Serial__c != '' && uploadedEquipment[i].Chassis_Serial__c != null ) ||
                       (uploadedEquipment[i].CAID__c != '' && uploadedEquipment[i].CAID__c != null) || 
                       (uploadedEquipment[i].SmartCard__c != '' && uploadedEquipment[i].SmartCard__c != null ))
                    {
                        if(uploadedEquipment[i].Serial_Number__c == '' || uploadedEquipment[i].Serial_Number__c == null || 
                           uploadedEquipment[i].Chassis_Serial__c == '' || uploadedEquipment[i].Chassis_Serial__c == null )
                        {
                            component.set("v.recordStatusMessage", "Please provide Chassis Serial # and Serial Number # for all uploaded Leased Smartbox Equipment.");
                            component.set("v.isStatusMessage", "errorStatus");
                            component.set("v.isLoading", false);
                            return;
                        }

                        if(uploadedEquipment[i].CAID__c != '' && uploadedEquipment[i].CAID__c != null)
                        {
                            //Milestone# MS-001255 -- Start
                            if(uploadedEquipment[i].CAID__c.trim().length != 11 && uploadedEquipment[i].CAID__c != '\r')
                            //Milestone# MS-001255 -- End
                            {
                                component.set("v.recordStatusMessage", "Please provide 11 character CAID # for uploaded Leased Smartbox Equipment.");
                                component.set("v.isStatusMessage", "errorStatus");
                                component.set("v.isLoading", false);
                                return;
                            }
                        }
                        if(uploadedEquipment[i].SmartCard__c != '' && uploadedEquipment[i].SmartCard__c != null)
                        {
                            //Milestone# MS-001255 -- Start
                             if(uploadedEquipment[i].SmartCard__c.trim().length != 11 && uploadedEquipment[i].SmartCard__c != '\r')
                            //Milestone# MS-001255 -- End    
                            {
                                console.log(uploadedEquipment[i].SmartCard__c + ' ' + uploadedEquipment[i].SmartCard__c.trim().length);
                                component.set("v.recordStatusMessage", "Please provide 11 character SmartCard # for uploaded Leased Smartbox Equipment.");
                                component.set("v.isStatusMessage", "errorStatus");
                                component.set("v.isLoading", false);
                                return;
                            }
                        }
                    }
                }
            }
            //Milestone# MS-000026 -- End
            
            if(uploadedEquipment!='')
            {
            	leasedSBData=JSON.stringify(uploadedEquipment);
                
            }
        }
        if(compPurchasedSB)
        {
           var uploadedEquipment=compPurchasedSB.get("v.uploadedEquipment");
            
            //Milestone# MS-000026 -- Mahim -- Start
            if(uploadedEquipment.length>0){
                for(var i=0; i<uploadedEquipment.length; i++){
                    if((uploadedEquipment[i].Serial_Number__c != '' && uploadedEquipment[i].Serial_Number__c != null) || 
                       (uploadedEquipment[i].Chassis_Serial__c != '' && uploadedEquipment[i].Chassis_Serial__c != null ) ||
                       (uploadedEquipment[i].CAID__c != '' && uploadedEquipment[i].CAID__c != null) || 
                       (uploadedEquipment[i].SmartCard__c != '' && uploadedEquipment[i].SmartCard__c != null ))
                    {
                        if(uploadedEquipment[i].Serial_Number__c == '' || uploadedEquipment[i].Serial_Number__c == null || 
                           uploadedEquipment[i].Chassis_Serial__c == '' || uploadedEquipment[i].Chassis_Serial__c == null )
                        {
                            component.set("v.recordStatusMessage", "Please provide Chassis Serial # and Serial Number # for all uploaded Purchased Smartbox Equipment.");
                            component.set("v.isStatusMessage", "errorStatus");
                            component.set("v.isLoading", false);
                            return;
                        }

                        if(uploadedEquipment[i].CAID__c != '' && uploadedEquipment[i].CAID__c != null)
                        {
                            //Milestone# MS-001255 -- Start
                            if(uploadedEquipment[i].CAID__c.trim().length != 11 && uploadedEquipment[i].SmartCard__c != '\r')
                            //Milestone# MS-001255 -- End
                            {
                                component.set("v.recordStatusMessage", "Please provide 11 character CAID # for uploaded Purchased Smartbox Equipment.");
                                component.set("v.isStatusMessage", "errorStatus");
                                component.set("v.isLoading", false);
                                return;
                            }
                        }

                        if(uploadedEquipment[i].SmartCard__c != '' && uploadedEquipment[i].SmartCard__c != null)
                        {
                            //Milestone# MS-001255 -- Start
                            if(uploadedEquipment[i].SmartCard__c.trim().length != 11 && uploadedEquipment[i].SmartCard__c != '\r')
                            //Milestone# MS-001255 -- End 
                            {
                                console.log(uploadedEquipment[i].SmartCard__c + ' ' + uploadedEquipment[i].SmartCard__c.trim().length);
                                component.set("v.recordStatusMessage", "Please provide 11 character SmartCard # for uploaded Purchased Smartbox Equipment.");
                                component.set("v.isStatusMessage", "errorStatus");
                                component.set("v.isLoading", false);
                                return;
                            }
                        }
                    }
                }
            }
            //Milestone# MS-000026 -- End
            
           if(uploadedEquipment!='')
            {
            	purchasedSBData=JSON.stringify(uploadedEquipment);
            }
        }
        if(compHE)
        {
            //Milestone# MS-000026 -- Mahim -- Start
            //var uploadedEquipment=compHE.get("v.uploadedEquipment");
            //if(uploadedEquipment!='')
            //{
            //	heData=JSON.stringify(uploadedEquipment);
            //}
            var uploadedEquipment=compHE.get("v.uploadedEquipment");
            var manualEquipment=compHE.get("v.addedEquipList");
            console.log('uploadedEquipment-'+JSON.stringify(uploadedEquipment));
			console.log('manualEquipment-'+JSON.stringify(manualEquipment));                        
            if(uploadedEquipment.length>0){
                for(var i=0; i<uploadedEquipment.length; i++){
                    if(uploadedEquipment[i].Name == '' || uploadedEquipment[i].Name == null || 
                      uploadedEquipment[i].Receiver_S__c == '' || uploadedEquipment[i].Receiver_S__c == null )
                    {
                        //component.set("v.recordStatusMessage", "Please provide Receiver # and SmartCard # for all uploaded Head-End Equipments.\nReceiver# and SmartCard # length must be 11 characters.");
                        component.set("v.recordStatusMessage", "Please provide 11 character Receiver # and SmartCard # for all uploaded Head-End Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                    if(uploadedEquipment[i].Name.length != 11 || uploadedEquipment[i].Receiver_S__c.length != 11)
                    {
                        component.set("v.recordStatusMessage", "Please provide 11 character Receiver # and SmartCard # for all uploaded Head-End Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                }
            }
            
            //On pageload, one row is added to ManualEquipment by default. 
            //If user have not provided any data for that one default row then remove the row.
            if(manualEquipment.length == 1 && manualEquipment[0].Name == "" && manualEquipment[0].Receiver_S__c == "" 
              && manualEquipment[0].Receiver_Model__c == "" && manualEquipment[0].Programming__c == "" && manualEquipment[0].Channel__c == ""
              && manualEquipment[0].Channel_No__c == "" && manualEquipment[0].Satellite__c == "" && manualEquipment[0].Satellite_Channel__c == ""
              && manualEquipment[0].Output_Channel__c == ""){
                manualEquipment = '';
            }
            
            if(manualEquipment.length>0){
                for(var i=0; i<manualEquipment.length; i++){
                    if(manualEquipment[i].Name == '' || manualEquipment[i].Name == null || 
                      manualEquipment[i].Receiver_S__c == '' || manualEquipment[i].Receiver_S__c == null )
                    {
                        //component.set("v.recordStatusMessage", "Please provide Receiver # and SmartCard # for all manually entered Head-End Equipments.\nReceiver# and SmartCard # length must be 11 characters.");
                        component.set("v.recordStatusMessage", "Please provide 11 character Receiver # and SmartCard # for all manually entered Head-End Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                    if(manualEquipment[i].Name.length != 11 || manualEquipment[i].Receiver_S__c.length != 11)
                    {
                        component.set("v.recordStatusMessage", "Please provide 11 character Receiver # and SmartCard # for all manually entered Head-End Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                }
            }
            
            if(uploadedEquipment=='' && manualEquipment!='')
            	heData=JSON.stringify(manualEquipment);
            
            if(uploadedEquipment!='' && manualEquipment=='')
            	heData=JSON.stringify(uploadedEquipment);
            
            if(uploadedEquipment!='' && manualEquipment!='')          
            	heData=JSON.stringify(uploadedEquipment.concat(manualEquipment));
            //Milestone# MS-000026 -- Mahim -- End
        }
        if(compTE)
        {
            //Milestone# MS-000026 -- Mahim -- Start
            //var uploadedEquipment=compTE.get("v.uploadedEquipment");
            //if(uploadedEquipment!='')
            //{
            //	teData=JSON.stringify(uploadedEquipment);
            //}
            
            var uploadedEquipment=compTE.get("v.uploadedEquipment");
            var manualEquipment=compTE.get("v.addedEquipList");
            
            if(uploadedEquipment.length>0){
                for(var i=0; i<uploadedEquipment.length; i++){
                    if(uploadedEquipment[i].Name == '' || uploadedEquipment[i].Name == null || 
                      uploadedEquipment[i].Smart_Card__c == '' || uploadedEquipment[i].Smart_Card__c == null )
                    {
                        //component.set("v.recordStatusMessage", "Please provide Receiver # and SmartCard # for all uploaded Tenant Equipments.\nReceiver# and SmartCard # length must be 11 characters.");
                        component.set("v.recordStatusMessage", "Please provide 11 character Receiver # and SmartCard # for all uploaded Tenant Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                    if(uploadedEquipment[i].Address__c == '' || uploadedEquipment[i].Address__c == null || 
                      uploadedEquipment[i].City__c == '' || uploadedEquipment[i].City__c == null ||
                      uploadedEquipment[i].State__c == '' || uploadedEquipment[i].State__c == null || 
                      uploadedEquipment[i].Zip_Code__c == '' || uploadedEquipment[i].Zip_Code__c == null ||
                      uploadedEquipment[i].Unit__c == '' || uploadedEquipment[i].Unit__c == null)
                    {
                        component.set("v.recordStatusMessage", "Please provide Unit, Address, City, State & Zip Code for all uploaded Tenant Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                    if(uploadedEquipment[i].Name.length != 11 || uploadedEquipment[i].Smart_Card__c.length != 11)
                    {
                        component.set("v.recordStatusMessage", "Please provide 11 character Receiver # and SmartCard # for all uploaded Tenant Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                }
            }
            
            //On pageload, one row is added to ManualEquipment by default. 
            //If user have not provided any data for that one default row then remove the row.
            if(manualEquipment.length == 1 && manualEquipment[0].Name == "" && manualEquipment[0].Smart_Card__c == "" 
              && manualEquipment[0].Address__c == "" && manualEquipment[0].City__c == "" && manualEquipment[0].State__c == ""
              && manualEquipment[0].Zip_Code__c == "" && manualEquipment[0].Unit__c == "" && manualEquipment[0].Customer_First_Name__c == ""
              && manualEquipment[0].Customer_Last_Name__c == "" && manualEquipment[0].Phone_Number__c == "" 
              && manualEquipment[0].Email_Address__c == ""){
                manualEquipment = '';
            }
            
            if(manualEquipment.length>0){
                for(var i=0; i<manualEquipment.length; i++){
                    if(manualEquipment[i].Name == '' || manualEquipment[i].Name == null || 
                      manualEquipment[i].Smart_Card__c == '' || manualEquipment[i].Smart_Card__c == null )
                    {
                        //component.set("v.recordStatusMessage", "Please provide Receiver # and SmartCard # for all manually entered Tenant Equipments.\nReceiver# and SmartCard # length must be 11 characters.");
                        component.set("v.recordStatusMessage", "Please provide 11 character Receiver # and SmartCard # for all manually entered Tenant Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                    if(manualEquipment[i].Address__c == '' || manualEquipment[i].Address__c == null || 
                      manualEquipment[i].City__c == '' || manualEquipment[i].City__c == null ||
                      manualEquipment[i].Zip_Code__c == '' || manualEquipment[i].Zip_Code__c == null ||
                      manualEquipment[i].Unit__c == '' || manualEquipment[i].Unit__c == null)
                    {
                        component.set("v.recordStatusMessage", "Please provide Unit, Address, City, State & Zip Code for all manually entered Tenant Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                    if(manualEquipment[i].Name.length != 11 || manualEquipment[i].Smart_Card__c.length != 11)
                    {
                        component.set("v.recordStatusMessage", "Please provide 11 character Receiver # and SmartCard # for all manually entered Tenant Equipments.");
                        component.set("v.isStatusMessage", "errorStatus");
                        component.set("v.isLoading", false);
                        return;
                    }
                }
            }
            
            if(uploadedEquipment=='' && manualEquipment!='')
            	teData=JSON.stringify(manualEquipment);
            
            if(uploadedEquipment!='' && manualEquipment=='')
            	teData=JSON.stringify(uploadedEquipment);
            
            if(uploadedEquipment!='' && manualEquipment!='')          
            	teData=JSON.stringify(uploadedEquipment.concat(manualEquipment));
            //Milestone# MS-000026 -- Mahim -- End
        }
        
        component.set("v.isStatusMessage", "hideMessage");	//Milestone# MS-000026 - Added by Mahim
        console.info("JSON Data");
        console.info(leasedSBData);
        console.info(purchasedSBData);
        console.info(heData);
        console.info(teData);
        //Milestone# MS-000210 -- Mahim -- Start
        if(leasedSBData.length>0 || purchasedSBData.length>0 || heData.length>0 || teData.length>0)
            component.set("v.EquipmentDataAdded", true);
        //Milestone# MS-000210 -- Mahim -- End
        
        var action= component.get("c.saveAll");
        action.setParams({
            "leasedSBData": leasedSBData
            ,"purchasedSBData": purchasedSBData
            ,"heData": heData
            ,"teData": teData
        });
        action.setCallback(this, function(a)
        {
            
            var state = a.getState();
            console.log('state - '+ state);
            console.log('v.isStatusMessage- '+ component.get('v.isStatusMessage'));
            if (state === "ERROR")
            {
                var errors = a.getError();
                var message = 'Unknown error';
                
                if (errors && Array.isArray(errors) && errors.length > 0)
                {
                    message = errors[0].message;
                }
                
                component.set("v.isStatusMessage", "errorStatus");
                //component.set("{!v.recordStatusMessage}", "There is an error while saving record: "+message);
                component.set("v.recordStatusMessage", "There is an error while saving record: "+message);
                component.set("v.isLoading", false);
                return;
            }
            
            //Milestone# MS-001273 - Mahim - to handle Data Insert lag issue - 22 March 2020 - Start
            if (state === "SUCCESS"){
                var actionClicked = event.getSource().getLocalId();
        		var navigate = component.get('v.navigateFlow');
                
                if(component.get('v.isStatusMessage') != "errorStatus")	//Milestone# MS-000026 - Added by Mahim
        			navigate(actionClicked);
            }
            //Milestone# MS-001273 - End
        });
        $A.enqueueAction(action);
	}
})