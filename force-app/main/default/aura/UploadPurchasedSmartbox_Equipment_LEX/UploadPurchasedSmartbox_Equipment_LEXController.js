({
    //Milestone# MS-001252 - 3Jun20 - Start
    doInit: function(component, event, helper) {
        console.log('123 ');
        component.set("v.isLoading", true);
        console.log('Test1');
        var action = component.get("c.getUserAccess");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state-'+state);
            if (state === "SUCCESS") {
                component.set("v.userHaveAccess", response.getReturnValue().currentUserHaveAccess);
                component.set("v.noAccessMsg", response.getReturnValue().noAccessMsg);               
                component.set("v.isLoading", false);
            }
            else{
                console.log('Error Details - '+ JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    //Milestone# MS-001252 - End
    
    uploadFile: function (component, event, helper) {
        var fileTypes = ['csv', 'xls', 'xlsx'];
        var fileInput = component.find("file").getElement();
        
       component.set("v.isLoading", true);
        var oppId=component.get("v.recordId");
        console.info(oppId);
        var file = fileInput.files[0];
      
         
        if (file) {
             
         var extension = file.name.split('.').pop().toLowerCase(),  //file extension from input file
             isSuccess = fileTypes.indexOf(extension) > -1;
        
           
       if(isSuccess) {
            
           component.set("v.isError", "false");
           
        var reader = new FileReader();
        
        reader.onload = $A.getCallback(function() {
            var csv = reader.result;
            console.log('@@@ csv file contains'+ csv);
            var n= csv.length;
           
            if(csv.length>2){
           
                var action= component.get("c.ReadFile");
            
           
            action.setParams({
                "strNameFile":csv,
                "opportunityId": oppId
                 
            });
               
              
            action.setCallback(this,function(a){
            var state = a.getState();
                console.log(a.getReturnValue())
               component.set("v.uploadedEquipment", a.getReturnValue());
               component.set("v.isLoading","false");
               
                
            });
           
            
             $A.enqueueAction(action);   
            }else
            {
              component.set("v.isError", "true");
            component.set("{!v.ErrorMessage}", "The uploaded file does not contain any data. "); 
             component.set("v.isLoading", false);  
            }
           
        });
            
            reader.onerror = function (evt) {
                component.set("v.isStatusMessage", "errorStatus");
                component.set("{!v.recordStatusMessage}", "There is an error while reading file");
                console.log("error reading file");
        }
            
            reader.readAsText(file, "UTF-8");
        
            
    }
        else
        {
             component.set("v.isLoading","false");
             component.set("v.isError", "true");
             component.set("{!v.ErrorMessage}", "Invalid file format, please select appropriate file format(csv,xls and xlsx) and Try Again. ");
            
         
            
        }
            
        }
        else
        {   
            component.set("v.isError", "true");
            component.set("{!v.ErrorMessage}", "You haven't uploaded the file, or it's empty. Try it again, please. "); 
             component.set("v.isLoading", false);
            
          
        }

    },
    removeComponent:function(component, event, helper){
        //get event and set the parameter of Aura:component type, as defined in the event above.
        var compEvent = component.getEvent("RemoveComponent");
        compEvent.setParams({
            "comp" : component
        });
    compEvent.fire();
    }, 
    
    showToast: function(component, event, helper) { 
        var showToast = $A.get("e.force:showToast"); 
        showToast.setParams({ 
            'title' : 'Custom Toast!', 
            'message' : 'Records saved Succesfully.' 
        }); 
        showToast.fire(); 
    } ,
 
   saveFile:function (component, jsonstr) {
            component.set("v.isLoading", true);
            var equipmentdata = component.get("v.uploadedEquipment");
           
            if(equipmentdata!='')
            {
            
         	var jsonData=JSON.stringify(equipmentdata);
            var action= component.get("c.save");
            action.setParams({
           "lexAccstoupload": jsonData
       });
            action.setCallback(this, function(a) {
            console.info(a);
            var state = a.getState();
            console.info(state);    
            if (state === "SUCCESS") {  
                component.set("v.isStatusMessage", "SaveSuccess");
                component.set("{!v.recordStatusMessage}", "Your record(s) has been saved successfully.");
               component.set("v.uploadedEquipment",null);
                var ID= component.get("v.recordId");
                var isCommunity=component.get("v.isCommunity");
                var doRedirect=component.get("v.doRedirect");
                if(!isCommunity && doRedirect)
                {
            		window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");             
                }
                
                else if(isCommunity && doRedirect)
                {
                    window.open('/lex/s/opportunity/'+ID+'/view',"_self");
                }
            
                var name = a.getReturnValue();

            }
               else if (state === "ERROR") {
                var errors = a.getError();
                var message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                console.error(errors);
     			component.set("v.isStatusMessage", "errorStatus");
                component.set("{!v.recordStatusMessage}", "There is an error while saving record: "+message);
                
                 
            }
          component.set("v.isLoading", false);
        }); 
           
        $A.enqueueAction(action);
            } else{
              component.set("v.isError", "true");
              component.set("{!v.ErrorMessage}", "You did not upload the file, or file is empty. Please try again. "); 
              component.set("v.isLoading", false);
            
               
            }
       
    },
     onCancel : function(component, event, helper) {
        
       var ID= component.get("v.recordId");
      
       window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");
           
} ,
    hideStatusMesssage : function(component, event, helper) {
        
        component.set("v.isStatusMessage", "hideMessage");
    },
   
})