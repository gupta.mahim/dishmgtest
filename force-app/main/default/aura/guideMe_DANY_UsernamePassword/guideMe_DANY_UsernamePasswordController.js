({
    
    upperCase : function(component, event, helper) { 
        var username = component.get("v.username");
        component.set("v.username", username.toUpperCase());
    },
    
    btnNextClick : function(component, event, helper) {       
        var password = component.get("v.password");
        var username = component.get("v.username");
        var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,128}$/;
        var passwordValid = false;

        if((username.length > 0 && password.length == 0) || (username.length == 0 && password.length > 0)){
            alert('Please provide Dish Anywhere Username and Password.');
            return false;
        }       
        if(username.length > 0 && (username.length <4 || username.length >128)){
            alert('Dish Anywhere Username must be between 4-128 characters long');
            return false;
        }
        if(password.length > 0 && (password.length <8 || password.length >128)){
            alert('Dish Anywhere Password must be between 8-128 characters long');
            return false;
        }
        if(username.length > 0 && password.length > 0 && username.toString() == password.toString()){
            alert('Dish Anywhere Username and Password cannot be same.');
            return false;
        }
        
        if(password.length > 0) {            
            if(password.toString().match(decimal)) { 
                console.log('Correct');
                passwordValid = true;
                
            }
            else{ 
                alert('Please provide correct Password pattern.\nPassword must be between 8-128 characters long, including a capital letter, lowercase, letter, number, and special character.');
                return false;
            }
        }
        
        if((username.length == 0 && password.length == 0) || passwordValid == true){
            var response = event.getSource().getLocalId();
            component.set("v.value", response);
            var navigate = component.get("v.navigateFlow");
            navigate("NEXT");
        }
        
        /*if(password.length >= 8) {
            strengthValue.length = true;
        }
         
        //Calculate Password Strength
        for(let index=0; index < password.length; index++) {
            let char = password.charCodeAt(index);
            if(!strengthValue.caps && char >= 65 && char <= 90) {
                strengthValue.caps = true;
            } else if(!strengthValue.numbers && char >=48 && char <= 57){
                strengthValue.numbers = true;
            } else if(!strengthValue.small && char >=97 && char <= 122){
                strengthValue.small = true;
            } else if(!strengthValue.numbers && char >=48 && char <= 57){
                strengthValue.numbers = true;
            } else if(!strengthValue.special && (char >=33 && char <= 47) || (char >=58 && char <= 64)) {
                strengthValue.special = true;
            }
        }*/
    },
    
    btnPreviousClick : function(component, event, helper) {
        var response = event.getSource().getLocalId();
        component.set("v.value", response);
        var navigate = component.get("v.navigateFlow");
        navigate("BACK");        
    },
})