({

	openRecord : function(component, event, helper) {
        var IsfromGuideMe = component.get("v.isFromGuideMe");
    	var ID= component.get("v.recordId");
        var ObjID= component.get("v.objectId");
        var IsGuideMe = component.get("v.isFromGuideMe");
       
        var isCommunity=component.get("v.isCommunity");
        if(!isCommunity && !IsGuideMe)
        {
            window.open('/lightning/r/'+ObjID+'/'+ID+'/view',"_self");
        }
        else if(isCommunity && !IsGuideMe)
        {
            window.open('/lex/s/detail/'+ID,"_self");
        }
        else if(isCommunity && IsGuideMe)
        {
            window.open('/lex/s/'+ObjID+'/'+ID,"_self");
        }
	}
})