({
    loadEquip : function(component, event, helper) 
    {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        var sParameterName, i, equipIds, OppID, TAId;
        var equipValues;
        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == 'equipIds') {
                equipIds = sParameterName[1];
                //equipValues = equipIds.split(',');
            }
            if (sParameterName[0] == 'oppId') 
                OppID = sParameterName[1];
            if (sParameterName[0] == 'TAId') 
                TAId = sParameterName[1];            
        }
        
        var equipValues;
        if(equipIds != '' && equipIds != null && equipIds != 'undefined'){
            equipValues = equipIds.split(',');
            component.set("v.Equips",equipValues);
        }
        var action = component.get("c.getActiveEquips");
        var hasOverrideCase=component.get("v.hasOverrideCase");
        action.setParams({
            equipIds: component.get("v.Equips"),
            oppId:OppID,
            taId:TAId
        });
        action.setCallback(this,function(a){
            var taData=a.getReturnValue();
            component.set("v.Equips", taData);
        });
        $A.enqueueAction(action);
        
        var delay=2000; //2 seconds
        setTimeout(function() {
            console.log('Inside delay: ');
            window.print();
        }, delay);
    },
    
        
})