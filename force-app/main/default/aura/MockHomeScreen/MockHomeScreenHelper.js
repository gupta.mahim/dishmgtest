({
	 OpenCreateTEFlow : function (component, event) {
        document.getElementById('CreateProp').style.fontWeight = 'normal';
        document.getElementById('CreateTE').style.fontWeight = 'bold';
        
        // Find the component whose aura:id is "flowData"
        component.set("v.IsCreateProperty",false);
        component.set("v.IsCreateTE",true);
        component.set("v.selectedGuideMeOption","CreateTA");
         }
})