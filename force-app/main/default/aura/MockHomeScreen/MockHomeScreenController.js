({
    doInit : function (component, event) {
        
        var selectedGuideME=component.get("v.selectedGuideMeOption");

        //Milestone# MS-001252 - 6Jul20 - Start
        var buttonNext = component.find('btnNext');
        var action = component.get("c.getUserAccess");
        var varNoAccessToOppMsg = 'You currently do not have access to Create New Property. Please reach out to your account manager or CommercialOperations@dish.com to request access.';
        var varAccessToTAMsg = 'You do have access to Create New Tenant Account. Please click on the "Create New" link under TENANT in the menu to create new Tenant Account.';
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.userHaveAccessProp", response.getReturnValue().PropertyAccess.currentUserHaveAccess);
                component.set("v.userHaveAccessTA", response.getReturnValue().TenantAccountAccess.currentUserHaveAccess);
                component.set("v.noAccessMsgProp", response.getReturnValue().PropertyAccess.noAccessMsg); 
                component.set("v.noAccessMsgTA", response.getReturnValue().PropertyAccess.noAccessMsg);

                if(component.get("v.userHaveAccessProp") == true){
                    component.find('btnNext').set('v.disabled',false);
                }
                else{
                    component.find('btnNext').set('v.disabled',true);
                }
                
                if(component.get("v.userHaveAccessProp") == false && component.get("v.userHaveAccessTA") == true){
                    component.set("v.noAccessMsgProp", varNoAccessToOppMsg); 
                    component.set("v.accessToTAMsg", varAccessToTAMsg); 
                }
            }
            else{
                console.log('Error Details - '+ JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
        //Milestone# MS-001252 - End
 
        if(selectedGuideME=="CreateProperty"){
            component.set("v.IsCreateProperty",true);
            component.set("v.IsCreateTE",false);
        }
        else if(selectedGuideME=="CreateTA"){
            component.set("v.IsCreateProperty",false);
            component.set("v.IsCreateTE",true);
        }  
        //component.set("v.selectedGuideMeOption","CreateProperty");
    },
    
    OpenCreateTEFlow : function (component, event) {
        // Find the component whose aura:id is "flowData"
        component.set("v.IsCreateProperty",false);
        component.set("v.IsCreateTE",true);
        component.set("v.selectedGuideMeOption","CreateTA");
        document.getElementById('CreateProp').style = ' text-decoration: none !important;font-weight:normal;border-left:0px solid;border-left-color: null;color: black;';
        document.getElementById('CreateTE').style = 'box-shadow: inset 0.125rem 0 0 #0099de;text-decoration: none !important;font-weight:bold;border-left:2px solid;border-left-color: #005fb2;color: #0070d2;';
        
        //Milestone# MS-001252 - 21Jul20 - Start
        var buttonNext = component.find('btnNext');
        if(component.get("v.userHaveAccessTA") == true)
            buttonNext.set('v.disabled',false);
        else
            buttonNext.set('v.disabled',true);
    	//Milestone# MS-001252 - End
    },
    
    OpenCreatePropertyflow : function (component) {
        document.getElementById('CreateTE').style = ' text-decoration: none !important;font-weight:normal;border-left:0px solid;border-left-color: null;color: black;';
        document.getElementById('CreateProp').style = 'box-shadow: inset 0.125rem 0 0 #0099de;text-decoration: none !important;font-weight:bold;border-left:2px solid;border-left-color: #005fb2;color: #0070d2;';
        // Find the component whose aura:id is "flowData"
        component.set("v.IsCreateProperty",false);
        component.set("v.IsCreateTE",false);
        component.set("v.selectedGuideMeOption","CreateProperty");
        
        //Milestone# MS-001252 - 21Jul20 - Start
        var buttonNext = component.find('btnNext');
        if(component.get("v.userHaveAccessProp") == true)
            buttonNext.set('v.disabled',false);
        else
            buttonNext.set('v.disabled',true);
    	//Milestone# MS-001252 - End
    },
    
    OpenModifyPropertyflow : function (component) {
        document.getElementById('CreateProp').style = ' text-decoration: none !important;font-weight:normal;border-left:0px solid;border-left-color: null;color: black;';
        document.getElementById('CreateTE').style = ' text-decoration: none !important;font-weight:normal;border-left:0px solid;border-left-color: null;color: black;';
        document.getElementById('ModifyProp').style = 'box-shadow: inset 0.125rem 0 0 #0099de;text-decoration: none !important;font-weight:bold;border-left:2px solid;border-left-color: #005fb2;color: #0070d2;';
        // Find the component whose aura:id is "flowData"
        component.set("v.IsModifyProperty",true);    
        component.set("v.IsCreateProperty",false);
        component.set("v.IsCreateTE",false);
        component.set("v.IsModifyProperty2",false);
        component.set("v.selectedGuideMeOption","ModifyProp");
    },
    
    OpenModifyPropertyflow2 : function (component) {
        document.getElementById('CreateProp').style = ' text-decoration: none !important;font-weight:normal;border-left:0px solid;border-left-color: null;color: black;';
        document.getElementById('CreateTE').style = ' text-decoration: none !important;font-weight:normal;border-left:0px solid;border-left-color: null;color: black;';
        document.getElementById('ModifyProp').style = ' text-decoration: none !important;font-weight:normal;border-left:0px solid;border-left-color: null;color: black;';
        document.getElementById('ModifyProp2').style = 'box-shadow: inset 0.125rem 0 0 #0099de;text-decoration: none !important;font-weight:bold;border-left:2px solid;border-left-color: #005fb2;color: #0070d2;';
        // Find the component whose aura:id is "flowData"
        component.set("v.IsModifyProperty",false);
        component.set("v.IsModifyProperty2",true);
        component.set("v.IsCreateProperty",false);
        component.set("v.IsCreateTE",false);
        component.set("v.selectedGuideMeOption","ModifyProp2");  
    },
    
    //Milestone# MS-001252 - 21Jul20 - Start
    btnNextClick : function(component, event, helper) {       
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    },
    
    /*btnPreviousClick : function(component, event, helper) {
        var navigate = component.get("v.navigateFlow");
        navigate("BACK");        
    },*/
    //Milestone# MS-001252 - End
})