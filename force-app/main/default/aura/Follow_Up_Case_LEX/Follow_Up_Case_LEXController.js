({
    updateCaseLDS: function(component, event, helper) {
        console.log("Testing.....");
        //debugger;
        component.set("v.recObjFields.Status", "Pending Customer Follow-Up");


        component.find("recordEditor").saveRecord(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                
                // record is saved successfully
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was processed."
                });
                resultsToast.fire();
                
            } else if (saveResult.state === "INCOMPLETE") {
                // handle the incomplete state
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "mode": 'sticky',
                    "title": "Not Saved",
                    "message": JSON.stringify(saveResult.error[0].message)
                });
                resultsToast.fire();
                // handle the error state
                console.log('Problem saving contact, error: ' + 
                            JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state +
                            ', error: ' + JSON.stringify(saveResult.error));
            }
        });
        

        // Close the quick action
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
        
        var url = "/email/author/emailauthor.jsp?retURL=/" + component.get("v.recOppFields.Id") + "&p3_lkid=" + component.get("v.recOppFields.Id") + "&rtype=003&p2_lkid=" + component.get("v.recOppFields.Created_By_Email__c") + "&p24=" + component.get("v.recOppFields.Created_By_Email__c") + "&p4=" + component.get("v.recOppFields.Secondary_Email_Address__c") + "&template_id=00X60000004dkwc&p5=";
        //console.log("URL=" + url);
        //sforce.one.navigateToURL(url,true);
        
        var urlEvent = $A.get("e.force:navigateToURL");
        if(urlEvent){
                urlEvent.setParams({
                  "url": url,
                  "isredirect": "true"
                });
            	urlEvent.fire();
        }
    }
})