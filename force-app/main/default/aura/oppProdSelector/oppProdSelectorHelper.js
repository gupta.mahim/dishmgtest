({
    fetchData: function (cmp, fetchData, numberOfRecords) {
        var dataPromise,
            latitude,
            longitude,
            fakerLib = this.mockdataLibrary.getFakerLib();
        
        fetchData.address = {type : function () {
            return {
                latitude : fakerLib.address.latitude(),
                longitude : fakerLib.address.longitude()
            }
        }};
        
        fetchData.confidence =  { type : function () {
            return fakerLib.random.number(10) / 10;
        }};
        
        dataPromise = this.mockdataLibrary.lightningMockDataFaker(fetchData,numberOfRecords);
        dataPromise.then(function(results) {
            cmp.set('v.data', results);
        });
    },reviewSummary: function (component, event, helper)
    {
        var prodCategories=component.get("v.prodCategories");
        var allSelectedData=[];
        var allSaveData=[];
        var allRemoveData=[];
        
        var allPrices={};
        var oneTimePrice={};
        oneTimePrice["items"]=[];
        var otpFound=false;
        for(var i=0;i<prodCategories.length;i++)
        {
            var selectedData={};
            var prodCategory=prodCategories[i]['categoryName'];
            
            var cmp=component.find(prodCategory);
            if(cmp)
            {
                var selectedData=cmp.get("v.selectedData");
                var removedDisplayData=cmp.get("v.removedDisplayData");
                if(prodCategory=='Core')
                {
                    component.set("v.isCoreSelected",cmp.get("v.isCoreSelected"));
                }
                if(selectedData && selectedData[prodCategory])
                {
                    selectedData['type']=prodCategory;
                    var selectedEntries=selectedData[prodCategory];
                    var prodItems=[];
                    if(removedDisplayData && removedDisplayData!=null && removedDisplayData[prodCategory])
                    {
                        var removedData=removedDisplayData[prodCategory];
                        if(removedData && removedData.length>0)
                        {
                            for(var rd=0;rd<removedData.length;rd++)
                            {
                                var prodItem={};
                                prodItem['Name']=removedData[rd].PricebookEntry.Product2.Name;
                                prodItem['Description']=removedData[rd].PricebookEntry.Product2.Description;
                                prodItem['UnitPrice']=removedData[rd].PricebookEntry.Sales_Price2__c;
                                prodItem['IsRemoved']=true;
                                prodItems.push(prodItem);
                            }
                        }
                    }
                    for(var e=0;e<selectedEntries.length;e++)
                    {
                        allSaveData.push(selectedEntries[e]);
                        var prodItem={};
                        prodItem['Name']=selectedEntries[e].Product2.Name;
                        prodItem['Description']=selectedEntries[e].Product2.Description;
                        prodItem['UnitPrice']=selectedEntries[e].Sales_Price2__c;
                        prodItem['RecId']=selectedEntries[e].Id;
                        prodItem['IsRemoved']=false;
                        
                        var otpValue=selectedEntries[e].Product2.One_Time_Price__c;
                        if(otpValue)
                        {
                            otpFound=true;
                            oneTimePrice["type"]="One Time Price";
                            var otpItems= oneTimePrice["items"];
                            
                            var optItem={};
                            optItem['Name']=selectedEntries[e].Product2.Name;
                            optItem['Description']=selectedEntries[e].Product2.Description;
                            optItem['UnitPrice']=selectedEntries[e].Product2.One_Time_Price__c;
                            optItem['IsRemoved']=false;
                            otpItems.push(optItem);
                        }        
                        prodItems.push(prodItem);
                    }
                    
                    selectedData['items']=prodItems;
                    allSelectedData.push(selectedData);
                    
                    if(otpFound)
                    {
                        allSelectedData.push(oneTimePrice);
                    }
                }
                
                var removeData=cmp.get("v.removedData");
                if(removeData)
                {
                    for(var rd=0;rd<removeData.length;rd++)
                    {
                        allRemoveData.push(removeData[rd]);
                    }
                }
            }
        }
        if(allSelectedData && allSelectedData.length>0)
        {
            var allNewPrices=[];
            var totalPrice=0.00;
            var existingProdData=component.get("v.existingProdData");
            for(var asd=0;asd<allSelectedData.length;asd++)
            {
                var sDataRow=allSelectedData[asd];
                var sDataCategory=sDataRow['type'];
                var prodItems=sDataRow['items'];
                var sDataPrice=0.00;
                
                var newPrice={}
                for(var pitem=0;pitem<prodItems.length;pitem++)
                {
                    var prodItem=prodItems[pitem];
                    var isRemoved=prodItem['IsRemoved'];
                    if(sDataCategory=='Core' && isRemoved){continue;}
                    if(isRemoved){sDataPrice-=prodItem['UnitPrice'];}
                    else{sDataPrice+=prodItem['UnitPrice'];}
                }
                newPrice["type"]=sDataCategory;
                newPrice["Price"]=sDataPrice;
                allNewPrices.push(newPrice);
                if(sDataCategory!="One Time Price")
                {
                    totalPrice+=sDataPrice;
                }
            }
            if(existingProdData && existingProdData!=null && existingProdData.length>0)
            {
                var changedProds={};
                var coreFound=false;
                for(var exData=0;exData<existingProdData.length;exData++)
                {
                    if(existingProdData[exData].family=='Authorization Code(s)')
                    {
                        continue;
                    }
                    for(var anPrice=0;anPrice<allNewPrices.length;anPrice++)
                    {
                        if(allNewPrices[anPrice]["type"]=="Core"){coreFound=true;}
                        if(existingProdData[exData].family==allNewPrices[anPrice]["type"] && allNewPrices[anPrice]["type"]!="Core")
                        {
                            var storedPrice=allNewPrices[anPrice]["Price"];
                            
                            storedPrice+=existingProdData[exData].TotalPrice;
                            
                            allNewPrices[anPrice]["Price"]=storedPrice;
                            totalPrice+=existingProdData[exData].TotalPrice;
                            changedProds[existingProdData[exData].family]=true;
                        }
                    }
                }
                for(var exData=0;exData<existingProdData.length;exData++)
                {
                    if(existingProdData[exData].family=='Authorization Code(s)')
                    {
                        continue;
                    }
                    var pFamily=existingProdData[exData].family;
                    if(pFamily=="Core" && coreFound==false)
                    {
                        var newPrice={}
                        newPrice["type"]=pFamily;
                        newPrice["Price"]=existingProdData[exData].TotalPrice;
                        totalPrice+=existingProdData[exData].TotalPrice;
                        allNewPrices.push(newPrice);
                    }
                    if(!changedProds[pFamily] && pFamily!="Core")
                    {
                        var newPrice={}
                        newPrice["type"]=pFamily;
                        newPrice["Price"]=existingProdData[exData].TotalPrice;
                        totalPrice+=existingProdData[exData].TotalPrice;
                        allNewPrices.push(newPrice);
                    }
                }
            }
            var totalPriceRec={};
            totalPriceRec["type"]="Monthly Unit Price";
            totalPriceRec["Price"]=totalPrice;
            allNewPrices.push(totalPriceRec);
        }
        component.set("v.allNewPrices",allNewPrices);
        component.set("v.allSelectedData",allSelectedData);
        component.set("v.allSaveData",allSaveData);
        component.set("v.allRemoveData",allRemoveData);
    },saveChanges: function (component, event, helper) 
    {
        helper.reviewSummary(component, event, helper);
        var allSaveData=component.get("v.allSaveData");
        var allRemoveData=component.get("v.allRemoveData");
        
        var isCoreSelected=component.get("v.isCoreSelected");
        var isGuideMe=component.get("v.isGuideMe");
        if(isCoreSelected==false)
        {
            component.set("v.recordStatusMessage", "Please select a Core before saving your changes.");
            component.set("v.hideStatusMessage", false);
        }
        else
        {
            if(isGuideMe==false)
            {
                component.set("v.isMainLoading", true);    
            }
            
            var jsonData=JSON.stringify(allSaveData);
            var removeData=JSON.stringify(allRemoveData);
            
            var recIdSD = component.get("v.recordId");//"00663000008fO8H"
            var action = component.get("c.saveProducts");
            action.setParams({oppId: recIdSD,strNewProdData: jsonData,strRemoveProdData:removeData});
            action.setCallback(this, function(a)
                               {
                                   component.set("v.isMainLoading", false);
                                   var state = a.getState();
                                   console.info(state);
                                   if (state === "SUCCESS")
                                   {
                                       //component.set("v.recordStatusMessage", "Your changes have been saved successfully.");
                                       //component.set("v.hideStatusMessage", false);
                                       
                                       var ID= component.get("v.recordId");
                                       var isCommunity=component.get("v.isCommunity");
                                       var isGuideMe=component.get("v.isGuideMe");
                                       console.log('isCommunity-'+isCommunity);
                                       console.log('isGuideMe-'+isGuideMe);
                                       console.log('3');
                                       if(!isGuideMe)
                                       {
                                           if(!isCommunity)
                                           {
                                            //Updated by Mahim 7Oct20 for testing ---------start
                                            
                                          	console.log('4');
                                               /*var navService = component.find("navService");
                                               // Uses the pageReference definition in the init handler
                                               var pageReference = {
                                                   type: 'standard__objectPage',
                                                   attributes: {
                                                       objectApiName: 'Opportunity',
                                                       actionName: 'list'
                                                   },
                                                   state: {
                                                       filterName: 'MyOpportunities'
                                                   } 
                                               };
                                               
                                               //event.preventDefault();
                                               navService.navigate(pageReference);
                                             */  
                                            console.log('5');   
    
                                            
                                            //window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");
                                               window.open('https://dish--full.lightning.force.com/lightning/r/Opportunity/'+ID+'/view',"_self");
                                               //https://dish--full.lightning.force.com/lightning/r/Opportunity/0066000001qq7J1AAI/view
                                            //window.close();
                                            //window.open('/lightning/r/Opportunity/'+ID+'/view');
                                            //--------end
                                           }
                                           else
                                           {
                                               window.open('/lex/s/opportunity/'+ID+'/view',"_self");
                                           }    
                                       }
                                       else
                                       {
                                           component.set("v.recordStatusMessage", "Your changes have been saved successfully.");
                                           component.set("v.hideStatusMessage", false);
                                       }
                                   }
                                   else
                                   {
                                       var errors = a.getError();
                                       console.info("errors");
                                       console.info(errors);
                                       console.info(errors[0].pageErrors[0].message);
                                       if (errors) {
                                           if (errors[0] && errors[0].pageErrors[0].message) {
                                               component.set("v.recordStatusMessage",errors[0].pageErrors[0].message);
                                               component.set("v.hideStatusMessage",false);
                                           }
                                       }
                                       else {
                                           
                                           component.set("v.recordStatusMessage","Request Failed.");
                                           component.set("v.hideStatusMessage",false);
                                       }
                                   }
                               });
            $A.enqueueAction(action);    
        }
        
    },
    
    getButtonState: function (value, maxRowSelection) {
        return !(value !== "" && this.isPositiveInteger(value) && Number(value) !== maxRowSelection);
    },
    
    isPositiveInteger: function (value) {
        return /^\d+$/.test(value);
    }
})