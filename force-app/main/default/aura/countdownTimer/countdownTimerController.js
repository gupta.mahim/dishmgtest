({
    init : function(component, event, helper) {
        var tempEndTime = parseInt(component.get("v.disconnectEndTime"), 10);
        var disconnectEndTime = new Date(tempEndTime);
        //console.log(disconnectEndTime); 
        
        // Update the count down every 1 second
        var timer = setInterval(function() {
            var now = new Date().getTime();
            
            // Find the difference between now and the disconnect End Time
            var diff = disconnectEndTime - now;
            
            // Time calculations for hours, minutes and seconds
            var hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((diff % (1000 * 60)) / 1000);
            
            var timeLeft =  minutes + "min " + seconds + "sec ";
            component.set("v.timeLeft", timeLeft);
            
            if (timeLeft == '0min 0sec ' || parseInt(seconds, 10) < 0)  { 
                clearInterval(timer);
                let buttonNext = component.find('btnNext');
                buttonNext.set('v.disabled',false);
            }
        }, 1000);
    },
    
    btnNextClick : function(component, event, helper) {       
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    },
    
    btnPreviousClick : function(component, event, helper) {
        var navigate = component.get("v.navigateFlow");
        navigate("BACK");        
    },
})