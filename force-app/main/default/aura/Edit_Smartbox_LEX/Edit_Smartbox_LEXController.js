({
    //Milestone# MS-001252 - 3Jun20 - Start
    doInit: function(component, event, helper) {
        component.set("v.isLoading", true);
        var action = component.get("c.getUserAccess");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.userHaveAccess", response.getReturnValue().currentUserHaveAccess);
                component.set("v.noAccessMsg", response.getReturnValue().noAccessMsg);               
                component.set("v.isLoading", false);
            }
            else{
                console.log('Error Details - '+ JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    //Milestone# MS-001252 - End
    
    smartboxread : function(component, event, helper) {
        var action = component.get("c.getSmartboxItems");
        component.set("v.isLoading", true);
        action.setParams({
            oppId: component.get("v.recordId")
        });
        console.log("KA:: " +  action);
        action.setCallback(this,function(a){
            component.set("v.ediSmartBox", a.getReturnValue());
            component.set("v.isLoading", false); 
        });
        $A.enqueueAction(action);
    },removeComponent:function(component, event, helper){
        //get event and set the parameter of Aura:component type, as defined in the event above.
        var compEvent = component.getEvent("RemoveComponent");
        compEvent.setParams({
            "comp" : component
        });
    compEvent.fire();
    }, 
     onCancel : function(component, event, helper) {
        
       var ID= component.get("v.recordId");
       window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");
           
} ,
     SaveProduct  : function(component, event, helper){
        
        component.set("v.isLoading", true);
        var equipData=component.get("v.ediSmartBox")
        var jsonData=JSON.stringify(equipData);
        console.info(jsonData);
        var action = component.get("c.saveRecords");
        action.setParams({
            strEquipData: jsonData
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                
                component.set("v.isStatusMessage", "SaveSuccess");
                component.set("{!v.recordStatusMessage}", "Your record has been updated successfully");
                var ID= component.get("v.recordId");
                var isCommunity=component.get("v.isCommunity");
                if(!isCommunity)
                {
                    window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");   
                }
                
                
                else
                {
                    window.open('/lex/s/opportunity/'+ID+'/view',"_self");
                }
                
                var name = a.getReturnValue();
            }
            else if (state === "ERROR") {
     			component.set("v.isStatusMessage", "errorStatus");
                component.set("{!v.recordStatusMessage}", "There is an error while saving record");
            }
             component.set("v.isLoading", false);
        });

        $A.enqueueAction(action);  
    },
    hideStatusMesssage : function(component, event, helper) {
        
        component.set("v.isStatusMessage", "hideMessage");
    }             
})