({
    doInit : function(component, event, helper) {
        var quoteRec = component.get("v.quoteRec");
        var quoteLineRec = component.get("v.quoteLineRec");
        var quoteRecCopy = Object.assign({}, quoteRec);
        var quoteLineCopy = Object.assign({}, quoteLineRec);
        
        component.set("v.tempoQuoteRec", JSON.parse(JSON.stringify(component.get("v.quoteRec"))));
        component.set("v.ARPU", quoteLineRec.ARPU__c);
        component.set("v.IRR", quoteLineRec.IRR2__c);
        
        
        var getProfile = component.get("c.getProfileCustomSet");
        getProfile.setCallback(this, function(response){
            var state = response.getState();
            console.log('state: ' + state);
            if (state === "SUCCESS") {
                //console.log('Quote: ' + JSON.stringify(response.getReturnValue().Id));
                component.set("v.isVisible", response.getReturnValue());
                console.log('RETVAL',response.getReturnValue());
                //var quoteLine = response.getReturnValue().SBQQ__LineItems__r[0];
               // component.set("v.quoteLineRec", quoteLine);
                // IF THERE'S NO QUOTELINE ITEM, RETURN ERROR TO QUICK SAVE QUOTELINE FIRST BEFORE USING LEVERS
                //console.log('QuoteLine: ' + quoteLine);
                //console.log('Id: ' + JSON.stringify(quoteLine.Id));
                //console.log('ARPU: ' + JSON.stringify(quoteLine.ARPU__c));
                //component.set('v.hasLoaded', true);
            }
        })
        $A.enqueueAction(getProfile);
        
    },
    
    handleCalculate : function(component, event, helper){
        component.set("v.isCalculating", true);
        var tempoQuoteRec = component.get("v.tempoQuoteRec");
        var tempoQuoteLine = component.get("v.tempoQuoteRec").SBQQ__LineItems__r[0];
        console.log(tempoQuoteLine);
        var action = component.get("c.handleBundleQuoteLine");
        action.setParams({
            "quoteRecord": tempoQuoteRec,
            "quoteLineRec" : tempoQuoteLine
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state: ' + state);
            if (state === "SUCCESS") {
                //console.log(response.getReturnValue().SBQQ__LineItems__r[0]);
                
                var updatedQuoteLineRec = response.getReturnValue().SBQQ__LineItems__r[0];
                component.set("v.tempoQuoteRec", response.getReturnValue());
                console.log('RETURN',response.getReturnValue());
                console.log('Check:' + component.get("v.tempoQuoteRec").SBQQ__LineItems__r[0].PaybackPercent__c);
                console.log('Check2:' + component.get("v.tempoQuoteRec").SBQQ__LineItems__r[0].Payback_Period__c);
                
                console.log('Checkx:' + component.get("v.tempoQuoteRec").SBQQ__LineItems__r[0].ApprovalText__c);
                console.log('Checky:' + component.get("v.tempoQuoteRec").SBQQ__LineItems__r[0].ApproverLevel__c);
                //component.set("v.tempoQuoteRec.SBQQ__LineItems__r[0].IRR2__c", updatedQuoteLineRec.IRR2__c);
                //component.set("v.tempoQuoteRec.SBQQ__LineItems__r[0].Payback_Period__c", updatedQuoteLineRec.Payback_Period__c);
                //component.set("v.tempoQuoteRec.SBQQ__LineItems__r[0].NPV2__c ", updatedQuoteLineRec.NPV2__c );
            }
            component.set("v.isCalculating", false);
            component.set("v.pressCalculate", true);
        })
        $A.enqueueAction(action);
    },
    
    handleQuoteSelection : function(component, event, helper){
        component.set("v.isCalculating", true);
        helper.quoteSelectionHelper(component, event, helper);
    },
    
    handleQuoteClose : function(component, event, helper){
        
        component.destroy();
    }
})