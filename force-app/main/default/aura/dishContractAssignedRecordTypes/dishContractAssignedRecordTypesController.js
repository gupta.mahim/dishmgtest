({
	loadRecordTypes : function(component, event, helper) {
        try{var sObject=component.get("v.sObject");
        var value=component.get("v.value");
        var action = null;
       
            
        if(sObject=="DishContract")
        {
            action=component.get("c.getDishContractRecordTypes");
        }
       else if(sObject=="Case")
        {
            action=component.get("c.getCaseRecordTypes");
        }
        else if(sObject=="Contact")
        {
            action=component.get("c.getContactRecordTypes");
        }
        else if(sObject=="Opportunity")
        {
        	action=component.get("c.getOpportunityRecordTypes");
        }
        action.setCallback(this,function(a){
            component.set("v.recTypes", a.getReturnValue());
        });
            $A.enqueueAction(action);}catch(e){}
        }
})