({
    equipmentReadInit: function(component, event, helper)
    {
        component.set("v.isLoading",true);
  window.setTimeout(
    $A.getCallback(function() {
         component.set("v.isLoading",false);
        helper.equipmentRead(component, event, helper)
    }), 8000
);
    },
    
       
    SaveRecords  : function(component, event, helper){
        
        component.set("v.isLoading", true);
        var equipData=component.get("v.equipmentEdit")
        var jsonData=JSON.stringify(equipData);
        console.info(jsonData);
        var action = component.get("c.saveRecords");
        action.setParams({
            strEquipData: jsonData
        });
        action.setCallback(this, function(a) {
            
            var state = a.getState();
            if (state === "SUCCESS") {
                        
                        component.set("v.isStatusMessage", "SaveSuccess");
                    	component.set("{!v.recordStatusMessage}", "Your record has been updated successfully");
                            // show toast message 
                           var ID= component.get("v.recordId");
                           
                        
                var name = a.getReturnValue();
                component.set("v.equipmentEdit", a.getReturnValue());  
                  var isCommunity=component.get("v.isCommunity");
                    if(!isCommunity)
                    {
                       // window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");
                   
                    }
                else
                {
                  
                     //window.open('/lex/s/opportunity/'+ID+'/view',"_self");
                }
            }
            else if (state === "ERROR") {
     			component.set("v.isStatusMessage", "errorStatus");
                component.set("{!v.recordStatusMessage}", "There is an error while saving record");
            }
            component.set("v.isLoading", false);
        });

        $A.enqueueAction(action);  
    },
    closeComponent  : function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    },
    hideStatusMesssage : function(component, event, helper) {
        
        component.set("v.isStatusMessage", "hideMessage");
    }
})