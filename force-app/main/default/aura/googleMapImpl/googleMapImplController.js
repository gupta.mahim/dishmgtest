({
    doInit: function (component, event, helper) {
        // call getLocation apex class method 
        var action = component.get("c.getLocation");
        action.setParams({
            currentID : component.get("v.recordId"),
        });
        console.log('currentID-'+component.get("v.recordId"));
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.mapMarkersData',response.getReturnValue());
                // set the mapCenter location.
                /*component.set('v.mapCenter', {
                    location: {
                        Country: 'United States'
                    }
                });*/
                // set map markers title  
                //component.set('v.markersTitle', 'Google Office locations.');
            }
            else{
                console.log("Error message: " + response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})