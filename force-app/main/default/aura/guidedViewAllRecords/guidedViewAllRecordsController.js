({
    loadEquip : function(component, event, helper) 
    {
        var varEquipsId = component.get("v.Equips"); 
        console.log('varEquipsId-'+varEquipsId);
        component.set("v.equipsId", varEquipsId);
        console.log('equipsId-'+component.get("v.equipsId"));
        var action = component.get("c.getActiveEquips");
        var hasOverrideCase=component.get("v.hasOverrideCase");
        action.setParams({
            equipIds: component.get("v.Equips"),
            oppId:component.get("v.SelectedOpportunity"),
            taId:component.get("v.SelectedTA")
        });
        action.setCallback(this,function(a){
            var taData=a.getReturnValue();
            component.set("v.Equips", taData);
            /*if(taData[0].overrideAddress==true 
               || taData[0].hasOtherActiveElsewhere==true 
               || taData[0].hasOwnActiveElsewhere==true
               || hasOverrideCase==true)
            {
                component.set("v.hasErrors",true);
            }*/
        });
        $A.enqueueAction(action);
    },
    
    print : function(component, event, helper){
        console.log('2');
        window.print();
        /*var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:guideMe_PrintSummary",
        });
        evt.fire();
        console.log('1');
        console.log('1');
        window.print();
        var printContents = document.getElementById("printableArea").innerHTML;
        console.log('printContents'+ printContents);
        document.body.innerHTML = printContents;
        console.log('2');
     window.print();
        
        var printContents = document.getElementById("printableArea").innerHTML;
        console.log('printContents'+ printContents);
        
        var originalContents = document.body.innerHTML;
        console.log('originalContents'+ originalContents);
        
        
                var newWin= window.open("");
               newWin.document.write(printContents);
               newWin.print();
               newWin.close();
        
        document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
        
        document.body.innerHTML = printContents;
        
        window.print();
        
        document.body.innerHTML = originalContents;
        
        var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        
        mywindow.document.write('<html>');
        mywindow.document.write('<body >');
        //mywindow.document.write('<h1>' + document.title  + '</h1>');
        mywindow.document.write(document.getElementById("printableArea").innerHTML);
        mywindow.document.write('</body></html>');
        console.log('printContents'+ document.getElementById("printableArea").innerHTML);
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        
        mywindow.print();
        mywindow.close();
        console.log('2');*/
    },
    
    navigateToComTwo : function(component, event, helper) {  
          
        event.preventDefault();  
        var navService = component.find( "navService" ); 
        console.log("1");
        var pageReference = {  
            type: "comm__namedPage",  
            attributes: {  
                "componentName": "c:guideMe_PrintSummary"  
            },  
            state: {  
                sampleVar: component.get( "v.Txt" )  
            }  
        };  
        sessionStorage.setItem('pageTransfer', JSON.stringify(pageReference.state));  
        navService.navigate(pageReference);  
        console.log("2");  
    },
    handleClick: function(component, event, helper) {
        console.log("2");
        var pageReference = {
            type: 'comm__namedPage',
            attributes: {
                'componentName': 'c:guideMe_PrintSummary'
            },
            state: {
                type: 'open'
            }
        };
        var navService = component.find("navService");
        navService.navigate(pageReference);
        console.log("2"); 
    },
    
    handleClick2: function(component, event, helper) {
        console.log("1");
        //window.open('www.google.com');
        window.open("/lex/s/GuideMeCreateTASummaryPage?equipIds="+component.get("v.equipsId")+"&oppId="+component.get('v.SelectedOpportunity')+"&TAId="+component.get('v.SelectedTA'), "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=80,left=200,width=800,height=500");
    }
})