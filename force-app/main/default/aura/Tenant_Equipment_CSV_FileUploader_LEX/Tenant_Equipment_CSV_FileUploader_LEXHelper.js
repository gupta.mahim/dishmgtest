({
    AddWaitforFlow : function(component, event, helper) {
        
        alert('h');
        component.set("v.isStatusMessage","SaveSuccess");
        component.set("v.isLoading",false);         
        
    },
    
    //Milestone# MS-000026 -- Mahim -- Start
    addTERecords: function(component, event) {
        var CheckInvalid = component.get("v.Invalidinp");
        //get the account List from component  
        var addedEquipList = component.get("v.addedEquipList");
        var oppId = component.get("v.recordId");
        //Add New Account Record
        addedEquipList.push({
            'Name': '',
            'Smart_Card__c': '',
            'Opportunity__c':oppId,
            'Address__c':'',
            'City__c':'',
            'State__c':'',
            'Zip_Code__c':'',
            'Unit__c':'',
            'Customer_First_Name__c':'',
            'Customer_Last_Name__c':'',
            'Phone_Number__c':'',
            'Email_Address__c':'',
        }
                           );
        component.set("v.addedEquipList", addedEquipList);
    }
    //Milestone# MS-000026 -- Mahim -- End
})