import { LightningElement , wire, track, api} from 'lwc';

import getAccountLocations from '@salesforce/apex/DisplayMapController.getAccountLocations';

export default class DisplayLWCMap extends LightningElement {

    @api cityNameParam; 
    @track error;   //this holds errors

    @track mapMarkers = [];
    @track markersTitle = 'Accounts';
    @track zoomLevel = 10;
    handleCityChange(event){
        this.cityNameParam = event.target.value;
     }
    /* Load address information based on cityNameParam from Controller */
    @wire(getAccountLocations, { cityNameInitial: '$cityNameParam'})
    wiredOfficeLocations({ error, data }) {
        if (data) {            
            data.forEach(dataItem => {
                this.mapMarkers = [...this.mapMarkers ,
                    {
                        location: {
                            Street: dataItem.BillingStreet,
                            City: dataItem.BillingCity,
                            Country: dataItem.BillingCountry,
                        },
        
                        icon: 'custom:custom26',
                        title: dataItem.Name,
                    }                                    
                ];
              });            
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.contacts = undefined;
        }
    }

}